<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.travelpayouts.com/?locale=en
 * @since      1.0.0
 *
 * @package    Travelpayouts
 * @subpackage Travelpayouts/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Travelpayouts
 * @subpackage Travelpayouts/public
 * @author     travelpayouts <solomashenko.roman.1991@gmail.com>
 */

namespace tp\frontend;

use tp\frontend\controllers\shortcodes;
use tp\frontend\controllers\shortcodes\flights;
use tp\frontend\controllers\shortcodes\hotels;
use tp\frontend\controllers\shortcodes\railways;
use tp\frontend\controllers\shortcodes\widgets;
use tp\frontend\includes\TP_Enqueue_Resources;

class TP_Frontend
{

    public function __construct()
    {
        $this->load_dependencies();
    }

    private function load_dependencies()
    {
        $this->load_enqueue_resources();
        $this->load_shortcodes();

    }

    private function load_enqueue_resources()
    {
        $enqueue_resources = new TP_Enqueue_Resources();
        $enqueue_resources->init();
    }

    private function load_shortcodes()
    {
        // flights
        $this->load_flights_shortcodes();
        // hotels
        $this->load_hotels_shortcodes();
        // railways
        $this->load_railways_shortcodes();
        // widgets
        $this->load_widgets_shortcodes();
        // Search form
        new shortcodes\TP_Search_Controller();
        // Link
        new shortcodes\TP_Link_Controller();
    }

    private function load_flights_shortcodes()
    {
        new flights\TP_Price_Calendar_Month_Controller();
        new flights\TP_Price_Calendar_Week_Controller();
        new flights\TP_Cheapest_Flights_Controller();
        new flights\TP_Cheapest_Ticket_Each_Day_Month_Controller();
        new flights\TP_Cheapest_Tickets_Each_Month_Controller();
        new flights\TP_Direct_Flights_Route_Controller();
        new flights\TP_Direct_Flights_Controller();
        new flights\TP_Popular_Routes_From_City_Controller();
        new flights\TP_Popular_Destinations_Airlines_Controller();
        new flights\TP_Our_Site_Search_Controller();
        new flights\TP_From_Our_City_Fly_Controller();
        new flights\TP_In_Our_City_Fly_Controller();
    }

    private function load_hotels_shortcodes()
    {
        new hotels\TP_Hotels_Selections_Discount_Controller();
        new hotels\TP_Hotels_Selections_Date_Controller();
    }

    private function load_railways_shortcodes()
    {
        new railways\TP_Tutu_Controller();
    }

    private function load_widgets_shortcodes()
    {
        new widgets\TP_Map_Controller();
        new widgets\TP_Hotelmap_Controller();
        new widgets\TP_Calendar_Controller();
        new widgets\TP_Subscriptions_Controller();
        new widgets\TP_Hotel_Controller();
        new widgets\TP_Popular_Routes_Controller();
        new widgets\TP_Hotel_Selections_Controller();
        new widgets\TP_Ducklett_Controller();
    }
}
