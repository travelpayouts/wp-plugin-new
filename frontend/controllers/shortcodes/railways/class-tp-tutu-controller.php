<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/30/2018
 * Time: 7:16 PM
 */

namespace tp\frontend\controllers\shortcodes\railways;


use tp\frontend\controllers\shortcodes\base\TP_Base_Railway_Shortcode_Controller;
use tp\admin\includes\models\tables\content\railway as RailwaysModels;
use tp\includes\table\TP_Table_Railway;

/**
 * Class TP_Tutu_Controller
 * @package tp\frontend\controllers\shortcodes\railways
 * Railway timetable
 */
class TP_Tutu_Controller extends TP_Base_Railway_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_tutu';
    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new RailwaysModels\TP_Model_Tutu();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                TP_Table_Railway::COLUMN_ROUTE,
                TP_Table_Railway::COLUMN_DURATION,
                TP_Table_Railway::COLUMN_ORIGIN,
                TP_Table_Railway::COLUMN_DESTINATION,
                TP_Table_Railway::COLUMN_DEPARTURE_TIME,
                TP_Table_Railway::COLUMN_ARRIVAL_TIME,
                TP_Table_Railway::COLUMN_ROUTE_FIRST_STATION,
                TP_Table_Railway::COLUMN_ROUTE_LAST_STATION,
            )
        ), $atts, $tag);

        $request = $this->api()->get_tutu($tag, $this->get_shortcode_attributes());

        $this->render('/shortcodes/railways/tp-tutu.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'columns' => $this->get_table_columns()
        ));
    }

    public function get_table_columns()
    {
        $table_columns = array(
            TP_Table_Railway::COLUMN_TRAIN => TP_Table_Railway::get_column_option_label_train(),
            TP_Table_Railway::COLUMN_ROUTE => TP_Table_Railway::get_column_option_label_route(),
            TP_Table_Railway::COLUMN_DEPARTURE => TP_Table_Railway::get_column_option_label_departure(),
            TP_Table_Railway::COLUMN_ARRIVAL => TP_Table_Railway::get_column_option_label_arrival(),
            TP_Table_Railway::COLUMN_DURATION => TP_Table_Railway::get_column_option_label_duration(),
            TP_Table_Railway::COLUMN_PRICES => TP_Table_Railway::get_column_option_label_prices(),
            TP_Table_Railway::COLUMN_DATES => TP_Table_Railway::get_column_option_label_dates(),
            TP_Table_Railway::COLUMN_ORIGIN => TP_Table_Railway::get_column_option_label_origin(),
            TP_Table_Railway::COLUMN_DESTINATION => TP_Table_Railway::get_column_option_label_destination(),
            TP_Table_Railway::COLUMN_DEPARTURE_TIME => TP_Table_Railway::get_column_option_label_departure_time(),
            TP_Table_Railway::COLUMN_ARRIVAL_TIME => TP_Table_Railway::get_column_option_label_arrival_time(),
            TP_Table_Railway::COLUMN_ROUTE_FIRST_STATION => TP_Table_Railway::get_column_option_label_route_first_station(),
            TP_Table_Railway::COLUMN_ROUTE_LAST_STATION => TP_Table_Railway::get_column_option_label_route_last_station(),
        );
        return $table_columns;
    }
}