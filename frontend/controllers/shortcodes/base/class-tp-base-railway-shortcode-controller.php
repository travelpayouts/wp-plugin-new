<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/30/2018
 * Time: 7:12 PM
 */

namespace tp\frontend\controllers\shortcodes\base;

use \tp\includes\request_api;
use tp\includes;

abstract class TP_Base_Railway_Shortcode_Controller extends TP_Base_Shortcode_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->request_api = request_api\TP_Request_Api_Railway::get_instance();
        $this->request_api->init(
            '74230',
            'f90c512970b28d468a4c4220a9a995d2',
            '',
            '',
            12,
            true,
            'https://hydra.aviasales.ru'
        );

        $this->set_default_shortcode_attributes(array(
            'origin' => '', // Origin
            'destination' => '', // Destination
            'subid' => '', // Subid
            'currency' => includes\TP_Currency_Utils::get_default_currency(), // Currency
            'off_title' => 'false', // No title
            'return_type' => TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW, // Return type view | url | array_cache | array_api,
            'update_cache' => 'false',
            'theme' => 'default', // default | aquamarine | gules | blue | green | purpur | dark_yellow | purpur_dark
            // | purpur_red | yellow_dark
            'format_date' => 'd/m/Y',
            'shortcode_type' => TP_PLUGIN_SHORTCODE_TYPE_RAILWAY
        ));
    }

    /**
     *
     * @return request_api\TP_Request_Api_Railway
     */
    public function api()
    {
        return $this->request_api;
    }
}