<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 10:58 AM
 */

namespace tp\frontend\controllers\shortcodes\base;


abstract class TP_Base_Widget_Shortcode_Controller extends TP_Base_Shortcode_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->set_default_shortcode_attributes(array(
            'subid' => '', // Subid
            'return_type' => TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW, // Return type view | url | array_cache | array_api,
            'shortcode_type' => TP_PLUGIN_SHORTCODE_TYPE_WIDGET,
            'marker' => '17942', // Marker
            'extra_marker' => '', // Extra marker
        ));
    }

    /**
     * @param array  $atts
     * @param string $shortcode
     *
     * @return array
     */
    public function filter_shortcode_atts(array $atts, $shortcode)
    {
        if (array_key_exists('responsive', $atts)){
            $atts['responsive'] = $this->boolval($atts['responsive']);
        }

        if (array_key_exists('direct', $atts)){
            $atts['direct'] = $this->convert_boolean_to_string($atts['direct']);
        }

        if (array_key_exists('one_way', $atts)){
            $atts['one_way'] = $this->convert_boolean_to_string($atts['one_way']);
        }

        if (array_key_exists('powered_by', $atts)){
            $atts['powered_by'] = $this->convert_boolean_to_string($atts['powered_by']);
        }

        if (array_key_exists('hide_logo', $atts)){
            $atts['hide_logo'] = $this->convert_boolean_to_string($atts['hide_logo']);
        }


        $atts = parent::filter_shortcode_atts($atts, $shortcode);

        return $atts;
    }

    /*protected function make_marker(){}

    protected function make_marker_url($extra_table_marker, $subid)
    {
        $marker_url = '';
        $marker_url = $this->get_marker().'.wpplugin';
        if (!empty($extra_table_marker)){
            $marker_url .= '_'.$extra_table_marker;
        }
        if (!empty($subid)){
            $marker_url .= '_'.$subid;
        }
        $marker_url .= '.$69';
        return $marker_url;
    }*/
}