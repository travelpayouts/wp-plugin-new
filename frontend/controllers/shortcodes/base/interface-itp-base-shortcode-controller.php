<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 2:42 PM
 */

namespace tp\frontend\controllers\shortcodes\base;


interface ITP_Base_Shortcode_Controller
{

    /**
     * @param array $atts  - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content  - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag  - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '');
}