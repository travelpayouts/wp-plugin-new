<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 8:09 PM
 */

namespace tp\frontend\controllers\shortcodes\base;

use \tp\includes\request_api;
use tp\includes;
use tp\includes\dictionary;

/**
 * Class TP_Base_Flight_Shortcode_Controller
 * @package tp\frontend\controllers\shortcodes\base
 */
abstract class TP_Base_Flight_Shortcode_Controller extends TP_Base_Shortcode_Controller
{
    public function __construct()
    {

        parent::__construct();
        $this->request_api = request_api\TP_Request_Api_Flight::get_instance();
        $this->request_api->init('74230', 'f90c512970b28d468a4c4220a9a995d2', '', '', 12, true,
            'https://hydra.aviasales.ru', true);

        $this->set_default_shortcode_attributes(array(
            'origin' => '', // Origin
            'destination' => '', // Destination
            'airline_code' => '',
            'airline' => '',
            'subid' => '', // Subid
            'currency' => includes\TP_Currency_Utils::get_default_currency(), // Currency
            'off_title' => 'false', // No title
            'return_type' => TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW, // Return type view | url | array_cache | array_api,
            'is_column_link' => 'false',
            'is_redirect' => 'false',
            'is_rel_nofollow' => 'false',
            'is_target_blank' => 'false',
            'update_cache' => 'false',
            'theme' => 'default', // default | aquamarine | gules | blue | green | purpur | dark_yellow | purpur_dark
            // | purpur_red | yellow_dark
            'format_date' => 'd/m/Y',
            'shortcode_type' => TP_PLUGIN_SHORTCODE_TYPE_FLIGHT,
            'case_origin' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
            'case_destination' => dictionary\TP_Dictionary_Cities::CASE_ACCUSATIVE,
        ));
    }

    /**
     *
     * @return request_api\TP_Request_Api_Flight
     */
    public function api()
    {
        return $this->request_api;
    }

    /**
     * @param array $atts
     * @param string $shortcode
     *
     * @return array
     */
    public function filter_shortcode_atts(array $atts, $shortcode)
    {
        $bool_attributes = array(
            'paginate',
            'off_title',
            'is_column_link',
            'is_redirect',
            'is_rel_nofollow',
            'is_target_blank',
            'update_cache',
            'one_way',
            'link_without_dates',
        );

        foreach ($bool_attributes as $bool_attribute) {
            if (array_key_exists($bool_attribute, $atts))
                $atts[$bool_attribute] = $this->boolval($atts[$bool_attribute]);
        }

        if (array_key_exists('hidden_columns', $atts)) {
            $atts['hidden_columns'] = $this->stringval_to_array($atts['hidden_columns']);
        }

        $atts = parent::filter_shortcode_atts($atts, $shortcode);

        return $atts;
    }

}