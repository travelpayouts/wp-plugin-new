<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 1/24/18
 * Time: 23:57
 */

namespace tp\frontend\controllers\shortcodes\base;

use \tp\includes\request_api;
use tp\includes;

/**
 * Class TP_Base_Hotel_Shortcode_Controller
 * @package tp\frontend\controllers\shortcodes\base
 */
abstract class TP_Base_Hotel_Shortcode_Controller extends TP_Base_Shortcode_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->request_api = request_api\TP_Request_Api_Hotel::get_instance();
        $this->request_api->init(
            '74230',
            'f90c512970b28d468a4c4220a9a995d2',
            '',
            '',
            12,
            true,
            'https://search.hotellook.com',
            'ru-RU'
        );

        //'language' => 'ru-RU'
        $this->set_default_shortcode_attributes(array(
            'city' => '', // City
            'subid' => '', // Subid
            'currency' => includes\TP_Currency_Utils::get_default_currency(), // Currency
            'off_title' => 'false', // No title
            'return_type' => TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW, // Return type view | url | array_cache | array_api,
            'type_selections' => 'popularity', // Selection type
            'number_results' => 20, //Number of results
            'link_without_dates' => 'false', //Land without dates
            'language' => 'ru', //Lang
            'update_cache' => 'false',
            'theme' => 'default', // default | aquamarine | gules | blue | green | purpur | dark_yellow | purpur_dark
            // | purpur_red | yellow_dark
            'format_date' => 'd/m/Y',
            'shortcode_type' => TP_PLUGIN_SHORTCODE_TYPE_HOTEl
        ));
    }

    /**
     *
     * @return request_api\TP_Request_Api_Hotel
     */
    public function api()
    {
        return $this->request_api;
    }
}