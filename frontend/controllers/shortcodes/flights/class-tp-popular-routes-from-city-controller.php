<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 7:55 PM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;
use tp\admin\includes\models\tables\content\flight as FlightsModels;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Popular_Routes_From_City_Controller
 * @package tp\frontend\controllers\shortcodes\flights
 * Popular Destinations from origin
 */
class TP_Popular_Routes_From_City_Controller extends TP_Base_Flight_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_popular_routes_from_city';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_popular_routes_from_city_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_Popular_Routes_From_City();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                TP_Table_Flight::COLUMN_AIRLINE_LOGO,
                TP_Table_Flight::COLUMN_RETURN_DATE,
                TP_Table_Flight::COLUMN_FLIGHT_NUMBER,
                TP_Table_Flight::COLUMN_FLIGHT,
                TP_Table_Flight::COLUMN_AIRLINE,
                TP_Table_Flight::COLUMN_ORIGIN_DESTINATION
            )
        ), $atts, $tag);

        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());

        if ($atts['update_cache'] == true){
            return false;
        }

        $this->render('/shortcodes/flights/tp-popular-routes-from-city.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $tag),
            'columns' => $this->get_table_columns()
        ));
    }

    public function get_table_columns(){
        $table_columns = array(
            TP_Table_Flight::COLUMN_FLIGHT_NUMBER => TP_Table_Flight::get_column_option_label_flight_number(),
            TP_Table_Flight::COLUMN_FLIGHT => TP_Table_Flight::get_column_option_label_flight(),
            TP_Table_Flight::COLUMN_DEPART_DATE => TP_Table_Flight::get_column_option_label_depart_date(),
            TP_Table_Flight::COLUMN_RETURN_DATE => TP_Table_Flight::get_column_option_label_return_date(),
            TP_Table_Flight::COLUMN_PRICE => TP_Table_Flight::get_column_option_label_price(),
            TP_Table_Flight::COLUMN_AIRLINE => TP_Table_Flight::get_column_option_label_airline(),
            TP_Table_Flight::COLUMN_AIRLINE_LOGO => TP_Table_Flight::get_column_option_label_airline_logo(),
            TP_Table_Flight::COLUMN_DESTINATION => TP_Table_Flight::get_column_option_label_destination(),
            TP_Table_Flight::COLUMN_BUTTON => TP_Table_Flight::get_column_option_label_button(),
            TP_Table_Flight::COLUMN_ORIGIN_DESTINATION => TP_Table_Flight::get_column_option_label_origin_destination(),
        );
        return $table_columns;
    }

}