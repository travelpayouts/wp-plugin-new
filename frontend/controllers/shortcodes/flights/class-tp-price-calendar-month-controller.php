<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 4:31 PM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;
use tp\admin\includes\models\tables\content\flight as FlightsModels;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Price_Calendar_Month_Controller
 * @package tp\frontend\controllers\shortcodes
 * Flights from origin to destination, One Way (next month)
 */
class TP_Price_Calendar_Month_Controller extends TP_Base_Flight_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_price_calendar_month';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_price_calendar_month_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_Price_Calendar_Month();
        $this->shortcode_attributes($model->attributes, $atts, $tag);
        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());
        if ($atts['update_cache'] == true) return false;
        $this->render('/shortcodes/flights/tp-price-calendar-month.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $tag),
            'shortcode_output' => $request,
            'columns' => $this->get_table_columns()
        ));
    }

    public function get_table_columns()
    {
        $table_columns = array(
            'depart_date' => __('Departure date', TP_PLUGIN_TEXTDOMAIN),
            'return_date' => __('Return date', TP_PLUGIN_TEXTDOMAIN),
            'price' => __('Price', TP_PLUGIN_TEXTDOMAIN),
            'number_of_changes' => __('Stops', TP_PLUGIN_TEXTDOMAIN),
            'trip_class' => __('Flight class', TP_PLUGIN_TEXTDOMAIN),
            'button' => __('Find Ticket', TP_PLUGIN_TEXTDOMAIN),
        );
        return $table_columns;
    }
}