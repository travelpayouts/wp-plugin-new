<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 7:16 PM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;

use tp\admin\includes\models\tables\content\flight as FlightsModels;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Cheapest_Ticket_Each_Day_Month
 * @package tp\frontend\controllers\shortcodes\flights
 * Cheapest Flights from origin to destination (next month)
 */
class TP_Cheapest_Ticket_Each_Day_Month_Controller extends TP_Base_Flight_Shortcode_Controller
{

    const SHORTCODE_TAG_TABLE = 'tp_cheapest_ticket_each_day_month';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_cheapest_ticket_each_day_month_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_Cheapest_Ticket_Each_Day_Month();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'filter_airline' => '', // Filter by airline
            'filter_flight_number' => '', //Filter by flight # (enter manually)
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                TP_Table_Flight::COLUMN_NUMBER_OF_CHANGES,
                TP_Table_Flight::COLUMN_AIRLINE_LOGO,
                TP_Table_Flight::COLUMN_FLIGHT_NUMBER,
                TP_Table_Flight::COLUMN_FLIGHT,
                TP_Table_Flight::COLUMN_AIRLINE
            )
        ), $atts, $tag);

        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());

        $this->render('/shortcodes/flights/tp-cheapest-ticket-each-day-month.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $tag),
            'columns' => $this->get_table_columns()
        ));

    }

    public function get_table_columns(){
        $table_columns = array(
            'flight_number' => __('Flight number', TP_PLUGIN_TEXTDOMAIN),
            'flight' => __('Flight', TP_PLUGIN_TEXTDOMAIN),
            'depart_date' => __('Departure date', TP_PLUGIN_TEXTDOMAIN),
            'return_date' => __('Return date', TP_PLUGIN_TEXTDOMAIN),
            'price' => __('Price', TP_PLUGIN_TEXTDOMAIN),
            'number_of_changes' => __('Stops', TP_PLUGIN_TEXTDOMAIN),
            'airline' => __('Airlines', TP_PLUGIN_TEXTDOMAIN),
            'airline_logo' => __('Airlines', TP_PLUGIN_TEXTDOMAIN),
            'button' => __('Find Ticket', TP_PLUGIN_TEXTDOMAIN),
        );
        return $table_columns;
    }
}