<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/24/2018
 * Time: 11:34 AM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;
use tp\includes\table\TP_Table_Flight;
use tp\admin\includes\models\tables\content\flight as FlightsModels;

/**
 * Class TP_Our_Site_Search_Controller
 * @package tp\frontend\controllers\shortcodes\flights
 * Searched on our website
 */
class TP_Our_Site_Search_Controller extends TP_Base_Flight_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_our_site_search';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_our_site_search_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_In_Our_City_Fly();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'limit' => $model->limit,
            'period_type' => 'year',
            'trip_class' => 0,
            'stops' => 0, // Number of stops
            'one_way' => 'false',
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                'small' => [
                    TP_Table_Flight::COLUMN_DISTANCE,
                    TP_Table_Flight::COLUMN_NUMBER_OF_CHANGES,
                    TP_Table_Flight::COLUMN_ORIGIN_DESTINATION,
                ],
                'medium' => [
                    TP_Table_Flight::COLUMN_TRIP_CLASS,
                    TP_Table_Flight::COLUMN_FOUND_AT,
                    TP_Table_Flight::COLUMN_PRICE_DISTANCE,
                ]
            ),
        ), $atts, $tag);

        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());

        $this->render('/shortcodes/flights/tp-our-site-search.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $tag),
            'columns' => $this->get_table_columns()
        ));

    }

    public function get_table_columns()
    {
        $table_columns = array(
            'origin' => __('Origin', TP_PLUGIN_TEXTDOMAIN),
            'destination' => __('Destination', TP_PLUGIN_TEXTDOMAIN),
            'depart_date' => __('Departure date', TP_PLUGIN_TEXTDOMAIN),
            'return_date' => __('Return date', TP_PLUGIN_TEXTDOMAIN),
            'found_at' => __('When found', TP_PLUGIN_TEXTDOMAIN),
            'price' => __('Price', TP_PLUGIN_TEXTDOMAIN),
            'number_of_changes' => __('Stops', TP_PLUGIN_TEXTDOMAIN),
            'trip_class' => __('Flight class', TP_PLUGIN_TEXTDOMAIN),
            'distance' => __('Distance', TP_PLUGIN_TEXTDOMAIN),
            'price_distance' => __('Price/distance', TP_PLUGIN_TEXTDOMAIN),
            'button' => __('Find Ticket', TP_PLUGIN_TEXTDOMAIN),
            'origin_destination' => __('Origin - Destination', TP_PLUGIN_TEXTDOMAIN)
        );
        return $table_columns;
    }
}