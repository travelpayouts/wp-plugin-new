<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 8:20 PM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;
use tp\admin\includes\models\tables\content\flight as FlightsModels;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Popular_Destinations_Airlines_Controller
 * @package tp\frontend\controllers\shortcodes\flights
 * Most popular flights within this Airlines
 */
class TP_Popular_Destinations_Airlines_Controller extends TP_Base_Flight_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_popular_destinations_airlines';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_popular_destinations_airlines_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_Popular_Destinations_Airlines();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'limit' => $model->limit, // Limit
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array()
        ), $atts, $tag);

        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());
        //if ($atts['update_cache'] == true){
        //    return false;
        //

        $this->render('/shortcodes/flights/tp-popular-destinations-airlines.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $tag),
            'columns' => $this->get_table_columns()
        ));
    }

    public function get_table_columns()
    {
        $table_columns = array(
            TP_Table_Flight::COLUMN_PLACE => TP_Table_Flight::get_column_option_label_place(),
            TP_Table_Flight::COLUMN_DIRECTION => TP_Table_Flight::get_column_option_label_direction(),
            TP_Table_Flight::COLUMN_BUTTON => TP_Table_Flight::get_column_option_label_button(),

        );
        return $table_columns;
    }
}