<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 7:50 PM
 */

namespace tp\frontend\controllers\shortcodes\flights;


use tp\frontend\controllers\shortcodes\base\TP_Base_Flight_Shortcode_Controller;
use tp\admin\includes\models\tables\content\flight as FlightsModels;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Direct_Flights_Controller
 * @package tp\frontend\controllers\shortcodes\flights
 * Direct Flights from origin
 */
class TP_Direct_Flights_Controller extends TP_Base_Flight_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_direct_flights';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_direct_flights_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new FlightsModels\TP_Model_Direct_Flights();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'filter_airline' => '', // Filter by airline
            'filter_flight_number' => '', //Filter by flight # (enter manually)
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'limit' => $model->limit, // Limit
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                TP_Table_Flight::COLUMN_AIRLINE_LOGO,
                TP_Table_Flight::COLUMN_FLIGHT_NUMBER,
                TP_Table_Flight::COLUMN_FLIGHT,
                TP_Table_Flight::COLUMN_AIRLINE,
                TP_Table_Flight::COLUMN_ORIGIN_DESTINATION
            )
        ), $atts, $tag);

        $request = $this->api()->get_response_by_shortcode($tag, $this->get_shortcode_attributes());

        $this->render('/shortcodes/flights/tp-direct-flight.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'shortcode_title' => $this->render_title($this->get_shortcode_attributes(), $this->get_shortcode_attributes()),
            'columns' => $this->get_table_columns()
        ));
    }

    public function get_table_columns(){
        $table_columns = array(
            'flight_number' => __('Flight number', TP_PLUGIN_TEXTDOMAIN),
            'flight' => __('Flight', TP_PLUGIN_TEXTDOMAIN),
            'depart_date' => __('Departure date', TP_PLUGIN_TEXTDOMAIN),
            'return_date' => __('Return date', TP_PLUGIN_TEXTDOMAIN),
            'price' => __('Price', TP_PLUGIN_TEXTDOMAIN),
            'airline' => __('Airlines', TP_PLUGIN_TEXTDOMAIN),
            'airline_logo' => __('Airlines', TP_PLUGIN_TEXTDOMAIN),
            'destination' => __('Destination', TP_PLUGIN_TEXTDOMAIN),
            'button' => __('Find Ticket', TP_PLUGIN_TEXTDOMAIN),
            'origin_destination' => __('Origin - Destination', TP_PLUGIN_TEXTDOMAIN),
        );
        return $table_columns;
    }
}