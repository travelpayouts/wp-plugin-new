<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 2:42 PM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use Adbar\Dot;
use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;
use tp\includes;

/**
 * Class TP_Hotel_Selections_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Hotels Selections Widget
 */
class TP_Hotel_Selections_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_hotel_selections';
    const SHORTCODE_TAG_OLD = 'tp_hotel_selections_widget';
    const URL_LANG_RU = "//www.travelpayouts.com/ducklett/scripts.js";
    const URL_LANG_EN = "//www.travelpayouts.com/ducklett/scripts_en.js";
    const URL_LANG_TH = "//www.travelpayouts.com/ducklett/scripts_th.js";

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'id' => '', // City
            'ids' => '', // Hotel
            'cat1' => '3stars', //
            'cat2' => 'popularity', //
            'cat3' => 'distance', //
            'type' => 'full', //
            'width' => 500, //
            'limit' => 10, //
            'responsive' => 'true', // Responsive
            'currency' => includes\TP_Currency_Utils::get_default_currency(), // Currency
            'powered_by' => 'false', //Add my referral link // string bool
            'white_label' => 'search.hotellook.com', // White Label
            'url' => $this->getURLByLang(includes\TP_Lang_Utils::getLang()), // url

        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-hotel-selections.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }

    /**
     * Get wudget url by lang
     * @param $lang
     *
     * @return mixed
     */
    private function getURLByLang($lang)
    {
        $url = new Dot(array(
            includes\TP_Lang_Utils::getLangRU() =>  self::URL_LANG_RU,
            includes\TP_Lang_Utils::getLangEN() =>  self::URL_LANG_EN,
            includes\TP_Lang_Utils::getLangTH() =>  self::URL_LANG_TH,
        ));

        return $url->get($lang, self::URL_LANG_RU);
    }


}