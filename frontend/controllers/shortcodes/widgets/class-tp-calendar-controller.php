<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 1:24 PM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;
use tp\admin\includes\models\widgets as WidgetsModels;
use tp\includes;

/**
 * Calendar Widget
 * Class TP_Calendar_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Calendar Widget
 */
class TP_Calendar_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_calendar';
    const SHORTCODE_TAG_OLD = 'tp_calendar_widget';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new WidgetsModels\airlines\TP_model_Airtickets_Low_Price_Calendar();

        $this->shortcode_attributes(array(
            'origin' => '', // Origin
            'destination' => '', // Destination
            'currency' => includes\TP_Currency_Utils::get_default_currency(), // Currency
            'period' => 'year', // Calendar period
            'period_day_from' => '7', // Range, days
            'period_day_to' => '14', // Range, days
            'direct' => 'false', // Direct Flights Only // string bool
            'one_way' => 'false', // One way // string bool
            'responsive' => true, // Responsive
            'width' => 800, // Width
            'powered_by' => 'false', //Add my referral link // string bool
            'lang' => tp_get_lang(), // Language
            'white_label' => 'hydra.aviasales.ru', // White Label
            'marker' => '17942', // Marker
            'extra_marker' => 'extraMarker', // Extra marker
        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-calendar.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag(),
            //'model' => $model,
        ));
    }

}