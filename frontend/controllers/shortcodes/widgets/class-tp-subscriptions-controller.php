<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 1:46 PM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;

/**
 * Class TP_Subscriptions_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Subscription Widget
 */
class TP_Subscriptions_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_subscriptions';
    const SHORTCODE_TAG_OLD = 'tp_subscriptions_widget';
    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'origin' => '', // Origin
            'destination' => '', // Destination
            'responsive' => true, // Responsive
            'width' => 800, // Width
            'powered_by' => 'false', //Add my referral link // string bool
            'white_label' => 'hydra.aviasales.ru', // White Label
            'color' => '#00b1dd', // Color
        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-subscriptions.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }

    /**
     * @param array  $atts
     * @param string $shortcode
     *
     * @return array
     */
    public function filter_shortcode_atts(array $atts, $shortcode)
    {
        if (array_key_exists('color', $atts)){
            $atts['color'] = rawurlencode($atts['color']);
        }

        $atts = parent::filter_shortcode_atts($atts, $shortcode);

        return $atts;
    }
}