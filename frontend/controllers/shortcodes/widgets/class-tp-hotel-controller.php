<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 1:58 PM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;

/**
 * Class TP_Hotel_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Hotel Widget
 */
class TP_Hotel_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_hotel';
    const SHORTCODE_TAG_OLD = 'tp_hotel_widget';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'hotel_id' => '', // Origin
            'responsive' => 'true', // Responsive
        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-hotel.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }
}