<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 12:05 PM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;

/**
 * Class TP_Hotelmap_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Hotels Map Widget
 */
class TP_Hotelmap_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_hotelmap';
    const SHORTCODE_TAG_OLD = 'tp_hotelmap_widget';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'coordinates' => '', // Coordinates
            'lat' => '', // Lat Coordinates
            'lng' => '', // Lng Coordinates
            'zoom' => 12, // Zoom
            'width' => 500, // Width
            'height' => 500, // Height
            'draggable' => false, // Draggable
            'disable_zoom' => false, // Disable zoom
            'scrollwheel' => false, // Scroll Wheel Zoom
            'map_styled' => false, // Map Style
            'color' => '#00b1dd', // Color
            'map_color' => '#00b1dd', // Map color
            'contrast_color' => '#ffffff', // Contrast color
            'white_label' => 'hotellook.ru', // White Label
            'base_diameter' => 16,
            'changeflag' => 0,
            'lang' => 'ru', // Lang
        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-hotelmap.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }

    /**
     * @param array  $atts
     * @param string $shortcode
     *
     * @return array
     */
    public function filter_shortcode_atts(array $atts, $shortcode)
    {

        if (array_key_exists('draggable', $atts)){
            $atts['draggable'] = $this->convert_boolean_to_string($atts['draggable']);
        }

        if (array_key_exists('disable_zoom', $atts)){
            $atts['disable_zoom'] = $this->convert_boolean_to_string($atts['disable_zoom']);
        }

        if (array_key_exists('scrollwheel', $atts)){
            $atts['scrollwheel'] = $this->convert_boolean_to_string($atts['scrollwheel']);
        }

        if (array_key_exists('map_styled', $atts)){
            $atts['map_styled'] = $this->convert_boolean_to_string($atts['map_styled']);
        }

        if (array_key_exists('color', $atts)){
            $atts['color'] = rawurlencode($atts['color']);
        }

        if (array_key_exists('map_color', $atts)){
            $atts['map_color'] = rawurlencode($atts['map_color']);
        }

        if (array_key_exists('contrast_color', $atts)){
            $atts['contrast_color'] = rawurlencode($atts['contrast_color']);
        }

        $atts = parent::filter_shortcode_atts($atts, $shortcode);

        return $atts;
    }
}