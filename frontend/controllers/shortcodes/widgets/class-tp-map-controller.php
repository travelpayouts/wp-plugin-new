<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 11:27 AM
 */

namespace tp\frontend\controllers\shortcodes\widgets;


use tp\frontend\controllers\shortcodes\base\TP_Base_Widget_Shortcode_Controller;

/**
 * Class TP_Map_Controller
 * @package tp\frontend\controllers\shortcodes\widgets
 * Map Widget
 */
class TP_Map_Controller extends TP_Base_Widget_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_map';
    const SHORTCODE_TAG_OLD = 'tp_map_widget';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'origin' => '', // Origin
            'width' => 500, // Width
            'height' => 500, // Height
            'direct' => 'false', // Direct Flights Only
            'hide_logo' => 'false', // Hide Logo
            'white_label' => 'hydra.aviasales.ru', // White Label
            'lang' => 'ru', // Lang
            'auto_fit_map' => 'true',
            'hide_sidebar' => 'true',
            'hide_reformal' => 'true',
            'disable_googlemaps_ui' => 'true',
            'zoom' => 3,
            'show_filters_icon' => 'true',
            'redirect_on_click' => 'true',
            'small_spinner' => 'true',
            'lines_type' => 'TpLines',
            'cluster_manager' => 'TpWidgetClusterManager',
            'show_tutorial' => 'false',
        ), $atts, $tag);

        $this->render('/shortcodes/widgets/tp-map.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }

}