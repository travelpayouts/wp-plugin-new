<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/31/2018
 * Time: 2:57 PM
 */

namespace tp\frontend\controllers\shortcodes;


use tp\frontend\controllers\shortcodes\base\TP_Base_Shortcode_Controller;

/**
 * Class TP_Search_Controller
 * @package tp\frontend\controllers\shortcodes
 */
class TP_Search_Controller extends TP_Base_Shortcode_Controller
{
    const SHORTCODE_TAG = 'tp_search';
    const SHORTCODE_TAG_OLD = 'tp_search_shortcodes';

    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     * @return mixed
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $this->shortcode_attributes(array(
            'origin' => '', // Origin
            'destination' => '', // Destination
        ), $atts, $tag);

        $this->render('/shortcodes/tp-search.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_tag' => self::get_shortcode_tag()
        ));
    }

}