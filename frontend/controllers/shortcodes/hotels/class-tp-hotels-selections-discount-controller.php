<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 1/25/18
 * Time: 00:10
 */

namespace tp\frontend\controllers\shortcodes\hotels;


use tp\frontend\controllers\shortcodes\base\TP_Base_Hotel_Shortcode_Controller;
use tp\admin\includes\models\tables\content\hotel as HotelsModels;
use tp\includes\table\TP_Table_Hotel;

/**
 * Class TP_Hotels_Selections_Discount_Controller
 * @package tp\frontend\controllers\shortcodes\hotels
 * Hotels collection - Discounts
 */
class TP_Hotels_Selections_Discount_Controller extends TP_Base_Hotel_Shortcode_Controller
{
    const SHORTCODE_TAG_TABLE = 'tp_hotels_selections_discount';
    const SHORTCODE_TAG_TABLE_OLD = 'tp_hotels_selections_discount_shortcodes';
    /**
     * Add shortcodes
     * add_shortcode( $tag , $func );
     * @return mixed
     */
    public function add_shortcodes()
    {
        // TODO: Implement add_shortcodes() method.
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE, array(&$this, 'index_action'));
        $this->add_shortcode(self::SHORTCODE_TAG_TABLE_OLD, array(&$this, 'index_action'));
    }

    /**
     * @param array $atts - an associative array of attributes, or an empty string if no attributes are given
     * @param string $content - the enclosed content (if the shortcode is used in its enclosing form)
     * @param string $tag - the shortcode tag, useful for shared callback functions
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function index_action($atts = array(), $content = '', $tag = '')
    {
        // TODO: Implement index_action() method.
        $model = new HotelsModels\TP_Model_Selections_Discount();
        $this->shortcode_attributes(array(
            'title' => $model->title, // Alternate title
            'title_tag' => $model->title_tag, // Title tag
            'paginate' => $model->paginate, // Paginate
            'paginate_rows_per_page' => $model->paginate_rows_per_page, // Paginate rows per page
            'extra_table_marker' => $model->extra_table_marker,
            'button_title' => $model->button_title,
            'hidden_columns' => array(
                TP_Table_Hotel::COLUMN_RATING,
                TP_Table_Hotel::COLUMN_DISTANCE,
                TP_Table_Hotel::COLUMN_PRICE_PN,
                TP_Table_Hotel::COLUMN_OLD_PRICE_PN,
                TP_Table_Hotel::COLUMN_OLD_PRICE_AND_DISCOUNT,
                TP_Table_Hotel::COLUMN_OLD_PRICE_AND_NEW_PRICE,
            ),
        ), $atts, $tag);

        $request = $this->api()->get_hotels_selections_discount($tag, $this->get_shortcode_attributes());

        $this->render('/shortcodes/hotels/tp-hotels-selections-discount.twig', array(
            'shortcode' => $tag,
            'shortcode_atts' => $this->get_shortcode_attributes(),
            'shortcode_output' => $request,
            'columns' => $this->get_table_columns()
        ));

    }

    public function get_table_columns(){
        $table_columns = array(
            TP_Table_Hotel::COLUMN_HOTEL => TP_Table_Hotel::get_column_option_label_hotel(),
            TP_Table_Hotel::COLUMN_STARS => TP_Table_Hotel::get_column_option_label_stars(),
            TP_Table_Hotel::COLUMN_DISCOUNT => TP_Table_Hotel::get_column_option_label_discount(),
            TP_Table_Hotel::COLUMN_OLD_PRICE_AND_NEW_PRICE => TP_Table_Hotel::get_column_option_label_old_price_and_new_price(),
            TP_Table_Hotel::COLUMN_PRICE_PN => TP_Table_Hotel::get_column_option_label_price_pn(),
            TP_Table_Hotel::COLUMN_OLD_PRICE_AND_DISCOUNT => TP_Table_Hotel::get_column_option_label_old_price_and_discount(),
            TP_Table_Hotel::COLUMN_DISTANCE => TP_Table_Hotel::get_column_option_label_distance(),
            TP_Table_Hotel::COLUMN_OLD_PRICE_PN => TP_Table_Hotel::get_column_option_label_old_price_pn(),
            TP_Table_Hotel::COLUMN_RATING => TP_Table_Hotel::get_column_option_label_rating(),
            TP_Table_Hotel::COLUMN_BUTTON => TP_Table_Hotel::get_column_option_label_button(),

        );
        return $table_columns;
    }
}