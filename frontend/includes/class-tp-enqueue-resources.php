<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 3/6/2018
 * Time: 8:49 AM
 */

namespace tp\frontend\includes;


use tp\includes\base\ITP_Enqueue_Resources;
use tp\includes\base\TP_Base_Setup_Hooks;
use tp\includes;

class TP_Enqueue_Resources extends TP_Base_Setup_Hooks implements ITP_Enqueue_Resources
{
    protected $webpack_assets;

    public function init()
    {
        $this->webpack_assets = new includes\Tp_Webpack(TP_PLUGIN_FRONTEND_SCRIPTS_PATH . 'stats.json');


        return parent::init();
    }

    protected function define_hooks()
    {
        // TODO: Implement define_hooks() method.
        add_action('wp_enqueue_scripts', array(&$this, 'enqueue_script'));
        add_action('wp_enqueue_scripts', array(&$this, 'enqueue_style'));
        add_action('wp_head', array(&$this, 'set_custom_webpack_path'), 5);
        add_action('wp_footer', array(&$this, 'footer_enqueue_script'));
    }

    public function enqueue_script($hook)
    {
        // TODO: Implement enqueue_script() method.
        if ($this->is_script() == false) {
            return;
        }

        wp_enqueue_script(
            TP_PLUGIN_TEXTDOMAIN . '/manifest.js',
            TP_PLUGIN_FRONTEND_SCRIPTS_URL . 'manifest.js',
            array(
                'jquery',
            ),
            TP_PLUGIN_RESOURCES_VER,
            $this->is_script_footer()
        );

        $main_assets = $this->get_webpack_assets()->get_assets_by_name('main', TP_PLUGIN_FRONTEND_DIST_URL, 'js');
        foreach ($main_assets as $main_asset) {
            wp_enqueue_script(
                TP_PLUGIN_TEXTDOMAIN . basename($main_asset),
                $main_asset,
                array(),
                TP_PLUGIN_RESOURCES_VER,
                $this->is_script_footer()
            );
        }
    }

    public function enqueue_style($hook)
    {
        // TODO: Implement enqueue_style() method.
        if ($this->is_script() == false) {
            return;
        }
        $main_assets = $this->get_webpack_assets()->get_assets_by_name('main', TP_PLUGIN_FRONTEND_DIST_URL, 'css');
        foreach ($main_assets as $main_asset) {
            wp_enqueue_style(
                TP_PLUGIN_TEXTDOMAIN . basename($main_asset),
                $main_asset,
                array(),
                TP_PLUGIN_RESOURCES_VER
            );
        }
    }

    public function set_custom_webpack_path()
    {
        // TODO: Implement set_custom_webpack_path() method.
        if ($this->is_script() == false) {
            return;
        }
        $path = get_home_url();
        echo '<script type="text/javascript">var TP_WEBPACK_PUBLIC_PATH = "' . TP_PLUGIN_FRONTEND_DIST_URL . '";</script>';
    }

    public function footer_enqueue_script()
    {
        if ($this->is_script() == false) {
            return;
        }
    }

    // $option_limit_script option settings
    public function is_script()
    {
        return true;
        global $post;
        $is_script = false;
        $option_limit_script = false;
        if ($option_limit_script) {
            $is_script = true;
        } else {
            if (false === tp_in_array_recursive('travelpayouts', wp_get_sidebars_widgets()) &&
                false === $this->is_shortcode_post($post, '[tp') && !is_home() &&
                false === strpos(term_description(), 'travel-table-wrap')) {
                $is_script = false;
            } else {
                $is_script = true;
            }
        }
        return $is_script;
    }

    public function is_shortcode_post($post, $shortcode)
    {
        if (is_object($post)) {
            if (isset($post->post_content)) {
                return strpos($post->post_content, $shortcode);
            }
        }
        return false;
    }

    public function is_script_footer()
    {
        return false;
    }

    /**
     * Get Tp_Webpack instance
     * @return includes\Tp_Webpack
     */
    protected function get_webpack_assets()
    {
        return $this->webpack_assets;
    }
}