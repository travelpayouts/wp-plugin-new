<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/23/2018
 * Time: 10:31 AM
 */

namespace tp\frontend\components;

use \tp\includes as Includes;
use Symfony\Component\Form\FormFactory;

abstract class TP_Base_Frontend_Controller
{
    protected $form;
    protected $template;
    protected $templating = false;

    protected function __construct()
    {
        try {
            $template_engine = $this->get_templating_instance();
            $template_engine->add_template(TP_PLUGIN_FRONTEND_PATRIAL_PATH);
            if (!defined('TP_PHPUNIT')) {
                $this->template = $this->get_template();
                $this->template->addExtension(new Includes\table\TP_Table_Extension());
                $this->form = $this->get_form();
            }
        } catch (\Exception $exception) {
            wp_die($exception->getMessage(), $exception->getMessage());
        }
    }

    protected function before_render()
    {
    }

    public function render($name, array $parameters = array(), $return = false)
    {
        $this->before_render();

        try {
            $this->template->addGlobal('_base', '/shortcodes/layouts/main.twig');
            $this->template->addGlobal('_img_url', TP_PLUGIN_FRONTEND_IMG_URL);
            //$this->template->addExtension(new Includes\table\TP_Table_Extension());
            $result = $this->template->render($name, $parameters);
            if ($return) {
                return $result;
            } else {
                echo $result;
            }
        } catch (\Exception $exception) {
            wp_die($exception->getMessage(), $exception->getMessage());
        }


        return false;
    }

    /**
     * Get form factory
     * @return FormFactory
     * @throws \Exception
     */
    protected function get_form()
    {
        if ($this->templating instanceof Includes\TP_Templating) {
            return $this->templating->form();
        } else {
            throw new \Exception('"templating" property must be an instance of TP_Templating');
        }
    }

    /**
     * Get templating engine
     * @return \Twig_Environment
     * @throws \Exception
     */
    protected function get_template()
    {
        if ($this->templating instanceof Includes\TP_Templating) {
            return $this->templating->template();
        } else {
            throw new \Exception('"templating" property must be an instance of TP_Templating');
        }
    }

    /**
     * Get TP_Templating instance
     * @return bool|Includes\TP_Templating
     */
    protected function get_templating_instance()
    {
        if (!$this->templating) {
            $this->templating = new Includes\TP_Templating('ru');;
        }
        return $this->templating;
    }

}