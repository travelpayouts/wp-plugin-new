jQuery(document).ready(function($) {
    var tableHead = $('.travel-table-head__col'),
        ticketBtn = $('.travel-table--default .travel-table-body__col a');

    tableHead.each(function() {
        let headTitle = $(this).children('span').text();
        $(this).prop('title', headTitle);
    });
    ticketBtn.each(function() {
        let btnTitle = $(this).children('span').text();
        $(this).prop('title', 'Tickets from ' + btnTitle + ' USD');
    });

    const table = $(".travel-table-wrap");
    const sizes = {
        "small": {
            "maxWidth": 400
        },
        "medium": {
            "minWidth": 400
        },
        "large": {
            "minWidth": 851
        }
    }
    const tableAdaptiveWidth = 851;
    const tableAdaptiveClassName = 'narrow-table';
    const setTableClasses = (tableEl) => {
        let table_size = '';
        let range_flag = 0;
        for (var size in sizes) {
            if (inRange(tableEl.parent().outerWidth(), sizes[size])) {
                table_size = size;
                tableEl.attr("size", size)
                range_flag = 1;
            }

        }
        if (!range_flag) tableEl.attr("size", '');


        if (tableEl.parent().outerWidth() <= tableAdaptiveWidth) {
            tableEl.addClass(tableAdaptiveClassName);
        } else {
            tableEl.removeClass(tableAdaptiveClassName);
        }
    };
    // Update table classes on resize
    $(window).on('resize', () => setTableClasses(table));
    // Set or remove table classes on init
    setTableClasses(table);

    function inRange(width, range) {
        let min = range.minWidth || 0;
        let max = range.maxWidth || Infinity;
        return width >= min && width <= max;
    }

    table.each((index, tableEl) => {
        $(tableEl).find('.travel-table__row')
            .each((index_row, el_row) => {
                const row = $(el_row);
                const colsCallback = (index_col, colEl) => $(colEl).attr('data-table__col', index_col);
                row.attr('data-table__row', index_row);
                row.find(".travel-table-head__col").each(colsCallback);
                row.find(".travel-table-body__col").each(colsCallback);
            });
    });

    $(".travel-table-head .travel-table-head__col").click(function(event) {
        var order_by = "",
            this_table = $(this).closest('.travel-table-wrap'),
            sort_colimn = 0;

        if ($(this).hasClass('sort_desc')) {
            $(this).removeClass('sort_desc');
            $(this).addClass('sort_asc');
            // if(!$(this).hasClass('is-active')){
            // this_table.find('.travel-table-head__col').removeClass('is-active');
            // $(this).addClass('is-active');
            // }

            order_by = "sort_asc";
            sort_colimn = $(this).data('table__col');
        } else {
            $(this).removeClass('sort_asc');
            $(this).addClass('sort_desc');
            // if(!$(this).hasClass('is-active')){
            // this_table.find('.travel-table-head__col').removeClass('is-active');
            // $(this).addClass('is-active');
            // }

            order_by = "sort_desc";
            sort_colimn = $(this).data('table__col');
        }
        var row_array = [];

        this_table.find('.travel-table__row').each(function(index__row, el__row) {
            var row_array_col = [];
            $(this).find('.travel-table-body__col').each(function(index__col, el__col) {
                if ($(this).children().data('avia')) {
                    row_array_col.push($(this).children());

                } else {
                    row_array_col.push($.trim($(this).find('span').text()));
                }
            });
            row_array.push(row_array_col);
        });

        if ($(this).hasClass('sort_desc')) {
            row_array.sort(function(a, b) {
                // console.log("SORT DESC");
                var a1 = a[sort_colimn],
                    b1 = b[sort_colimn];

                if ($.isNumeric(a[sort_colimn])) {
                    // console.log("SORT DESC NUMBER");
                    a1 = Number(a[sort_colimn]);
                    b1 = Number(b[sort_colimn]);
                } else if (a[sort_colimn].length == 10 && a[sort_colimn].indexOf(".") != -1) {
                    var new_a = a[sort_colimn].split(".")[1] + "." + a[sort_colimn].split(".")[0] + "." + a[sort_colimn].split(".")[2];
                    var new_b = b[sort_colimn].split(".")[1] + "." + b[sort_colimn].split(".")[0] + "." + b[sort_colimn].split(".")[2];
                    if (Date.parse(new_a)) {
                        // console.log("SORT DESC DATE");
                        a1 = Number(Date.parse(new_a));
                        b1 = Number(Date.parse(new_b));
                    }
                } else if ($.type(a[sort_colimn]) === "object") {
                    // console.log("SORT DESC DATA-ATTR");
                    a1 = a[sort_colimn].data("avia");
                    b1 = b[sort_colimn].data("avia");
                } else {
                    // console.log("SORT DESC ELSE");

                    a1 = a[sort_colimn];
                    b1 = b[sort_colimn];

                    if (a[sort_colimn].indexOf("%") != -1) {
                        // console.log("SORT DESC SALE");
                        a1 = Number(a[sort_colimn].replace("%", ""));
                    }
                    if (b[sort_colimn].indexOf("%") != -1) {
                        b1 = Number(b[sort_colimn].replace("%", ""));
                    }
                    if (a[sort_colimn].indexOf("Прямой") != -1) {
                        // console.log("SORT DESC transplantation");
                        a1 = "0 Пересадок";
                    }
                    if (b[sort_colimn].indexOf("Прямой") != -1) {
                        b1 = "0 Пересадок";
                    }
                }

                // console.log(a1 + " == " + b1);
                if (a1 == b1) return 0;
                return a1 > b1 ? 1 : -1;
            });
        } else {
            row_array.sort(function(a, b) {
                // console.log("SORT ASC");
                var a1 = a[sort_colimn],
                    b1 = b[sort_colimn];

                if ($.isNumeric(a[sort_colimn])) {
                    // console.log("SORT ASC NUMBER");
                    a1 = Number(a[sort_colimn]);
                    b1 = Number(b[sort_colimn]);
                } else if (a[sort_colimn].length == 10 && a[sort_colimn].indexOf(".") != -1) {
                    var new_a = a[sort_colimn].split(".")[1] + "." + a[sort_colimn].split(".")[0] + "." + a[sort_colimn].split(".")[2];
                    var new_b = b[sort_colimn].split(".")[1] + "." + b[sort_colimn].split(".")[0] + "." + b[sort_colimn].split(".")[2];
                    if (Date.parse(new_a)) {
                        // console.log("SORT ASC DATE");
                        a1 = Number(Date.parse(new_a));
                        b1 = Number(Date.parse(new_b));
                    }
                } else if ($.type(a[sort_colimn]) === "object") {
                    // console.log("SORT ASC DATA-ATTR");
                    a1 = a[sort_colimn].data("avia");
                    b1 = b[sort_colimn].data("avia");
                } else {
                    // console.log("SORT ASC ELSE");
                    a1 = a[sort_colimn];
                    b1 = b[sort_colimn];


                    if (a[sort_colimn].indexOf("%") != -1) {
                        // console.log("SORT ASC SALE");
                        a1 = Number(a[sort_colimn].replace("%", ""));
                    }
                    if (b[sort_colimn].indexOf("%") != -1) {
                        b1 = Number(b[sort_colimn].replace("%", ""));
                    }
                    if (a[sort_colimn].indexOf("Прямой") != -1) {
                        // console.log("SORT DESC transplantation");
                        a1 = "0 Пересадок";
                    }
                    if (b[sort_colimn].indexOf("Прямой") != -1) {
                        b1 = "0 Пересадок";
                    }
                }

                if (a1 == b1) return 0;
                return a1 < b1 ? 1 : -1;
            });
        }


        this_table.find('.travel-table__row').each(function(index, el) {
            $(this).find(".travel-table-body__col").each(function(index__col, el__col) {
                if ($.type(row_array[index][index__col]) === "object") {
                    // console.log(row_array[index][index__col]);
                    $(this).html(row_array[index][index__col]);
                } else {
                    $(this).find('span').text(row_array[index][index__col]);
                }
            });
        });
    });

});