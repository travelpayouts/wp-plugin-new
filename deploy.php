<?php

namespace Deployer;
require 'recipe/common.php';
require 'vendor/deployer/recipes/recipe/slack.php';
require 'vendor/deployer/recipes/recipe/npm.php';

set('default_stage', 'dev');
// Project name
set('application', 'travelpayouts');

// Project repository
set('repository', ' git@bitbucket.org:travelpayouts/wp-plugin-new.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('slack_webhook', 'https://hooks.slack.com/services/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');
set('slack_success_text', 'Deploy to *{{deployed_to}}* successful. Deployed branch is *{{branch}}*.');
set('slack_failure_text', 'Deploy to *{{deployed_to}}* failed. Deployed branch is *{{branch}}*.');
// Shared files/dirs between deploys 

add('shared_dirs', [
]);

// Writable dirs by web server
add('writable_dirs', [
]);

add('copy_dirs', []);

task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'npm:install',
    'webpack',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success',
    'reloadApache',
]);

// Hosts
host('dev')
    ->stage('production')
    ->hostname('dist.ooo')
    ->port('2837')
    ->user('tp')
    ->set('deploy_path', '/home/travelpayouts')
    ->set('deployed_to','http://wp.dist.ooo');


before('deploy', 'what_branch');

task('what_branch', function () {
    $branch = ask('What branch to deploy?');
    on(roles('app'), function ($host) use ($branch) {
        set('branch', $branch);
    });
})->local();

task('npm:install', function () {
    if (has('previous_release')) {
        run('cp -R {{previous_release}}/node_modules {{release_path}}/node_modules');
    }

    run('cd {{release_path}} && npm install');
});

task('webpack', function () {
    cd('{{release_path}}');
    run('npm run build-admin:production', array(
        'timeout' => 1800,
    ));
    run('npm run build-frontend:production', array(
        'timeout' => 1800,
    ));
});

task('reloadApache', function () {
    try {
        run('sudo /etc/init.d/apache2 restart');
    } catch (\Exception $exception) {
        echo('Apache successfully restarted');
    }
});

after('reloadApache', 'slack:notify:success');


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
after('deploy:failed', 'slack:notify:failure');