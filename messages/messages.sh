#!/usr/bin/env bash
find ../ \( -iname "*.php" -o -iname "*.twig" \) -not -path "*vendor*" | xargs xgettext --language=PHP --from-code=UTF-8 -k_e -k_x -k__ --add-comments -o messages.pot