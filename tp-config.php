<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 14.09.17
 * Time: 9:48 AM
 */
if (!function_exists('get_plugins')) {
    require_once tp_get_admin_path() . 'includes/plugin.php';
}

if (!defined('TP_PLUGIN_PATH')) {
    define('TP_PLUGIN_PATH', dirname(__FILE__) . '/');
}
if (!defined('TP_PLUGIN_ADMIN_PATRIAL_PATH')) {
    define('TP_PLUGIN_ADMIN_PATRIALS_PATH', TP_PLUGIN_PATH . 'admin/partials');
}

if (!defined('TP_PLUGIN_ADMIN_VIEWS_PATH')) {
    define('TP_PLUGIN_ADMIN_VIEWS_PATH', TP_PLUGIN_PATH . 'admin/views');
}

if (!defined('TP_PLUGIN_ADMIN_SCRIPTS_PATH')) {
    define('TP_PLUGIN_ADMIN_SCRIPTS_PATH', TP_PLUGIN_PATH . 'admin/dist/');
}

if (!defined('TP_PLUGIN_FRONTEND_PATRIAL_PATH')) {
    define('TP_PLUGIN_FRONTEND_PATRIAL_PATH', TP_PLUGIN_PATH . 'frontend/partials');
}

if (!defined('TP_PLUGIN_FRONTEND_SCRIPTS_PATH')) {
    define('TP_PLUGIN_FRONTEND_SCRIPTS_PATH', TP_PLUGIN_PATH . 'frontend/dist/');
}

if (!defined('TP_PLUGIN_URL')) {
    define('TP_PLUGIN_URL', plugin_dir_url(__FILE__));
}

if (!defined('TP_PLUGIN_ADMIN_DIST_URL')) {
    define('TP_PLUGIN_ADMIN_DIST_URL', TP_PLUGIN_URL . 'admin/dist/');
}
if (!defined('TP_PLUGIN_ADMIN_IMG_URL')) {
    define('TP_PLUGIN_ADMIN_IMG_URL', TP_PLUGIN_ADMIN_DIST_URL . 'images/');
}
if (!defined('TP_PLUGIN_ADMIN_STYLES_URL')) {
    define('TP_PLUGIN_ADMIN_STYLES_URL', TP_PLUGIN_ADMIN_DIST_URL . 'styles/');
}
if (!defined('TP_PLUGIN_ADMIN_SCRIPTS_URL')) {
    define('TP_PLUGIN_ADMIN_SCRIPTS_URL', TP_PLUGIN_ADMIN_DIST_URL . 'scripts/');
}

if (!defined('TP_PLUGIN_FRONTEND_DIST_URL')) {
    define('TP_PLUGIN_FRONTEND_DIST_URL', TP_PLUGIN_URL . 'frontend/dist/');
}
if (!defined('TP_PLUGIN_FRONTEND_IMG_URL')) {
    define('TP_PLUGIN_FRONTEND_IMG_URL', TP_PLUGIN_FRONTEND_DIST_URL . 'images/');
}
if (!defined('TP_PLUGIN_FRONTED_STYLES_URL')) {
    define('TP_PLUGIN_FRONTED_STYLES_URL', TP_PLUGIN_FRONTEND_DIST_URL . 'styles/');
}
if (!defined('TP_PLUGIN_FRONTEND_SCRIPTS_URL')) {
    define('TP_PLUGIN_FRONTEND_SCRIPTS_URL', TP_PLUGIN_FRONTEND_DIST_URL . 'scripts/');
}

if (!defined('TP_PLUGIN_RESOURCES_VER')) {
    define('TP_PLUGIN_RESOURCES_VER', null);
}

$tp_plugin_headers = get_plugin_data(TP_PLUGIN_PATH . '/' . basename(TP_PLUGIN_PATH) . '.php', false, false);
if (!defined('TP_PLUGIN_SLUG')) {
    define('TP_PLUGIN_SLUG', $tp_plugin_headers['TextDomain']);
}
if (!defined('TP_PLUGIN_TEXTDOMAIN')) {
    define('TP_PLUGIN_TEXTDOMAIN', $tp_plugin_headers['TextDomain']);
}
if (!defined('TP_PLUGIN_FRONTEND_TEXTDOMAIN')) {
    define('TP_PLUGIN_FRONTEND_TEXTDOMAIN', TP_PLUGIN_TEXTDOMAIN . '_frontend');
}
if (!defined('TP_PLUGIN_VERSION')) {
    define('TP_PLUGIN_VERSION', $tp_plugin_headers['Version']);
}
if (!defined('TP_PLUGIN_NAME')) {
    define('TP_PLUGIN_NAME', $tp_plugin_headers['Name']);
}
if (!defined('TP_PLUGIN_OPTION_NAME')) {
    define('TP_PLUGIN_OPTION_NAME', TP_PLUGIN_SLUG . '_options');
}
if (!defined('TP_PLUGIN_OPTION_VERSION')) {
    define('TP_PLUGIN_OPTION_VERSION', TP_PLUGIN_OPTION_NAME . '_version');
}
if (!defined('TP_PLUGIN_AJAX_URL')) {
    define('TP_PLUGIN_AJAX_URL', admin_url('admin-ajax.php'));
}
if (!defined('TP_PLUGIN_LOCALIZATION_PATH')) {
    define('TP_PLUGIN_LOCALIZATION_PATH', plugin_basename(TP_PLUGIN_PATH . 'languages/'));
}
if (!defined('TP_PLUGIN_LOCALIZATION_ABSPATH')) {
    define('TP_PLUGIN_LOCALIZATION_ABSPATH', TP_PLUGIN_PATH . 'languages/');
}
if (!defined('TP_PLUGIN_LOCALIZATION_CUSTOM')) {
    define("TP_PLUGIN_LOCALIZATION_CUSTOM", false);
}

if (!defined('TP_PLUGIN_DEBUG_LOG')) {
    define('TP_PLUGIN_DEBUG_LOG', true);
}
if (!defined('TP_PLUGIN_DATABASE')) {
    define('TP_PLUGIN_DATABASE', 8);
}
if (!defined('TP_PLUGIN_DB_VER')) {
    define('TP_PLUGIN_DB_VER', '1.0.0');
}

if (!defined('TP_PLUGIN_CONTENT_CACHE_DIR')) {
    define('TP_PLUGIN_CONTENT_CACHE_DIR', wp_normalize_path(WP_CONTENT_DIR . '/' . TP_PLUGIN_TEXTDOMAIN));
}

if (!defined('TP_PLUGIN_UPLOAD_CACHE_DIR')) {
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    define('TP_PLUGIN_UPLOAD_CACHE_DIR', wp_normalize_path($upload_dir . '/' . TP_PLUGIN_TEXTDOMAIN));
}

if (!defined('TP_PLUGIN_CACHE_DIR')) {
    define('TP_PLUGIN_CACHE_DIR', wp_normalize_path(TP_PLUGIN_PATH . 'cache'));
}

// Constant shortcode return type
//view | html | url | array_cache | array_api
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW')) {
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW', 'view');
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_HTML')) {
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_HTML', 'html');
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL')) {
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL', 'url');
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_CACHE')) {
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_CACHE', 'array_cache');
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_API')) {
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_API', 'array_api');
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPES')) {

    $return_types = array(
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_VIEW,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_HTML,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_CACHE,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_API
    );
    define('TP_PLUGIN_SHORTCODE_RETURN_TYPES', implode('|', $return_types));
}
if (!defined('TP_PLUGIN_SHORTCODE_RETURN_TYPE_DEBUG_MODE')) {
    $return_type_debug = array(
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_CACHE,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_API
    );

    define('TP_PLUGIN_SHORTCODE_RETURN_TYPE_DEBUG_MODE', implode('|', $return_type_debug));
}

if (!defined('TP_PLUGIN_SHORTCODE_NOCACHE_MODE')) {
    $no_cache_mode = array(
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL,
        TP_PLUGIN_SHORTCODE_RETURN_TYPE_ARRAY_API
    );
    define('TP_PLUGIN_SHORTCODE_NOCACHE_MODE', implode('|', $no_cache_mode));
}
if (!defined('TP_PLUGIN_SHORTCODE_TYPE_FLIGHT')) {
    define('TP_PLUGIN_SHORTCODE_TYPE_FLIGHT', 'flight');
}
if (!defined('TP_PLUGIN_SHORTCODE_TYPE_HOTEl')) {
    define('TP_PLUGIN_SHORTCODE_TYPE_HOTEl', 'hotel');
}
if (!defined('TP_PLUGIN_SHORTCODE_TYPE_RAILWAY')) {
    define('TP_PLUGIN_SHORTCODE_TYPE_RAILWAY', 'railway');
}
if (!defined('TP_PLUGIN_SHORTCODE_TYPE_WIDGET')) {
    define('TP_PLUGIN_SHORTCODE_TYPE_WIDGET', 'widget');
}