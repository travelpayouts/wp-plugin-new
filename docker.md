# Travelpayouts plugin

### Starting docker container
To start your wordpress instance with travelpayouts plugin installed you need execute this command:

**notice:** At the first time it take about 3-5 minutes. At this time composer package manager will install dependencies

```
docker-compose up -d wordpress
```

You can also check logs of container to understand that everything works fine.

Just execute the command:
```
docker-compose logs -f wordpress
```

If you see something like this, it means that your wordpress instance started without any mistakes
```
wordpress_1  | [Wed Oct 10 15:09:51.709008 2018] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.25 (Debian) PHP/5.6.38 configured -- resuming normal operations
wordpress_1  | [Wed Oct 10 15:09:51.709167 2018] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
```

Now open [http://127.0.0.1:8000/](http://127.0.0.1:8000/) and setup your wordpress blog.

At next you need to go to plugins section and activate Travelpayouts plugin.


### Stopping docker container
If you want to stop wordpress instance just execute the command:

```
docker-compose down
```