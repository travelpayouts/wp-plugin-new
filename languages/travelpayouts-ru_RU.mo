��          t       �       �   H   �   
        !     0     7     I     V     _  ;   g  3   �  M  �  k   %  #   �     �  
   �     �     �             ^   /  �   �   A problem with the plugin? Have some suggestions or ideas? Contact us at Auto-links Flight Tickets Hotels Railways schedule Search forms Settings Widgets You do not have sufficient permissions to access this page. You may add several anchors, use comma as separator Project-Id-Version: Travelpayouts
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-16 13:01+0300
PO-Revision-Date: 2018-02-06 14:19+0000
Last-Translator: 
Language-Team: Russian (Russia)
Language: ru-RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Loco-Source-Locale: uk_UA
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-WPHeader: travelpayouts.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Loco-Parser: loco_parse_po Проблема с плагином? Есть пожелания или идеи? Пишите нам на Подстановка ссылок Авиабилеты Отели ЖД расписание Поисковые формы Настройки Виджеты У вас недостаточно прав для доступа к этой странице Вы можете добавить несколько якорей, использовать запятую в качестве разделителя 