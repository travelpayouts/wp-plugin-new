<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 14.09.17
 * Time: 3:59 PM
 */

/**
 * @return mixed
 */
function tp_get_admin_path()
{
    $admin_path = str_replace(get_bloginfo('url') . '/', ABSPATH, get_admin_url());
    $admin_path = apply_filters('tp_get_admin_path', $admin_path);
    return $admin_path;
}

function tp_register_wp_autoload($prefix, $base_dir)
{
    if (function_exists('register_wp_autoload')) {
        return register_wp_autoload('tp\\', __DIR__);
    }

}

function tp_create_dir_cache(){
    $cache_dir = TP_PLUGIN_CONTENT_CACHE_DIR;
    if ( ! wp_mkdir_p( $cache_dir ) ){
        $cache_dir = TP_PLUGIN_UPLOAD_CACHE_DIR;
        if ( ! wp_mkdir_p( $cache_dir ) ){
            $cache_dir = TP_PLUGIN_CACHE_DIR;
        }
    }
    return $cache_dir;
}

function tp_in_array_recursive($search, $array){
    $result = false;
    if (in_array($search, $array)) {
        return true;
    }
    foreach ($array as $value) {
        if (is_array($value)) {
            $result = tp_in_array_recursive($search, $value);
            if ($result) {
                return true;
            }//if
        } else {
            return (false !== strpos( $value, $search ));
        }//if
    }
    return $result;
}

/**
 * @param $return_type
 * @return bool
 */
function tp_is_shortcode_debug_mode($return_type){
    if (in_array($return_type, explode('|', TP_PLUGIN_SHORTCODE_RETURN_TYPE_DEBUG_MODE))){
        return true;
    }
    return false;
}

/**
 * @param $return_type
 *
 * @return bool
 */
function tp_is_shortcode_html_mode($return_type){
    if ($return_type == TP_PLUGIN_SHORTCODE_RETURN_TYPE_HTML){
        return true;
    }
    return false;
}

/**
 * @param null $sections
 * @param null $default_value
 * @return bool|mixed
 */
function tp_get_option($sections = null, $default_value = null){
    $value = \tp\includes\Options::get($sections, $default_value);
    return $value;
}

/**
 * @param null $sections
 * @param null $value
 *
 * @return array
 */
function tp_set_option($sections = null, $value = null){
    return \tp\includes\Options::set($sections, $value);
}

function tp_get_flight_shortcodes(){
    return array_merge(array(),
        \tp\frontend\controllers\shortcodes\flights\TP_Price_Calendar_Week_Controller::get_shortcode_tag(),
        \tp\frontend\controllers\shortcodes\flights\TP_Price_Calendar_Month_Controller::get_shortcode_tag(),
        \tp\frontend\controllers\shortcodes\flights\TP_Cheapest_Flights_Controller::get_shortcode_tag(),
        \tp\frontend\controllers\shortcodes\flights\TP_Cheapest_Ticket_Each_Day_Month_Controller::get_shortcode_tag()
        );
}

/**
 * @return string
 */
function tp_get_lang(){
    return 'ru';
}