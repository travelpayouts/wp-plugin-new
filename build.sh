########################################################
#   Подготавливаем билд для каталога WP
#   author: Andrey Polyakov
########################################################

#!/bin/bash

PLUGIN_NAME="travelpayouts"
BUILD_DIR="build"
BUILD_NAME=${PLUGIN_NAME}_$(date +"%Y-%m-%d_%H-%M-%S").zip

echo 'Копируем файлы'
cd ../

## Если папка с билдом отсутсвует, создаем ее
if [ ! -d "$BUILD_DIR" ]; then
    mkdir $BUILD_DIR
    else
    cd $BUILD_DIR
    ## Удаляем старую папку билда, если имеется
    if [ -d "$PLUGIN_NAME" ]; then
        rm -rf $PLUGIN_NAME
    fi
    cd ../
fi

cp -R $PLUGIN_NAME build
cd build/$PLUGIN_NAME
echo 'Запускаем вебпак'
echo '============================================================'
./node_modules/.bin/webpack --progress --hide-modules
echo '============================================================'
echo 'Подчищаем хвосты'
rm -rf node_modules
( find . -type d -name ".git" && find . -name ".gitignore" && find . -name ".gitmodules" && find . -name "composer.json" ) | xargs rm -rf
rm package-lock.json package.json composer.lock composer.json build.sh webpack.config.js postcss.config.js
rm -rf .idea
cd ../
echo "Пакуем билд в архив"
zip $BUILD_NAME $PLUGIN_NAME/ -9 -r -q -p
rm -rf $PLUGIN_NAME
echo "Билд $BUILD_NAME успешно создан"
