<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/5/2017
 * Time: 10:28 AM
 */

namespace tp\common;

use tp\includes;

class TP_Common
{
    public function __construct()
    {
        $this->load_dependencies();
    }

    private function load_dependencies(){
        $this->set_locale();
    }

    private function set_locale(){
        $plugin_i18n = new includes\TP_I18n();
    }
}