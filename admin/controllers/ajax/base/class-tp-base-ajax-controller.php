<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax\base;

use yii\base\ViewContextInterface;
use Yii;

class TP_Base_Ajax_Controller implements ViewContextInterface
{
    public function getViewPath()
    {
        return TP_PLUGIN_ADMIN_VIEWS_PATH . '/shortcode';
    }

    public function response($data, $status = 'ok')
    {
        $result = array(
            'data' => $data,
            'status' => $status
        );
        echo json_encode($result);
        wp_die();
    }

    /**
     * Return rendering function
     * @param $name
     * @param array $params
     * @return string
     */
    public function render($name, $params = [])
    {
        return Yii::$app->getView()->render($name, $params, $this);
    }
}