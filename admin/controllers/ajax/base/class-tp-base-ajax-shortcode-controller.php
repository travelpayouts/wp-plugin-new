<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax\base;

use Exception;
use Yii;
use yii\base\Model;
use tp\admin\includes\models\shortcodes\BaseModel;
use Adbar\Dot;

abstract class TP_Base_Ajax_Shortcode_Controller extends TP_Base_Ajax_Controller
{
    public $model = false;

    protected function get_shortcode(BaseModel $model)
    {
        try {
            $shortcodeData = $model->shortcodeData;
            return $this->render('_base/generator', [
                'shortcode' => $shortcodeData->shortcode,
                'attributes' => $shortcodeData->params]);
        } catch (\Exception $exception) {
            return '';
        }
    }


    public function action_get_shortcode()
    {
        $shortcode_data = new Dot(Yii::$app->request->post('shortcode_data'), '/');
        if ($shortcode_data) {
            $model = $this->get_model();
            if ($shortcode_data->has('current_table')) {
                $model->current_table = $shortcode_data->get('current_table');
            }
            $model->attributes = $shortcode_data->all();
            if (!$model->validate()) {
                $this->response([
                    'messages' => $model->getErrors()
                ], 'error');
                return false;
            }
            $result = [
                'shortcode' => $this->get_shortcode($model)
            ];
            $this->response($result);
        }
    }


    public function get_model()
    {
        if ($this->model) {
            return new $this->model();
        }
        throw new Exception('Can\'t find model attribute');
    }

    protected function get_translated_messages()
    {
        return array(
            'success_add' => _x('Shortcode successfully added',
                'tp post shortcode added message', TP_PLUGIN_TEXTDOMAIN),
            'fail_to_add' => _x('There was an error adding shortcode',
                'tp post shortcode error added message', TP_PLUGIN_TEXTDOMAIN),
            'fail_to_validate' => _x('Please correct errors',
                'tp post shortcode error validate message', TP_PLUGIN_TEXTDOMAIN)
        );
    }

}