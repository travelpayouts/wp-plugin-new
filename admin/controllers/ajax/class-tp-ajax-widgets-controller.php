<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

use tp\admin\includes\models\shortcodes\HotelsModel;
use tp\includes;

class TP_Ajax_Widgets_Controller extends base\TP_Base_Ajax_Shortcode_Controller
{
    public $model = 'tp\admin\includes\models\shortcodes\WidgetsModel';
    public function action_modal_data()
    {
        $model = $this->get_model();
        $data = array(
            'modalContent' => $this->render('widgets/modal_content',
                [
                    'model' => $model,
                ]),
            'messages' => $this->get_translated_messages(),
            'attributes' => $model->attributes,
            'hotelLists' => HotelsModel::instance()->hotel_types,
            'table_fields' => $model->table_fields,
        );

        $this->response($data);
    }
}