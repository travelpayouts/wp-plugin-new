<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

use tp\admin\includes\models\TP_Model_Search_Form;

class TP_Ajax_Form_Controller extends base\TP_Base_Ajax_Shortcode_Controller
{
    public $model = 'tp\admin\includes\models\shortcodes\FormModel';

    public function action_modal_data()
    {
        $model = $this->get_model();
        $data = array(
            'modalContent' => $this->render('search_forms/modal_content',
                [
                    'model' => $model,
                    'search_forms' => $this->get_search_forms(),
                ]),
            'messages' => $this->get_translated_messages(),
            'attributes' => $model->attributes,
        );

        $this->response($data);
    }

    /**
     * Return
     * @return array
     */
    private function get_search_forms()
    {
        $search_forms = TP_Model_Search_Form::find()
            ->select(['id', 'title', 'slug'])
            ->limit(100)
            ->all();

        return array_map(function (TP_Model_Search_Form $search_form) {
            return array_filter($search_form->attributes);
        }, $search_forms);
    }
}