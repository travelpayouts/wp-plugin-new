<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

class TP_Ajax_Hotels_Controller extends base\TP_Base_Ajax_Shortcode_Controller
{
    public $model = 'tp\admin\includes\models\shortcodes\HotelsModel';

    public function action_modal_data()
    {
        $model = $this->get_model();
        $data = array(
            'modalContent' => $this->render('hotels/modal_content',
                [
                    'model' => $model
                ]),
            'table_fields' => $model->table_fields,
            'messages' => $this->get_translated_messages(),
            'attributes' => $model->attributes,
            'hotelLists' => $model->hotel_types,
        );
        $this->response($data);
    }
}