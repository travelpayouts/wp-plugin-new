<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

use phpFastCache\CacheManager;

class TP_Ajax_Autocomplete_Controller extends base\TP_Base_Ajax_Controller
{
    protected $instance_cache;

    public function __construct()
    {
        $this->instance_cache = CacheManager::Files(array(
            'storage' => 'files',
            'default_chmod' => 0777,
            'htaccess' => true,
            'securityKey' => TP_PLUGIN_TEXTDOMAIN,
            'path' => tp_create_dir_cache(),
        ));
    }

    public function action_airlines($query, $lang)
    {
        $allowed_lang = array(
            'ru',
            'en',
            'th',
            'zh-CN'
        );
        $lang = in_array($lang, $allowed_lang) ? $lang : 'en';

        $cache_key = 'airlines_' . $lang;
        $cached_data = $this->instance_cache->get($cache_key);

        if (!$cached_data) {
            $source_path = TP_PLUGIN_PATH . 'data/airlines.json';
            if (file_exists($source_path)) {
                $source_data = json_decode(file_get_contents($source_path), true);

                $mapped_airlines = array_map(function ($airline) use ($lang) {
                    if (isset($airline['names']) && isset($airline['names'][$lang])) {
                        $airline['name'] = $airline['names'][$lang];
                        unset($airline['names']);
                        return $airline;
                    }
                    return false;
                }, $source_data);
                $cached_data = array_filter($mapped_airlines);
                $this->instance_cache->set($cache_key, $cached_data, 86400);

            }
        }

        $data = array_filter($cached_data, function ($airline) use ($query) {
            $query = mb_strtolower($query);
            $airline_name = mb_strtolower($airline['name']);
            return preg_match('/' . $query . '/iu', $airline_name);
        });
        $this->response($data);
    }
}