<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

class TP_Ajax_Railway_Controller extends base\TP_Base_Ajax_Shortcode_Controller
{
    public $model = 'tp\admin\includes\models\shortcodes\RailwayModel';

    public function action_modal_data()
    {
        $model = $this->get_model();
        $data = array(
            'modalContent' => $this->render('railway/modal_content',
                [
                    'model' => $model
                ]),
            'messages' => $this->get_translated_messages(),
            'attributes' => $model->attributes,
        );

        $this->response($data);
    }
}