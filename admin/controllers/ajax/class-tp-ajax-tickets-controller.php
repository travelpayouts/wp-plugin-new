<?php

/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\controllers\ajax;

class TP_Ajax_Tickets_Controller extends base\TP_Base_Ajax_Shortcode_Controller
{
    public $model = 'tp\admin\includes\models\shortcodes\FlightsModel';

    public function action_modal_data()
    {
        $model = $this->get_model();
        $data = [
            'modalContent' => $this->render('flight_tickets/modal_content', [
                'model' => $model,
                // @TODO : If you found it change it please to actual value
                'locale' => 'en',
            ]),
            'table_fields' => $model->table_fields,
            'messages' => $this->get_translated_messages(),
            'attributes' => $model->attributes,
        ];

        $this->response($data);
    }
}