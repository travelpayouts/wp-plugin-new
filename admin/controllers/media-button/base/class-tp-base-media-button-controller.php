<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/3/2017
 * Time: 10:07 AM
 */

namespace tp\admin\controllers\media_button\base;

use yii\base\ViewContextInterface;
use tp\admin\components\TP_Base_Admin_Controller;

/**
 * Class TP_Base_Media_Button_Controller
 * @package tp\admin\controllers\media-button\base
 * @property string $button_title_full
 * @property string $button_title_short
 * @property integer $button_title_choice
 * @property string $button_id
 * @property boolean $show_in_category_add
 * @property boolean $show_in_category_edit
 * @property boolean $show_in_tag_add
 * @property boolean $show_in_tag_edit
 * @property string $icon_url (Optional) The URL to the icon to be used for this menu.
 * @property string $modal_title (Optional)
 */
abstract class TP_Base_Media_Button_Controller extends TP_Base_Admin_Controller implements ITP_Base_Media_Button_Controller, ViewContextInterface
{
    public function getViewPath()
    {
        return TP_PLUGIN_ADMIN_VIEWS_PATH . '/button';
    }

    protected function define_hooks()
    {
        // TODO: Implement define_hooks() method.
        if ($this->isShow() == false) {
            return false;
        }

        add_action('media_buttons', array($this, 'add_media_button'));
        if ($this->show_in_category_add) {
            add_action('category_add_form_fields', array($this, 'add_media_button'));
        }
        if ($this->show_in_category_edit) {
            add_action('category_edit_form_fields', array($this, 'add_media_button'));
        }
        if ($this->show_in_tag_add) {
            add_action('add_tag_form_fields', array($this, 'add_media_button'));
        }
        if ($this->show_in_tag_edit) {
            add_action('edit_tag_form_fields', array($this, 'add_media_button'));
        }
    }

    protected function isShow()
    {
        // Option
        return true;
    }


    public function add_media_button($editor_id)
    {
        $hidden_editors = [
            'category'
        ];
        if (in_array($editor_id, $hidden_editors)) return false;

        $button_data = array(
            'id' => $this->button_id,
            'url' => $this->get_button_url(),
            'class' => $this->get_button_class(),
            'title' => $this->get_button_title(),
            'icon' => $this->icon_url,
            'modal_title' => $this->modal_title,
            'opened' => false,
        );
//        // @TODO Remove this, just for tests
//        if ($this->button_id === 'tp-media-button-link') {
//            $button_data['opened'] = true;
//        }

        add_action('admin_footer', array($this, 'render_controller'));
        add_action('wp_footer', array($this, 'render_controller'));
        echo $this->_render('shortcode', $button_data);
    }


    public function get_button_url()
    {
        return '#';
    }

    public function get_button_class()
    {
        return 'su-generator-button button';
    }

    public function get_button_title()
    {
        $title = $this->button_title_full;
        if ($this->button_title_choice == 1) {
            $title = $this->button_title_short;
        }
        return $title;
    }


    public function get_button_title_choice()
    {
        // TODO: Implement get_button_title_choice() method.
        // Option
        return 1;
    }

    public function get_show_in_category_add()
    {
        // TODO: Implement get_show_in_category_add() method.
        return true;
    }

    public function get_show_in_category_edit()
    {
        // TODO: Implement get_show_in_category_edit() method.
        return true;
    }

    public function get_show_in_tag_add()
    {
        // TODO: Implement get_show_in_tag_add() method.
    }

    public function get_show_in_tag_edit()
    {
        // TODO: Implement get_show_in_tag_edit() method.
        return true;
    }

    /**
     * Access to data via magic methods
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        $method_name = 'get_' . $name;
        if (method_exists($this, $method_name)) {
            return $this->$method_name();
        }
        return null;
    }
}
