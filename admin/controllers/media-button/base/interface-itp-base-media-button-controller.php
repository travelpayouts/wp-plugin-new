<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/3/2017
 * Time: 8:06 PM
 */

namespace tp\admin\controllers\media_button\base;


interface ITP_Base_Media_Button_Controller
{
    public function get_button_title_full();
    public function get_button_title_short();
    public function get_show_in_category_add();
    public function get_show_in_category_edit();
    public function get_show_in_tag_add();
    public function get_show_in_tag_edit();
    public function get_icon_url();
    public function get_button_id();
    public function get_button_url();
    public function get_button_class();
    public function get_button_title();
    public function get_button_title_choice();

}