<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/5/2017
 * Time: 8:05 PM
 */

namespace tp\admin\controllers\media_button;


use tp\admin\controllers\media_button\base\TP_Base_Media_Button_Controller;

class TP_Search_Forms_Controller extends TP_Base_Media_Button_Controller
{

    public function run()
    {
        // TODO: Implement render() method.
        //$this->render('/layouts/footer.twig');
        return false;
    }

    public function get_button_title_full()
    {
        // TODO: Implement get_button_title_full() method.
        return _x('Insert search form', 'admin media button button title full', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_button_title_short()
    {
        // TODO: Implement get_button_title_short() method.
        return _x('Form', 'admin media button button title short', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_icon_url()
    {
        // TODO: Implement get_icon_url() method.
        return TP_PLUGIN_ADMIN_IMG_URL . 'tp-media-button-search-forms.svg';
    }

    public function get_button_id()
    {
        // TODO: Implement get_button_id() method.
        return 'tp-media-button-search-forms';
    }

    public function get_modal_title()
    {
        return _x('Constructor search form', 'admin media button modal title', TP_PLUGIN_TEXTDOMAIN);
    }
}