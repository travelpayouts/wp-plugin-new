<?php
/**
 * Created by PhpStorm.
 * User: andrey
 */

namespace tp\admin\controllers\menu;

use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;

class TP_Flight_Tickets_Controller extends TP_Base_Submenu_Controller
{

    public function get_menu_title()
    {
        return _x('Flight Tickets', 'admin menu flights tickets menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_page_title()
    {
        return _x('Flight Tickets', 'admin menu flights tickets page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function run()
    {
        $this->render('/menu/tickets/index.twig');
    }

    public function get_parent_slug()
    {
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN.'_flight_tickets';
    }
}