<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 23.09.17
 * Time: 1:09 PM
 */

namespace tp\admin\controllers\menu\base;


interface ITP_Base_Rootmenu_Controller
{
    public function get_icon_url();
    public function get_position();
}