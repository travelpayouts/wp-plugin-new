<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 23.09.17
 * Time: 12:25 PM
 */

namespace tp\admin\controllers\menu\base;

/**
 * Class TP_Base_Rootmenu_Controller
 * @package tp\admin\controllers\menu\base
 * @property string $icon_url (Optional) The URL to the icon to be used for this menu.
 * @property string $position (Optional) The position in the menu order this one should appear.
 *
 **/
abstract class TP_Base_Rootmenu_Controller extends TP_Base_Menu_Controller implements ITP_Base_Rootmenu_Controller
{
    protected function define_hooks()
    {
        add_action( 'admin_menu', array($this, 'add_menu_page'));
    }

    public function add_menu_page()
    {
        $plugin_page = add_menu_page(
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->menu_slug,
            array($this, 'render_controller'),
            $this->icon_url,
            $this->position
        );
        $this->set_plugin_page($plugin_page);
    }
}