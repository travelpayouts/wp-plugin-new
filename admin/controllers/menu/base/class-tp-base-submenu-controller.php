<?php
/**
 * Created by PhpStorm.
 * User: ubuntu-pc
 * Date: 23.09.17
 * Time: 11:26
 */

namespace tp\admin\controllers\menu\base;


/**
 * Class TP_Base_Submenu_Controller
 * @package tp\admin\controllers\menu\base
 * @property string $parent_slug The slug name for the parent menu (or the file name of a standard WordPress admin page).
 *
 **/
abstract class TP_Base_Submenu_Controller extends TP_Base_Menu_Controller implements ITP_Base_Submenu_Controller
{

    protected function define_hooks()
    {
        add_action( 'admin_menu', array($this, 'add_submenu_page') );
    }

    public function add_submenu_page()
    {
        $plugin_page = add_submenu_page(
            $this->parent_slug,
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->menu_slug,
            array($this, 'render_controller')
        );
        $this->set_plugin_page($plugin_page);
    }
}