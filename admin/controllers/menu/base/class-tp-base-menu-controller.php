<?php
/**
 * Created by PhpStorm.
 * User: ubuntu-pc
 * Date: 20.09.17
 * Time: 7:02
 */

namespace tp\admin\controllers\menu\base;

use tp\admin\components\TP_Base_Admin_Controller;
use yii\base\ViewContextInterface;
use Yii;

/**
 * Class TP_Base_Menu_Controller
 * @package tp\admin\controllers\menu\base
 * @property string $page_title (Required) The text to be displayed in the title tags of the page when the menu is selected.
 * @property string $menu_title (Required) The text to be used for the menu.
 * @property string $capability (Required) The capability required for this menu to be displayed to the user.
 * @property string $menu_slug  (Required) The slug name to refer to this menu by (should be unique for this menu).
 */
abstract class TP_Base_Menu_Controller extends TP_Base_Admin_Controller implements ITP_Base_Menu_Controller, ViewContextInterface
{
    protected $plugin_page;

    public function getViewPath()
    {
        return TP_PLUGIN_ADMIN_VIEWS_PATH . '/pages';
    }

    /**
     * Return rendering function
     * @param $name
     * @param array $params
     * @return string
     */
    public function _render($name, $params = [])
    {
        return Yii::$app->getView()->render($name, $params, $this);
    }

    /**
     * Access to data via magic methods
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        $method_name = 'get_' . $name;
        if (method_exists($this, $method_name)) {
            return $this->$method_name();
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function get_plugin_page()
    {
        return $this->plugin_page;
    }

    /**
     * @param $plugin_page
     */
    protected function set_plugin_page($plugin_page)
    {
        $this->plugin_page = $plugin_page;
    }

    /**
     * @return string
     */
    public function get_menu_url()
    {
        //menu_page_url($this->menu_slug, false) not working
        return admin_url('admin.php?page=' . $this->menu_slug);
    }

    /**
     * Return menu parameters
     * @return array
     */
    public function get_menu_params()
    {
        return array(
            'parent' => $this->parent_slug,
            'title' => $this->page_title,
            'url' => $this->menu_slug,
            'actions' => $this->actions
        );
    }

}