<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 23.09.17
 * Time: 1:03 PM
 */

namespace tp\admin\controllers\menu\base;


interface ITP_Base_Menu_Controller
{
    public function get_page_title();
    public function get_menu_title();
    public function get_capability();
    public function get_menu_slug();
}