<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 23.09.17
 * Time: 1:18 PM
 */

namespace tp\admin\controllers\menu\base;


interface ITP_Base_Submenu_Controller
{
    public function get_parent_slug();
}