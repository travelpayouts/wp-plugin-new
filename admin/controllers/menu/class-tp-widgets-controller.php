<?php
/**
 * Created by PhpStorm.
 * User: andrey
 */

namespace tp\admin\controllers\menu;

use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;
use tp\admin\includes\models\widgets as Widgets;
use tp\includes\TP_Request as Request;

class TP_Widgets_Controller extends TP_Base_Submenu_Controller
{
    public $widget_list = array(
        'tp\admin\includes\models\widgets\airlines\TP_model_Airtickets_Low_Price_Calendar',
        'tp\admin\includes\models\widgets\airlines\TP_model_Airtickets_Offers',
        'tp\admin\includes\models\widgets\airlines\TP_model_Airtickets_Popular',
        'tp\admin\includes\models\widgets\airlines\TP_model_Airtickets_Price_Map',
        'tp\admin\includes\models\widgets\airlines\TP_model_Airtickets_Price_Subscription',
        'tp\admin\includes\models\widgets\hotels\TP_model_Hotels_Collections',
        'tp\admin\includes\models\widgets\hotels\TP_model_Hotels_Collections_Dates',
        'tp\admin\includes\models\widgets\hotels\TP_model_Hotels_Collections_Location',
        'tp\admin\includes\models\widgets\hotels\TP_model_Hotels_Map',
        'tp\admin\includes\models\widgets\hotels\TP_model_Hotels_Widget'
    );

    public function run()
    {
        $template_output = $this->prepare_forms();
        $this->render('/menu/widgets/index.twig', $template_output);
    }


    public function prepare_forms()
    {
        $form_list = array();

        foreach ($this->widget_list as $widget_form) {
            if (class_exists($widget_form)) {
                $model = new $widget_form();
                $form = $this->get_model_form($model);
                if ($this->request()->request->has($form->getName())
                || Request::instance()->get_query($form->getName())) {
                    $form->handleRequest();
                    $model->submit_form($form);
                }
                $form_name = str_replace('tp_model_', '', strtolower(class_basename($model)));
                $form_list[$form_name] = $form->createView();
            }
        }
        return $form_list;
    }

    public function get_menu_title()
    {
        return _x('Widgets', 'admin menu widgets menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_page_title()
    {
        return _x('Widgets', 'admin menu widgets page title', TP_PLUGIN_TEXTDOMAIN);
    }


    public function get_parent_slug()
    {
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_widgets';
    }

    protected function define_hooks()
    {
        parent::define_hooks();
    }
}