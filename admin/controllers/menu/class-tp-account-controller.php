<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 11/7/2017
 * Time: 12:55 PM
 */

namespace tp\admin\controllers\menu;


use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;
use Adbar\Dot;

class TP_Account_Controller extends TP_Base_Submenu_Controller
{
    public function run()
    {
        $model = new \tp\admin\includes\models\TP_model_account();
        $form = $this->get_model_form($model);
        $form->handleRequest();

        $model->submit_form($form);

        $this->render('/menu/account/index.twig', array(
            'form' => $form->createView()
        ));
    }


    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Account', 'admin menu flights account menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Account', 'admin menu flights account menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_account';
    }

    public function get_parent_slug()
    {
        // TODO: Implement get_parent_slug() method.
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }
}