<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 11/7/2017
 * Time: 11:34 AM
 */

namespace tp\admin\controllers\menu;


use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;
use tp\admin\includes\models\tables\content;
use tp\admin\includes\models\tables\settings;


class TP_Tables_Controller extends TP_Base_Submenu_Controller
{
    public $actions = array(
        array(
            'url'=> 'content',
            'title'=> 'Content'
        ),
        array(
            'url'=> 'design',
            'title'=> 'Design'
        ),
        array(
            'url'=> 'settings',
            'title'=> 'Settings'
        )
    );


    public function prepare_forms(array $model_list)
    {
        $form_list = array();
        foreach ($model_list as $model_item) {
            if (class_exists($model_item)) {
                $model = new $model_item();
                $form = $this->get_model_form($model);
                if (is_null($form)){
                    continue;
                }
                $form->handleRequest();
                $model->submit_form($form);
                $form_name = 'form_'.str_replace('tp_model_', '', strtolower(class_basename($model)));
                $form_list[$form_name] = $form->createView();
            }
        }
        return $form_list;
    }


    public function run()
    {
        $action = $this->request()->query->get('action');
        if (!$action) {
            $this->redirect('travelpayouts_tables',array(
                'action'=> 'content'
            ));
        }
    }

    public function action_content()
    {
        $template_output = $this->prepare_forms(array(
            // flights
            content\flight\TP_Model_Price_Calendar_Month::get_class(),
            content\flight\TP_Model_Price_Calendar_Week::get_class(),
            content\flight\TP_Model_Cheapest_Flights::get_class(),
            content\flight\TP_Model_Cheapest_Ticket_Each_Day_Month::get_class(),
            content\flight\TP_Model_Cheapest_Tickets_Each_Month::get_class(),
            content\flight\TP_Model_Direct_Flights_Route::get_class(),
            content\flight\TP_Model_Direct_Flights::get_class(),
            content\flight\TP_Model_Popular_Routes_From_City::get_class(),
            content\flight\TP_Model_Popular_Destinations_Airlines::get_class(),
            content\flight\TP_Model_Our_Site_Search::get_class(),
            content\flight\TP_Model_From_Our_City_Fly::get_class(),
            content\flight\TP_Model_In_Our_City_Fly::get_class(),
            // hotel
            content\hotel\TP_Model_Selections_Discount::get_class(),
            content\hotel\TP_Model_Selections_Discount_Date::get_class(),
            // railway
            content\railway\TP_Model_Tutu::get_class()
        ));
        //error_log(print_r($template_output, true));
        $this->render('/menu/tables/content.twig', $template_output);
    }

    public function action_design()
    {
        $this->render('/menu/tables/design.twig');

    }

    public function action_design_settings()
    {
        $this->render('/menu/tables/design_settings.twig');

    }


    public function action_settings()
    {
        $template_output = $this->prepare_forms(array(
            settings\TP_Model_Basic::get_class(),
            settings\TP_Model_Flight::get_class(),
            settings\TP_Model_Hotel::get_class()
        ));
        $this->render('/menu/tables/settings.twig', $template_output);

    }

    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Tables', 'admin menu tables page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Tables', 'admin menu tables page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_tables';
    }

    public function get_parent_slug()
    {
        // TODO: Implement get_parent_slug() method.
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }
}