<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 27.09.17
 * Time: 12:52 AM
 */

namespace tp\admin\controllers\menu;

use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;

class TP_Wizard_Controller extends TP_Base_Submenu_Controller
{

    public function run()
    {
        // TODO: Implement render() method.
        $this->render('/menu/wizard/index.twig');
    }

    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Wizard', 'admin menu wizard page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Wizard', 'admin menu wizard menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_wizard';
    }

    public function get_parent_slug()
    {
        // TODO: Implement get_parent_slug() method.
        return null;
    }
}