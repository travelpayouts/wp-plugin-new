<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 27.09.17
 * Time: 12:25 AM
 */

namespace tp\admin\controllers\menu;


use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;

class TP_Settings_Controller extends TP_Base_Submenu_Controller
{

    public function run()
    {
        $model = new \tp\admin\includes\models\TP_model_settings();
        $form = $this->get_model_form($model);
        $form->handleRequest();

        $model->submit_form($form);

        $this->render('/menu/settings/index.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Settings', 'admin menu settings page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Settings', 'admin menu settings menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_settings';
    }

    public function get_parent_slug()
    {
        // TODO: Implement get_parent_slug() method.
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }
}