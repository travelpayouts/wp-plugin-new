<?php
/**
 * Created by PhpStorm.
 * User: ubuntu-pc
 * Date: 21.09.17
 * Time: 1:33
 */

namespace tp\admin\controllers\menu;

use tp\admin\includes\models\auto_replace_links as models;
use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;
use tp\includes\TP_form_helper;

class TP_Auto_Replace_Links_Controller extends TP_Base_Submenu_Controller
{
    protected $form_items = 'tp\admin\includes\forms\auto_replace_links\TP_Form_items';


    public function run()
    {

        $model = new models\TP_model_main();
        $form = $this->get_model_form($model);
        $form->handleRequest();

        $model->submit_form($form);

        $replace_links_array = models\TP_model_items::all()->toArray();

        $this->render('/menu/auto-links/index.twig', array(
            'form' => $form->createView(),
            'replaced_links' => $replace_links_array
        ));

    }

    public function action_add()
    {
        $form = $this->form->createBuilder($this->form_items)->getForm();
        $form->handleRequest();

        TP_form_helper::submit_form($form,
            null,
            array($this, 'add_item_callback'),
            _x('Auto link was added', 'tp_search form autolink added', TP_PLUGIN_TEXTDOMAIN),
            array(
                'redirect' => 'travelpayouts_auto_replace_links'
            )
        );

        $this->render('/menu/auto-links/add.twig', array(
            'form' => $form->createView()
        ));
    }


    public function action_edit()
    {
        $id = $this->request()->query->get('id');
        $item_model = models\TP_model_items::find($id);

        if ($item_model) {
            $form = $this->form->createBuilder($this->form_items, $item_model->attributesToArray())->getForm();
            $form->handleRequest();

            TP_form_helper::submit_form($form,
                null,
                array($this, 'edit_item_callback'),
                _x('Auto link was updated', 'tp_search form autolink updated', TP_PLUGIN_TEXTDOMAIN),
                array(
                    'item' => $item_model,
                    'redirect' => 'travelpayouts_auto_replace_links'
                )
            );

            $this->render('/menu/auto-links/add.twig', array(
                'form' => $form->createView()
            ));
        }
    }

    public function add_item_callback($form)
    {
        $item_model = new models\TP_model_items;
        $item_model->fill($form->getData());
        $item_model->save();
        return $item_model->id !== 0;
    }

    public function edit_item_callback($form, $options)
    {
        $options['item']->fill($form->getData());
        return $options['item']->save();
    }



    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Auto-links', 'admin submenu auto replace links page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Auto-links', 'admin submenu auto replace links menu title', TP_PLUGIN_TEXTDOMAIN)
            . '<span class="update-plugins"><span class="plugin-count">beta</span></span>';
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_auto_replace_links';
    }

    public function get_parent_slug()
    {
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }
}