<?php
/**
 * Created by PhpStorm.
 * User: ubuntu-pc
 * Date: 20.09.17
 * Time: 7:04
 */

namespace tp\admin\controllers\menu;

use tp\admin\controllers\menu\base\TP_Base_Rootmenu_Controller;
use tp\admin\includes\forms;
use Symfony\Component\HttpFoundation\Session\Session;


class TP_Index_Controller extends TP_Base_Rootmenu_Controller
{

    public function run()
    {
        $this->render('/menu/index/index.twig', array());
    }


    public function get_page_title()
    {
        // TODO: Implement get_page_title() method.
        return _x('Travelpayouts', 'admin menu index page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_menu_title()
    {
        // TODO: Implement get_menu_title() method.
        return _x('Travelpayouts', 'admin menu index menu titlqe', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN;
    }

    public function get_icon_url()
    {
        // TODO: Implement get_icon_url() method.
        return TP_PLUGIN_ADMIN_IMG_URL . 'tp.svg';
    }

    public function get_position()
    {
        // TODO: Implement get_position() method.
        return null;
    }
}

