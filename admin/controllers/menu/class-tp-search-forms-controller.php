<?php
/**
 * Created by PhpStorm.
 * User: andrey
 */

namespace tp\admin\controllers\menu;

use tp\admin\includes\models\search_form as models;
use tp\admin\controllers\menu\base\TP_Base_Submenu_Controller;
use tp\includes\TP_form_helper;

class TP_Search_Forms_Controller extends TP_Base_Submenu_Controller
{

    protected $form_items = 'tp\admin\includes\forms\search_form\TP_Form_items';

    public function get_menu_title()
    {
        return _x('Search forms', 'admin menu search forms menu title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function get_page_title()
    {
        return _x('Search forms', 'admin menu search forms page title', TP_PLUGIN_TEXTDOMAIN);
    }

    public function run()
    {
        $shortcodes_array = models\TP_model_items::all()->toArray();

        $this->render('/menu/search-forms/index.twig', array(
            'shortcodes_list' => $shortcodes_array
        ));
    }

    public function action_add()
    {

        $form = $this->form->createBuilder($this->form_items)->getForm();
        $form->handleRequest();


        TP_form_helper::submit_form($form,
            array($this, 'add_item_callback'),
            _x('Search shortcode was added', 'tp_search form shortcode added', TP_PLUGIN_TEXTDOMAIN),
            array(
                'redirect' => 'travelpayouts_search_forms'
            )
        );

        $this->render('/menu/search-forms/add.twig', array(
            'form' => $form->createView(),
            'id' => $this->get_latest_row()
        ));
    }

    public function action_edit()
    {
        $id = $this->request()->query->get('id');

        $item_model = models\TP_model_items::find($id);

        if ($item_model) {
            $form = $this->form->createBuilder($this->form_items, $item_model->attributesToArray())->getForm();
            $form->handleRequest();

            TP_form_helper::submit_form($form,
                array($this, 'edit_item_callback'),
                _x('Search shortcode was updated', 'tp_search form shortcode updated', TP_PLUGIN_TEXTDOMAIN),
                array(
                    'item' => $item_model,
                    'redirect' => 'travelpayouts_search_forms'
                )
            );

            $this->render('/menu/search-forms/add.twig', array(
                'form' => $form->createView(),
                'id' => $id
            ));
        }
    }

    public function add_item_callback($form)
    {
        $item_model = new models\TP_model_items;
        $item_model->fill($form->getData());
        $item_model->save();
        return $item_model->id !== 0;
    }

    public function edit_item_callback($form, $options)
    {
        $options['item']->fill($form->getData());
        return $options['item']->save();
    }

    private function get_latest_row()
    {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $mylink = $wpdb->get_row("SELECT max(id) as last_id FROM " . $prefix . "tp_search_shortcodes");
        if (property_exists($mylink, 'last_id')) {
            return intval($mylink->last_id) + 1;
        }
        return 0;
    }

    public function get_parent_slug()
    {
        return TP_Index_Controller::get_instance()->get_menu_slug();
    }

    public function get_capability()
    {
        // TODO: Implement get_capability() method.
        return 'manage_options';
    }

    public function get_menu_slug()
    {
        // TODO: Implement get_menu_slug() method.
        return TP_PLUGIN_TEXTDOMAIN . '_search_forms';
    }
}