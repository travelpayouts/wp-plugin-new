<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.travelpayouts.com/?locale=en
 * @since      1.0.0
 *
 * @package    Travelpayouts
 * @subpackage Travelpayouts/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Travelpayouts
 * @subpackage tp/admin
 * @author     travelpayouts <solomashenko.roman.1991@gmail.com>
 */

namespace tp\admin;

use tp\admin\controllers\menu;
use tp\admin\controllers\media_button;
use tp\admin\includes\TP_Cache;
use tp\admin\includes\TP_Enqueue_Resources;
use tp\includes;


class TP_Admin
{
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */

    protected $controllers_list = array(
        'TP_Index_Controller' => array('show_menu' => true),
        'TP_Tables_Controller' => array('show_menu' => true),
        'TP_Search_Forms_Controller' => array('show_menu' => true),
        'TP_Widgets_Controller' => array('show_menu' => true),
        'TP_Auto_Replace_Links_Controller' => array('show_menu' => true),
        'TP_Settings_Controller' => array('show_menu' => true),
        'TP_Account_Controller' => array('show_menu' => true),
        'TP_What_News_Controller' => array('show_menu' => false),
        'TP_Wizard_Controller' => array('show_menu' => false),
    );
    protected $sidebar_menu_url = array();

    public function __construct()
    {
        $this->load_dependencies();
        $this->run_migration();
    }


    private function load_dependencies()
    {
        $this->load_enqueue_resources();
        $this->load_menu();
        $this->load_media_button();
        $cache = new TP_Cache();
    }

    private function load_menu()
    {
        $menu = $this->get_controllers_links();

        foreach ($this->controllers_list as $controller_name => $controller_params) {
            //Path to controller
            $controller_class = 'tp\\admin\\controllers\\menu\\' . $controller_name;
            // If controller exists
            if (class_exists($controller_class)) {
                $controller_class::get_instance()->init()->set_menu_items($menu);
            }
        }
        //error_log(print_r($this->sidebar_menu_url, true));
    }

    protected function get_controllers_links()
    {
        foreach ($this->controllers_list as $controller_name => $controller_params) {
            //Path to controller
            $controller_class = 'tp\\admin\\controllers\\menu\\' . $controller_name;
            // If controller exists
            if (class_exists($controller_class)) {
                // Add controllers with params show_menu === true
                if ($controller_params['show_menu']) {
                    $this->sidebar_menu_url[] = $controller_class::get_instance()->get_menu_params();
                }
            }
        }
        return $this->sidebar_menu_url;
    }

    protected function set_sidebar_menu_url($page, $url)
    {
        $this->sidebar_menu_url[$page] = $url;
    }

    private function load_media_button()
    {
        media_button\TP_Flight_Tickets_Controller::get_instance()->init();
        media_button\TP_Hotels_Controller::get_instance()->init();
        media_button\TP_Railway_Controller::get_instance()->init();
        media_button\TP_Widgets_Controller::get_instance()->init();
        media_button\TP_Search_Forms_Controller::get_instance()->init();
        media_button\TP_Link_Controller::get_instance()->init();
    }

    private function load_enqueue_resources()
    {
        $enqueue_resources = new TP_Enqueue_Resources();
        $enqueue_resources->init();
    }


    private function run_migration()
    {
        $migrate = new includes\TP_Migration();
        $migrate->run();
    }




}
