<tp-post-button
        buttonId="<?= $id; ?>"
        buttonTitle="<?= $title; ?>"
        buttonUrl="<?= $url; ?>"
        buttonClass="<?= $class; ?>"
        buttonIcon="<?= $icon; ?>"
        buttonModalTitle="<?= $modal_title; ?>"
        modalOpened="<?= $opened; ?>"
>
    <button disabled id="<?= $id; ?>" class="<?= $class; ?>">
        <?php if ($icon): ?>
            <img src="<?= $icon; ?>" alt="<?= $modal_title; ?>">
        <?php endif; ?>
        <?= $title ?>
    </button>
</tp-post-button>