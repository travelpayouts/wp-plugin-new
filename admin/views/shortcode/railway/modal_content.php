<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\RailwayModel
 */
?>

<tp-modal title="{{ modalTitle }}" modal="{{ modal }}">
    <tp-shortcode-form>
        {{#!attributes.off_title}}
        <?= $this->render('../_base/attributes/title', ['model' => $model]); ?>
        {{ /attributes.off_title }}
        <tp-form-row for="origin" label="<?= $model->getAttributeLabel('origin'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'origin',
                    'source' => 'tutu',
                    'filterFn' => '_ac.railway',
                ]
            ); ?>
        </tp-form-row>

        <tp-form-row for="destination" label="<?= $model->getAttributeLabel('destination'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'destination',
                    'source' => 'tutu',
                    'filterFn' => '_ac.railway',
                ]
            ); ?>
        </tp-form-row>
        <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'paginate'
        ]); ?>
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'off_title'
        ]); ?>
        <?= $this->render('../_base/modal_footer'); ?>
    </tp-shortcode-form>
</tp-modal>
