<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\HotelsModel
 */
?>
<tp-modal title="{{modalTitle}}" modal="{{modal}}">
    <?= $this->render('../_base/shortcode_table_selector', ['table_list' => $model->table_list]); ?>
    {{#if showForm()}}
    <tp-shortcode-form>
        {{#if showForm('title')}}
        {{#!attributes.off_title}}
        <?= $this->render('../_base/attributes/title', ['model' => $model]); ?>
        {{/!attributes.off_title}}
        {{/if}}
        {{#if showForm('city')}}
        <tp-form-row for="city"
                     label="<?= $model->getAttributeLabel('city'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'city',
                    'source' => 'hotellook',
                    'filterFn' => '_ac.hotels_city_name',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}
        {{#if showForm('subid')}}
        <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
        {{/if}}
        {{#if showForm('type_selections')}}
        {{#attributes.city}}

        <tp-form-row for="type_selections"
                     label="<?= $model->getAttributeLabel('type_selections'); ?>">
            <tp-select value="{{attributes.type_selections}}"
                       disabled="{{!sectionsListResolved}}"
                       placeholder="<?= _x('Select shortcode type', 'shortcode modal', TP_PLUGIN_TEXTDOMAIN); ?>"
                       options="{{sectionsList}}">
            </tp-select>
        </tp-form-row>
        {{/attributes.city}}
        {{/if}}
        {{#if showForm('check_in') || showForm('check_out')}}
        <tp-form-row
                label="<?= _x('Check dates', 'tp_admin_page_settings_сonstructor_hotels_tables_field_check_dates_label', TP_PLUGIN_TEXTDOMAIN); ?>">
            <tp-date-range
                    from="{{attributes.check_in}}"
                    to="{{attributes.check_out}}">
                <div class="travel-modal__row">
                    {{#if showForm('check_in')}}
                    <div class="travel-modal__col-3">
                        <tp-form-row for="check_in"
                                     label="<?= $model->getAttributeLabel('check_in'); ?>">

                            <tp-date-from>
                                <input type="text" readonly
                                       value="{{ attributes.check_in }}"
                                       class="regular-text">
                            </tp-date-from>
                        </tp-form-row>

                    </div>
                    {{/if}}
                    {{#if showForm('check_out')}}
                    <div class="travel-modal__col-3">
                        <tp-form-row for="check_out"
                                     label="<?= $model->getAttributeLabel('check_out'); ?>">
                            <tp-date-to>
                                <input type="text" readonly
                                       value="{{ attributes.check_out }}"
                                       class="regular-text">
                            </tp-date-to>
                        </tp-form-row>
                    </div>
                    {{/if}}
                </div>
            </tp-date-range>
        </tp-form-row>
        {{/if}}
        {{#if showForm('number_results')}}
        <tp-form-row for="number_results"
                     label="<?= $model->getAttributeLabel('number_results'); ?>">
            <tp-input-number
                    min="1"
                    value="{{attributes.number_results}}">
            </tp-input-number>
        </tp-form-row>
        {{/if}}
        {{#if showForm('paginate')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'paginate'
        ]); ?>
        {{/if}}

        {{#if showForm('off_title')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'off_title'
        ]); ?>
        {{/if}}

        {{#if showForm('link_without_dates')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'link_without_dates'
        ]); ?>
        {{/if}}
        {{#if showForm('help_text')}}
        <tp-form-row>
            <div class="ui info message">
                <?= _x('If we don\'t have prices in our cache for these dates – no table will be shown', 'tp admin page settings сonstructor hotels tables', TP_PLUGIN_TEXTDOMAIN); ?>
            </div>
        </tp-form-row>
        {{/if}}
        <?= $this->render('../_base/modal_footer'); ?>
    </tp-shortcode-form>
    {{/if}}
</tp-modal>
