<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\TP_Model_Shortcode_Widgets
 */
?>
<tp-form-row for="city"
             label="<?= $model->getAttributeLabel('city'); ?>">
    <?= $this->render('../_base/autocomplete',
        [
            'model' => $model,
            'attribute' => 'city',
            'source' => 'hotellook',
            'filterFn' => '_ac.hotels_city_name',
        ]); ?>
</tp-form-row>

{{#if showForm('city_categories')}}
{{#attributes.city}}
<tp-repeated value="{{attributes.city_categories}}" min="3" max="3"
             addButtonTitle="<?= _x('Add', 'tp_admin_page_settings_add_label', TP_PLUGIN_TEXTDOMAIN); ?>"
             removeButtonTitle="<?= _x('Remove', 'tp_admin_page_settings_remove_label', TP_PLUGIN_TEXTDOMAIN); ?>">
    <tp-form-row for="city_categories.{{@key}}"
                 label="<?= _x('Selection type', 'tp_admin_page_settings_сonstructor_hotels_tables_field_hotels_selections_type_label', TP_PLUGIN_TEXTDOMAIN); ?>">
        <input type="hidden" value="{{ item.value }}">
        <tp-select value="{{item.value}}"
                   disabled="{{!hotelsSectionsListResolved}}"
                   placeholder="<?= _x('Selection type', 'tp_admin_page_settings_сonstructor_hotels_tables_field_hotels_selections_type_label', TP_PLUGIN_TEXTDOMAIN); ?>"
                   options="{{hotelsSectionsList}}">
        </tp-select>
    </tp-form-row>
</tp-repeated>
{{/attributes.city}}
{{ /if }}