<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\WidgetsModel
 */

use tp\includes\helpers\viewHelper;

$calendar_periods = viewHelper::get_label_value_from_array($model->calendar_periods);
?>


<tp-modal title="{{modalTitle}}" modal="{{modal}}">
    <?= $this->render('../_base/shortcode_table_selector', ['table_list' => $model->table_list]); ?>

    {{#if showForm()}}
    <tp-shortcode-form>
        {{#if showForm('subid')}}
        <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
        {{/if}}

        {{#if showForm('origin')}}
        <tp-form-row
                for="origin"
                label="<?= $model->getAttributeLabel('origin'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'origin',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('destination')}}
        <tp-form-row
                for="destination"
                label="<?= $model->getAttributeLabel('destination'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'destination',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('hotel_coordinates')}}
        <tp-form-row for="hotel_coordinates"
                     label="<?= $model->getAttributeLabel('hotel_coordinates'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'hotel_coordinates',
                    'source' => 'hotellook',
                    'filterFn' => '_ac.hotels_and_cities',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('hotel_name')}}
        <tp-form-row for="hotel_name"
                     label="<?= $model->getAttributeLabel('hotel_name'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'hotel_name',
                    'source' => 'hotellook',
                    'filterFn' => '_ac.hotels_fullName',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('destination_multiple')}}
        <tp-repeated value="{{attributes.destination_multiple}}"
                     min="1"
                     max="10"
                     onchange="window.tpCurrentModal.checkOverflow();"
                     addButtonTitle="<?= _x('Add', 'tp_admin_page_settings_add_label', TP_PLUGIN_TEXTDOMAIN); ?>"
                     removeButtonTitle="<?= _x('Remove', 'tp_admin_page_settings_remove_label', TP_PLUGIN_TEXTDOMAIN); ?>">
            <tp-form-row for="destination_multiple.{{@key}}"
                         label="<?= $model->getAttributeLabel('destination'); ?> #{{@key+1}}">
                <tp-autocomplete value="{{item}}"
                                 source="aviasales"
                                 filterFn="{{_ac.flights_airport_iata}}"
                                 placeholder="<?= $model->getAttributeLabel('destination'); ?>"
                ></tp-autocomplete>
            </tp-form-row>
        </tp-repeated>
        {{/if}}

        {{#if showForm('size_width')}}
        <tp-form-row
                label="<?= _x('Size', 'tp_admin_page_settings_сonstructor_widgets_field_size_label', TP_PLUGIN_TEXTDOMAIN); ?>">
            <div class="travel-modal__row">
                <div class="travel-modal__col-2">
                    {{#if showForm('size_width')}}
                    <tp-form-row for="size_width"
                                 label="<?= $model->getAttributeLabel('size_width'); ?>"
                                 inline="true">
                        <tp-input-number
                                min="1"
                                max="3000"
                                inline="true"
                                value="{{attributes.size_width}}">
                        </tp-input-number>
                    </tp-form-row>
                    {{/if}}
                </div>
                <div class="travel-modal__col-1" style="text-align: center;">
                    <div style="font-size: 40px;font-weight: bold;color: #b3c0c8;padding: 27px 0 0 0;">×
                    </div>
                </div>
                <div class="travel-modal__col-2">
                    {{#if showForm('size_height')}}
                    <tp-form-row for="size_height"
                                 label="<?= $model->getAttributeLabel('size_height'); ?>"
                                 inline="true">
                        <tp-input-number
                                min="1"
                                max="3000"
                                inline="true"
                                value="{{attributes.size_height}}">
                        </tp-input-number>
                    </tp-form-row>
                    {{/if}}
                </div>
            </div>
        </tp-form-row>
        {{/if}}

        {{#if showForm('zoom')}}
        <tp-form-row label="<?= $model->getAttributeLabel('zoom'); ?>">
            <?= $this->render('../_base/range_slider',
                [
                    'model' => $model,
                    'value' => 'zoom',
                    'min' => 1,
                    'max' => 19,
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('calendar_period')}}
        <tp-form-row
                for="tp_calendar_period"
                label="<?= $model->getAttributeLabel('calendar_period'); ?>">
            <tp-select value="{{attributes.calendar_period}}"
                       options="<?= viewHelper::js_object($calendar_periods) ?>">
            </tp-select>
        </tp-form-row>
        {{/if}}


        {{#if showForm('period_from')}}
        <tp-form-row
                for="tp_period_from"
                label="<?= _x('Range, days', 'tp_admin_page_settings_сonstructor_widgets_field_period_day_label', TP_PLUGIN_TEXTDOMAIN); ?>">
            <?= $this->render('../_base/range_slider',
                [
                    'model' => $model,
                    'value-min' => 'period_from',
                    'value-max' => 'period_to',
                    'min' => 1,
                    'max' => 30,
                ]
            ); ?>

        </tp-form-row>
        {{/if}}

        {{#if showForm('city')}}
        <?= $this->render('_city', ['model' => $model]); ?>
        {{/if}}


        {{#if showForm('type')}}
        <tp-form-row
                for="tp_type"
                label="<?= $model->getAttributeLabel('type'); ?>">
            <tp-radio-group selectedValue="{{attributes.type}}" inline="true">
                <?php foreach ($model->types as $key => $value): ?>
                    <tp-radio value="<?= $key; ?>" label="<?= $value; ?>"></tp-radio>
                <?php endforeach; ?>
            </tp-radio-group>
        </tp-form-row>
        {{/if}}


        {{#if showForm('widget_type')}}
        <tp-form-row
                for="type"
                label="<?= $model->getAttributeLabel('widget_type'); ?>">
            <tp-radio-group selectedValue="{{attributes.widget_type}}" inline="true">
                <?php foreach ($model->widget_types as $key => $value): ?>
                    <tp-radio value="<?= $key; ?>" label="<?= $value; ?>"></tp-radio>
                <?php endforeach; ?>
            </tp-radio-group>
        </tp-form-row>
        {{/if}}

        {{#if showForm('filter_by')}}
        <?= $this->render('_filter_by', ['model' => $model]); ?>
        {{/if}}

        {{#if showForm('limit')}}
        <tp-form-row for="limit" label="<?= $model->getAttributeLabel('limit'); ?>">
            <?= $this->render('../_base/range_slider',
                [
                    'model' => $model,
                    'value' => 'limit',
                    'min' => 1,
                    'max' => 10,
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('limit_ducklett')}}
        <tp-form-row for="limit" label="<?= $model->getAttributeLabel('limit'); ?>">
            <?= $this->render('../_base/range_slider',
                [
                    'model' => $model,
                    'value' => 'limit',
                    'min' => 1,
                    'max' => 21,
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('direct')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'direct'
        ]); ?>
        {{/if}}

        {{#if showForm('one_way')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'one_way'
        ]); ?>
        {{/if}}

        {{#if showForm('responsive')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'responsive'
        ]); ?>

        {{#!attributes.responsive}}
        <tp-form-row for="responsive_width" subitem="true"
                     label="<?= $model->getAttributeLabel('responsive_width'); ?>">
            <tp-input-number
                    min="1"
                    max="3000"
                    value="{{attributes.responsive_width}}">
            </tp-input-number>
        </tp-form-row>
        {{/!attributes.responsive}}
        {{/if}}

        <?= $this->render('../_base/modal_footer'); ?>
    </tp-shortcode-form>
    {{/if}}
</tp-modal>
