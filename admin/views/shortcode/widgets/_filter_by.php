<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\TP_Model_Shortcode_Widgets
 */
?>

{{#if showForm('filter_by')}}
<tp-form-row
        label="<?= $model->getAttributeLabel('filter_by'); ?>:">
    <tp-radio-group selectedValue="{{attributes.filter_by}}" inline="true">
        <?php foreach ($model->filter_by_types as $key => $value): ?>
            <tp-radio value="<?= $key; ?>" label="<?= $value; ?>"></tp-radio>
        <?php endforeach; ?>
    </tp-radio-group>
</tp-form-row>


{{#if attributes.filter_by == '0'}}
<tp-form-row subitem="true">
    <tp-repeated subitem="false" value="{{attributes.airlines}}"
                 min="1"
                 max="10"
                 onchange="window.tpCurrentModal.checkOverflow();"
                 addButtonTitle="<?= _x('Add', 'tp_admin_page_settings_add_label', TP_PLUGIN_TEXTDOMAIN); ?>"
                 removeButtonTitle="<?= _x('Remove', 'tp_admin_page_settings_remove_label', TP_PLUGIN_TEXTDOMAIN); ?>">
        <tp-form-row
                for="airlines.{{@key}}"
                label="<?= _x('Airline', 'tp_admin_page_settings_сonstructor_widgets_field_airline_widget_8_label', TP_PLUGIN_TEXTDOMAIN); ?> #{{i+1}}">
            <tp-autocomplete value="{{item}}"
                             source="airlines"
                             filterFn="{{_ac.airlines}}"
                             placeholder="<?= _x('Airline', 'tp_admin_page_settings_сonstructor_widgets_field_airline_widget_8_label', TP_PLUGIN_TEXTDOMAIN); ?>"
            ></tp-autocomplete>
        </tp-form-row>
    </tp-repeated>
</tp-form-row>

{{/if}}

{{#if attributes.filter_by == '1'}}
<tp-form-row subitem="true">
    <tp-form-row
            for="origin"
            label="<?= $model->getAttributeLabel('origin'); ?>">
        <?= $this->render('../_base/autocomplete',
            [
                'model' => $model,
                'attribute' => 'origin',
                'source' => 'aviasales',
                'filterFn' => '_ac.flights_airport_iata',
            ]
        ); ?>
    </tp-form-row>
    <tp-form-row
            for="destination"
            label="<?= $model->getAttributeLabel('destination'); ?>">
        <?= $this->render('../_base/autocomplete',
            [
                'model' => $model,
                'attribute' => 'destination',
                'source' => 'aviasales',
                'filterFn' => '_ac.flights_airport_iata',
            ]
        ); ?>
    </tp-form-row>
</tp-form-row>
{{/if}}

{{/if}}