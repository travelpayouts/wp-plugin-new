<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 *
 * @var $model \tp\admin\includes\models\shortcodes\LinkModel
 */

use tp\includes\helpers\viewHelper;

?>

<tp-modal title="{{modalTitle}}" modal="{{modal}}">
    <tp-shortcode-form>
        <tp-form-row for="text_link" label="<?= $model->getAttributeLabel('text_link') ?>">
            <tp-input type="text" value="{{attributes.text_link}}"
                      placeholder="<?= $model->getAttributeLabel('text_link') ?>">
            </tp-input>
        </tp-form-row>
        <!--@TODO remove it-->
        <?php if (true === false): ?>
            <tp-form-row for="type"
                         label="<?= $model->getAttributeLabel('type'); ?>">
                <tp-select value="{{attributes.type}}"
                           placeholder="<?= _x('Select the link', 'tp_admin_page_settings_сonstructor_link_field_type_link_label', TP_PLUGIN_TEXTDOMAIN); ?>"
                           options="<?= viewHelper::js_object(viewHelper::get_label_value_from_array($model->types)) ?>">
                </tp-select>
            </tp-form-row>
        <?php endif; ?>
        <!--/@TODO remove it-->


        <tp-form-row for="type"
                     label="<?= $model->getAttributeLabel('type'); ?>">
            <tp-radio-group selectedValue="{{attributes.type}}"
                            inline="true"
                            onchange="window.tpCurrentModal.checkOverflow();"
            >
                <?php foreach ($model->types as $key => $value): ?>
                    <tp-radio value="<?= $key; ?>" label="<?= $value; ?>"></tp-radio>
                <?php endforeach; ?>
            </tp-radio-group>
        </tp-form-row>

        {{#if attributes.type == '1'}}
        <hr class="travel-modal__divider">
        <tp-form-row
                for="origin"
                label="<?= $model->getAttributeLabel('origin') ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'origin',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>
        <tp-form-row
                for="destination"
                label="<?= $model->getAttributeLabel('destination') ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'destination',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>

        <tp-form-row for="departure_date" label="<?= $model->getAttributeLabel('departure_date') ?>">
            <tp-input type="text" value="{{attributes.departure_date}}"
                      prepend="today +"
                      placeholder="<?= $model->getAttributeLabel('departure_date') ?>">
            </tp-input>
        </tp-form-row>
        <tp-form-row for="return_date" label="<?= $model->getAttributeLabel('return_date') ?>">
            <tp-input type="text" value="{{attributes.return_date}}"
                      prepend="today +"
                      placeholder="<?= $model->getAttributeLabel('return_date') ?>">
            </tp-input>
        </tp-form-row>

        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'one_way'
        ]); ?>

        <hr class="travel-modal__divider">
        {{/if}}


        {{#if attributes.type == '2'}}
        <hr class="travel-modal__divider">
        <tp-form-row for="city"
                     label="<?= $model->getAttributeLabel('city'); ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'city',
                    'source' => 'hotellook',
                    'filterFn' => '_ac.hotels_city_name',
                ]
            ); ?>
        </tp-form-row>
        <tp-form-row for="check_in" label="<?= $model->getAttributeLabel('check_in') ?>">
            <tp-input type="text" value="{{attributes.check_in}}"
                      prepend="today +"
                      placeholder="<?= $model->getAttributeLabel('check_in') ?>">
            </tp-input>
        </tp-form-row>
        <tp-form-row for="check_in" label="<?= $model->getAttributeLabel('check_out') ?>">
            <tp-input type="text" value="{{attributes.check_out}}"
                      prepend="today +"
                      placeholder="<?= $model->getAttributeLabel('check_out') ?>">
            </tp-input>
        </tp-form-row>
        <hr class="travel-modal__divider">
        {{/if}}

        {{#if attributes.type}}
        <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
        {{/if}}

        <?= $this->render('../_base/modal_footer'); ?>
    </tp-shortcode-form>
</tp-modal>