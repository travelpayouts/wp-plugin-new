<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model \tp\admin\includes\models\shortcodes\FlightsModel
 */

use tp\includes\helpers\viewHelper;

?>
<tp-modal title="{{modalTitle}}" modal="{{modal}}">
    <?= $this->render('../_base/shortcode_table_selector', ['table_list' => $model->table_list]); ?>
    {{#if showForm()}}
    <tp-shortcode-form>
        {{#if showForm('title')}}

        {{#!attributes.off_title}}
        <?= $this->render('../_base/attributes/title', ['model' => $model]); ?>
        {{/!attributes.off_title}}

        {{/if}}

        {{#if showForm('origin')}}
        <tp-form-row
                for="origin"
                label="<?= $model->getAttributeLabel('origin') ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'origin',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('destination')}}
        <tp-form-row
                for="destination"
                label="<?= $model->getAttributeLabel('destination') ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'destination',
                    'source' => 'aviasales',
                    'filterFn' => '_ac.flights_airport_iata',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('country')}}
        <tp-form-row for="country" label="<?= $model->getAttributeLabel('destination') ?>">
            <tp-input type="text" value="{{attributes.country}}"
                      placeholder="<?= $model->getAttributeLabel('destination') ?>"
            >
            </tp-input>
        </tp-form-row>
        {{/if}}

        {{#if showForm('subid')}}
        <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
        {{/if}}

        {{#if showForm('airline')}}
        <tp-form-row for="airline" label="<?= $model->getAttributeLabel('airline') ?>">
            <?= $this->render('../_base/autocomplete',
                [
                    'model' => $model,
                    'attribute' => 'airline',
                    'source' => 'airlines',
                    'filterFn' => '_ac.airlines',
                ]
            ); ?>
        </tp-form-row>
        {{/if}}

        {{#if showForm('flight_number')}}

        {{#if airline}}
        <tp-form-row for="flight_number" label="<?= $model->getAttributeLabel('flight_number') ?>">
            <tp-input type="text" value="{{attributes.flight_number}}"
                      placeholder="<?= $model->getAttributeLabel('flight_number') ?>"
                      title="<?= _x('Use this filter only if you absolutely accurately know the route number', 'tp_admin_page_settings_сonstructor_tables_field_filter_flight_number_help') ?>">
            </tp-input>
        </tp-form-row>
        {{/if}}
        {{/if}}

        {{#if showForm('currency')}}
        <tp-form-row
                label="<?= $model->getAttributeLabel('currency') ?>">
            <tp-select value="{{attributes.currency}}"
                       searchable="true"
                       options="<?= viewHelper::js_object(viewHelper::get_label_value_from_array($model->currency_list)) ?>"
                       placeholder="<?= $model->getAttributeLabel('currency') ?>"
            >
            </tp-select>
        </tp-form-row>
        {{/if}}

        {{#if showForm('limit')}}
        <tp-form-row
                for="limit"
                label="<?= $model->getAttributeLabel('limit') ?>">
            <tp-input-number
                    min="1"
                    max="300"
                    value="{{attributes.limit}}"/>
        </tp-form-row>
        {{/if}}


        {{#if showForm('stops')}}
        <tp-form-row label="<?= $model->getAttributeLabel('stops') ?>"
                     for="stops">
            <tp-select value="{{attributes.stops}}"
                       placeholder="<?= $model->getAttributeLabel('stops') ?>"
                       options="<?= viewHelper::js_object(viewHelper::get_label_value_from_array($model->stops_number)) ?>">
            </tp-select>
        </tp-form-row>
        {{/if}}

        {{#if showForm('paginate')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'paginate'
        ]); ?>
        {{/if}}


        {{#if showForm('one_way')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'one_way'
        ]); ?>
        {{/if}}


        {{#if showForm('off_title')}}
        <?= $this->render('../_base/checkbox', [
            'model' => $model,
            'attribute' => 'off_title'
        ]); ?>
        {{/if}}
        <?= $this->render('../_base/modal_footer'); ?>
    </tp-shortcode-form>
    {{/if}}
</tp-modal>
