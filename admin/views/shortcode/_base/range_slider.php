<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model yii\base\Model
 * @var $attribute string
 * @var $_params_ array
 */

use Adbar\Dot;
use tp\includes\helpers\viewHelper;

$params = new Dot($_params_);

$range_slider_attributes = [
    'multiple' => ($params->has('value-min') && $params->has('value-max')) ? 'true' : 'false',
    'min' => $params->get('min', 0),
    'max' => $params->get('max', 10),
];
$attribute_names =
    [
        'value-min',
        'value-max',
        'value',
    ];

foreach ($attribute_names as $attribute_name) {
    if ($params->has($attribute_name)) {
        $value = $params->get($attribute_name);
        $range_slider_attributes[$attribute_name] = viewHelper::wrap_in_brackets("attributes.$value");
    }
}
?>
<tp-range-slider
    <?= viewHelper::array_to_element_attributes($range_slider_attributes); ?>
></tp-range-slider>