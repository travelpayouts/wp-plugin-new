<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model yii\base\Model
 * @var $attribute string
 */

use tp\includes\helpers\viewHelper;

?>
<tp-form-row>
    <tp-checkbox value="<?= viewHelper::wrap_in_brackets("attributes.$attribute"); ?>"
                 label="<?= $model->getAttributeLabel($attribute) ?>">
    </tp-checkbox>
</tp-form-row>