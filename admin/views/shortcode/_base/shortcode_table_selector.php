<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $table_list array
 */

use tp\includes\helpers\viewHelper;

ob_clean();

$table_list_labels = viewHelper::get_label_value_from_array($table_list);
$table_list_numbered = array_map(function ($table, $key) {
    $key++;
    $table['label_original'] = $table['label'];
    $table['label'] = "{$key}. {$table['label']}";
    return $table;
}, $table_list_labels, array_keys($table_list_labels));

?>

<tp-shortcode-form>
    <tp-form-row>
        <tp-select value="{{attributes.current_table}}"
                   placeholder="<?= _x('Select shortcode type', 'shortcode modal', TP_PLUGIN_TEXTDOMAIN); ?>"
                   options="<?= viewHelper::js_object($table_list_numbered) ?>">
            {{ #partial label}}
            <strong>{{num+1}}.</strong> {{label_original}}
            {{ /partial }}
        </tp-select>
    </tp-form-row>
</tp-shortcode-form>
