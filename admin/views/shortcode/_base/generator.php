<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use tp\includes\helpers\viewHelper;
?>
[<?= $shortcode; ?> <?= viewHelper::array_to_element_attributes($attributes->all()); ?>]