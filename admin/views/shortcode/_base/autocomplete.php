<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model yii\base\Model
 * @var $attribute string
 * @var $source string
 * @var $filterFn string
 */
use tp\includes\helpers\viewHelper;
?>
<tp-autocomplete value="<?= viewHelper::wrap_in_brackets("attributes.$attribute"); ?>"
                 source="<?= $source; ?>"
                 onchange="window.tpCurrentModal.checkOverflow();"
                 filterFn="<?= viewHelper::wrap_in_brackets($filterFn); ?>"
                 placeholder="<?= $model->getAttributeLabel($attribute); ?>"
></tp-autocomplete>