<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model yii\base\Model
 */
?>

<tp-form-row
        for="title"
        label="<?= $model->getAttributeLabel('title') ?>">
    <tp-input type="text" value="{{attributes.title}}"
              placeholder="<?= $model->getAttributeLabel('title') ?>">
    </tp-input>
</tp-form-row>
