<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 * @var $model  yii\base\Model
 */
?>
<tp-form-row for="subid"
             label="<?= $model->getAttributeLabel('subid'); ?>">
    <tp-input type="text" value="{{attributes.subid}}"
              placeholder="<?= $model->getAttributeLabel('subid') ?>">
    </tp-input>
</tp-form-row>
