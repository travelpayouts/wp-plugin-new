<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
?>
<tp-form-row>
    <div class="travel-modal__action">
        <button type="submit" class="travel-modal__btn btn btn--color"
                role="button">
            <?= _x('Create', 'general_create_button', TP_PLUGIN_TEXTDOMAIN); ?>
        </button>
        <button type="button"
                on-click="@this.closeModal()"
                class="travel-modal__btn btn btn--bg"
                role="button">
            <?= _x('Cancel', 'general_cancel_button', TP_PLUGIN_TEXTDOMAIN); ?>
        </button>
    </div>
</tp-form-row>