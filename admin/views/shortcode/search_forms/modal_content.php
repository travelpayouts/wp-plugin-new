<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 *
 * @var $model \tp\admin\includes\models\shortcodes\FormModel
 * @var $search_forms array
 */

use tp\includes\helpers\viewHelper;

?>
<tp-modal title="{{modalTitle}}" modal="{{modal}}">
    <tp-shortcode-form>

        <?php if (!empty($search_forms)): ?>
            <tp-form-row
                    for="search_form"
                    label="<?= $model->getAttributeLabel('search_form') ?>">
                <tp-select value="{{attributes.search_form}}"
                           labelprop="title"
                           valueprop="id"
                           options="<?= viewHelper::js_object($search_forms) ?>"
                           placeholder="<?= $model->getAttributeLabel('search_form') ?>"
                >
                </tp-select>
            </tp-form-row>


            <tp-form-row
                    for="origin"
                    label="<?= $model->getAttributeLabel('origin') ?>">
                <?= $this->render('../_base/autocomplete',
                    [
                        'model' => $model,
                        'attribute' => 'origin',
                        'source' => 'aviasales',
                        'filterFn' => '_ac.flights_airport_iata',
                    ]
                ); ?>
            </tp-form-row>

            <tp-form-row
                    for="destination"
                    label="<?= $model->getAttributeLabel('destination') ?>">
                <?= $this->render('../_base/autocomplete',
                    [
                        'model' => $model,
                        'attribute' => 'destination',
                        'source' => 'aviasales',
                        'filterFn' => '_ac.flights_airport_iata',
                    ]
                ); ?>
            </tp-form-row>
            <?= $this->render('../_base/attributes/subId', ['model' => $model]); ?>
            <?= $this->render('../_base/modal_footer'); ?>

        <?php else: ?>
            <div class="ui warning message">
                <?= __('You didn\'t create any search form to using this shortcode', TP_PLUGIN_TEXTDOMAIN); ?>
                <br>
                <a href="./admin.php?page=travelpayouts_search_forms" style="font-weight: bold;">
                    <?= __('Create a new one', TP_PLUGIN_TEXTDOMAIN); ?>
                </a>
            </div>
        <?php endif; ?>

    </tp-shortcode-form>
</tp-modal>