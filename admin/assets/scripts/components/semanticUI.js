/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import semanticDropdown from 'semantic-ui-dropdown';

require('semantic-ui-sass/js/api');
require('semantic-ui-sass/js/checkbox');
require('semantic-ui-sass/js/search');
require('semantic-ui-sass/js/transition');
// require('semantic-ui-sass/js/colorize');
// require('semantic-ui-sass/js/form');
// require('semantic-ui-sass/js/state');
// require('semantic-ui-sass/js/visibility');
// require('semantic-ui-sass/js/visit');
// require('semantic-ui-sass/js/site');
// require('semantic-ui-sass/js/accordion');
// require('semantic-ui-sass/js/dimmer');
// require('semantic-ui-sass/js/embed');
// require('semantic-ui-sass/js/modal');
// require('semantic-ui-sass/js/nag');
// require('semantic-ui-sass/js/popup');
// require('semantic-ui-sass/js/progress');
// require('semantic-ui-sass/js/rating');
// require('semantic-ui-sass/js/shape');
// require('semantic-ui-sass/js/sidebar');
// require('semantic-ui-sass/js/sticky');
// require('semantic-ui-sass/js/tab');
$.fn.dropdown = semanticDropdown;
