/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import tpForm from './../adminPages/form';

const enable_form_handler = true;
export default class formHandler {
    static ajaxSubmitForm() {

        if (enable_form_handler) {
            $('body').on('submit', 'form.tp-form', function (e) {
                let method = $(this).attr('method');
                let action = $(this).attr('action');

                //Clear old errors
                $('.form-row', this).removeClass('error-field is-error');
                $('.error-info').remove();

                formHandler.sendData(this);

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        }

    }

    static sendData(form, data = false, displayMessage = true) {

        let method = $(form).attr('method');
        let action = $(form).attr('action');

        if (data === false) {
            data = $(form).serialize();
        }

        $.ajax({
            dataType: "json",
            type: method,
            url: action,
            data: data,
        }).done((result) => {
            if (result) {
                if (result.status === 'ok') {
                    result.noticeClass = 'updated';
                } else if (result.status === 'error') {
                    result.noticeClass = 'error';
                    if (result.field_messages) {
                        let fieldMessages = result.field_messages;
                        for (let fieldErrors in fieldMessages) {
                            if (fieldMessages.hasOwnProperty(fieldErrors)) {
                                let field = $(`#${fieldErrors}`);
                                let fieldMessage = fieldMessages[fieldErrors].join(' ');

                                field.closest('.form-row').addClass('error-field is-error');
                                // If error element is input
                                if (field.prop('nodeName') === 'INPUT') {
                                    field.after(`<span class="error-info">${fieldMessage}</span>`);
                                }
                            }
                        }
                    }
                }

                // show error message
                if (displayMessage) {
                    $.growl({
                        style: result.status === 'ok' ? 'notice' : 'error',
                        title: "",
                        message: result.message,
                        location: 'tr',
                        delayOnHover: false,
                        size: 'large',
                        duration: 1200
                    });
                }


                if (result.redirect && result.status === 'ok') {
                    const params = {
                        'page': result.redirect
                    };
                    let redirectUrl = window.location.origin + window.location.pathname + '?' + $.param(params);
                    setTimeout(() => window.location = redirectUrl, 1000);
                }
            }
        });
    }

    static resetForm() {
        if (enable_form_handler) {

            $('body').on('click', 'form.tp-form .tp-form-reset', function (e) {
                let form = $(this).closest('form.tp-form');
                let formName = form.attr('name');
                let formData = {};

                formData[formName] = {
                    reset: true
                };

                let postUrl = window.location.href;
                $.ajax({
                    dataType: "json",
                    type: form.attr('method'),
                    url: `${postUrl}&form_action=reset`,
                    data: $.param(formData),
                }).done((resetData) => {
                    // if (resetData.status && resetData.status === 'ok') {
                    form.addClass('tp-form__loading');
                    $.ajax({
                        type: 'get'
                    }).done((result) => {
                        let newForm = $(result).find(`form[name="${formName}"]`);
                        // console.log(newForm.html());
                        form.html(newForm.html()).promise().done(() => {
                            tpForm.init(form);
                            tpForm.initDropdowns(form);
                            $.growl({
                                style: 'notice',
                                title: "",
                                message: 'Form data successfully reset',
                                location: 'tr',
                                delayOnHover: false,
                                size: 'large',
                                duration: 1200
                            });
                        });

                        form.removeClass('tp-form__loading');

                    });
                    // }
                });


                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        }

    }
}