/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
export default class citySearch {
    static getApiUrl(dropdownApiSetting = true) {
        if (dropdownApiSetting) {
            return '//www.jetradar.com/autocomplete/places?q={query}';
        } else {
            return '//www.jetradar.com/autocomplete/places';
        }
    }

    static getDropdownOptions() {
        return {
            apiSettings: {
                url: this.getApiUrl(),
                onResponse: (response) => {
                    let result = {
                        'results': typeof response === 'object' ? response : {}
                    };
                    return result;
                }
            },
            fields: {
                remoteValues: 'results', // grouping for api results
                name: 'title',   // displayed dropdown text
                value: 'code'   // actual dropdown value
            },
            onChange: (value, text, $choice) => {
                let inputRow = $choice.closest('.form-row__citySearch');
                $('.form-row__citySearch-name', inputRow).text(value);
            },
            saveRemoteData: false,
            minCharacters: 3,
        };
    }

    static getLabelForValue(value, callback) {
        return $.ajax({
            dataType: "json",
            type: "GET",
            url: citySearch.getApiUrl(false),
            data: {
                q: value
            }
        }).done((result) => {
            let title = '';
            if (result.length > 0) {
                title = result[0].title;
            }

            callback(title)
        });
    }

    static init(container) {
        let citySearchSelector = $('.form-row__citySearch',container);
        let self = this;
        if (citySearchSelector.length) {
            citySearchSelector.dropdown(citySearch.getDropdownOptions());

            // Set labels by IATA code
            citySearchSelector.each(function () {
                let inputValue = $('input[type="hidden"]', this).val();
                if (inputValue) {
                    citySearch.getLabelForValue(inputValue, (title) => {
                        $('.form-row__citySearch-placeholder', this).text(title).removeClass('default');
                    })
                }
            });
        }
    }
}