/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
export default class Slider {

    static init(container) {
        let sliderContainer = $('.form-row__tp_slider', container);

        if (sliderContainer.length) {

            $(sliderContainer).each(function () {
                let input = $('input[type="hidden"]', this);

                if (!input.val()) {
                }
                let rangeOptions = {
                    from: parseInt(input.attr('from')),
                    to: parseInt(input.attr('to')),
                    step: parseInt(input.attr('step')),
                    width: parseInt(input.attr('width')),
                    showScale: input.attr('showScale') === 'true',
                    format: input.attr('format'),
                    showLabels: input.attr('showLabels') === 'true',
                    isRange: input.attr('isRange') === 'true'
                };

                // Fill values if empty
                if (!input.val()) {
                    if (rangeOptions.isRange) {
                        input.val(`${rangeOptions.from},${rangeOptions.to}`);
                    } else {
                        input.val(`${rangeOptions.from}`);
                    }
                }
                input.jRange(rangeOptions);
            });


        }
    }

}