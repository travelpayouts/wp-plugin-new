/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import formHandler from './formHandler';

export default class uiSortableConnected {

    static init(container) {
        this.sortableSelector = $('.form-row__collection .sort_table',container);
        if (this.sortableSelector.length) {
            $('.sort_table__list', this.sortableSelector).each(function () {
                let lists = $(this).closest('.sort_table').find('.sort_table__list');

                $(this).sortable({
                    connectWith: lists,
                    placeholder: "ui-state-highlight",
                    update: function () {
                            uiSortableConnected.updateSortable(this);
                    }
                }).disableSelection();
            });
        }
    }

    static updateSortable(el) {
        let form = $(el).closest('form');

        let sortables = $(el).closest('.sort_table').find('.sort_table__list');
        let serializedData = [
            $(form).serialize()
        ];
        sortables.each(function () {
            let rowName = `${$(this).attr('data-row-name')}[${this.id}][]`;
            let arrayData = $(this).sortable('toArray');

            if (arrayData.length) {
                serializedData.push($(this).sortable('serialize', {key: rowName}));
            }
        });
        let formData = serializedData.join('&');
        formHandler.sendData(form, formData, false);
    }
}