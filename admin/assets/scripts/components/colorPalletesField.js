/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
const BG_WIDGET_CLASSNAME = '.tp_color_pallete_bg_widget';
const COLOR_WIDGET_CLASSNAME = '.tp_color_pallete_widget';

export default class colorPalletesField {

    static init(container) {
        if ($(BG_WIDGET_CLASSNAME, container)) {
            colorPalletesField.backgroundWidgetInit();
        }

        if ($(COLOR_WIDGET_CLASSNAME, container)) {
            colorPalletesField.colorWidgetInit();
        }
    }

    static backgroundWidgetInit() {
        const self = this;
        let widgetContainer = $(BG_WIDGET_CLASSNAME);
        self.findActiveItemBackgroundWidget(BG_WIDGET_CLASSNAME);

        $('.color-pallete__item', widgetContainer).on('click', function () {
            let el = $(this);
            let palleteWrapper = el.closest('.bg-pallete');
            let inputField = $('#' + palleteWrapper.attr('data-field'));
            let currentColor = $(this).attr('data-value');

            if (palleteWrapper) {
                $('.color-pallete__item', palleteWrapper).removeClass('active');
                el.addClass('active');
                let customValueContainer = el.closest(BG_WIDGET_CLASSNAME).find('.tp_color_pallete_widget__custom');
                if (currentColor) {
                    inputField.val(currentColor);
                    customValueContainer.slideUp(() => {
                        self.setColorPicker(inputField, currentColor);
                    });
                } else {
                    customValueContainer.slideDown();
                }
            }

        });

    }

    static colorWidgetInit() {
        const self = this;
        let widgetContainer = $(COLOR_WIDGET_CLASSNAME);
        self.findActiveItemColorWidget(COLOR_WIDGET_CLASSNAME);
        $('.color-pallete__item', widgetContainer).on('click', function () {
            let el = $(this);
            let palleteWrapper = el.closest('.color-pallete');
            let inputField = '#' + palleteWrapper.attr('data-field');
            let currentColor = {
                pin: $(this).attr('data-pin'),
                text: $(this).attr('data-text')
            };

            if (palleteWrapper) {
                let customValueContainer = el.closest(COLOR_WIDGET_CLASSNAME).find('.tp_color_pallete_widget__custom');

                $('.color-pallete__item', palleteWrapper).removeClass('active');
                el.addClass('active');

                if (currentColor.pin && currentColor.text) {
                    let pinColorField = $(`${inputField}_pin_color`);
                    let textColorField = $(`${inputField}_text_color`);

                    // apply changes to form fields
                    customValueContainer.slideUp(() => {
                        pinColorField.val(currentColor.pin);
                        textColorField.val(currentColor.text);
                        self.setColorPicker(pinColorField, currentColor.pin);
                        self.setColorPicker(textColorField, currentColor.text);
                    });

                } else {
                    customValueContainer.slideDown();
                }
            }

        });


    }


    /**
     * Find and mark selected color as active item
     */
    static findActiveItemBackgroundWidget(className) {
        $(className).each(function () {
            let self = $(this);
            let palleteWrapper = $('.bg-pallete', this);
            let inputField = $('#' + palleteWrapper.attr('data-field'));
            let inputValue = inputField.val().toUpperCase();
            let colorValues = $(".color-pallete__item", this).map(function () {
                return $(this).attr('data-value');
            });
            colorValues = colorValues.get();

            if (colorValues.indexOf(inputValue) !== -1) {
                $(`.color-pallete__item[data-value="${inputValue}"]`, self).addClass('active');
            } else {
                $('.tp_color_pallete_widget__custom', self).show();
                $('.color-pallete__item_custom', self).addClass('active');
            }
        })
    }

    static findActiveItemColorWidget(className) {

        $(className).each(function () {
            let self = $(this);
            let palleteWrapper = $('.color-pallete', this);
            const inputFieldClassName = '#' + palleteWrapper.attr('data-field');

            let inputFieldData = [
                $(`${inputFieldClassName}_pin_color`).val().toUpperCase(),
                $(`${inputFieldClassName}_text_color`).val().toUpperCase()
            ];
            let inputValue = inputFieldData.join('');

            // let inputValue = inputField.val().toUpperCase();

            let colorValues = $(".color-pallete__item", this).map(function () {
                return $(this).attr('data-pin') + $(this).attr('data-text');
            });

            colorValues = colorValues.get();

            if (colorValues.indexOf(inputValue) !== -1) {
                $(`.color-pallete__item[data-pin="${inputFieldData[0]}"]`, self).addClass('active');
            } else {
                $('.tp_color_pallete_widget__custom', self).show();
                $('.color-pallete__item_custom', self).addClass('active');
            }
        });
    }

    static setColorPicker(input, color) {
        let inputParent = input.parent();
        // If colorpicker class found
        if (inputParent.hasClass('color-pallete-choose__inner')) {
            input.spectrum("set", color);
            $('.color-pallete-choose__hex', inputParent).text(color);
        }
    }
}