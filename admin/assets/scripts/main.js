__webpack_public_path__ = window.TP_WEBPACK_PUBLIC_PATH;

$(document).ready(() => {
    // Load admin styles dynamically
    if ($('#travelpayouts_admin').length) {
        require.ensure(['./adminPages/index'], (require) => {
            require('./adminPages/index');
        });
    }
    // If found shortcode buttons container, load tpShortcode instance as separate chunk
    if ($('#wp-content-media-buttons').length) {
        require.ensure(['./shortcodes/tp_shortcodes.js'], (require) => {
            const tpShortcode = require('./shortcodes/tp_shortcodes');
            tpShortcode.default.init();
        });
    }
});
