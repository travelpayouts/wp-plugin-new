/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        isolated: false,
        template,
        data: ()=>({
            label: false,
            for: false,
            fieldError: false,
        }),
        closeModal: function () {
            const modal = this.get('modal');
            if (modal) {
                modal.close();
            }
            return false;
        },
        onrender: function () {
            const vm = this;
            const forAttr = vm.get('for');
            vm.set('forAttr', forAttr);
            if (forAttr) {
                vm.observe(`errors.${forAttr}`, (newValue) => {
                    vm.set('fieldMessage', newValue ? newValue : '');
                    vm.set('fieldError', !!newValue);
                });
            }
        },
        elementFocus: function () {
            const vm = this;
            const el = $(vm.find('div'));
            const focusSelector = [
                'input[type="text"]',
                'input[type="password"]',
                'input[type="number"]',
                'textarea',
            ];

            const clickSelector = [
                '.ui.dropdown.search>input.search',
                '.ui.dropdown',
            ];

            const findElement = (selectors, callback = false) => {
                const selectorsString = selectors.join(',');
                const foundElement = $(selectorsString, el);
                if (foundElement && typeof callback === 'function') return callback(foundElement);

            };
            findElement(focusSelector, (el) => el.focus());
            findElement(clickSelector, (el) => el.click());
        }
    });
}
