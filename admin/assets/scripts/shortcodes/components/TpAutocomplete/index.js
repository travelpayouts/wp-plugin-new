/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import {map as _map, findWhere as _findWhere, isFunction as _isFunction} from 'underscore';
import Ractive from 'ractive';
import template from './template.html';
import semanticDropdown from 'semantic-ui-dropdown';

$.fn.semanticDropdown = semanticDropdown;

export default function () {
    return Ractive.extend({
        data: () => ({
            value: false,
            position: 'auto',
            locale: 'en',
            filterfn: false,
            onchange: false,
            source: 'aviasales',
        }),
        isolated: true,
        template,
        oncomplete: function () {
            const vm = this;
            // Available positions:  upward || downward || auto
            const position = this.get('position') || 'auto';
            const componentSelector = $(this.find('.dropdown'));
            const locale = this.get('locale') || 'en';
            const source = this.get('source') || 'aviasales';
            const filterFn = this.get('filterfn');
            const previousLabel = this.get('value._label');


            let apiResponse = [];
            let currentVal = false;
            let currentText = '';
            let dropDownOptions = {
                apiSettings: {
                    cache: false,
                    // this url parses query server side and returns filtered results
                    url: this.getAutocompleteSource(source, locale),
                    onResponse: (response) => {
                        let result = {
                            results: {}
                        };

                        if (filterFn && _isFunction(filterFn)) {
                            const filterResult = filterFn(response, source);
                            if (filterResult) {
                                apiResponse = filterResult;
                                result.results = filterResult;
                            }
                        }

                        return result;
                    },
                    onSuccess: (response, el) => {
                        el.semanticDropdown('internal', 'remove').message();
                        el.semanticDropdown('internal', 'setup').menu({
                            values: response.results
                        });
                        if (response.results && response.results.length) {
                            el.semanticDropdown('show');
                        }
                    },
                },
                filterRemoteData: false,
                onChange: (value, text) => {
                    currentText = text;
                    currentVal = value;
                    vm.runCallback();
                },
                onHide: () => {
                    let searchQuery = {
                        value: currentVal
                    };
                    const selectedItem = _findWhere(apiResponse, searchQuery);
                    if (selectedItem && selectedItem.data) {
                        selectedItem.data._label = currentText;
                        this.set('value', selectedItem.data);
                    }
                },
                onShow: () => {
                    setTimeout(() => vm.runCallback(), 300);
                },
                saveRemoteData: false,
                minCharacters: 3,
                direction: position
            };
            componentSelector.semanticDropdown(dropDownOptions);

            if (previousLabel) {
                componentSelector.semanticDropdown('set text', previousLabel);
            }
        },
        getAutocompleteSource(source, locale) {
            let apiUrl = '';
            switch (source) {
                case 'aviasales':
                    apiUrl = `https://places.aviasales.ru/?term={query}&locale=${locale}`;
                    break;
                case 'jetradar':
                    apiUrl = `https://www.jetradar.com/autocomplete/places?q={query}`;
                    break;
                case 'hotellook':
                    apiUrl = `https://yasen.hotellook.com/autocomplete?term={query}&lang=${locale}`;
                    break;
                case 'tutu':
                    apiUrl = `https://www.tutu.ru/suggest/railway_simple/?name={query}`;
                    break;
                case 'airlines':
                    apiUrl = `${window.ajaxurl}?action=travelpayouts&page=/autocomplete/airlines&query={query}&lang=${locale}`;
                    break;
            }

            return apiUrl;
        },
        runCallback: function () {
            const callback = this.get('onchange');
            if (callback)
                eval(callback);
        }
    });
}
