/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        data: () => ({
            disabled: false,
            type: 'text',
            value: '',
            placeholder: '',
        }),
        isolated: true,
        template,
        onrender: function () {
            const vm = this;
            vm.observe('disabled', (newValue, oldValue, keypath) => {
                const inputElement = vm.find('.tp-input-input');
                if (newValue) {
                    inputElement.setAttribute('disabled', 'disabled');
                } else {
                    inputElement.removeAttribute('disabled');
                }
            });
        },
    });
}