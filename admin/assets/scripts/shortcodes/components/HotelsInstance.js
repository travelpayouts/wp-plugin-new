/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import axios from 'axios';
import {filter, pick, includes, get} from 'lodash';
import {WP_AJAX} from './WpAjax';

export class HotelsInstance {

    static getData() {
        return {
            sectionsList: null,
            sectionsListResolved: false,
        };
    }

    static init($r) {
        $r.observe('attributes.city', (newValue) => {
            if (newValue !== null) {
                this.getAvailableSections($r, newValue.id).then(() => {
                    const currentSection = $r.get('attributes.selections_type');
                    const sectionsList = $r.get('sectionsList');
                    if (currentSection) {
                        if (!sectionsList.hasOwnProperty(currentSection)) {
                            $r.set('attributes.selections_type', null);
                        }
                    }

                });
            }
        });
    }

    static getAvailableSections($r, id) {
        $r.set('sectionsListResolved', false);
        return axios.get('https://yasen.hotellook.com/tp/v1/available_selections.json',
            {
                params: {
                    id,
                }
            }).then((response) => {
            const hotelTypes = $r.get('hotelLists', []);
            const sections = response.data;
            const availableSections = filter(hotelTypes, (hotelType) => {
                const hotelValue = get(hotelType, 'value');
                return includes(sections, hotelValue);
            });
            $r.set('sectionsList', availableSections);
            $r.set('sectionsListResolved', true);
        });
    }
}