/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import {isNull} from 'lodash';
import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        isolated: false,
        data: () => ({
            addItem: false,
            removeItem: false,
            showRemove: false,
            showAdd: false,
            value: [],
            onchange: false,
        }),
        template,
        onrender: function () {
            const vm = this;
            let value = vm.get('value');

            if (isNull(value)) {
                vm.set('value', []);
                value = [];
            }
            if (value !== undefined) {
                const minVal = vm.get('min');
                const valueLegnth = value.length;
                if (valueLegnth < minVal) {
                    for (let index = valueLegnth; index < minVal; index++) {
                        vm.push('value', {});
                    }
                }
            }
        },

        addItem: function () {
            this.push('value', {});
            this.runCallback();
            return false;
        },
        removeItem: function (key) {
            this.splice('value', key, 1);
            this.runCallback();
            return false;
        },
        showRemove: function (i) {
            const minVal = this.get('min') || 0;
            if (minVal === (i + 1)) {
                return false;
            }

            return this.get('value').length > minVal;
        },
        showAdd: function (i) {
            const [value, maxVal] = [this.get('value'), this.get('max')];
            let isMax = false;
            if (!maxVal) {
                isMax = true;
            } else {
                isMax = value.length < maxVal;
            }
            return isMax && (i === (value.length - 1));
        },
        runCallback: function () {
            const callback = this.get('onchange');
            if (callback)
                eval(callback);
        }
    });
}