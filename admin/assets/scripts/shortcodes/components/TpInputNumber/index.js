/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        data: {
            min: 0,
            max: 999999,
            disabled: false,
        },
        isolated: true,
        template,
        onrender: function () {
            const vm = this;
            let [min, max] = [vm.get('min'), vm.get('max')];

            vm.observe('value', (newValue, oldValue, keypath) => {
                if (newValue < min || !newValue) {
                    vm.set(keypath, min);
                } else if (newValue > max) {
                    vm.set(keypath, max);
                }
            });

            vm.observe('disabled', (newValue, oldValue, keypath) => {
                const inputElement = vm.find('.tp-input-number-input');
                if (newValue) {
                    inputElement.setAttribute('disabled', 'disabled');
                } else {
                    inputElement.removeAttribute('disabled');
                }
            });

        },
        increase: function () {
            if (this.get('disabled')) return false;
            const value = this.get('value');
            if (!value) {
                this.set('value', 1);
            } else {
                this.set('value', parseFloat(value) + 1);
            }
            return false;
        },
        decrease: function () {
            if (this.get('disabled')) return false;
            const value = this.get('value');
            const newValue = parseFloat(value) - 1;
            if (newValue >= 0) {
                this.set('value', newValue);
            }
            if (!value) {
                this.set('value', 0);
            }
            return false;
        },
    });
}