/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import Ractive from 'ractive';
import template from './template.html';
import tingle from 'tingle.js';
import {WP_AJAX} from '../WpAjax';
import {get, has} from 'lodash';
import {ModalInstance} from '../ModalInstance';
import './style.scss';

export default function () {
    return Ractive.extend({
        template,
        data: ()=>({
            loading: false,
            modalId: false,
            modalTemplate: false,
            modalWindow: false,
            modalData: false,
            modalopened: false,
            instanceCreated: false,
        }),
        oninit: function () {
            const vm = this;
            const buttonId = this.get('buttonid');
            vm.set('modalId', buttonId.replace('tp-media-button-', ''));

            const modalWindow = new tingle.modal({
                footer: false,
                stickyFooter: false,
                closeMethods: ['button', 'escape'],
                closeLabel: 'Close',
                cssClass: ['tp-tingle-modal', 'travel-modal', 'tingle-modal--noClose', 'tingle-modal--noOverlayClose'],
                beforeOpen: () => {
                    if (!vm.get('instanceCreated')) {
                        vm.set('instanceCreated', true);
                        const modalData = vm.get('modalData');
                        const modalWindow = vm.get('modalWindow');
                        return new ModalInstance(modalWindow.modalBoxContent, modalData);
                    }

                },
                beforeClose: function () {
                    vm.set('modalopened', false);
                    window.tpCurrentModal = undefined;
                    return true; // close the modal
                }
            });

            vm.set('modalWindow', modalWindow);

            vm.observe('modalopened', (newValue, oldValue, keypath) => {
                if (newValue) {
                    if (!vm.get('modalTemplate')) {
                        vm.loadModalData(() => this.openModal());
                    } else {
                        this.openModal();
                    }
                }
            });
        },
        /**
         * Display modal
         */
        openModal: function () {
            const vm = this;
            const modal = vm.get('modalWindow');
            const modalTemplate = vm.get('modalTemplate');
            if (modalTemplate) {
                if (!vm.get('instanceCreated')) {
                    modal.setContent(modalTemplate);
                }
                modal.open();
                vm.set('modalopened', true);
                window.tpCurrentModal = modal;
            }
        },
        /**
         * Trying to load modal content
         * @param callback
         */
        loadModalData: function (callback) {
            const vm = this;
            let modalId = vm.get('modalId');
            this.set('loading', true);
            WP_AJAX.get('',
                {
                    params: {
                        page: `/modal/shortcode/${modalId}`
                    }
                })
                .then((response) => {
                    this.set('loading', false);
                    let data = response.data;
                    if (data.status === 'error') {
                        vm.showErrorMessages(data.messages);
                    } else {
                        if (has(data, 'data.modalContent')) {
                            const responseData = get(data, 'data', {});
                            const modalTemplate = get(data, 'data.modalContent');
                            const modalData = Object.assign({}, responseData, {
                                modal: vm.get('modalWindow'),
                                modalId: vm.get('modalId'),
                                modalTitle: this.get('buttonmodaltitle'),
                            });
                            return vm.set('modalData', modalData).then(() =>
                                vm.set('modalTemplate', modalTemplate).then(() => {
                                    if (callback && typeof callback === 'function')
                                        callback();
                                }));
                        }
                    }
                });
        },

        /**
         * Displaying error messages
         * @param messages
         */
        showErrorMessages(messages) {
            messages.forEach(message => {
                $.growl({
                    style: 'error',
                    title: '',
                    message: message,
                    location: 'tr',
                    delayOnHover: false,
                    size: 'medium',
                    duration: 1200
                });
            });

        }
    });
}