/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import noUiSlider from 'nouislider';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        isolated: true,
        template,
        onrender: function () {
            const componentHandler = this.find('.tp-range-slider__handler');
            const stepOption = this.get('step') || 1;
            const multipleOption = this.get('multiple') || false;
            const [minVal, maxVal] = [this.get('value-min') || this.get('value'), this.get('value-max')];

            let componentOptions = {
                start: [],
                step: stepOption,
                tooltips: true,
                format: {
                    from: (value) => `${parseFloat(value).toFixed(0)}`,
                    to: (value) => `${parseFloat(value).toFixed(0)}`,
                },
                range: {
                    'min': [this.get('min') || 1],
                    'max': [this.get('max') || 100]
                }
            };

            if (multipleOption) {
                componentOptions.connect = true;
                if (minVal && maxVal) {
                    componentOptions.start = [minVal, maxVal];
                }


            } else {
                componentOptions.start = minVal;
            }


            const sliderComponent = noUiSlider.create(componentHandler, componentOptions);
            //
            sliderComponent.on('update', (values) => {
                if (multipleOption) {
                    this.set('value-min', values[0]);
                    this.set('value-max', values[1]);
                } else {
                    this.set('value', values[0]);
                }
            });
            // Watching changes from outside
            if (multipleOption) {
                this.observe('value-min', (newValue) => {
                    const sliderValues = sliderComponent.get();
                    if (newValue !== sliderValues[0]) {
                        sliderComponent.set([newValue, null]);
                    }
                });

                this.observe('value-max', (newValue) => {
                    const sliderValues = sliderComponent.get();
                    if (newValue !== sliderValues[1]) {
                        sliderComponent.set([null, newValue]);
                    }
                });
            } else {
                this.observe('value', (newValue) => {
                    const sliderValue = sliderComponent.get();
                    if (newValue !== sliderValue) {
                        sliderComponent.set([newValue]);
                    }
                });
            }

        }
    });
}