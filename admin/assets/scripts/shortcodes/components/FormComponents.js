/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import TpSelect from './TpSelect';
import TpCheckbox from './TpCheckbox';
import TpInputNumber from './TpInputNumber';
import TpInput from './TpInput';
import TpFormRow from './TpFormRow';
import TpDateRangePicker from './TpDateRangePicker';
import TpAutocomplete from './TpAutocomplete';
import TpRangeSlider from './TpRangeSlider';
import TpRepeated from './TpRepeated';
import TpRadioGroup from './TpRadioGroup';

export default {
    'tp-select': TpSelect,
    'tp-checkbox': TpCheckbox,
    'tp-input': TpInput,
    'tp-input-number': TpInputNumber,
    'tp-form-row': TpFormRow,
    'tp-date-range': TpDateRangePicker,
    'tp-autocomplete': TpAutocomplete,
    'tp-range-slider': TpRangeSlider,
    'tp-repeated': TpRepeated,
    'tp-radio-group': TpRadioGroup,
}