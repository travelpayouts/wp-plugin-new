/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import TpShortcodeForm from './TpShortcodeForm';
import Ractive from 'ractive';
import TpModal from './TpModal';
import {HotelsInstance} from './HotelsInstance'
import {WidgetsInstance} from './WidgetsInstance'
import TpSelect from './TpSelect'
import {map as _map, isFunction as _isFunction} from 'underscore';

export class ModalInstance {
    constructor(container, data) {
        const vm = this;
        const modalContainer = $(container);
        this.instances = {
            hotels: HotelsInstance,
            widgets: WidgetsInstance
        };

        const modalInstance = Ractive({
            target: modalContainer,
            template: modalContainer.html(),
            data: () => this.setData(data),
            components: {
                'tp-select': () => TpSelect,
                'tp-shortcode-form': TpShortcodeForm,
                'tp-modal': TpModal,
            }
        });

        modalInstance.observe('attributes.current_table', () => {
            modalInstance.set('errors', undefined);
        });

        if (this.instances.hasOwnProperty(data.modalId)) {
            const currentInstance = this.instances[data.modalId];
            if (_isFunction(currentInstance.init)) {
                currentInstance.init(modalInstance);
            }
        }
    }

    setData(data) {
        data.errors = [];
        let newData = {
            showForm: function (fieldId = false) {
                const currentTable = this.get('attributes.current_table');
                const tableFields = data.table_fields;
                if (!currentTable) return false;
                setTimeout(() => data.modal.checkOverflow(), 100);
                if (!fieldId) {
                    return currentTable !== null || currentTable !== undefined;
                } else {
                    return tableFields[currentTable].indexOf(fieldId) !== -1;
                }
            },
            _ac: this.getAutocompleteCallbacks()
        };

        //Pass modal instances data to main instance
        if (this.instances.hasOwnProperty(data.modalId)) {
            const currentInstance = this.instances[data.modalId];
            if (_isFunction(currentInstance.getData)) {
                const currentInstanceData = currentInstance.getData();
                if (typeof currentInstanceData === 'object') {
                    Object.assign(data, currentInstance.getData());
                }
            }
        }

        return Object.assign(data, newData);
    }

    getAutocompleteCallbacks() {
        return {
            flights_iata: (response, source) => {
                if (source === 'aviasales') {
                    return _map(response, (flight) => {
                        return {
                            name: `${flight.name} [${flight.iata}]`,
                            value: flight.iata,
                            data: flight
                        }
                    })

                } else if (source === 'jetradar') {
                    return _map(response, (flight) => {
                        return {
                            name: `${flight.city_fullname} [${flight.code}]`,
                            value: flight.code,
                            data: flight
                        }
                    })
                }
            },
            flights_airport_iata: (response, source) => {
                if (source === 'aviasales') {
                    return _map(response, (flight) => {
                        return {
                            name: `${flight.name} ${flight.airport_name || ''} [${flight.iata}]`,
                            value: flight.iata,
                            data: flight
                        }
                    })

                } else if (source === 'jetradar') {
                    return _map(response, (flight) => {
                        return {
                            name: `${flight.city_fullname} ${flight.name || ''} [${flight.code}]`,
                            value: flight.code,
                            data: flight
                        }
                    })
                }
            },
            hotels_city_name: (response) => {
                return _map(response.cities, (city) => {
                    return {
                        name: city.fullname,
                        value: city.id.toString(),
                        data: city
                    }
                });
            },
            hotels_and_cities: (response) => {
                let result = [];
                if (response.cities) {
                    const cities = _map(response.cities, (city) => {
                        return {
                            name: `${city.fullname} [${city.hotelsCount} hotels]`,
                            value: city.id.toString(),
                            data: city
                        }
                    });
                    result = result.concat(cities);
                }

                if (response.hotels) {
                    const hotels = _map(response.hotels, (hotel) => {
                        return {
                            name: hotel.hotelFullName,
                            value: hotel.id.toString(),
                            data: hotel
                        }
                    });
                    result = result.concat(hotels);

                }

                return result;
            },
            hotels_fullName: (response) => {
                return _map(response.hotels, (hotel) => {
                    return {
                        name: hotel.hotelFullName,
                        value: hotel.id.toString(),
                        data: hotel
                    }
                });
            },
            hotels_cities: (response) => {
                return _map(response.cities, (city) => {
                    return {
                        name: city.fullname,
                        value: city.id.toString(),
                        data: city
                    }
                });
            },
            railway: (response) => {
                return _map(response, (data) => {
                    return {
                        name: data.value,
                        value: data.id.toString(),
                        data: data
                    }
                });
            },
            airlines: (response) => {
                return _map(response.data, (data) => {
                    return {
                        name: data.name,
                        value: data.iata.toString(),
                        data: data
                    }
                });
            }
        }

    }
}