/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import {find, get} from 'lodash';

export default function () {
    return Ractive.extend({
        data: {
            selectedValue: false,
        },
        template,
        oncomplete: function () {
            const vm = this;
            const componentSelector = $(this.find('.radio'));
            vm.parent.observe('selectedvalue', (newValue) => {
                vm.set('selectedValue', newValue);
            });
            componentSelector.checkbox({
                onChange: () => {
                    vm.parent.set('selectedvalue', vm.get('value'));
                }
            });
        }
    });
}