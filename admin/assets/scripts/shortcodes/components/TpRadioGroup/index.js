/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import TpRadio from './TpRadio';
import './style.scss';

export default function () {
    return Ractive.extend({
        data: {
            selectedvalue: false,
            inline: false,
            onchange: false,
        },
        isolated: true,
        components: {
            'tp-radio': TpRadio,
        },
        template,
        onrender: function () {
            this.observe('selectedvalue', (newValue) => {
                if (newValue) this.runCallback();
            });
        },
        runCallback: function () {
            const callback = this.get('onchange');
            if (callback)
                eval(callback);
        }
    });
}