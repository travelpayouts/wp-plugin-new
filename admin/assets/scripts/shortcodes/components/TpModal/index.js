/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        isolated: false,
        template,
        closeModal: function () {
            const modalWindow = this.get('modal');
            if (modalWindow) {
                modalWindow.close();
            }
            return false;
        }
    });
}