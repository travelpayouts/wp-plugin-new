/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import axios from 'axios';

export const WP_AJAX = axios.create({
    baseURL: window.ajaxurl,
    params: {
        action: 'travelpayouts'
    }
});