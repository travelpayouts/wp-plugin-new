/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import Ractive from 'ractive';
import template from './template.html';
import './style.scss';

export default function () {
    return Ractive.extend({
        data: {value: false},
        isolated: true,
        template,
        toggleValue: function () {
            const vm = this;
            const currentValue = vm.get('value');
            vm.set('value', !currentValue);
            return false;
        }
    });
}
