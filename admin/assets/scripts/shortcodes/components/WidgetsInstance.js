/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import axios from 'axios';
import {pick as _pick, each as _each} from 'underscore';
import {WP_AJAX} from './WpAjax';
import {filter, get, includes} from "lodash";

export class WidgetsInstance {

    static getData() {
        return {
            hotelsSectionsList: null,
            hotelsSectionsListResolved: false,
        };
    }

    static init($r) {
        $r.observe('attributes.city', (newValue) => {
            console.log(newValue);
            if (newValue) {
                this.getAvailableSections($r, newValue.id);
            }
        });
    }

    static getTypes($r) {
        return WP_AJAX.get('',
            {
                params: {
                    page: `/hotels/types`
                }
            })
            .then((response) => {
                if (response.data.status === 'ok') {
                    $r.set('hotelsTypes', response.data.data);
                    return response.data.data;
                }
                return {};
            });
    }

    static getAvailableSections($r, id) {
        $r.set('hotelsSectionsListResolved', false);
        return axios.get('https://yasen.hotellook.com/tp/v1/available_selections.json',
            {
                params: {id},
            }).then((response) => {
            const hotelTypes = $r.get('hotelLists', []);
            const sections = response.data;
            const availableSections = filter(hotelTypes, (hotelType) => {
                const hotelValue = get(hotelType, 'value');
                return includes(sections, hotelValue);
            });
            $r.set('hotelsSectionsList', availableSections);
            $r.set('hotelsSectionsListResolved', true);
        });
    }
}