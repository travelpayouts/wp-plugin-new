/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import template from './template.html';
import {get, has, set, find} from 'lodash';

export default function () {
    return Ractive.extend({
        data: ()=>({
            searchable: false,
            disabled: false,
            multiple: false,
            options: [],
            placeholder: '',
            value: '',
            position: 'auto',  // Available positions:  upward || downward || auto
            onChange: false,
            onShow: false,
            onHide: false,
            _dropdown: false,
            _componentSelector: false,
            valueprop: 'value',
            labelprop: 'label',
            hasCustomLabelTemplate: false,
            asinput: false,
        }),
        isolated: true,
        template,
        oninit: function () {
            const vm = this;
            const hasCustomLabelTemplate = has(vm, 'partials.label');
            vm.set('hasCustomLabelTemplate', hasCustomLabelTemplate);
        },
        oncomplete: function () {
            const vm = this;
            const componentSelector = $(this.find('.dropdown'));
            vm.set('_componentSelector', componentSelector);
            if (componentSelector) vm.initDropdown();
            vm.observe('value', (newValue) => {
                if (newValue === null || newValue === undefined || newValue === '') return false;
                const options = vm.get('options');
                const foundedValue = find(options, {value: newValue});
                if (foundedValue)
                    vm.setDropDownValue(get(foundedValue, 'label'));
            });
        },
        initDropdown: function () {
            const vm = this;
            const componentSelector = vm.get('_componentSelector');
            const dropdown = componentSelector.dropdown({
                onChange: (value) => {
                    vm.set('value', value);
                    const callback = vm.get('onChange');
                    if (callback && typeof callback === 'function') callback();
                },
                onShow: () => {
                    const callback = vm.get('onShow');
                    if (callback && typeof callback === 'function') callback();
                },
                onHide: () => {
                    const callback = vm.get('onHide');
                    if (callback && typeof callback === 'function') callback();
                },
                fullTextSearch: vm.get('searchable'),
                direction: vm.get('position'),
                placeholder: vm.get('placeholder'),
            });

            vm.set('_dropdown', dropdown);

        },
        setDropDownValue: function (label) {
            const vm = this;
            const dropdown = vm.get('_dropdown');
            if (dropdown) {
                if (label) {
                    dropdown.dropdown('set text', label);
                } else {
                    dropdown.dropdown('clear');
                }
            }
        }
    });
}