/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import Ractive from 'ractive';
import {DateRangePicker} from 'tiny-date-picker/dist/date-range-picker';
import {format, parse} from 'date-fns';
import '../TpDatePicker/style.scss';
import './style.scss';
import template from './template.html';

export default function () {
    return Ractive.extend({
        isolated: false,
        template,
        data: {
            from: false,
            to: false,
        },
        onrender: function () {
            const vm = this;
            const [inputStart, inputEnd] = [
                vm.find('tp-date-from input'),
                vm.find('tp-date-to input')
            ];
            const dateFormat = 'dd-MM-yyyy';

            if (inputStart && inputEnd && vm.find('.TpDateRangePicker__picker')) {
                const contentSelector = vm.find('.TpDateRangePicker__content');
                const pickerSelector = vm.find('.TpDateRangePicker__picker');
                const visibleClassName = 'TpDateRangePicker__picker_visible';
                const dateRangePicker = DateRangePicker(pickerSelector)
                    .on('statechange', (_, rp) => {
                        // Update the inputs when the state changes
                        const range = rp.state;
                        vm.set('from', range.start ? vm.formatDate(range.start, dateFormat) : '');
                        vm.set('to', range.end ? vm.formatDate(range.end, dateFormat) : '');
                    });

                inputStart.addEventListener('focus', () => pickerSelector.classList.add(visibleClassName));
                inputEnd.addEventListener('focus', () => pickerSelector.classList.add(visibleClassName));

                let previousTimeout;
                contentSelector.addEventListener('focusout', () => {
                    clearTimeout(previousTimeout);
                    previousTimeout = setTimeout(() => {
                        if (!contentSelector.contains(document.activeElement)) {
                            pickerSelector.classList.remove(visibleClassName);
                        }
                    }, 10);
                });
            }

        },
        formatDate: (date, dateFormat) => format(date, dateFormat),
        parseDate: (date, dateFormat) => parse(date, dateFormat, new Date())
    });
}