/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import {get} from 'lodash';
import Ractive from 'ractive';
import template from './template.html';
import {WP_AJAX} from '../WpAjax';
import './style.scss';
import formComponents from '../FormComponents';

export default function () {
    return Ractive.extend({
        components: formComponents,
        isolated: false,
        template,
        submitForm: function () {
            const vm = this;
            this.requestUrl = `/shortcode/${this.get('modalId')}`;
            this.set('errors', {});
            const messages = this.get('messages');

            const modalContainer = $('.travel-modal .tingle-modal-box');

            modalContainer.addClass('travel-modal_validating');
            WP_AJAX.post('',
                {
                    shortcode_data: vm.get('attributes'),

                }, {
                    'params': {
                        page: this.requestUrl
                    }
                }).then((result) => {
                modalContainer.removeClass('travel-modal_validating');

                const data = result.data;
                let growlData = {
                    style: 'error',
                    message: messages.fail_to_add
                };

                if (data.status && data.status === 'ok') {
                    this.insertShortcode(data.data.shortcode);
                    growlData = {
                        style: 'notice',
                        message: messages.success_add
                    };
                    window.tpCurrentModal.close();
                } else {
                    vm.set('errors', get(data, 'data.messages', []));
                }
                $.growl(Object.assign({
                    title: '',
                    location: 'tr',
                    delayOnHover: false,
                    size: 'medium',
                    duration: 1500
                }, growlData));
            });
            return false;
        },
        insertShortcode(code) {
            if (window.tinyMCE && window.QTags) {
                if (!tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
                    if (window.QTags.insertContent(code) !== true)
                        $('#content').val((index, val) => val + code);
                } else if (tinyMCE && tinyMCE.activeEditor) {
                    tinyMCE.activeEditor.selection.setContent(code);
                }
            } else {
                $('.wp-editor-area').val((index, val) => val + code);
            }
        },
    });
}