/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import 'jquery.growl';
import Ractive from 'ractive/ractive.js';
import TpPostButton from './components/TpPostButton';
import '../components/semanticUI';
import '../../styles/tp_shortcodes.scss';

export default class {
    static init() {
        this.shortcodesContainer = '#wp-content-media-buttons';
        Ractive.defaults.delimiters = ['{{', '}}'];
        Ractive.defaults.tripleDelimiters = ['{{{', '}}}'];
        Ractive({
            target: this.shortcodesContainer,
            template: $(this.shortcodesContainer).html(),
            data: () => ({}),
            components: {
                'tp-post-button': TpPostButton,
            }
        });
    }
}
