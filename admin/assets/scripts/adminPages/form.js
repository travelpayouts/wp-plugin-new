import 'jquery.growl';
import citySearch from '../components/citySearch'
import slider from '../components/slider';
import uiSortableConnected from '../components/uiSortableConnected';
import formHandler from '../components/formHandler'
import colorPalletes from '../components/colorPalletesField'

export default class TravelPayoutsForm {
    static init(container) {
        let formRow = $('.tp-form', container);

        slider.init(container);
        citySearch.init(container);
        uiSortableConnected.init(container);

        colorPalletes.init(container);
        $('.form-row__checkbox', container).checkbox().addClass('ui checkbox');
        $('.form-row__radio', container).checkbox().addClass('ui checkbox radio');
        // if ($('input[type="radio"]', this.formRow).length) this.wrapRadios();
        if ($('input[type="file"]', formRow).length) TravelPayoutsForm.wrapFile(container);
        // $('select', formRow).dropdown();

    }

    static initFormHandler() {
        formHandler.ajaxSubmitForm();
        formHandler.resetForm();
    }

    static initDropdowns(container) {
        $('select', container).dropdown();
    }

    static wrapFile(container) {
        let fieldWrapperSelector = '.shortcode-form__field';
        let fileInput = $('.tp-form input[type="file"]', container);
        $(fileInput).each(function () {
            let parentRow = $(this).closest(fieldWrapperSelector);
            parentRow.addClass('text-link__file text-link__file--export');
            parentRow.find('label').removeClass('table-info__subtitle');
        });
    }
}

