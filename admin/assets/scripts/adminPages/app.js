;(function ($) {
    const showDropDownMessage = true;
    var tippy = require('tippy.js');

    var $body = $('body');

    $(document).ready(function () {
        /*------Init tooltip*/
        tippy(document.querySelectorAll('a'), {
            arrow: true
        });

        $(".js-modal-open").on('click', function () {
            $('.modal-dialog').fadeIn();
        });
        $(".modal-dialog__close, #constructorShortcodesButtonCancel").on('click', function () {
            $('.modal-dialog').fadeOut();
        });
        /*-----Custom select*/
        if ($('#select_shortcodes').length) {
            $('#select_shortcodes').select2();
        }
        if ($('#currency').length) {
            $('#currency').select2();
        }
        if ($('#select_trip_class').length) {
            $('#select_trip_class').select2();
        }
        if ($('#transplant').length) {
            $('#transplant').select2();
        }
        /*------Add js_menu_border class to menu link and open submenu*/
        var $menuLink = $('.navigation__menu').children('li').children('a');
        $($menuLink).on('click', function (event) {
            event.preventDefault();
            var $this = $(this),
                $subMenu = $this.next('ul'),
                link = $this.attr('href');

            $this.closest('.navigation__menu').find('.js_menu_border, .js_menu_arrow').removeClass('js_menu_border js_menu_arrow');
            $this.addClass('js_menu_border');
            if ($subMenu.length) {
                $subMenu.addClass('js_menu_border');
            } else {
                $this.addClass('js_menu_arrow');
                window.location.href = link;
            }
        });
        /* end click*/

        $('.navigation__submenu').on('click', 'a', function () {
            var $this = $(this);

            $this.closest('.navigation__submenu').find('.js_menu_arrow').removeClass('js_menu_arrow');
            $this.addClass('js_menu_arrow');
        });
        /* end click*/
        /*------Close drop down message*/
        $(document).on('click', '.drop_down__close', function (event) {
            event.preventDefault();
            openCloseDropDownMessage($('.drop_down'));
        });
        /*------Add class to result table sort */
        $(document).on('click', '.result-table__sort', function (event) {
            event.preventDefault();
            $(this).toggleClass('result-table__sort--asc');
        });
        /*-------Show hide aside menu*/
        $(document).on('click', '.hamburger', function (event) {
            event.preventDefault();
            $body.toggleClass('js_sidebar_active');
        });
        /*-------Add need class depend on inputs checked or not*/

 $('.result-table__choice').click(function (event) {
            event.preventDefault();
            $(this).toggleClass('active');
            $('.search-result__btn--delete').toggleClass('disabled');
        });
        
        // $('.result-table__choice')
        //     .mouseenter(function (event) {
        //         event.preventDefault();
        //         var $this = $(this);
        //         $('.search-form').find('input').each(function (index, el) {
        //             var checked = $(this).prop('checked');
        //             if (!checked) {
        //                 $this.addClass('js_choice_all');
        //             } else {
        //                 $this.removeClass('js_choice_all');
        //                 $this.addClass('js_choice_delete');
        //             }
        //         });
        //     }).mouseleave(function (event) {
        //     event.preventDefault();
        //     var $this = $(this);
        //     $this.removeClass('js_choice_all js_choice_delete');
        // }).click(function (event) {
        //     event.preventDefault();
        //     $('.search-result__btn--delete').toggleClass('disabled');
        // });
        /*-------Set tables item and tables content data attribute */
        setDataTab($('.tables__nav').find('a'), $('.tables-content__item'));

        /*-------Show tables content*/
        $('.tables__nav').on('click', 'a', function (event) {
            event.preventDefault();
            var tabId = $(this).attr('data-tab');
            var $tabsWrap = $('.main-content__inner');

            $tabsWrap.find('.js_tables_active').removeClass('js_tables_active');
            $(this).addClass('js_tables_active');
            $tabsWrap.find('.tables-content__item[data-tab=' + tabId + ']').addClass('js_tables_active');
        });
        /*-------Tables accordion*/
        $(document).on('click', '.tables-content__title', function (event) {
            event.preventDefault();
            var $this = $(this),
                current_body = $this.next('.table-info'),
                tableContent = $this.closest('.tables-content__item');

            tableContent.find('.table-info').slideUp();
            tableContent.find('.js_tables_active').removeClass('js_tables_active');

            // Close, open current accordion content
            if (current_body.css('display') != 'block') {
                current_body.slideDown();
                $this.addClass('js_tables_active');
            } else {
                current_body.slideUp();
                $this.removeClass('js_tables_active');
            }
        });
        /*--Close tables accordion*/
        $(document).on('click', '.table-info__close', function (event) {
            event.preventDefault();
            var $thisParent = $(this).closest('.table-info');
            $thisParent.slideUp();
            $thisParent.prev('.tables-content__title').removeClass('js_tables_active');
        });
        /*-------Show preview-info*/
        $(document).on('click', '.table-info__preview-title', function (event) {
            event.preventDefault();
            var $this = $(this);
            $this.toggleClass('js_preview-info_show');
            $this.next('.preview-info').slideToggle();
        });
        /*-------Show select*/
        $(document).on('click', '.select__title', function (event) {
            event.preventDefault();
            var $this = $(this);
            $this.toggleClass('js_select_show');
            $this.next('.select__list').slideToggle();
        });

        $('.select__list').on('click', 'label', function (event) {
            event.preventDefault();
            var $this = $(this),
                text = $this.text(),
                $span = $this.find('span'),
                $parentList = $this.closest('.select'),
                $selectTitle = $parentList.find('.select__title');

            // Remove checked attribute from previous value
            $('input:checked', $parentList).attr('checked', '');
            $('input', $this).attr('checked', 'checked');

            $selectTitle.removeClass('js_select_show');

            if ($span.length) {
                var borderStyle = $this.find('input').val();
                $selectTitle.find('span').text(text).css('border-bottom-style', borderStyle);
            } else {
                $selectTitle.find('span').text(text);
            }

            $parentList.find('.select__list').slideUp();

        });
        /*-------Sort table*/
        if ($('#sort_table_not_select').length || $('#sort_table_select').length) {
            $("#sort_table_not_select, #sort_table_select").sortable({
                connectWith: '.sort_table__list',
                placeholder: 'ui-state-highlight',
                helper: 'clone',
                start: function (e, ui) {
                    ui.placeholder.text(ui.item.text());
                }
            }).disableSelection();
        }
        /*-------Pagination settings*/

        var paginationQtySelector = '.pagination-qty';
        var $paginationQty = $(paginationQtySelector);

        jQuery('body').on('click', `${paginationQtySelector} button`, function () {
            event.preventDefault();
            var $this = $(this),
                $inputPagination = $this.closest('.pagination-qty').find('input[type="number"]'),
                oldValue = parseFloat($inputPagination.val()),
                newValue;

            if ($this.hasClass('pagination-qty-plus')) {
                newValue = oldValue + 1;
            } else if ($this.hasClass('pagination-qty-minus')) {
                if (oldValue <= 1) {
                    newValue = oldValue;
                } else {
                    newValue = oldValue - 1;
                }
            }

            $inputPagination.val(newValue);
            $inputPagination.trigger('change');
        });
        /*--------Disable pagination input and buttons qty*/
        $(document).on('change', '.pagination-on', function (event) {
            event.preventDefault();
            var $this = $(this),
                state = $this.prop('checked'),
                $paginationSettings = $this.closest('.pagination-settings'),
                $paginationQty = $paginationSettings.find('.pagination-qty'),
                $inputQty = $paginationQty.find('input'),
                $buttonsQty = $paginationQty.find('button');


            if (!state) {
                $inputQty.attr('disabled', true);
                $buttonsQty.attr('disabled', true);
            } else {
                $inputQty.attr('disabled', false);
                $buttonsQty.attr('disabled', false);
            }
        });
        /*--------Enable scaling-width input and buttons qty*/
        $(document).on('change', '.pagination-off', function (event) {
            event.preventDefault();
            var $this = $(this),
                state = $this.prop('checked'),
                $paginationSettings = $this.closest('.pagination-settings');

            disableInput(state, $paginationSettings);
        });
        $('.form-row__checkbox').find('input').on('change', function () {
            var $this = $(this),
                state = $this.prop('checked'),
                $paginationSettings = $this.closest('.form-row__checkbox').next('.form-row__integer');

            disableInput(state, $paginationSettings);
        });
        /*-------Accept agreement*/
        $(document).on('click', '.js_accept_agreement', function (event) {
            event.preventDefault();
            $('.js_agreement').removeClass('js_train_tickets_active');
            $('.js_train_info').addClass('js_train_tickets_active');
        });
        /*-------Show answers form*/
        $(document).on('click', '.select-radio__label', function (event) {
            var $this = $(this),
                current_body = $this.next('.select-radio__drop'),
                all_bodies = $this.closest('.select-radio')
                    .find('.select-radio__drop');

            all_bodies.slideUp();

            // Close, open current answers form content
            if (current_body.css('display') != 'block') {
                current_body.slideDown();
            } else {
                current_body.slideUp();
            }
        });
        /*-------Color picker*/
        $('.color-pallete-choose').find('.color-pallete-choose__color').each(function (index, el) {
            var $this = $(this);
            if ($this.length) {
                var pinColor = $this.val();
                $this.spectrum({
                    color: pinColor,
                    showInput: true,
                    showInitial: true,
                    showPalette: true,
                    showSelectionPalette: true,
                    maxSelectionSize: 10,
                    preferredFormat: "hex",
                    cancelText: "Отмена",
                    chooseText: "Выбрать",
                    localStorageKey: "spectrum.demo",
                    change: function (color) {
                        $this.closest('.color-pallete-choose__inner').find('.color-pallete-choose__hex').text(color.toHexString());
                    },
                    palette: [
                        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                            "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                    ]
                });
            }
        });
        /*-------Show custom color picker*/
        // $('.color-pallete__item').find('input').on('click', function() {
        // 	var $colorPalleteChoose = $('.color-pallete-choose');
        // 	if($(this).is('#custom-color-pallete')) {
        // 		$colorPalleteChoose.slideDown();
        // 	} else {
        // 		$colorPalleteChoose.slideUp();
        // 	}
        // });
        // $('.color-pallete__item').find('label').on('click', function() {
        // 	var $this = $(this);
        // 	$bgPallete = $this.closest('.bg-pallete');
        // 	if($bgPallete.length) {
        // 		$bgPallete.find('.color-pallete__item').removeClass('active');
        // 		$this.closest('.color-pallete__item').addClass('active');
        // 	}
        // });
        /*-------Travel time range*/
        if ($('.travel-time').length) {
            $('.travel-time').jRange({
                from: 0,
                to: 30,
                step: 1,
                width: 270,
                showScale: false,
                format: '%s',
                showLabels: true,
                isRange: true
            });
        }
        /*-------Selection Hotel Count range*/
        if ($('.selection-hotel-count').length) {
            $('.selection-hotel-count').jRange({
                from: 0,
                to: 15,
                step: 1,
                width: 270,
                showScale: false,
                format: '%s',
                showLabels: true,
                isRange: false
            });
        }
        /*-------Selection Hotel Count range*/
        if ($('.limit-special-offer').length) {
            $('.limit-special-offer').jRange({
                from: 0,
                to: 30,
                step: 1,
                width: 270,
                showScale: false,
                format: '%s',
                showLabels: true,
                isRange: false
            });
        }
        /*-------Set widget-design input value*/
        $(document).on('change', 'widget-design-checked', function () {
            var state = $(this).prop('checked'),
                $input = $this.closest('.widget-design').find('input[name="widget-design"]'),
                checkedVal = $input.data('checked'),
                unCheckedVal = $input.data('unchecked');
            if (!state) {
                $input.val(checkedVal);
            } else {
                $input.val(unCheckedVal);
            }
        });
        /*-------Custom scroll*/
        if ($('.custom-scroll-y').length) {
            $('.custom-scroll-y').mCustomScrollbar();
        }
        if ($('.modal-dialog__content').length) {
            $('.modal-dialog__content').mCustomScrollbar();
        }
        if ($('.custom-scroll-x').length) {
            $('.custom-scroll-x').mCustomScrollbar({
                axis: 'x'
            });
        }

        /*------Table design add  active class*/
        $(document).on('click', '.table-design-form__btn', function (event) {
            event.preventDefault();
            $('.table-designs').find('.js_table_design_active').removeClass('js_table_design_active');
            $(this).closest('.table-design').addClass('js_table_design_active');
        });
        /*-------Show, close modal*/
        $(document).on('click', '.js_modal_active', function (event) {
            event.preventDefault();
            openCloseModal();
        }); // end click

        $('.modal-wrap').click(function (e) {
            openCloseModal();
        }).children().click(function (e) {
            e.stopPropagation();
        }); // end click

        $('.js_modal_close').click(function (event) {
            event.preventDefault();
            openCloseModal();
        }); // end click

        $(document).on('click', '.helper', function (e) {
            e.preventDefault();
        }); //end click
    });
    /* end ready*/

    $(window).on('load resize', function () {
        /*-----Set equal height to main content block*/
        var menuHeight = $('.navigation__menu').outerHeight(),
            $mainContent = $('.main-content');
        if (window.matchMedia('(min-width: 769px)').matches) {
            $mainContent.css('min-height', menuHeight);
        } else {
            $mainContent.css('min-height', '');
        }
    });
    /* end load rezise*/
    if (showDropDownMessage) {
        $(window).on('load', function () {
            /*----Show drop down message*/
            setTimeout(function () {
                openCloseDropDownMessage($('.drop_down'));
            }, 1500);
        });
        /* end load*/

    }

    $(window).on('scroll', function () {

    });

    /* end scroll*/

    function openCloseDropDownMessage(win) {
        var heightWin = win.outerHeight();
        var $updateNag = $('.update-nag');
        var heightWinForHeader = 0;
        var $mainInfo = $('.main-info');
        var $headerWelcome = $('.header--welcome');

        if ($($updateNag).length) {
            var updateNagHeight = $updateNag.outerHeight();
            heightWinForHeader = ((heightWin - updateNagHeight) + 20);
        } else {
            heightWinForHeader = (heightWin + 20);
        }
        if (!win.hasClass('drop_down_shown')) {
            win.addClass('drop_down_shown');
            if ($($headerWelcome).length) {
                $headerWelcome.css('margin-top', heightWinForHeader);
            } else {
                $mainInfo.css('margin-top', heightWin);
            }
        } else {
            win.removeClass('drop_down_shown');
            $mainInfo.css('margin-top', '0');
            $headerWelcome.css('margin-top', '0');
        }
    }

    function setDataTab(menuItem, contentItem) {
        var counter = 1;

        menuItem.each(function (index, el) {
            $(this).attr('data-tab', counter);
            counter++;
        }); // end each

        counter = 1; // reset counter value

        contentItem.each(function (index, el) {
            $(this).attr('data-tab', counter);
            counter++;
        }); // end each
    }

    function openCloseModal() {
        if (!$body.hasClass('js-modal-active')) {
            $body.addClass('js-modal-active');
            $('.modal-wrap').addClass('js-modal-active');
        } else {
            $body.removeClass('js-modal-active');
            $('.modal-wrap').removeClass('js-modal-active');
        }
    }

    function disableInput(state, parent) {
        var $paginationQty = parent.find('.pagination-qty'),
            $inputQty = $paginationQty.find('input'),
            $buttonsQty = $paginationQty.find('button');

        if (state) {
            $inputQty.attr('disabled', true);
            $buttonsQty.attr('disabled', true);
        } else {
            $inputQty.attr('disabled', false);
            $buttonsQty.attr('disabled', false);
        }
    }
})(jQuery);

function dropdownPosition() {
    var dropDown = document.querySelector('.drop_down'),
        header = document.getElementById('wpadminbar'),
        sideBarWidth = document.getElementById('adminmenuback').offsetWidth,
        windowTop = window.pageYOffset;
    dropDown.style.marginTop = header.offsetHeight + 'px';
    dropDown.style.marginLeft = sideBarWidth + 'px';
    if (windowTop > header.offsetHeight) {
        dropDown.style.marginTop =  0;
    }
};

window.onload = (function(){
    dropdownPosition();
});

window.onresize = (function() {
    dropdownPosition();
});
window.onscroll = (function() {
    dropdownPosition();
});

