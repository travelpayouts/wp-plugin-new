/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import Ractive from 'ractive'
import TpForm from './components/TpForm/controller';

export default class {
    static init() {
        this.shortcodesContainer = '#travelpayouts_admin';
        Ractive.defaults.delimiters = ['[[', ']]'];
        Ractive.defaults.tripleDelimiters = ['[[[', ']]]'];
        Ractive({
            target: this.shortcodesContainer,
            template: $(this.shortcodesContainer).html(),
            data: {},
            components: {
                'tp-admin-form': () => TpForm.component(),
            },
            onrender: function () {
                $(this.el).fadeIn()
            }
        });

        window.tpCurrentModal = {
            checkOverflow: () => {
            }
        }
    }
}
