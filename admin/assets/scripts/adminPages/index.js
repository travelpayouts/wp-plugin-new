/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

import 'smoothscroll-for-websites';
import 'malihu-custom-scrollbar-plugin';
import 'jquery-range';
import 'jquery-ui-touch-punch-c';
import 'select2';
import 'spectrum-colorpicker/spectrum';
import 'tippy.js';
import '../components/semanticUI';
import '../../styles/main.scss';
import tpForm from './form';
import './app'
import adminPages from './adminPages';

// adminPages.init();
tpForm.init($('body'));
tpForm.initFormHandler();
