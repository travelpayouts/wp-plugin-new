/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import Ractive from 'ractive'
import template from './template.html'
import './style.scss';
import axios from 'axios';
import qs from 'qs';
import TpSelect from '../../../shortcodes/components/TpSelect';
import TpCheckbox from '../../../shortcodes/components/TpCheckbox';
import TpInputNumber from "../../../shortcodes/components/TpInputNumber";

export default class index {
    static component() {
        return Ractive.extend({
            data: {},
            components: {
                'tp-select': TpSelect,
                'tp-checkbox': TpCheckbox,
                'tp-input-number': TpInputNumber,
            },
            isolated: false,
            template,
            oncomplete: function () {
                console.log('completed');
                console.log({
                    name: this.get('name'),
                    data: this.get('data')
                });
                this.set('data.test', 'TEST');
                axios.get(`${window.location.href}&${this.get('name')}=true`);

                this.observe('data', function (newValue) {
                    console.log('form changes', newValue);
                });
            },
            resetForm: function () {
                this.set('data', {});
            },
            submitForm: function () {
                const form = this.find('form');
                const method = $(form).attr('method');
                const action = $(form).attr('action') || window.location.href;
                console.log({method, action});
                console.log('submit');
                console.log(this.get('data'));
                const submittedData = {};
                submittedData[this.get('name')] = this.get('data');


                console.log(qs.stringify(submittedData));

                axios.post(action, qs.stringify(submittedData),
                    {
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded',
                            'X-Requested-With': 'xmlhttprequest'
                        }
                    });

                return false;
            },
            getValue: function (path) {
                return this.get(`data.${path}`);
            }
        });
    }
}