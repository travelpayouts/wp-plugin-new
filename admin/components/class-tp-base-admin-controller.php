<?php
/**
 * Created by PhpStorm.
 * User: ubuntu-pc
 * Date: 20.09.17
 * Time: 6:53
 */

namespace tp\admin\components;

use Yii;
use \tp\includes as Includes;
use \tp\admin\includes as AdminIncludes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactory;

abstract class TP_Base_Admin_Controller extends Includes\base\TP_Base_Setup_Hooks
{
    private static $instances;
    protected $form;
    protected $template;
    protected $request;
    protected $templating = false;
    protected $session;
    public $action;
    protected $menu = array();
    public $actions = array();

    /**
     * @return mixed
     */
    final public static function get_instance()
    {
        $class_name = get_called_class();
        if (isset(self::$instances[$class_name]) == false) {
            self::$instances[$class_name] = new static();
        }
        return self::$instances[$class_name];
    }

    protected function __construct()
    {
        try {
            $template_engine = $this->get_templating_instance();
            $template_engine->add_template(TP_PLUGIN_ADMIN_PATRIALS_PATH);
//            $template_engine->add_form_theme('_form/new_form_div_layout.html.twig');
            $template_engine->add_form_theme('_form/form_div_layout.html.twig');
            $template_engine->add_form_theme('_form/form_custom_layout.twig');
            $this->template = $this->get_template();
            $this->form = $this->get_form();
            $this->request = Request::createFromGlobals();

            // Add ajax requests handler
            add_action('wp_ajax_' . strtolower(TP_PLUGIN_NAME), array(new AdminIncludes\TP_Ajax_Routing, 'route'));
        } catch (\Exception $exception) {
            wp_die(_x('Error while rendering travelpayouts plugin content',
                'errors general', TP_PLUGIN_TEXTDOMAIN));
        }
    }


    final private function __clone()
    {
    }

    abstract public function run();

    /**
     * Handle controller render
     */
    public function render_controller()
    {
        $this->before_render();

        $action = $this->request()->query->get('action');
        $controller_action = 'action_' . $action;

        if ($action && method_exists($this, 'action_' . $action)) {
            $this->action = $action;
            $this->$controller_action();
        } else {
            $this->run();
        }
    }

    /**
     * Render template
     * @param $name
     * @param array $parameters
     * @param bool $return
     * @return mixed
     */
    public function render($name, array $parameters = array(), $return = false)
    {
        $this->template->addGlobal('_base', '/layouts/main.twig');
        $this->template->addGlobal('_img_url', TP_PLUGIN_ADMIN_IMG_URL);
        $this->template->addGlobal('_page_url', $this->request()->query->get('page'));
        $this->template->addGlobal('_menu_items', $this->get_menu_items());
        $this->template->addGlobal('_action', $this->action);

        $this->template->addFunction(new \Twig_SimpleFunction('breadcrumb_title', function ($page, $action = false) {
            return $this->get_breadcrumb_title($page, $action);
        }));
        return $this->templating->render($name, $parameters, $return);
    }

    /**
     * Render a part of template
     * @param $name
     * @param array $parameters
     * @param bool $return
     * @return mixed
     */
    public function render_partial($name, array $parameters = array(), $return = false)
    {
        return $this->templating->render($name, $parameters, $return);
    }

    /**
     * Handle ajax requests
     * @return mixed
     */
    public function render_ajax()
    {
        return AdminIncludes\TP_Ajax_Routing::get_instance();
    }

    /**
     * Get Request instance
     * @return Request static
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * Get menu items
     * @return array
     */
    public function get_menu_items()
    {
        return $this->menu;
    }

    /**
     * Set menu items
     * @param array $items
     */
    public function set_menu_items(array $items)
    {
        $this->menu = $items;
    }

    /**
     * @deprecated
     * @return null
     */
    public function session()
    {
        return null;
    }

    // @TODO : MB remove it?
    public function redirect($controller, $options = array())
    {
        $admin_url = admin_url() . 'admin.php?';
        $options['page'] = $controller;
        $redirect_url = $admin_url . http_build_query($options);
        wp_redirect($redirect_url);
    }

    // @todo Where we using this method? Maybe remove it?
    public function strip_tags_content($text, $tags = '', $invert = false)
    {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == false) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == false) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    /**
     * Getting breadcrumb title
     * @param $page
     * @param bool $action
     * @return string
     */
    public function get_breadcrumb_title($page, $action = false)
    {
        $menu_items = $this->get_menu_items();
        $item_key = array_search($page, array_column($menu_items, 'url'), true);
        if ($item_key !== false) {
            $menu_item = $menu_items[$item_key];
            if ($action && isset($menu_item['actions'])) {
                $subitem_actions = $menu_item['actions'];
                $subitem_key = array_search($action, array_column($subitem_actions, 'url'), true);
                if ($subitem_key !== false) {
                    $menu_subitem = $subitem_actions[$subitem_key];
                    return $menu_subitem['title'];
                }

            } else {
                return $menu_item['title'];
            }
        }
        return '';
    }

    /**
     * Create formBuilder from model
     * @param $model
     * @return null|\Symfony\Component\Form\FormInterface
     * @throws \Exception
     */
    public function get_model_form($model)
    {
        if (get_parent_class($model) !== 'tp\includes\TP_Model')
            throw new \Exception('Model must be an instance of tp\includes\TP_Model');


        if (method_exists($model, 'get_form')) {
            return $this->form->createBuilder($model->get_form(), $model)->getForm();
        }
        return null;
    }

    /**
     * Validate form data without making a request
     * @param $model
     * @param $data
     * @return bool
     *
     * Example :
     *      $model = new Models\Example_model();
     *      // Modify form data
     *      $model->example_data = 'new Data';
     *      if ($this->validate_model_form($model, $model->get_data())) {
     *          // Some actions after validating (ex: save new data)
     *      }
     */
    public function validate_model_form($model, $data)
    {
        try {
            $form = $this->get_model_form($model);
            $form->submit($data);
            return $form->isValid();
        } catch (\Exception $e) {
            // Stupidest method to say user or developer that's something went wrong
            $exception_message = addslashes('Exception: ' . $e->getMessage());
            echo("<script>alert('{$exception_message}');</script>");
            return false;
        }
    }

    /**
     * Add some actions before render_controller function will be called
     */
    protected function before_render()
    {
    }

    /**
     * Get form factory
     * @return FormFactory
     * @throws \Exception
     */
    protected function get_form()
    {
        if ($this->templating instanceof Includes\TP_Templating) {
            return $this->templating->form();
        } else {
            throw new \Exception('"templating" property must be an instance of TP_Templating');
        }
    }

    /**
     * Get templating engine
     * @return \Twig_Environment
     * @throws \Exception
     */
    protected function get_template()
    {
        if ($this->templating instanceof Includes\TP_Templating) {
            return $this->templating->template();
        } else {
            throw new \Exception('"templating" property must be an instance of TP_Templating');
        }
    }

    /**
     * Get TP_Templating instance
     * @return bool|Includes\TP_Templating
     */
    protected function get_templating_instance()
    {
        if (!$this->templating) {
            $this->templating = new Includes\TP_Templating('ru');;
        }
        return $this->templating;
    }


    public function _render($name, $params = [])
    {
        return Yii::$app->getView()->render($name, $params, $this);
    }
}