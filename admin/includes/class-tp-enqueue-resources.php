<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/6/2017
 * Time: 6:48 PM
 */

namespace tp\admin\includes;

use tp\includes\base\ITP_Enqueue_Resources;
use tp\includes\base\TP_Base_Setup_Hooks;
use tp\includes;

class TP_Enqueue_Resources extends TP_Base_Setup_Hooks implements ITP_Enqueue_Resources
{
    protected $webpack_assets;

    public function init()
    {
        $this->webpack_assets = new includes\Tp_Webpack(TP_PLUGIN_ADMIN_SCRIPTS_PATH . 'stats.json');
        return parent::init();
    }

    protected function define_hooks()
    {
        // TODO: Implement define_hooks() method.
        add_action('admin_enqueue_scripts', array($this, 'enqueue_script'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_style'));
        add_action('admin_head', array($this, 'set_custom_webpack_path'));
    }

    public function enqueue_script($hook)
    {
        // TODO: Implement enqueue_script() method.
        wp_enqueue_script(
            TP_PLUGIN_TEXTDOMAIN . '/manifest.js',
            TP_PLUGIN_ADMIN_SCRIPTS_URL . 'manifest.js',
            array(
                'jquery',
                'jquery-ui-core',
                'jquery-ui-sortable'
            ),
            TP_PLUGIN_RESOURCES_VER,
            true
        );

        $main_assets = $this->get_webpack_assets()->get_assets_by_name('main', TP_PLUGIN_ADMIN_DIST_URL, 'js');
        foreach ($main_assets as $main_asset) {
            wp_enqueue_script(
                TP_PLUGIN_TEXTDOMAIN . basename($main_asset),
                $main_asset,
                array(),
                TP_PLUGIN_RESOURCES_VER,
                true
            );
        }


    }

    public function enqueue_style($hook)
    {
        $main_assets = $this->get_webpack_assets()->get_assets_by_name('main', TP_PLUGIN_ADMIN_DIST_URL, 'css');
        foreach ($main_assets as $main_asset) {
            wp_enqueue_style(
                TP_PLUGIN_TEXTDOMAIN . basename($main_asset),
                $main_asset,
                array(),
                TP_PLUGIN_RESOURCES_VER
            );
        }
    }


    function set_custom_webpack_path()
    {
        // TODO: Implement set_custom_webpack_path() method.
        $path = get_home_url();
        $admin_path = wp_normalize_path(str_replace($path, '', TP_PLUGIN_ADMIN_DIST_URL));
        echo '<script type="text/javascript">var TP_WEBPACK_PUBLIC_PATH = "..' . $admin_path . '";</script>';
    }

    /**
     * Get Tp_Webpack instance
     * @return includes\Tp_Webpack
     */
    protected function get_webpack_assets()
    {
        return $this->webpack_assets;
    }

}