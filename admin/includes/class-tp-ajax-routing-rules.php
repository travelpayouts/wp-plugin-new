<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes;


class TP_Ajax_Routing_Rules
{
    public $rules = array(
        array('GET', '/modal/shortcode/flights-tickets', 'TP_Ajax_tickets/modal_data'),
        array('POST', '/shortcode/flights-tickets', 'TP_Ajax_tickets/get_shortcode'),
        array('GET', '/modal/shortcode/hotels', 'TP_Ajax_Hotels/modal_data'),
        array('GET', '/hotels/types', 'TP_Ajax_Hotels/get_hotels_type'),
        array('POST', '/shortcode/hotels', 'TP_Ajax_Hotels/get_shortcode'),
        array('GET', '/modal/shortcode/railway', 'TP_Ajax_Railway/modal_data'),
        array('POST', '/shortcode/railway', 'TP_Ajax_Railway/get_shortcode'),
        array('GET', '/modal/shortcode/widgets', 'TP_Ajax_Widgets/modal_data'),
        array('POST', '/shortcode/widgets/validate', 'TP_Ajax_Widgets/async_validation'),
        array('POST', '/shortcode/widgets', 'TP_Ajax_Widgets/get_shortcode'),

        array('GET', '/autocomplete/airlines', 'TP_Ajax_Autocomplete/airlines'),
        array('GET', '/modal/shortcode/search-forms', 'TP_Ajax_Form/modal_data'),
        array('POST', '/shortcode/search-forms', 'TP_Ajax_Form/get_shortcode'),
        array('GET', '/modal/shortcode/link', 'TP_Ajax_Link/modal_data'),
        array('POST', '/shortcode/link', 'TP_Ajax_Link/get_shortcode'),

    );

    public function get_rules()
    {
        return $this->rules;
    }
}