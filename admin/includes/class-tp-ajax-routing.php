<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes;

use FastRoute;

class TP_Ajax_Routing
{
    const CONTROLLER_NAMESPACE = 'tp\admin\controllers\ajax\\';
    public $page = false;


    public function route()
    {
        set_exception_handler(array($this, 'exception_handler'));
        set_error_handler(array($this, 'error_handler'));

        if (isset($_GET['page'])) {
            $this->page = $_GET['page'];
        }
        $this->set_routing();
        wp_die();
    }

    protected function set_routing()
    {
        $dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
            foreach ($this->rules() as $rule) {
                if (count($rule) !== 3) {
                    throw new \Exception('Cannot find all needed routing params');
                }
                $r->addRoute($rule[0], $rule[1], $rule[2]);
            }
        });

        $http_method = $_SERVER['REQUEST_METHOD'];
        $route_info = $dispatcher->dispatch($http_method, $this->page);
        $route_info[] = $http_method;
        switch ($route_info[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
                throw new \Exception('Route has been not found');
                break;
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                throw new \Exception('This method is not allowed');
                break;
            case FastRoute\Dispatcher::FOUND:
                $route_info = $this->prepare_route_info($route_info);
                return $this->get_controller_action($route_info);
                break;
        }
    }


    protected function rules()
    {
        $rulesClass = new TP_Ajax_Routing_Rules();
        return $rulesClass->get_rules();
    }


    public function get_controller_action($route_info)
    {
        $controller = new $route_info['class_name']($route_info['params']);
        if ($controller) {
            if (method_exists($controller, $route_info['action_name'])) {
                $method_params = $this->get_method_argument_params($controller, $route_info['action_name']);
                $route_params = $route_info['params'];
                $filtered_params = array();

                foreach ($method_params as $method_param) {
                    if (isset($route_params[$method_param['name']])) {
                        $filtered_params[] = $route_params[$method_param['name']];
                    } else {
                        if ($method_param['required'] === false) {
                            $filtered_params[] = $method_param['default_value'];
                        } else {
                            throw new \Exception('Not found all required arguments');
                        }
                    }
                }
                return call_user_func_array(array($controller, $route_info['action_name']), $filtered_params);
            } else {
                throw new \Exception('Cannot find controller action');
            }
        }
        return false;
    }


    public function exception_handler($exception)
    {
        $data = array(
            'messages' => array(
                $exception->getMessage()
            )
        );
        $this->response($data, 'error');
    }

    public function error_handler($errno, $errstr, $errfile, $errline)
    {
        if ($errno == E_NOTICE || $errno == E_WARNING) {
            $data = array(
                'messages' => array(
                    $errno . ' ' . $errstr
                )
            );
            $this->response($data, 'error');
        }
    }

    public
    function response($data, $status = 'ok')
    {
        $data['status'] = $status;
        echo json_encode($data);
        wp_die();
    }

    protected function get_method_argument_params($class_name, $func_name)
    {
        $method = new \ReflectionMethod($class_name, $func_name);
        $result = array();
        foreach ($method->getParameters() as $param) {
            $result[$param->name] = array(
                'name' => $param->name,
                'required' => !$param->isOptional(),
                'default_value' => $param->isOptional() ? $param->getDefaultValue() : ''
            );
        }
        return $result;
    }

    protected function prepare_route_info($route_info)
    {
        $method_params = $this->get_request_data_by_method($route_info[3]);
        $data = array(
            'class_name' => '',
            'action_name' => 'action_index',
            'params' => array_merge($method_params, $route_info[2]),
            'method' => $route_info[3]
        );

        $handler_data = explode('/', $route_info[1]);
        if (count(array_filter($handler_data)) > 0) {
            if (isset($handler_data[0])) {
                $data['class_name'] = self::CONTROLLER_NAMESPACE . $handler_data[0] . '_Controller';
            }
            if (isset($handler_data[1])) {
                $data['action_name'] = 'action_' . $handler_data[1];
            }
        }

        if (empty($data['class_name']) || !class_exists($data['class_name'])) {
            throw new \Exception('Cannot find controller class');
        }
        return $data;
    }

    protected function get_request_data_by_method($method)
    {
        switch ($method) {
            case 'GET':
                return $_GET;
                break;
            case 'POST':
                $data = file_get_contents('php://input');
                return array_merge($_POST, json_decode($data, true));
                break;
            case 'PUT':
                $data = file_get_contents('php://input');
                return json_decode($data, true);
            case 'DELETE':
                return $_GET;
            default:
                return $_GET;
        }

    }
}