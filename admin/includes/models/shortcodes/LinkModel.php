<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

/**
 * Class LinkModel
 * @package tp\admin\includes\models\shortcodes
 * @property $types;
 */
class LinkModel extends BaseModel
{
    const shortcode_link = 'tp_link';
    const TYPE_FLIGHTS = 1;
    const TYPE_HOTELS = 2;

    public $text_link;
    public $origin;
    public $destination;
    public $subid;
    public $departure_date = 1;
    public $return_date = 12;
    public $one_way;
    public $city;
    public $check_in = 1;
    public $check_out = 12;
    public $type;

    public function init()
    {
        $this->current_table = self::shortcode_link;

        $this->shortcodeAttributes = [
            self::shortcode_link => [
                'shortcode' => '$model::shortcode_link',
                'origin' => '$model->type === $model::TYPE_FLIGHTS ? $mappedAttributes->get($attribute) : null',
                'destination' => '$model->type === $model::TYPE_FLIGHTS ? $mappedAttributes->get($attribute) : null',
                'text_link',
                'origin_date' => '$model->type === $model::TYPE_FLIGHTS ? $mappedAttributes->get("departure_date") : null',
                'destination_date' => '$model->type === $model::TYPE_FLIGHTS ? $mappedAttributes->get("return_date") : null',
                'one_way',
                'type',
                'subid',
                'hotel_id' => '$model->type === $model::TYPE_HOTELS ? "locationId=".$model->getArrayAttributeData("city","id") : null',
                'check_in' => '$model->type === $model::TYPE_HOTELS ? $model->$attribute : null',
                'check_out' => '$model->type === $model::TYPE_HOTELS ? $model->$attribute : null',
            ],
        ];
    }

    public function rules()
    {
        $rules = [
            [$this->attributes(), 'safe'],
            [['text_link', 'type'], 'required'],
            [['origin', 'destination', 'departure_date', 'return_date'], 'required', 'when' => function (LinkModel $model) {
                return $model->type == self::TYPE_FLIGHTS;
            }],
            [['city', 'check_in', 'check_out'], 'required', 'when' => function (LinkModel $model) {
                return $model->type == self::TYPE_HOTELS;
            }],
            ['type', 'in', 'range' => array_keys($this->types)],
            [['departure_date', 'return_date', 'check_in', 'check_out'], 'integer'],
            [['one_way'], 'boolean']
        ];

        return array_merge(parent::rules(), $rules);
    }


    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [

        ]);
    }

    public function getTypes()
    {
        return [
            self::TYPE_FLIGHTS => _x('Search for flights',
                'tp_admin_page_settings_сonstructor_link_field_type_link_value_1_label', TP_PLUGIN_TEXTDOMAIN),
            self::TYPE_HOTELS => _x('Search for hotels',
                'tp_admin_page_settings_сonstructor_link_field_type_link_value_2_label', TP_PLUGIN_TEXTDOMAIN),
        ];
    }

    public function mapping()
    {
        return [
            'origin' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'destination' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
        ];
    }
}