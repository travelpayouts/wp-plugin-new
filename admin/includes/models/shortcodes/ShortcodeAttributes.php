<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

use yii\base\Model;
use Adbar\Dot;

/**
 * Class ShortcodeAttributes
 * @package tp\admin\includes\models\shortcodes
 * @property $params
 */
class ShortcodeAttributes extends Model
{
    public $shortcode;
    protected $_params;

    public function setParams($values)
    {
        if (isset($values['shortcode'])) {
            $this->shortcode = $values['shortcode'];
            unset($values['shortcode']);
        } elseif (isset($values['current_table'])) {
            $this->shortcode = $values['current_table'];
            unset($values['current_table']);
        }

        $mappedValues = array_map(function ($value) {
            if (is_bool($value))
                $value = $value === true ? 'true' : 'false';
            return $value;
        }, $values);


        $this->_params = array_filter($mappedValues, function ($param) {
            return $param !== null;
        });
    }

    public function getParams()
    {
        return new Dot($this->_params);
    }
}