<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

use tp\admin\includes\models\TP_Model_Search_Form;
use Exception;

class FormModel extends BaseModel
{
    const shortcode_form = 'tp_search_shortcodes';

    public $search_form;
    public $origin;
    public $destination;
    public $subid;

    public function init()
    {
        $this->current_table = self::shortcode_form;
        $this->shortcodeAttributes = [
            self::shortcode_form => [
                'slug' => '$model->slugAttribute',
                'origin',
                'destination',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ]
        ];
    }


    public function mapping()
    {
        return array(
            'origin' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'destination' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
        );
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [

        ]);
    }

    public function rules()
    {
        $rules = [
            [$this->attributes(), 'safe'],
            [['origin', 'destination', 'search_form'], 'required'],
            [['search_form'], 'number'],
            [['origin', 'destination'], 'arrayHasValueValidator', 'params' => ['path' => 'iata']],
        ];
        return array_merge(parent::rules(), $rules);
    }

    public function getSlugAttribute()
    {
        if ($this->search_form) {
            $model = TP_Model_Search_Form::find()
                ->select(['id', 'slug'])
                ->where('id=:id')
                ->params([':id' => (int)$this->search_form])
                ->one();
            if (!$model) throw new Exception('Can\'t find shortcode form');
            return $model->slug !== null ? $model->slug : '';
        }

        return '';
    }
}