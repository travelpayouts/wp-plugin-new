<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

class RailwayModel extends BaseModel
{
    const shortcode_tutu = 'tp_tutu';

    public $title;
    public $origin;
    public $destination;
    public $subid;
    public $paginate = true;
    public $off_title = false;

    public function init()
    {
        $this->current_table = self::shortcode_tutu;

        $this->shortcodeAttributes = [
            self::shortcode_tutu => [
                'shortcode' => '$model::shortcode_tutu',
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'off_title',
                'subid',
            ],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[

        ]);
    }

    public function rules()
    {
        $rules = [
            [$this->attributes(), 'safe'],
            [['origin', 'destination'], 'required'],
        ];
        return $rules;
    }

    public function mapping()
    {
        return [
            'origin' => [
                'accessor' => '$model->getCityAttribute($attribute)',
            ],
            'destination' => [
                'accessor' => '$model->getCityAttribute($attribute)',
            ],
            'paginate' => [
                'accessor' => '$model->$attribute ? "true": "false"',
            ],
            'off_title' => [
                'accessor' => '$model->$attribute ? "true": "false"',
            ],
        ];

    }

    public function getCityAttribute($attribute_name)
    {
        $attribute = $this->$attribute_name;
        if ($attribute && is_array($attribute) && isset($attribute['id'])) {
            return $attribute['id'];
        }
        return null;
    }

}