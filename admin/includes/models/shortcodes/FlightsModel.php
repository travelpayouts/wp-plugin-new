<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

use tp\includes\Options;
use tp\includes\TP_Currency_Utils;

/**
 * Class FlightsModel
 * @package tp\admin\includes\models\shortcodes
 * @property $fieldRules
 * @property $table_list
 * @property $currency_list
 * @property $trip_class_list
 * @property $stops_number
 * @property $table_fields
 * @property $mappedAttributes
 */
class FlightsModel extends BaseModel
{
    const shortcode_price_calendar_month = 'tp_price_calendar_month_shortcodes';
    const shortcode_price_calendar_week = 'tp_price_calendar_week_shortcodes';
    const shortcode_cheapest_flights = 'tp_cheapest_flights_shortcodes';
    const shortcode_cheapest_ticket_each_day_month = 'tp_cheapest_ticket_each_day_month_shortcodes';
    const shortcode_cheapest_tickets_each_month = 'tp_cheapest_tickets_each_month_shortcodes';
    const shortcode_direct_flights_route = 'tp_direct_flights_route_shortcodes';
    const shortcode_direct_flights = 'tp_direct_flights_shortcodes';
    const shortcode_popular_routes_from_city = 'tp_popular_routes_from_city_shortcodes';
    const shortcode_popular_destinations_airlines = 'tp_popular_destinations_airlines_shortcodes';
    const shortcode_our_site_search = 'tp_our_site_search_shortcodes';
    const shortcode_from_our_city_fly = 'tp_from_our_city_fly_shortcodes';
    const shortcode_in_our_city_fly = 'tp_in_our_city_fly_shortcodes';
    const shortcode_special_offer = 'tp_special_offer_shortcodes';

    public $title;
    public $origin;
    public $destination;
    public $country;
    public $airline;
    public $subid;
    public $flight_number;
    public $currency;
    public $limit = 100;
    public $paginate;
    public $one_way;
    public $off_title;
    public $stops = 0;

    public function init()
    {
        $this->shortcodeAttributes = [
            self::shortcode_price_calendar_month => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'currency',
                'off_title',
                'stops',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_price_calendar_week => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'currency',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_cheapest_flights => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'currency',
                'filter_airline' => '$mappedAttributes->get("airline")',
                'filter_flight_number' => '""',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_cheapest_ticket_each_day_month => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'stops',
                'subid',
                'currency',
                'filter_airline' => '$mappedAttributes->get("airline")',
                'filter_flight_number' => '""',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_cheapest_tickets_each_month => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'currency',
                'filter_airline' => '$mappedAttributes->get("airline")',
                'filter_flight_number' => '""',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_direct_flights_route => [
                'origin',
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'currency',
                'filter_airline' => '$mappedAttributes->get("airline")',
                'filter_flight_number' => '""',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_direct_flights => [
                'origin',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'limit',
                'paginate',
                'subid',
                'currency',
                'filter_airline' => '$mappedAttributes->get("airline")',
                'filter_flight_number' => '""',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_popular_routes_from_city => [
                'origin',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'paginate',
                'subid',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_popular_destinations_airlines => [
                'airline',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'limit',
                'paginate',
                'subid',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_our_site_search => [
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_from_our_city_fly => [
                'origin',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_in_our_city_fly => [
                'destination',
                'title' => '$model->off_title? "" : $mappedAttributes->get($attribute)',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
                'shortcode' => '$mappedAttributes->get("current_table")',
                'theme' => '"default"',
            ],
            self::shortcode_special_offer => [
                'theme' => '"default"',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ]
        ];

        $this->currency = Options::instance()->get('settings/localization_currency', 1);
    }

    public function rules()
    {
        $safe_attributes = $this->attributes();

        $rules = array_merge(parent::rules(), [
            [$safe_attributes, 'safe'],
            ['stops', 'in', 'range' => array_keys($this->stops_number)],
            ['limit', 'integer', 'min' => 1, 'max' => 300],
            [['paginate', 'one_way', 'off_title'], 'boolean'],
            [['limit', 'stops', 'currency'], 'required', 'when' => function (FlightsModel $model, $attribute) use ($safe_attributes) {
                return in_array($attribute, $safe_attributes, true);
            }]
        ]);
        return array_merge($rules, $this->getRulesByCurrentTable());
    }

    public function mapping()
    {
        return array(
            'origin' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'destination' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'currency' => array(
                'accessor' => '$model->getCurrencyAttribute()',
            ),
            'paginate' => array(
                'accessor' => '$model->$attribute === true',
            ),
            'one_way' => array(
                'accessor' => '$model->$attribute === true',
            ),
            'off_title' => array(
                'accessor' => '$model->$attribute === true',
            ),
            'title' => array(
                'accessor' => '!$model->off_title ? $model->$attribute: null',
            ),
            'airline' => array(
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'country' => _x('Country', 'tp_admin_page_settings_сonstructor_tables_field_country_label', TP_PLUGIN_TEXTDOMAIN),
            'airline' => _x('Filter by airline', 'tp_admin_page_settings_сonstructor_tables_field_filter_airline_label', TP_PLUGIN_TEXTDOMAIN),
            'flight_number' => _x('Filter by flight # (enter manually)', 'tp_admin_page_settings_сonstructor_tables_field_filter_flight_number_label', TP_PLUGIN_TEXTDOMAIN),
            'limit' => _x('Limit', 'tp_admin_page_settings_сonstructor_tables_field_limit_label', TP_PLUGIN_TEXTDOMAIN),
            'paginate' => _x('Paginate', 'tp_plugin_widget_form_field_paginate_label', TP_PLUGIN_TEXTDOMAIN),
            'stops' => _x('Number of stops', 'tp_plugin_widget_form_field_stops_label', TP_PLUGIN_TEXTDOMAIN),
        ]);
    }

    protected function getRulesByCurrentTable()
    {
        switch ($this->current_table) {
            case self::shortcode_price_calendar_month:
            case self::shortcode_price_calendar_week:
            case self::shortcode_cheapest_flights:
            case self::shortcode_cheapest_ticket_each_day_month:
            case self::shortcode_cheapest_tickets_each_month:
            case self::shortcode_direct_flights_route:
                $rules = [
                    [['origin', 'destination'], 'required']
                ];
                break;
            case self::shortcode_direct_flights:
            case self::shortcode_popular_routes_from_city:
            case self::shortcode_from_our_city_fly:
                $rules = [
                    [['origin'], 'required']
                ];
                break;
            case self::shortcode_popular_destinations_airlines:
                $rules = [
                    [['airline'], 'required']
                ];
                break;
            case self::shortcode_in_our_city_fly:
                $rules = [
                    [['destination'], 'required']
                ];
                break;
            default:
                $rules = [];
        }
        return $rules;
    }

    public function getCurrencyAttribute()
    {
        if ($this->currency) {
            $currency_list = $this->currency_list;
            if (array_key_exists($this->currency, $currency_list))
                return $currency_list[$this->currency];
        }
        return null;
    }

    public function getTrip_class_list()
    {
        return [
            0 => _x('Economy', 'tp plugin local en trip class economy'),
            1 => _x('Business', 'tp plugin local en trip class business'),
            2 => _x('First', 'tp plugin local ru trip class first')
        ];
    }

    public function getStops_number()
    {
        return [
            0 => _x('All', 'tp_plugin_widget_form_field_stops_value_0_label'),
            1 => _x('No more than one stop', 'tp_plugin_widget_form_field_stops_value_1_label'),
            2 => _x('Direct', 'tp_plugin_widget_form_field_stops_value_2_label')
        ];
    }

    public function getCurrency_list()
    {
        return TP_Currency_Utils::get_all();
    }

    /**
     * Set which components will be shown on each table
     * @return array
     */
    public function getTable_fields()
    {
        $fields = array(
            self::shortcode_price_calendar_month => array(
                'title',
                'origin',
                'destination',
                'subid',
                'currency',
                'paginate',
                'off_title',
                'stops',
            ),
            self::shortcode_price_calendar_week => array(
                'title',
                'origin',
                'destination',
                'subid',
                'currency',
                'paginate',
                'off_title',
            ),
            self::shortcode_cheapest_flights => array(
                'title',
                'origin',
                'destination',
                'airline',
                'subid',
                'flight_number',
                'currency',
                'paginate',
                'off_title',
            ),
            self::shortcode_cheapest_ticket_each_day_month => array(
                'title',
                'origin',
                'destination',
                'airline',
                'subid',
                'flight_number',
                'currency',
                'paginate',
                'off_title',
                'stops',
            ),
            self::shortcode_cheapest_tickets_each_month => array(
                'title',
                'origin',
                'destination',
                'subid',
                'airline',
                'flight_number',
                'currency',
                'paginate',
                'off_title',
            ),
            self::shortcode_direct_flights_route => array(
                'title',
                'origin',
                'destination',
                'subid',
                'airline',
                'flight_number',
                'currency',
                'paginate',
                'off_title',
            ),
            self::shortcode_direct_flights => array(
                'title',
                'origin',
                'subid',
                'airline',
                'flight_number',
                'currency',
                'limit',
                'paginate',
                'off_title',
            ),
            self::shortcode_popular_routes_from_city => array(
                'title',
                'origin',
                'subid',
                'paginate',
                'off_title',
            ),
            self::shortcode_popular_destinations_airlines => array(
                'title',
                'subid',
                'airline',
                'limit',
                'paginate',
                'off_title',
            ),
            self::shortcode_our_site_search => array(
                'title',
                'subid',
                'currency',
                'limit',
                'paginate',
                'one_way',
                'off_title',
                'stops',
            ),
            self::shortcode_from_our_city_fly => array(
                'title',
                'origin',
                'subid',
                'currency',
                'limit',
                'paginate',
                'one_way',
                'off_title',
                'stops',
            ),
            self::shortcode_in_our_city_fly => array(
                'title',
                'destination',
                'subid',
                'currency',
                'limit',
                'paginate',
                'one_way',
                'off_title',
                'stops',
            ),
            self::shortcode_special_offer => array(
                'title',
                'origin',
                'destination',
                'country',
                'subid',
                'airline',
                'flight_number',
                'currency',
                'limit',
                'paginate',
                'one_way',
                'off_title',
                'stops',
            )
        );
        return $fields;
    }

    /**
     * List with all tables
     * @return array
     */
    public function getTable_list()
    {
        $tables_list_rub = array(
            self::shortcode_price_calendar_month => _x('Flights from origin to destination, One Way (next month)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_1', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_price_calendar_week => _x('Flights from Origin to Destination (next few days)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_2', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_flights => _x('Cheapest Flights from origin to destination, Round-trip',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_3', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_ticket_each_day_month => _x('Cheapest Flights from origin to destination (next month)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_4', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_tickets_each_month => _x('Cheapest Flights from origin to destination (next year)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_5', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_direct_flights_route => _x('Direct Flights from origin to destination',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_6', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_direct_flights => _x('Direct Flights from origin',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_7', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_popular_routes_from_city => _x('Popular Destinations from origin',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_8', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_popular_destinations_airlines => _x('Most popular flights within this Airlines',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_9', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_our_site_search => _x('Searched on our website',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_11', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_from_our_city_fly => _x('Cheap Flights from origin',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_12', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_in_our_city_fly => _x('Cheap Flights to destination',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_13', TP_PLUGIN_TEXTDOMAIN),
        );

        $tables_list_general = array(
            self::shortcode_price_calendar_month => _x('Flights from origin to destination, One Way (next month)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_1', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_price_calendar_week => _x('Flights from Origin to Destination (next few days)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_2', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_flights => _x('Cheapest Flights from origin to destination, Round-trip',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_3', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_ticket_each_day_month => _x('Cheapest Flights from origin to destination (next month)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_4', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_cheapest_tickets_each_month => _x('Cheapest Flights from origin to destination (next year)',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_5', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_direct_flights_route => _x('Direct Flights from origin to destination',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_6', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_direct_flights => _x('Direct Flights from origin',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_7', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_popular_routes_from_city => _x('Most popular flights within this Airlines',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_9', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_popular_destinations_airlines => _x('Searched on our website',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_11', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_our_site_search => _x('Cheap Flights from origin',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_12', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_from_our_city_fly => _x('Cheap Flights to destination',
                'tp_admin_page_settings_сonstructor_tables_field_select_table_value_13', TP_PLUGIN_TEXTDOMAIN),
        );
        //@TODO Check current currency
        return $tables_list_rub;
    }
}