<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

use DateTime;
use Adbar\Dot;

/**
 * Class HotelsModel
 * @package tp\admin\includes\models\shortcodes
 * @property $table_list
 * @property $table_fields
 * @property $hotel_types
 * @property $mappedAttributes
 */
class HotelsModel extends BaseModel
{
    const shortcode_discount = 'tp_hotels_selections_discount_shortcodes';
    const shortcode_date = 'tp_hotels_selections_date_shortcodes';

    public $title;
    public $city;
    public $city_label;
    public $subid;
    public $check_in;
    public $check_out;
    public $number_results;
    public $paginate = true;
    public $off_title = false;
    public $link_without_dates = false;
    public $type_selections;
    public $type_selections_label;

    public function init()
    {
        $this->shortcodeAttributes = [
            self::shortcode_date => [
                'city',
                'title',
                'paginate',
                'off_title',
                'type_selections',
                'city_label',
                'number_results',
                'subid',
                'check_in',
                'check_out',
                'type_selections_label',
                'link_without_dates',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_discount => [
                'city',
                'title',
                'paginate',
                'off_title',
                'number_results',
                'subid',
                'city_label',
                'type_selections_label',
                'link_without_dates',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
        ];

        $check_date = new DateTime();
        $this->check_in = $check_date->format('d-m-Y');
        $this->check_out = $check_date->modify('+3 day')->format('d-m-Y');
        $this->number_results = 20;

    }

    public function rules()
    {
        $check_date = new DateTime();
        $safe_attributes = $this->attributes();
        $rules = array_merge(parent::rules(), [
            [$safe_attributes, 'safe'],
            ['number_results', 'integer', 'min' => 1, 'max' => 300],
            [['paginate', 'link_without_dates', 'off_title'], 'boolean'],
            [['check_in', 'check_out'], 'datetime', 'format' => 'php:d-m-Y'],
            [['check_in', 'check_out'], 'datetime', 'format' => 'php:d-m-Y',
                'min' => $check_date->format('d-m-Y'),
                'max' => $check_date->modify('+60 days')->format('d-m-Y'),
            ],
            [['check_in', 'check_out', 'number_results'], 'required', 'when' => function (HotelsModel $model, $attribute) use ($safe_attributes) {
                return in_array($attribute, $safe_attributes, true);
            }]
        ]);
        return array_merge($rules, $this->getRulesByCurrentTable());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'city' => _x('City', 'tp_admin_page_settings_сonstructor_hotels_tables_field_city_label', TP_PLUGIN_TEXTDOMAIN),
            'city_label' => _x('City label', 'tp_admin_page_settings_сonstructor_hotels_tables_field_city_label', TP_PLUGIN_TEXTDOMAIN),
            'type_selections' => _x('Selection type', 'tp_admin_page_settings_сonstructor_hotels_tables_field_hotels_selections_type_label', TP_PLUGIN_TEXTDOMAIN),
            'type_selections_label' => _x('Selection type label', 'tp_admin_page_settings_сonstructor_hotels_tables_field_hotels_selections_type_label', TP_PLUGIN_TEXTDOMAIN),
            'check_in' => _x('Check-in', 'tp_admin_page_settings_сonstructor_hotels_tables_field_check_in_label', TP_PLUGIN_TEXTDOMAIN),
            'check_out' => _x('Check-out', 'tp_admin_page_settings_сonstructor_hotels_tables_field_check_out_label', TP_PLUGIN_TEXTDOMAIN),
            'number_results' => _x('Number of results', 'tp_admin_page_settings_сonstructor_hotels_tables_field_number_results_label', TP_PLUGIN_TEXTDOMAIN),
            'link_without_dates' => _x('Land without dates', 'tp admin page settings сonstructor hotels tables field hotels_link_without_dates label', TP_PLUGIN_TEXTDOMAIN),
        ]);
    }

    public function mapping()
    {
        return [
            'city' => [
                'accessor' => '$model->getArrayAttributeData("city","id")',
            ],
            'city_label' => [
                'accessor' => '$model->getArrayAttributeData("city","city")',
            ],
            'off_title' => [
                'accessor' => '$model->$attribute === true',
            ],
            'paginate' => [
                'accessor' => '$model->$attribute === true',
            ],
            'link_without_dates' => [
                'accessor' => '$model->$attribute === true',
            ],
            'type_selections_label' => [
                'accessor' => '$model->getTypeSelectionsLabel()',
            ],
        ];
    }

    public function getTypeSelectionsLabel()
    {
        $type_selection_attribute = $this->type_selections;
        if ($type_selection_attribute && is_string($type_selection_attribute)) {
            $type_selection_values = array_filter($this->hotel_types, function ($hotel_type) use ($type_selection_attribute) {
                return isset($hotel_type['value']) && $hotel_type['value'] === $type_selection_attribute;
            });
            if (is_array($type_selection_values)) {
                $type_selection_value = array_shift($type_selection_values);
                return isset($type_selection_value['label']) ? $type_selection_value['label'] : '';
            }
        }
        return '';
    }

    /**
     * List with all tables
     * @return array
     */
    protected function getTable_list()
    {
        $tables_list = array(
            self::shortcode_discount => _x('Hotels collection - Discounts',
                'tp_admin_page_settings_сonstructor_hotels_tables_field_select_table_value_1', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_date => _x('Hotels collections for dates',
                'tp_admin_page_settings_сonstructor_hotels_tables_field_select_table_value_2', TP_PLUGIN_TEXTDOMAIN),
        );


        return $tables_list;
    }

    /**
     * Set which components will be shown on each table
     * @return array
     */
    protected function getTable_fields()
    {
        $fields = array(
            self::shortcode_discount => array(
                'title',
                'city',
                'subid',
                'type_selections',
                'number_results',
                'paginate',
                'off_title',
                'link_without_dates',
            ),
            self::shortcode_date => array(
                'title',
                'city',
                'subid',
                'type_selections',
                'check_in',
                'check_out',
                'number_results',
                'paginate',
                'off_title',
                'link_without_dates',
                'help_text',
            )
        );
        return $fields;
    }

    protected function getHotel_types()
    {
        $data = [
            [
                'label' => _x('Top hotels', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'tophotels',
            ],
            [
                'label' => _x('Popularity', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'popularity',
            ],
            [
                'label' => _x('Cheap (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'price',
            ],
            [
                'label' => _x('Distance', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'distance',
            ],
            [
                'label' => _x('Rating', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'rating',
            ],
            [
                'label' => _x('0 stars', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '0stars',
            ],
            [
                'label' => _x('1 star (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '1stars',
            ],
            [
                'label' => _x('2 stars (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '2stars',
            ],
            [
                'label' => _x('3 stars (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '3stars',
            ],
            [
                'label' => _x('4 stars (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '4stars',
            ],
            [
                'label' => _x('5 stars (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '5stars',
            ],
            [
                'label' => _x('1 star', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '1-stars',
            ],
            [
                'label' => _x('2 stars', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '2-stars',
            ],
            [
                'label' => _x('3 stars', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '3-stars',
            ],
            [
                'label' => _x('4 stars', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '4-stars',
            ],
            [
                'label' => _x('5 stars', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => '5-stars',
            ],
            [
                'label' => _x('Luxury', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'luxury',
            ],
            [
                'label' => _x('Expensive (auto)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'highprice',
            ],
            [
                'label' => _x('Hotels in the center', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'center',
            ],
            [
                'label' => _x('Pool', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'pool',
            ],
            [
                'label' => _x('Gay friendly', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'title' => 'Gay friendly',
                'value' => 'gay',
            ],
            [
                'label' => _x('Smoking friendly', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'smoke',
            ],
            [
                'label' => _x('Restaurant', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'restaurant',
            ],
            [
                'label' => _x('Pet friendly', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'pets',
            ],
            [
                'label' => _x('Russian guests', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'russians',
            ],
            [
                'label' => _x('Sea view', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'sea_view',
            ],
            [
                'label' => _x('Lake view', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'lake_view',
            ],
            [
                'label' => _x('River view', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'river_view',
            ],
            [
                'label' => _x('Panoramic view', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'panoramic_view',
            ],
            [
                'label' => _x('Cheap hotels (manual)', 'tp hotels type', TP_PLUGIN_TEXTDOMAIN),
                'value' => 'cheaphotel',
            ],
        ];
        return $data;
    }

    protected function getRulesByCurrentTable()
    {
        switch ($this->current_table) {
            case self::shortcode_date:
                $rules = [
                    [[
                        'city',
                        'type_selections',
                        'check_in',
                        'check_out'], 'required'],
                    [['city_label', 'type_selections_label'], 'safe'],
                ];
                break;
            case self::shortcode_discount:
                $rules = [
                    [['city', 'type_selections'], 'required'],
                    [['city_label', 'type_selections_label'], 'safe'],
                ];
                break;
            default:
                $rules = [];
        }
        return $rules;
    }

}