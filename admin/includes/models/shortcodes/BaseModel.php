<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;

use yii\base\Model;
use Adbar\Dot;

/**
 * Class BaseModel
 * @package tp\admin\includes\models\shortcodes
 * @property $mappedAttributes
 * @property $shortcodeData
 * @property $shortcodeAttributes
 */
class BaseModel extends Model
{
    public $current_table;
    protected $_shortcodeAttributes = [];

    public function behaviors()
    {
        return [
            'modelMap' => 'tp\includes\behaviors\ModelMapBehavior',
        ];
    }

    public function rules()
    {
        return [
            ['current_table', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => _x('Alternate title', 'tp_admin_page_settings_сonstructor_tables_field_title_label', TP_PLUGIN_TEXTDOMAIN),
            'origin' => _x('Origin', 'tp_admin_page_settings_сonstructor_tables_field_origin_label', TP_PLUGIN_TEXTDOMAIN),
            'destination' => _x('Destination', 'tp_admin_page_settings_сonstructor_tables_field_destination_label', TP_PLUGIN_TEXTDOMAIN),
            'subid' => _x('Subid', 'tp_admin_page_settings_сonstructor_tables_field_subid_label', TP_PLUGIN_TEXTDOMAIN),
            'currency' => _x('Currency', 'tp_admin_page_settings_сonstructor_tables_field_currency_label', TP_PLUGIN_TEXTDOMAIN),
            'limit' => _x('Limit', 'tp_admin_page_settings_сonstructor_tables_field_limit_label', TP_PLUGIN_TEXTDOMAIN),
            'paginate' => _x('Paginate', 'tp_plugin_widget_form_field_paginate_label', TP_PLUGIN_TEXTDOMAIN),
            'one_way' => _x('One Way', 'tp_plugin_widget_form_field_one_way_label', TP_PLUGIN_TEXTDOMAIN),
            'off_title' => _x('No title', 'tp_plugin_widget_form_field_off_title_label', TP_PLUGIN_TEXTDOMAIN),
        ];
    }

    public function eachArrayHasValueValidator($attribute, $params)
    {
        if (!isset($params['path'])) throw new \Exception('You need to set path to validator params');
        $path = $params['path'];

        if ($this->$attribute && is_array($this->$attribute)) {
            $errors = array_map(function ($value) use ($path) {
                $validator_result = $this->arrayHasNotHaveValue($value, $path);
                return $validator_result ? $validator_result : '';
            }, $this->$attribute);
            if (!empty(array_filter($errors)))
                $this->addErrors([$attribute => $errors]);
        }
    }

    public function arrayHasValueValidator($attribute_name, $params)
    {
        if (!isset($params['path'])) throw new \Exception('You need to set path to validator params');
        $validator_result = $this->arrayHasNotHaveValue($this->$attribute_name, $params['path']);
        if ($validator_result)
            $this->addError($attribute_name, $validator_result);
    }


    /**
     * @param $data array
     * @param $path
     * @return bool
     */
    protected function arrayHasNotHaveValue($data, $path)
    {
        if ($data && is_array($data)) {
            $dot_data = new Dot($data);
            if (!$dot_data->has($path))
                return _x('Field has invalid value.', 'tp_validator_message', TP_PLUGIN_TEXTDOMAIN);
            if ($dot_data->isEmpty($path))
                return _x('Field is required.', 'tp_validator_message', TP_PLUGIN_TEXTDOMAIN);
        } else {
            return _x('Field is required.', 'tp_validator_message', TP_PLUGIN_TEXTDOMAIN);
        }
        return false;
    }

    public function getArrayAttributeData($attribute, $path, $defaultValue = null)
    {
        if ($this->$attribute && is_array($this->$attribute)) {
            $attributeData = new Dot($this->$attribute);
            return $attributeData->get($path, $defaultValue);
        }
        return $defaultValue;
    }

    /**
     * @return ShortcodeAttributes
     */
    public function getShortcodeData()
    {
        $shortcodeAttributes = new ShortcodeAttributes();

        $params = [];
        $mappedAttributes = $this->mappedAttributes;
        if (isset($this->shortcodeAttributes[$this->current_table])) {
            $attributes = $this->shortcodeAttributes[$this->current_table];
            foreach ($attributes as $attributeName => $attributeValue) {
                // if attributeName don't have any value set default expression as value
                if (is_numeric($attributeName)) {
                    $attributeName = $attributeValue;
                    $attributeValue = '$mappedAttributes->get($attribute)';
                }

                $params[$attributeName] = $this->evaluateExpression($attributeValue, array(
                    'model' => $this,
                    'attribute' => $attributeName,
                    'mappedAttributes' => new Dot($mappedAttributes),
                ));
            }
        } else {
            $params = $mappedAttributes;
        }
        $shortcodeAttributes->params = $params;
        return $shortcodeAttributes;
    }

    public function setShortcodeAttributes($value)
    {
        if (is_array($value))
            $this->_shortcodeAttributes = array_merge($this->_shortcodeAttributes, $value);
    }

    public function getShortcodeAttributes()
    {
        return $this->_shortcodeAttributes;
    }
}