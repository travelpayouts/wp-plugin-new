<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models\shortcodes;


/**
 * Class WidgetsModel
 * @package tp\admin\includes\models\shortcodes
 * @property $table_list
 * @property $table_fields
 * @property $calendar_periods
 * @property $types
 * @property $widget_types
 * @property $filter_by_types
 */
class WidgetsModel extends BaseModel
{
    const shortcode_map = 'tp_map_widget';
    const shortcode_hotelmap = 'tp_hotelmap_widget';
    const shortcode_calendar = 'tp_calendar_widget';
    const shortcode_subscriptions = 'tp_subscriptions_widget';
    const shortcode_hotel = 'tp_hotel_widget';
    const shortcode_popular_routes = 'tp_popular_routes_widget';
    const shortcode_hotel_selections = 'tp_hotel_selections_widget';
    const shortcode_ducklett = 'tp_ducklett_widget';
    const FILTER_BY_AIRLINES = 0;
    const FILTER_BY_ROUTES = 1;

    public $subid;
    public $origin;
    public $destination;
    public $size_width = 800;
    public $size_height = 500;
    public $hotel_coordinates;
    public $zoom = 12;
    public $calendar_period = 'current_month';
    public $type = 'full';
    public $direct = false;
    public $one_way = false;
    public $responsive = true;
    public $responsive_width = 800;
    public $period_from = 7;
    public $period_to = 14;
    public $hotel_name;
    public $limit = 10;
    public $destination_multiple;
    public $filter_by = 0;
    public $widget_type = 'brickwork';
    public $airlines;
    public $limit_ducklett = 9;
    public $city;
    public $city_categories;

    public function init()
    {
        $this->shortcodeAttributes = [
            self::shortcode_map => [
                'origin',
                'width' => '$mappedAttributes->get("size_width")',
                'height' => '$mappedAttributes->get("size_height")',
                'direct',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_hotelmap => [
                'coordinates' => '$mappedAttributes->get("hotel_coordinates")',
                'width' => '$mappedAttributes->get("size_width")',
                'height' => '$mappedAttributes->get("size_height")',
                'zoom',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_calendar => [
                'origin',
                'destination',
                'direct',
                'one_way',
                'responsive',
                'width' => '$model->responsive? null : $mappedAttributes->get("responsive_width")',
                'subid',
                'period_day_from' => '$mappedAttributes->get("period_from")',
                'period_day_to' => '$mappedAttributes->get("period_to")',
                'period' => '$mappedAttributes->get("calendar_period")',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_subscriptions => [
                'origin',
                'destination',
                'responsive',
                'width' => '$model->responsive? null : $mappedAttributes->get("responsive_width")',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_hotel => [
                'hotel_id' => '$model->getArrayAttributeData("hotel_name","id")',
                'responsive',
                'width' => '$model->responsive? null : $mappedAttributes->get("responsive_width")',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_popular_routes => [
                'destination',
                'responsive',
                'subid',
                'width' => '$model->responsive? null : $mappedAttributes->get("responsive_width")',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_hotel_selections => [
                'id' => '$model->getArrayAttributeData("city","id")',
                'cat1' => '$model->getArrayAttributeData("city_categories","0.value")',
                'cat2' => '$model->getArrayAttributeData("city_categories","1.value")',
                'cat3' => '$model->getArrayAttributeData("city_categories","2.value")',
                'type',
                'limit',
                'subid',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ],
            self::shortcode_ducklett => [
                'responsive',
                'width' => '$model->responsive? null : $mappedAttributes->get("responsive_width")',
                'limit' => '$mappedAttributes->get("limit_ducklett")',
                'type' => '$mappedAttributes->get("widget_type")',
                'filter' => '$mappedAttributes->get("filter_by")',
                'airline' => '$model->filter_by === 0 ? $model->getAirlineAttribute() : null',
                'subid',
                'origin' => '$model->filter_by === 1 ? $mappedAttributes->get($attribute,"123") : null',
                'destination' => '$model->filter_by === 1 ? $mappedAttributes->get($attribute,"123") : null',
                'shortcode' => '$mappedAttributes->get("current_table")',
            ]
        ];

    }

    public function rules()
    {

        $safe_attributes = $this->attributes();
        $rules = [
            [$safe_attributes, 'safe'],
            [['responsive_width'], 'integer', 'min' => 100, 'max' => 3000,
                'when' => function (WidgetsModel $model, $attribute) {
                    return $model->isAttributeSafe('responsive') && !$model->responsive;
                }],
        ];
        return array_merge(parent::rules(), $rules, $this->getRulesByCurrentTable());
    }

    public function mapping()
    {
        return [
            'origin' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'destination' => [
                'accessor' => '$model->getArrayAttributeData($attribute,"iata")',
            ],
            'hotel_coordinates' => [
                'accessor' => '$model->hotelCoordinatesAttribute',
            ],
        ];

//        coordinates="55.752041, 37.617508"
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [

        ]);
    }

    protected function getRulesByCurrentTable()
    {
        switch ($this->current_table) {
            case self::shortcode_map:
                $rules = [
                    [['origin', 'size_width', 'size_height'], 'required'],
                    [['size_width', 'size_height'], 'integer', 'min' => 100, 'max' => 3000],
                ];
                break;
            case self::shortcode_hotelmap:
                $rules = [
                    [
                        ['hotel_coordinates', 'size_width', 'size_height'], 'required'
                    ]
                ];
                break;
            case self::shortcode_calendar:
                $rules = [
                    [
                        ['origin', 'destination', 'calendar_period', 'period_from', 'period_to'], 'required'
                    ]
                ];
                break;
            case self::shortcode_subscriptions:
                $rules = [
                    [
                        ['origin', 'destination'], 'required'
                    ]
                ];
                break;
            case self::shortcode_hotel:
                $rules = [
                    [
                        ['hotel_name'], 'required'
                    ]
                ];
                break;
            case self::shortcode_popular_routes:
                $rules = [
                    [['destination_multiple'], 'required'],
                    [['destination_multiple'], 'eachArrayHasValueValidator', 'params' => ['path' => 'iata']]
                ];
                break;
            case self::shortcode_hotel_selections:
                $rules = [
                    [['city'], 'required'],
                    [['city'], 'arrayHasValueValidator', 'params' => ['path' => 'id']],
                    [['city_categories'], 'eachArrayHasValueValidator', 'params' => ['path' => 'value']]
                ];
                break;
            case self::shortcode_ducklett:
                $rules = [
                    [['filter_by', 'limit'], 'safe'],
                    [['filter_by'], 'required'],
                    ['filter_by', 'in', 'range' => array_keys($this->filter_by_types)],
                    ['widget_type', 'in', 'range' => array_keys($this->widget_types)],
                    [['airlines'], 'eachArrayHasValueValidator', 'params' => ['path' => 'iata']],
                    [['origin', 'destination'], 'required']
                ];

                break;
            default:
                $rules = [];
        }
        return $rules;
    }

    /**
     * List with all tables
     * @return array
     */
    protected function getTable_list()
    {
        $tables_list = array(
            self::shortcode_map => _x('Map Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_1', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_hotelmap => _x('Hotels Map Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_2', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_calendar => _x('Calendar Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_3', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_subscriptions => _x('Subscription Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_4', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_hotel => _x('Hotel Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_5', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_popular_routes => _x('Popular Destinations Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_6', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_hotel_selections => _x('Hotels Selections Widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_7', TP_PLUGIN_TEXTDOMAIN),
            self::shortcode_ducklett => _x('Best deals widget',
                'tp_admin_page_settings_сonstructor_widgets_field_select_widget_value_8', TP_PLUGIN_TEXTDOMAIN)

        );
        return $tables_list;
    }

    /**
     * Set which components will be shown on each table
     * @return array
     */
    protected function getTable_fields()
    {
        $fields = [
            self::shortcode_map => [
                'subid',
                'origin',
                'size_width',
                'size_height',
                'direct',
                'current_table',
            ],
            self::shortcode_hotelmap => [
                'subid',
                'hotel_coordinates',
                'size_width',
                'size_height',
                'zoom',
                'current_table',
            ],
            self::shortcode_calendar => [
                'subid',
                'origin',
                'destination',
                'calendar_period',
                'responsive',
                'responsive_width',
                'direct',
                'one_way',
                'period_from',
                'period_to',
                'current_table',
            ],
            self::shortcode_subscriptions => [
                'subid',
                'responsive',
                'responsive_width',
                'origin',
                'destination',
                'current_table',
            ],
            self::shortcode_hotel => [
                'subid',
                'responsive',
                'responsive_width',
                'hotel_name',
                'current_table',
            ],
            self::shortcode_popular_routes => [
                'subid',
                'destination_multiple',
                'current_table',
            ],
            self::shortcode_hotel_selections => [
                'subid',
                'type',
                'limit',
                'city_categories',
                'city',
                'current_table',
            ],
            self::shortcode_ducklett => [
                'subid',
                'responsive',
                'responsive_width',
                'filter_by',
                'widget_type',
                'limit_ducklett',
                'airlines',
                'current_table',
                'origin',
                'destination',
            ]
        ];
        return $fields;
    }

    protected function getCalendar_periods()
    {
        global $wp_locale;
        $month_names = array_map(array(&$wp_locale, 'get_month'), range(1, 12));
        $periods = array(
            'year' => _x('Year',
                'tp_admin_page_settings_сonstructor_widgets_field_calendar_period_value_year', TP_PLUGIN_TEXTDOMAIN),
            'current_month' => _x('Current month',
                'tp_admin_page_settings_сonstructor_widgets_field_calendar_period_value_current_month', TP_PLUGIN_TEXTDOMAIN),
        );
        return array_merge($periods, $month_names);
    }

    protected function getTypes()
    {
        return array(
            'full' => _x('Full',
                'tp_admin_page_settings_сonstructor_widgets_field_type_widget_7_value_full', TP_PLUGIN_TEXTDOMAIN),
            'compact' => _x('Compact',
                'tp_admin_page_settings_сonstructor_widgets_field_type_widget_7_value_compact', TP_PLUGIN_TEXTDOMAIN)
        );
    }

    protected function getWidget_types()
    {
        return array(
            'brickwork' => _x('Tile',
                'tp_admin_page_settings_сonstructor_widgets_field_type_widget_8_value_0', TP_PLUGIN_TEXTDOMAIN),
            'slider' => _x('Slider',
                'tp_admin_page_settings_сonstructor_widgets_field_type_widget_8_value_1', TP_PLUGIN_TEXTDOMAIN)
        );
    }

    protected function getFilter_by_types()
    {
        return array(
            self::FILTER_BY_AIRLINES => _x('By airlines',
                'tp_admin_page_settings_сonstructor_widgets_field_filter_widget_8_value_0_label', TP_PLUGIN_TEXTDOMAIN),
            self::FILTER_BY_ROUTES => _x('By routes',
                'tp_admin_page_settings_сonstructor_widgets_field_filter_widget_8_value_1_label', TP_PLUGIN_TEXTDOMAIN)
        );
    }

    public function getAirlineAttribute()
    {
        $attribute = $this->airlines;
        if ($attribute && is_array($attribute)) {
            $airlines = array_map(function ($airlineData) {
                return isset($airlineData['iata']) ? $airlineData['iata'] : null;
            }, $attribute);
            return implode(',', $airlines);
        }
        return null;
    }

    public function getHotelCoordinatesAttribute()
    {
        $location = $this->getArrayAttributeData('hotel_coordinates', 'location');
        return implode(', ', $location);
    }

}