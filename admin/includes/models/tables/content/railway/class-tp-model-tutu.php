<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 9:34 AM
 */

namespace tp\admin\includes\models\tables\content\railway;

use tp\includes;
use tp\admin\includes\forms\tables\content\railway\TP_Form_Tutu as Form;

class TP_Model_Tutu extends includes\TP_Model
{

    public $section = 'tables/content/railway/tutu';
    public $defaultOptions = array(
        'title' => 'Train schedule {origin} — {destination}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'calMonth',
        'table_columns' => array(
        ),
        'button_title' => 'Select date',
        'paginate' => true,
        'paginate_rows_per_page' => 10
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}