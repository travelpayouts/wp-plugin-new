<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 17.12.2017
 * Time: 03:32
 */

namespace tp\admin\includes\models\tables\content\hotel;

use tp\includes;
use tp\admin\includes\forms\tables\content\hotel\TP_Form_Selections_Discount as Form;

/**
 * Class TP_Model_Selections_Discount
 * @package tp\admin\includes\models\tables\content\hotel
 * Hotels collection - Discounts
 */

class TP_Model_Selections_Discount extends includes\TP_Model
{
    public $section = 'tables/content/hotel/selections_discount';
    public $defaultOptions = array(
        'title' => 'Hotels {location}: {selection_name}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'hotelsSelections',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'button_title' => 'View Hotel',
    );

        public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}