<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 09.12.2017
 * Time: 23:45
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Cheapest_Flights as Form;

/**
 * Class TP_Model_Cheapest_Flights
 * @package tp\admin\includes\models\tables\content\flights
 * Cheapest Flights from origin to destination, Round-trip
 */

class TP_Model_Cheapest_Flights extends includes\TP_Model
{
    public $section = 'tables/content/flights/cheapest_flights';
    public $defaultOptions = array(
        'title' => 'The Cheapest Round-trip Tickets from {origin} to {destination}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'direction',
        'table_columns' => array(
            'flight_number' => 'Flight'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}