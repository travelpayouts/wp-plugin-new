<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 09:38
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_In_Our_City_Fly as Form;

/**
 * Class TP_Model_In_Our_City_Fly
 * @package tp\admin\includes\models\tables\content\flights
 * Cheap Flights to destination
 */

class TP_Model_In_Our_City_Fly extends includes\TP_Model
{
    public $section = 'tables/content/flights/in_our_city_fly';
    public $defaultOptions = array(
        'title' => 'Cheap Flights to {destination}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'toCity',
        'table_columns' => array(
            'origin',
            'departure_date',
            'return_date',
            'button'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'limit' => 100,
        'type_transplant' => array(
            'direct'
        ),
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}