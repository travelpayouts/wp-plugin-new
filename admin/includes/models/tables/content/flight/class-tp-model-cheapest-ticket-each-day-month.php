<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 08:48
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Cheapest_Ticket_Each_Day_Month as Form;

/**
 * Class TP_Model_Cheapest_Ticket_Each_Day_Month
 * @package tp\admin\includes\models\tables\content\flights
 * Cheapest Flights from origin to destination (next month)
 */

class TP_Model_Cheapest_Ticket_Each_Day_Month extends includes\TP_Model
{
    public $section = 'tables/content/flights/cheapest_ticket_each_day_month';
    public $defaultOptions = array(
        'title' => 'The Cheapest Flights for this Month from {origin} to {destination}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'directionMonth',
        'table_columns' => array(
            'departure_date',
            'return_date',
            'number_of_changes',
            'airline_logo',
            'button'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}