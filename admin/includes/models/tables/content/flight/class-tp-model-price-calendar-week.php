<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 09.12.2017
 * Time: 23:39
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\includes\table\TP_Table_Flight as Table;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Price_Calendar_Week as Form;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Model_Price_Calendar_Week
 * @package tp\admin\includes\models\tables\content\flights
 * Flights from Origin to Destination (next few days)
 */
class TP_Model_Price_Calendar_Week extends includes\TP_Model
{
    public $section = 'tables/content/flights/price_calendar_week';
    public $defaultOptions = array(
        'title' => 'Flights from {origin} to {destination} for the Next Few Days',
        'title_tag' => 'h3',
        'extra_table_marker' => 'calWeek',
        'table_columns' => array(
            'notSelected' => array(
                Table::COLUMN_RETURN_DATE,
                Table::COLUMN_PRICE,
                Table::COLUMN_TRIP_CLASS,
            ),
            'selected' => array(
                Table::COLUMN_DEPART_DATE,
                Table::COLUMN_NUMBER_OF_CHANGES,
                Table::COLUMN_BUTTON,
            )
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'plus_days_departure_date' => 1,
        'plus_days_return_date' => 12,
        'hidden_columns' => array(
            TP_Table_Flight::COLUMN_TRIP_CLASS,
            TP_Table_Flight::COLUMN_RETURN_DATE
        ),
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}