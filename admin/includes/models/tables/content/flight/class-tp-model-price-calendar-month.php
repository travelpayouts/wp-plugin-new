<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 09.12.2017
 * Time: 23:18
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Price_Calendar_Month as Form;
use tp\includes\table\TP_Table_Flight;

/**
 * Class TP_Model_Price_Calendar_Month
 * @package tp\admin\includes\models\tables\content\flights
 * Flights from origin to destination, One Way (next month)
 */
class TP_Model_Price_Calendar_Month extends includes\TP_Model
{
    public $section = 'tables/content/flights/price_calendar_month';
    public $defaultOptions = array(
        'title' => 'Flight Prices for a Month from {origin} to {destination}, One Way',
        'title_tag' => 'h3',
        'extra_table_marker' => 'calMonth',
        'table_columns' => array(
            'departure_date',
            'number_of_changes',
            'button'
        ),
        'type_transplant' => array(
            'direct'
        ),
        'button_title' => 'OW tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'hidden_columns' => array(
            TP_Table_Flight::COLUMN_TRIP_CLASS,
            TP_Table_Flight::COLUMN_RETURN_DATE
        )

    );


    public function get_form()
    {
        return Form::$form;
    }

}