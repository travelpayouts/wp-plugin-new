<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 09:09
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Popular_Routes_From_City as Form;

/**
 * Class TP_Model_Popular_Routes_From_City
 * @package tp\admin\includes\models\tables\content\flights
 * Popular Destinations from origin
 */
class TP_Model_Popular_Routes_From_City extends includes\TP_Model
{
    public $section = 'tables/content/flights/popular_routes_from_city';
    public $defaultOptions = array(
        'title' => 'Popular Destinations from {origin}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'popularCity',
        'table_columns' => array(
            'destination',
            'departure_date',
            'return_date',
            'airline_logo',
            'button'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}