<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 08:59
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Direct_Flights_Route as Form;

/**
 * Class TP_Model_Direct_Flights_Route
 * @package tp\admin\includes\models\tables\content\flights
 * Direct Flights from origin to destination
 */

class TP_Model_Direct_Flights_Route extends includes\TP_Model
{
    public $section = 'tables/content/flights/direct_flights_route';
    public $defaultOptions = array(
        'title' => 'Direct Flights from {origin} to {destination}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'directionNostops',
        'table_columns' => array(
            'departure_date',
            'return_date',
            'airline_logo',
            'button'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}