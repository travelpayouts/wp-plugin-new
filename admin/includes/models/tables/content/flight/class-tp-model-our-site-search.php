<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 09:19
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Our_Site_Search as Form;

/**
 * Class TP_Model_Our_Site_Search
 * @package tp\admin\includes\models\tables\content\flights
 * Searched on our website
 */

class TP_Model_Our_Site_Search extends includes\TP_Model
{
    public $section = 'tables/content/flights/our_site_search';
    public $defaultOptions = array(
        'title' => 'Flights That Have Been Found on Our Website',
        'title_tag' => 'h3',
        'extra_table_marker' => 'onOurWebsite',
        'table_columns' => array(
            'origin_destination',
            'departure_date',
            'return_date',
            'button'
        ),
        'button_title' => 'Tickets from {price}',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'limit' => 100,
        'type_transplant' => array(
            'direct'
        ),
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}