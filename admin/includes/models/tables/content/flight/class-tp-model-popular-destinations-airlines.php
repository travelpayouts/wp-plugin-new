<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 11.12.2017
 * Time: 09:14
 */

namespace tp\admin\includes\models\tables\content\flight;

use tp\includes;
use tp\admin\includes\forms\tables\content\flight\TP_Form_Popular_Destinations_Airlines as Form;

/**
 * Class TP_Model_Popular_Destinations_Airlines
 * @package tp\admin\includes\models\tables\content\flights
 * Most popular flights within this Airlines
 */
class TP_Model_Popular_Destinations_Airlines extends includes\TP_Model
{
    public $section = 'tables/content/flights/popular_destinations_airlines';
    public $defaultOptions = array(
        'title' => 'Airline\'s popular flights {airline}',
        'title_tag' => 'h3',
        'extra_table_marker' => 'popularAirlines',
        'table_columns' => array(
            'place',
            'direction',
            'button'
        ),
        'button_title' => 'Find tickets',
        'paginate' => true,
        'paginate_rows_per_page' => 10,
        'limit' => 10,
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}