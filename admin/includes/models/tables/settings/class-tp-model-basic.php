<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/20/2017
 * Time: 10:01 AM
 */

namespace tp\admin\includes\models\tables\settings;

use tp\includes;
use tp\admin\includes\forms\tables\settings\TP_Form_Basic as Form;

/**
 * Class TP_Model_Basic
 * @package tp\admin\includes\models\tables\settings
 */

class TP_Model_Basic extends includes\TP_Model
{
    public $section = 'tables/settings/basic';
    public $defaultOptions = array(
        'type_distance' => 'km'
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}