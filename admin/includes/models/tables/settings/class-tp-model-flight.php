<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/20/2017
 * Time: 10:38 AM
 */

namespace tp\admin\includes\models\tables\settings;

use tp\includes;
use tp\admin\includes\forms\tables\settings\TP_Form_Flight as Form;

class TP_Model_Flight extends includes\TP_Model
{
    public $section = 'tables/settings/flights';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}