<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 1/22/18
 * Time: 01:52
 */

namespace tp\admin\includes\models\tables\settings;

use tp\includes;
use tp\admin\includes\forms\tables\settings\TP_Form_Hotel as Form;


class TP_Model_Hotel extends includes\TP_Model
{
    public $section = 'tables/settings/hotel';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}