<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 20.12.2017
 * Time: 06:53
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Fiery_Sunrise as Form;

/**
 * Class TP_Model_Fiery_Sunrise
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Fiery Sunrise
 */

class TP_Model_Fiery_Sunrise extends includes\TP_Model
{
    public $section = 'tables/design/theme/fiery_sunrise';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}