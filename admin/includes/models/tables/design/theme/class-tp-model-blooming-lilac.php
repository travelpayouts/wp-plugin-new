<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 12:44 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Blooming_Lilac as Form;

/**
 * Class TP_Model_Blooming_Lilac
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Blooming lilac
 */
class TP_Model_Blooming_Lilac extends includes\TP_Model
{
    public $section = 'tables/design/theme/blooming_lilac';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}