<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 20.12.2017
 * Time: 07:19
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Customizable as Form;

/**
 * Class TP_Model_Customizable
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Customizable
 */

class TP_Model_Customizable extends includes\TP_Model
{
    public $section = 'tables/design/theme/customizable';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}