<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 5:58 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Cat_In_The_Night as Form;

/**
 * Class TP_Model_Cat_In_The_Night
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Cat In The Night
 */

class TP_Model_Cat_In_The_Night extends includes\TP_Model
{
    public $section = 'tables/design/theme/cat_in_the_night';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}