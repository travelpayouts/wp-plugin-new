<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 5:48 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Toucan as Form;

/**
 * Class TP_Model_Toucan
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Toucan
 */

class TP_Model_Toucan extends includes\TP_Model
{
    public $section = 'tables/design/theme/toucan';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}