<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 12:53 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Mint_Freshness as Form;

/**
 * Class TP_Model_Mint_Freshness
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Mint Freshness
 */

class TP_Model_Mint_Freshness extends includes\TP_Model
{
    public $section = 'tables/design/theme/mint_freshness';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}