<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 4:00 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Arctic_Ocean as Form;

/**
 * Class TP_Model_Arctic_Ocean
 * @package tp\admin\includes\models\tables\design\theme
 * Theme The Arctic Ocean
 */

class TP_Model_Arctic_Ocean extends includes\TP_Model
{
    public $section = 'tables/design/theme/arctic_ocean';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}