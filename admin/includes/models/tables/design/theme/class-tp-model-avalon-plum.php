<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 5:40 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Avalon_Plum as Form;

/**
 * Class TP_Model_Avalon_Plum
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Avalon Plum
 */

class TP_Model_Avalon_Plum extends includes\TP_Model
{
    public $section = 'tables/design/theme/avalon_plum';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}