<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 5:33 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Spring_Grass as Form;

/**
 * Class TP_Model_Spring_Grass
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Spring grass
 */

class TP_Model_Spring_Grass extends includes\TP_Model
{
    public $section = 'tables/design/theme/spring_grass';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}