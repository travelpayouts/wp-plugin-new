<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 3:52 PM
 */

namespace tp\admin\includes\models\tables\design\theme;

use tp\includes;
use tp\admin\includes\forms\tables\design\theme\TP_Form_Winter_Rowan as Form;

/**
 * Class TP_Model_Winter_Rowan
 * @package tp\admin\includes\models\tables\design\theme
 * Theme Winter rowan
 */

class TP_Model_Winter_Rowan extends includes\TP_Model
{
    public $section = 'tables/design/theme/winter_rowan';
    public $defaultOptions = array(
    );

    public function get_form()
    {
        // TODO: Implement get_form() method.
        return Form::$form;
    }
}