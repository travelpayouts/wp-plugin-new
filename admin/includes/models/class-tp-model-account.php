<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\models;

use tp\includes;
use tp\admin\includes\forms\TP_Form_account as Form;

/**
 * Class TP_model_index
 * @package tp\admin\includes\models
 * @property $airline_logo_size
 */
class TP_model_account extends includes\TP_Model
{
    public $section = 'account';
    public $defaultOptions = array();

    public function get_form()
    {
        return Form::$form;
    }
}