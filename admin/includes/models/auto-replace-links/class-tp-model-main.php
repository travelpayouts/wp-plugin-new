<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\models\auto_replace_links;

use tp\includes;
use tp\admin\includes\forms\auto_replace_links\TP_Form_main as Form;

class TP_model_main extends includes\TP_Model
{
    public $section = 'auto_replace_links';
    public $defaultOptions = array(
        'replace_count' => 2
    );

    public function get_form()
    {
        return Form::$form;
    }
}