<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\models\auto_replace_links;

class TP_model_items extends \WeDevs\ORM\Eloquent\Model
{
    protected $table = 'tp_auto_replace_links';

    /**
     * Columns that can be edited - IE not primary key and timestamps if being uses
     */
    protected $fillable = [
        'url',
        'anchor',
        'nofollow',
        'replace',
        'target_blank'
    ];


    /**
     * Set primary key as ID, because WordPress
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Make ID guarded -- without this ID doesn't save.
     *
     * @var string
     */
    protected $guarded = [ 'id' ];

    /**
     * Overide parent method to make sure prefixing is correct.
     *
     * @return string
     */
    public function getTable()
    {
        //In this example, it's set, but this is better in an abstract class
        if (isset($this->table)) {
            $prefix = $this->getConnection()->db->prefix;
            return $prefix . $this->table;
        }

        return parent::getTable();
    }

}