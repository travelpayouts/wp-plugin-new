<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\models;


class TP_model_search_shortcodes extends \WeDevs\ORM\Eloquent\Model
{
    protected $table = 'tp_search_shortcodes';

    /**
     * Columns that can be edited - IE not primary key and timestamps if being uses
     */
    protected $fillable = [
        'title',
        'date_add',
        'type_shortcode',
        'code_form',
        'from_city',
        'to_city',
        'hotel_city',
        'type_form',
        'slug'
    ];

    /**
     * Disable created_at and update_at columns, unless you have those.
     */
    public $timestamps = false;

    /**
     * Set primary key as ID, because WordPress
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Make ID guarded -- without this ID doesn't save.
     *
     * @var string
     */
    protected $guarded = ['id'];

    /**
     * Overide parent method to make sure prefixing is correct.
     *
     * @return string
     */
    public function getTable()
    {
        //In this example, it's set, but this is better in an abstract class
        if (isset($this->table)) {
            $prefix = $this->getConnection()->db->prefix;
            return $prefix . $this->table;
        }

        return parent::getTable();
    }

}