<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\admin\includes\models;

use Yii;
use yii\db\ActiveRecord;

class TP_Model_Search_Form extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tp_search_shortcodes}}';
    }

}
