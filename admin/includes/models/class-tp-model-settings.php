<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\models;

use tp\includes;
use tp\admin\includes\forms\TP_Form_settings as Form;

class TP_model_settings extends includes\TP_Model
{
    public $section = 'settings';
    public $defaultOptions = array();

    public function get_form()
    {
        return Form::$form;
    }
}