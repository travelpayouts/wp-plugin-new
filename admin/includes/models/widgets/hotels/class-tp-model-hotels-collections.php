<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\hotels;

use tp\includes;
use tp\admin\includes\forms\widgets\hotels\TP_Form_Hotels_Collections as Form;

class TP_model_Hotels_Collections extends includes\TP_Model
{
    public $section = 'widgets.hotels.collections';
    public $defaultOptions = array(
        'dimensions' => array(
            'width' => 800
        ),
        'type' => Form::TYPE_COMPACT,
        'count' => 7
    );

    public function get_form()
    {
        return Form::$form;
    }
}