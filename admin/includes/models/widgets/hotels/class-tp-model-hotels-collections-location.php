<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\hotels;

use tp\includes;
use tp\admin\includes\forms\widgets\hotels\TP_Form_Hotels_Collections_Location as Form;

class TP_model_Hotels_Collections_Location extends includes\TP_Model
{
    public $section = 'widgets.hotels.collections_location';
    public $defaultOptions = array(
        'columns' => array(
            'notSelected' => array(
                Form::COLUMN_TYPE_PER_NIGHT,
                Form::COLUMN_TYPE_OLD_PRICE,
                Form::COLUMN_TYPE_TO_CENTER,
                Form::COLUMN_TYPE_PRICE_BEFORE_DISCOUNT,
                Form::COLUMN_TYPE_RATING
            ),
            'selected' => array(
                Form::COLUMN_TYPE_HOTEL,
                Form::COLUMN_TYPE_STAR,
                Form::COLUMN_TYPE_DISCOUNT,
                Form::COLUMN_TYPE_BUTTON
            )
        ),
        'header_text' => '',
        'header_tag' => 'H3',
        'button_text' => '',
        'sort_by' => Form::COLUMN_TYPE_RATING,
        'pagination' => array(
            'count' => 10,
            'available' => false
        )
    );

    public function __construct()
    {
        $this->defaultOptions['header_text'] = _x('Hotels in %{location}: {selection_name}', 'Tp_Form_Hotels_Collections_Location default field text', TP_PLUGIN_TEXTDOMAIN);
        $this->defaultOptions['button_text'] = _x('Show hotels', 'Tp_Form_Hotels_Collections_Location default field text', TP_PLUGIN_TEXTDOMAIN);

        parent::__construct();
    }

    public function get_form()
    {
        return Form::$form;
    }
}