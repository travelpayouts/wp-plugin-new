<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\hotels;

use tp\includes;
use tp\admin\includes\forms\widgets\hotels\TP_Form_Hotels_Widget as Form;

class TP_model_Hotels_Widget extends includes\TP_Model
{
    public $section = 'widgets.hotels.widget';
    public $defaultOptions = array(
        'dimensions'=> array(
            'width'=>660
        )
    );

    public function get_form()
    {
        return Form::$form;
    }
}