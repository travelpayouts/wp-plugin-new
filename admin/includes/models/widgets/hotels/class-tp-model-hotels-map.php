<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\hotels;

use tp\includes;
use tp\admin\includes\forms\widgets\hotels\Tp_Form_Hotels_Map as Form;

class TP_model_Hotels_Map extends includes\TP_Model
{
    public $section = 'widgets.hotels.map';
    public $defaultOptions = array(
        'width' => 500,
        'height' => 300,
        'diameter'=> 16,
        'color_scheme'=> array(
            'pin_color' => '#222222',
            'text_color' => '#FFFFFF'
        )
    );

    public function get_form()
    {
        return Form::$form;
    }
}