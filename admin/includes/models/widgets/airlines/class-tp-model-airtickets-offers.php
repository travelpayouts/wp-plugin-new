<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\airlines;

use tp\includes;
use tp\admin\includes\forms\widgets\airlines\TP_Form_Airtickets_Offers as Form;

class TP_model_Airtickets_Offers extends includes\TP_Model
{
    public $section = 'widgets/airlines/offers';
    public $defaultOptions = array(
        'dimensions' => array(
            'width' => 100,
        ),
        'limit' => 5,
        'type'=> Form::WIDGET_DESIGN_SLIDER,
        'filter'=> Form::FILTER_BY_AIRLINES
    );

    public function get_form()
    {
        return Form::$form;
    }
}