<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\airlines;

use tp\includes;
use tp\admin\includes\forms\widgets\airlines\TP_Form_Airtickets_Price_Subscription as Form;

class TP_model_Airtickets_Price_Subscription extends includes\TP_Model
{
    public $section = 'widgets/airlines/price_subscription';
    public $defaultOptions = array(
        'dimensions' => array(
            'width' => 100,
            'color_scheme'=> '#222222'
        )
    );

    public function get_form()
    {
        return Form::$form;
    }
}