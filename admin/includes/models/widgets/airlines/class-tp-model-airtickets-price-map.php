<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\airlines;

use tp\includes;
use tp\admin\includes\forms\widgets\airlines\TP_Form_Airtickets_Price_Map as Form;

class TP_model_Airtickets_Price_Map extends includes\TP_Model
{
    public $section = 'widgets/airlines/price_map';
    public $defaultOptions = array(
        'width'=> 500,
        'height'=> 500
    );

    public function get_form()
    {
        return Form::$form;
    }
}