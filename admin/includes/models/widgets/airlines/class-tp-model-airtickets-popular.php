<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\airlines;

use tp\includes;
use tp\admin\includes\forms\widgets\airlines\TP_Form_Airtickets_Popular as Form;

class TP_model_Airtickets_Popular extends includes\TP_Model
{
    public $section = 'widgets/airlines/popular';
    public $defaultOptions = array(
        'number' => 1,
        'dimensions' => array(
            'width' => 100,
        ),
    );

    public function get_form()
    {
        return Form::$form;
    }
}