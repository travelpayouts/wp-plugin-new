<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\models\widgets\airlines;

use tp\admin\includes\forms\widgets\airlines\TP_Form_Airtickets_Low_Price_Calendar as Form;
use tp\includes;

class TP_model_Airtickets_Low_Price_Calendar extends includes\TP_Model
{
    public $section = 'widgets/airlines/low_price';
    public $defaultOptions = array(
        'duration'=> '0,30',
        'width'=> 100
    );

    public function get_form()
    {
        return Form::$form;
    }
}