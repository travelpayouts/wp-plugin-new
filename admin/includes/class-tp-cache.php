<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 4/2/18
 * Time: 09:36
 */

namespace tp\admin\includes;


class TP_Cache
{
    public function __construct()
    {
        add_action( 'save_post', array($this, 'update_cache'), 10, 3);

    }

    /**
     * @param $post_ID
     * @param $post
     * @param $update
     * @return bool
     */
    public function update_cache($post_ID, $post, $update){
        if ($post->post_status != 'publish'){
            return false;
        }
        $shortcodes = $this->get_shortcodes_from_content($post->post_content);
        if ($shortcodes == false || count($shortcodes) < 1){
            return false;
        }
        $shortcodes = $this->set_shortcodes_atts($shortcodes);
        if ($shortcodes == false){
            return false;
        }
        do_shortcode($shortcodes);
    }

    /**
     * @param string $content
     * @return bool|string
     */
    public function get_shortcodes_from_content($content = ''){
        if (empty($content) || false === strpos( $content, '[' )){
            return false;
        }
        $shortcode_regex = get_shortcode_regex(tp_get_flight_shortcodes());
        preg_match_all( '/' . $shortcode_regex . '/', $content, $matches, PREG_SET_ORDER );
        if (count($matches) == 0){
            return false;
        }
        return $matches;
    }

    /**
     * @param $shortcode_list
     * @param array $atts
     * @return bool|string
     */
    public function  set_shortcodes_atts($shortcode_list, $atts = array('update_cache' => 'true')){
        if ($shortcode_list == false || count($shortcode_list) < 1){
            return false;
        }
        $shortcodes = '';
        $attribute = '';
        foreach ($atts as $key => $att){
            $attribute .= ' '.$key.'="'.$att.'"';
        }
        foreach ($shortcode_list as $item){
            if (array_key_exists(0, $item)) {
                $shortcodes .= substr_replace($item[0],$attribute.']',-1).' ';
            }
        }
        return $shortcodes;
    }
}