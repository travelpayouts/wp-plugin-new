<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\forms;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_account extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token', Type\TextType::class, array(
                'label' => _x('Token', 'tp_admin_page_settings_tab_account_field_token_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'tooltip' => _x('Token is a unique key consisting of numbers and Latin letters. It is automatically created for each user, right after registration in Travelpayouts.',
                        'tp_admin_page account token tooltip', TP_PLUGIN_TEXTDOMAIN)
                ),
                'attr' => array(
                    'placeholder' => _x('Insert your API token', 'tp_admin_page account token placeholder', TP_PLUGIN_TEXTDOMAIN),
                ),
                'constraints' => $this->get_rule('token')
            ))
            ->add('marker_id', Type\TextType::class, array(
                    'label' => _x('Marker',
                        'tp_admin_page_settings_tab_account_field_marker_label', TP_PLUGIN_TEXTDOMAIN),
                    'label_attr' => array(
                        'tooltip' => _x('A marker is an identification number that is automatically assigned to each user after registration in Travelpayouts.',
                            'tp_admin_page account marker tooltip', TP_PLUGIN_TEXTDOMAIN)
                    ),
                    'attr' => array(
                        'placeholder' => _x('Insert your partner marker', 'tp_admin_page account marker placeholder', TP_PLUGIN_TEXTDOMAIN),
                    ),
                    'constraints' => $this->get_rule('marker_id')

                )
            )
            ->add('whitelabel_airlines', Type\TextType::class, array(
                    'label' => _x('White Label (Flights)',
                        'tp_admin_page_settings_tab_account_field_white_label', TP_PLUGIN_TEXTDOMAIN),
                    'required' => false,
                    'attr' => array(
                        'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page account whitelabel placeholder', TP_PLUGIN_TEXTDOMAIN),
                    )
                )
            )
            ->add('whitelabel_separate', Type\TextType::class, array(
                    'label' => _x('White Label (Hotels)',
                        'tp_admin_page_settings_tab_account_field_white_label_hotel_label', TP_PLUGIN_TEXTDOMAIN),
                    'required' => false,
                    'attr' => array(
                        'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page account whitelabel placeholder', TP_PLUGIN_TEXTDOMAIN),
                    )
                )
            );

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'token' => array(
                new Assert\Length(
                    array(
                        'min' => 32,
                        'max' => 32
                    )
                )
            ),
            'marker_id' => array(
                new Assert\GreaterThan(0)
            )
        );
        return $rules;
    }
}