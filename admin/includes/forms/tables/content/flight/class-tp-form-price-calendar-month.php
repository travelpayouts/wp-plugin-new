<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 09.12.2017
 * Time: 22:59
 */

namespace tp\admin\includes\forms\tables\content\flight;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use tp\includes;

/**
 * Class TP_Form_Price_Calendar_Month
 * @package tp\admin\includes\forms\tables\flights
 * Flights from origin to destination, One Way (next month)
 */
class TP_Form_Price_Calendar_Month extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, array(
                'label' => _x('Title', 'tp_admin_page_flights_tab_tables_content_shortcode_field_title_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'required' => false,
            ))
            ->add('title_tag', Type\ChoiceType::class, array(
                'label' => _x('Title tag', 'tp_admin_page_flights_tab_tables_content_shortcode_field_tag_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'choices' => array(
                    'div' => 'div',
                    'h1' => 'h1',
                    'h2' => 'h2',
                    'h3' => 'h3',
                    'h4' => 'h4',
                ),
                'required' => false,
            ))
            ->add('extra_table_marker', Type\TextType::class, array(
                'label' => _x('SubId', 'tp_admin_page_flights_tab_tables_content_shortcode_field_extra_table_marker_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'required' => false,
            ))
            ->add('sort_column', Type\ChoiceType::class, array(
                'label' => _x('Sort by column', 'tp_admin_page_flights_tab_tables_content_shortcode_field_sort_column_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'choices' => array(
                    'departure_date' => 'Departure date',
                    'price' => 'Price',
                    'number_of_changes' => 'Stops',
                    'trip_class' => 'Flight class',
                    'distance' => 'Distance',
                    'button' => 'Button',
                ),
                'required' => false
            ))
            /*->add('table_columns', includes\form\type\TP_form_type_sortable_connected::class, array(
                 'label' => _x('Table Columns', 'tp_admin_page_flights_tab_tables_content_shortcode_field_table_columns_label', TP_PLUGIN_TEXTDOMAIN),
                 'description' => _x('We offer a ready-made combination for such a table, but you can edit the number of columns and the order of their location.', 'tp_admin_page_flights_tab_tables_content_shortcode_field_table_columns_description', TP_PLUGIN_TEXTDOMAIN),
                 'rows' => array(
                     'notSelected' => array(
                         'label' => 'Not selected label',
                         'defaultValueCollection' => true
                       ),
                     'selected' => array(
                         'label' => 'Selected label'
                     )
                 ),
                 'values' => array(
                     'departure_date' => 'Departure date',
                     'price' => 'Price',
                     'number_of_changes' => 'Stops',
                     'trip_class' => 'Flight class',
                     'distance' => 'Distance',
                     'button' => 'Button',
                 ),
             ))*/
            ->add('type_transplant', Type\ChoiceType::class, array(
                'label' => _x('Number of stops', 'tp_admin_page_flights_tab_tables_content_shortcode_field_type_transplant_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'choices' => array(
                    'all' => 'All flights',
                    'direct' => 'Direct',
                    'one_stop' => '1 stop',
                    'two_stop' => '2 stops',
                ),
                'expanded' => true,
                'multiple' => true,
                'required' => false
            ))
            ->add('button_title', Type\TextType::class, array(
                'label' => _x('Button Title', 'tp_admin_page_flights_tab_tables_content_shortcode_field_button_title_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'required' => false,
            ))
            ->add('paginate', Type\CheckboxType::class, array(
                'label' => _x('Paginate', 'tp_admin_page_flights_tab_tables_content_shortcode_field_paginate_label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('paginate_rows_per_page', Type\IntegerType::class, array(
                'label' => _x('Rows per page', 'tp_admin_page_flights_tab_tables_content_shortcode_field_paginate_rows_per_page_label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false,
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }

}