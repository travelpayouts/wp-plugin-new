<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/19/2017
 * Time: 3:48 PM
 */

namespace tp\admin\includes\forms\tables\design\theme;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use tp\includes;

/**
 * Class TP_Form_Winter_Rowan
 * @package tp\admin\includes\forms\tables\design\theme
 * Theme Winter rowan
 */

class TP_Form_Winter_Rowan extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        //Add transformers to form fields
        $this->add_transformers($builder);
    }
    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}