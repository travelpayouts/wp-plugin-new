<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 20.12.2017
 * Time: 06:50
 */

namespace tp\admin\includes\forms\tables\design\theme;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use tp\includes;

/**
 * Class TP_Form_Fiery_Sunrise
 * @package tp\admin\includes\forms\tables\design\theme
 * Theme Fiery Sunrise
 */

class TP_Form_Fiery_Sunrise extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        //Add transformers to form fields
        $this->add_transformers($builder);
    }
    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}