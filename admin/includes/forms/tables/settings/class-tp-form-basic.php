<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/20/2017
 * Time: 9:54 AM
 */
namespace tp\admin\includes\forms\tables\settings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use tp\includes;

/**
 * Class TP_Form_Basic
 * @package tp\admin\includes\forms\tables\settings
 */

class TP_Form_Basic extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type_distance', Type\ChoiceType::class, array(
                'label' => _x('Distance Units', 'tp_admin_page_tables_settings_tab_basic_shortcode_field_type_distance_label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(),
                'attr' => array(),
                'choices' => array(
                    'km' => _x('km', 'tp_admin_page_tables_settings_tab_basic_shortcode_field_type_distance_label', TP_PLUGIN_TEXTDOMAIN),
                    'mi' => _x('miles', 'tp_admin_page_tables_settings_tab_basic_shortcode_field_type_distance_label', TP_PLUGIN_TEXTDOMAIN)
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false,
            ))
            ;
        //Add transformers to form fields
        $this->add_transformers($builder);
    }
    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}