<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 12/20/2017
 * Time: 10:50 AM
 */

namespace tp\admin\includes\forms\tables\settings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use tp\includes;

class TP_Form_Hotel extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        //Add transformers to form fields
        $this->add_transformers($builder);
    }
    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}