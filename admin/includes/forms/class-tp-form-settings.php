<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\forms;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use tp\includes\LangUtils;

class TP_Form_settings extends AbstractType
{
    use includes\TP_Form_base;

    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('tickets_source_airlines', Type\ChoiceType::class, array(
                'label' => _x('Flights',
                    'tp_admin_page_settings_tab_localization_field_host_label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => includes\TP_Api_Hosts::get_instance()->get_flights_label(),
            ))
            ->add('tickets_source_hotels', Type\ChoiceType::class, array(
                'label' => _x('Hotels',
                    'tp_admin_page_settings_tab_localization_field_host_hotel_label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => includes\TP_Api_Hosts::get_instance()->get_hotels_label(),
            ))
            ->add('localization_lang', Type\ChoiceType::class, array(
                'label' => _x('Tables and Widgets Language',
                    'tp_admin_page_settings_tab_localization_field_localization_label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => LangUtils::getInstance()->getAvailableLabels(),
            ))
            ->add('localization_currency', Type\ChoiceType::class, array(
                'label' => _x('Currency',
                    'tp_admin_page_settings_tab_localization_field_currency_label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => includes\TP_Currency_Utils::get_all()
            ))
            ->add('localization_currency_position', Type\ChoiceType::class, array(
                'label' => _x('Show the currency',
                    'Admin page settings tab localization field currency_symbol_display label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => array(
                    0 => _x('After the price',
                        'Admin page settings tab localization field currency_symbol_display option 0 label',
                        TP_PLUGIN_TEXTDOMAIN),
                    1 => _x('Before the price',
                        'Admin page settings tab localization field currency_symbol_display option 1 label',
                        TP_PLUGIN_TEXTDOMAIN),
                    2 => _x('Hide',
                        'Admin page settings tab localization field currency_symbol_display option 2 label',
                        TP_PLUGIN_TEXTDOMAIN),
                    3 => _x('Сurrency code (after the price)',
                        'Admin page settings tab localization field currency_symbol_display option 3 label',
                        TP_PLUGIN_TEXTDOMAIN),
                    4 => _x('Currency code (before the price)',
                        'Admin page settings tab localization field currency_symbol_display option 4 label',
                        TP_PLUGIN_TEXTDOMAIN),
                )
            ))
            ->add('script_position', Type\ChoiceType::class, array(
                'label' => _x('Script Include',
                    'tp_admin_page_settings_tab_config_field_script_label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => array(
                    0 => _x('Inside <head> tag',
                        'tp_admin_page_settings_tab_config_field_script_value_0_label', TP_PLUGIN_TEXTDOMAIN),
                    1 => _x('Inside <footer> tag',
                        'tp_admin_page_settings_tab_config_field_script_value_1_label', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('always_require_scripts', Type\CheckboxType::class, array(
                    'required' => false,
                    'label' => 'Вызывать скрипты на всех страницах'
                )
            );


        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {

    }
}