<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\forms\search_form;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_items extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, array(
                'label' => _x('Title',
                    'tp_admin_page_add_search_forms_field_title_label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Enter form name', 'tp_admin_page_add_search_forms_field_code_form_placeholder', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('code_form', Type\TextareaType::class, array(
                'label' => _x('TravelPayouts Form Code',
                    'tp_admin_page_add_search_forms_field_code_form_label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false,
                'attr' => array(
                    'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page_add_search_forms_field_code_form_placeholder', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('from_city', Type\TextType::class, array(
                'label' => _x('Default City of Departure',
                    'tp_admin_page_add_search_forms_field_from_label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page_add_search_forms_field_code_form_placeholder', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('to_city', Type\TextType::class, array(
                'label' => _x('Default City of Arrival',
                    'tp_admin_page_add_search_forms_field_to_label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page_add_search_forms_field_code_form_placeholder', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('hotel_city', Type\TextType::class, array(
                'label' => _x('Default City/Hotel',
                    'tp_admin_page_add_search_forms_field_city_label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Enter the domain configured in Travelpayouts', 'tp_admin_page_add_search_forms_field_code_form_placeholder', TP_PLUGIN_TEXTDOMAIN)
                )
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {

    }
}