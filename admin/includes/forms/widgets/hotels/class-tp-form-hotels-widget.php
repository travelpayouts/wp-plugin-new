<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\hotels;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class Tp_Form_Hotels_Widget extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;


    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dimensions::full_width', Type\CheckboxType::class, array(
                'label' => _x('Stretch width', 'TP_Form_Airtickets_Low_Price_Calendar', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('dimensions::width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}