<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\hotels;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class Tp_Form_Hotels_Collections_Location extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    const COLUMN_TYPE_PER_NIGHT = 0;
    const COLUMN_TYPE_OLD_PRICE = 1;
    const COLUMN_TYPE_TO_CENTER = 2;
    const COLUMN_TYPE_PRICE_BEFORE_DISCOUNT = 3;
    const COLUMN_TYPE_RATING = 4;
    const COLUMN_TYPE_HOTEL = 5;
    const COLUMN_TYPE_STAR = 6;
    const COLUMN_TYPE_DISCOUNT = 7;
    const COLUMN_TYPE_BUTTON = 8;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('header_text', Type\TextType::class, array(
                'label' => _x('Table header text', 'Tp_Form_Hotels_Collections_Location label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'big_title' => true
                ),
            ))
            ->add('header_tag', Type\ChoiceType::class, array(
                'label' => _x('Header tag', 'Tp_Form_Hotels_Collections_Location label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => array(
                    'H1' => 'H1',
                    'H2' => 'H2',
                    'H3' => 'H3',
                    'H4' => 'H4',
                    'H5' => 'H5',
                    'H6' => 'H6'
                ),
            ))
            ->add('columns', includes\form\type\TP_form_type_sortable_connected::class, array(
                'label' => false,
                'rows' => array(
                    'notSelected' => array(
                        'label' => _x('Not selected', 'TP_form_type_sortable_connected row label', TP_PLUGIN_TEXTDOMAIN),
                        'defaultValueCollection' => true
                    ),
                    'selected' => array(
                        'label' => _x('Selected', 'TP_form_type_sortable_connected row label', TP_PLUGIN_TEXTDOMAIN)
                    )
                ),
                'values' => array(
                    self::COLUMN_TYPE_PER_NIGHT => _x('Price per night, from', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_OLD_PRICE => _x('Old price and discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_TO_CENTER => _x('To the center', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_PRICE_BEFORE_DISCOUNT => _x('Price before discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_RATING => _x('Rating', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_HOTEL => _x('Hotel', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_STAR => _x('Star', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_DISCOUNT => _x('A discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_BUTTON => _x('Button', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                )

            ))
            ->add('button_text', Type\TextType::class, array(
                'label' => _x('Button text', 'Tp_Form_Hotels_Collections_Location label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'big_title' => true
                ),
            ))
            ->add('sort_by', Type\ChoiceType::class, array(
                'label' => _x('Sort by column', 'Tp_Form_Hotels_Collections_Location label', TP_PLUGIN_TEXTDOMAIN),
                'choices' => array(
                    self::COLUMN_TYPE_PER_NIGHT => _x('Price per night, from', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_OLD_PRICE => _x('Old price and discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_TO_CENTER => _x('To the center', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_PRICE_BEFORE_DISCOUNT => _x('Price before discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_RATING => _x('Rating', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_HOTEL => _x('Hotel', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_STAR => _x('Star', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                    self::COLUMN_TYPE_DISCOUNT => _x('A discount', 'Tp_Form_Hotels_Collections_Location sort columns type', TP_PLUGIN_TEXTDOMAIN),
                ),
            ))
            ->add('apply_dates', Type\CheckboxType::class, array(
                'label' => _x('Apply dates at redirect', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('pagination::available', Type\CheckboxType::class, array(
                'label' => _x('Turn on pagination', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('pagination::count', Type\IntegerType::class, array(
                'label' => _x('Items in one widgets', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false,
                'label_attr' => array(
                    'big_title' => true,
                ),
            ))
            ->add('additional_marker', Type\TextType::class, array(
                'label' => _x('Additional marker', 'Tp_Form_Hotels_Collections_Location label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'big_title' => true,
                    'tooltip'=>  _x('Additional marker tooltip text', 'Tp_Form_Hotels_Collections_Location tooltip', TP_PLUGIN_TEXTDOMAIN)
                ),
                'required' => false
            ))

            ->add('apply_all', Type\CheckboxType::class, array(
                'label' => _x('Apply to all tables', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ));
        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}