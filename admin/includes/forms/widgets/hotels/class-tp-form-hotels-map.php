<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\hotels;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class Tp_Form_Hotels_Map extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            ))
            ->add('height', Type\IntegerType::class, array(
                'label' => _x('Height', 'TP_Form_Airtickets_Price_Map label', TP_PLUGIN_TEXTDOMAIN),
            ))->add('diameter', Type\IntegerType::class, array(
                'label' => _x('Diameter', 'TP_Form_Airtickets_Price_Map label', TP_PLUGIN_TEXTDOMAIN),
            ))
            ->add('color_scheme', includes\form\type\TP_Form_Type_Color_Pallete::class,
                array(
                    'label' => _x('Color scheme', 'TP_Form_Airtickets_Price_Subscription label', TP_PLUGIN_TEXTDOMAIN),
                    'choices' =>
                        array(
                            '#222222' => array(
                                'pin_color' => '#222222',
                                'text_color' => '#FFFFFF'),
                            '#98056A' => array(
                                'pin_color' => '#98056A',
                                'text_color' => '#FFFFFF'),
                            '#00AFE4' => array(
                                'pin_color' => '#00AFE4',
                                'text_color' => '#FFFFFF'),
                            '#74BA00' => array(
                                'pin_color' => '#74BA00',
                                'text_color' => '#FFFFFF'),
                            '#DADADA' => array(
                                'pin_color' => '#DADADA',
                                'text_color' => '#000')
                        ),
                    'required' => false
                )
            )
            ->add('drag', Type\CheckboxType::class, array(
                'label' => _x('Allow drag and drop', 'Tp_Form_Hotels_Map', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))->add('scaling', Type\CheckboxType::class, array(
                'label' => _x('Enable Scaling', 'Tp_Form_Hotels_Map', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))->add('scroll_scaling', Type\CheckboxType::class, array(
                'label' => _x('Scaling when scrolling', 'Tp_Form_Hotels_Map', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}