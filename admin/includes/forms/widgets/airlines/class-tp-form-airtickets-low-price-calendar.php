<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\airlines;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_Airtickets_Low_Price_Calendar extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;
    const DIRECTION_ONE_WAY = 0;
    const DIRECTION_ROUND = 1;

//--  [повтор]
//[label] Widget Dimensions
//[x] Stretch width +
//[num] WIDTH +
//-- [повтор]
//[label] Directions
//[ac] CITY OF DEPARTURE +
//[ac] CITY OF ARRIVAL +
//
//[label] Prices for the period +++
//--
//[x] Current month only
//[x] Whole year
//--
//[x] December
//[x] January
//[x] February
//[x] March
//[x] April
//[x] May
//[x] June
//[x] July
//[x] August
//[x] September
//[x] October
//[x] November
//--
//[range 0-30] Duration of the trip
//--
//[radio] Did not come up with the name of the setting
//-one way
//-round trip
//--
//[x] Only direct flights
//--


    public function form_builder(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dimensions::full_width', Type\CheckboxType::class, array(
                'label' => _x('Stretch width', 'TP_Form_Airtickets_Low_Price_Calendar', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('dimensions::width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false,
                'attr' => array(
                  'value' => 100,
                )
            ))
            ->add('origin', includes\form\type\TP_form_type_city::class, array(
                'label' => _x('Origin', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Please select a city', 'TP_Form_Airtickets_Low_Price_Calendar placeholder', TP_PLUGIN_TEXTDOMAIN),
                )
            ))
            ->add('destination', includes\form\type\TP_form_type_city::class, array(
                'label' => _x('Destination', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Please select a city', 'TP_Form_Airtickets_Low_Price_Calendar placeholder', TP_PLUGIN_TEXTDOMAIN),
                )
            ))
            ->add('duration', includes\form\type\TP_form_type_slider::class, array(
                'label' => _x('Duration', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'big_title'=> true
                ),
                'attr' => array(
                    'isRange' => 'true',
                    'from' => 0,
                    'to' => 30
                )
            ))
            ->add('direction', includes\form\type\TP_form_type_radio::class,
                array(
                    'label' => _x('Direction', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                    'choices' =>
                        array(
                            _x('One way', 'TP_Form_Airtickets_Low_Price_Calendar value', TP_PLUGIN_TEXTDOMAIN) => self::DIRECTION_ONE_WAY,
                            _x('Round trip', 'TP_Form_Airtickets_Low_Price_Calendar value', TP_PLUGIN_TEXTDOMAIN) => self::DIRECTION_ROUND
                        ),
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true
                )
            )
            ->add('direct', Type\CheckboxType::class, array(
                'label' => _x('Only direct flights', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ));


        $period_values = array(
            'current_month' => _x('Current month only', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'all_year' => _x('Whole year', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'dec' => _x('December', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'jan' => _x('January', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'feb' => _x('February', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'mar' => _x('March', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'apr' => _x('April', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'may' => _x('May', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'jun' => _x('June', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'jul' => _x('July', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'aug' => _x('August', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'sep' => _x('September', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'oct' => _x('October', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
            'nov' => _x('November', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
        );


        foreach ($period_values as $short_name => $long_name) {
            $builder
                ->add('period::' . $short_name, Type\CheckboxType::class, array(
                    'label' => $long_name,
                    'required' => false
                ));
        }

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}