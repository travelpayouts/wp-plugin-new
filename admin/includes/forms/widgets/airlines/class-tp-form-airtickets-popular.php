<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\airlines;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_Airtickets_Popular extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;


    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dimensions::full_width', Type\CheckboxType::class, array(
            'label' => _x('Stretch width', 'TP_Form_Airtickets_Low_Price_Calendar', TP_PLUGIN_TEXTDOMAIN),
            'required' => false
        ))
            ->add('dimensions::width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('number', includes\form\type\TP_form_type_radio::class,
                array(
                    'label' => _x('Number of widgets when added to a record', 'TP_Form_Airtickets_Popular label', TP_PLUGIN_TEXTDOMAIN),
                    'choices' =>
                        array(
                            _x('1', 'TP_Form_Airtickets_Popular value', TP_PLUGIN_TEXTDOMAIN) => 1,
                            _x('2', 'TP_Form_Airtickets_Popular value', TP_PLUGIN_TEXTDOMAIN) => 2,
                            _x('3', 'TP_Form_Airtickets_Popular value', TP_PLUGIN_TEXTDOMAIN) => 3
                        ),
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true
                )
            );

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}