<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\airlines;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_Airtickets_Price_Subscription extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;


    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dimensions::full_width', Type\CheckboxType::class, array(
            'label' => _x('Stretch width', 'TP_Form_Airtickets_Low_Price_Calendar', TP_PLUGIN_TEXTDOMAIN),
            'required' => false
        ))
            ->add('dimensions::width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('origin', includes\form\type\TP_form_type_city::class, array(
                'label' => _x('Origin', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Please select a city', 'TP_Form_Airtickets_Low_Price_Calendar placeholder', TP_PLUGIN_TEXTDOMAIN),
                )
            ))
            ->add('destination', includes\form\type\TP_form_type_city::class, array(
                'label' => _x('Destination', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Please select a city', 'TP_Form_Airtickets_Low_Price_Calendar placeholder', TP_PLUGIN_TEXTDOMAIN),
                )
            ))
            ->add('color_scheme', includes\form\type\TP_Form_Type_Color_Background_Pallete::class,
                array(
                    'label' => _x('Color scheme', 'TP_Form_Airtickets_Price_Subscription label', TP_PLUGIN_TEXTDOMAIN),
                    'choices' =>
                        array(
                            '#222222' => '#222222',
                            '#98056A' => '#98056A',
                            '#00AFE4' => '#00AFE4',
                            '#74BA00' => '#74BA00',
                            '#DB5521' => '#DB5521',
                            '#FFBC00' => '#FFBC00',
                        ),
                    'label_attr' => array(
                        'big_title' => true
                    )
                )
            );
        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}