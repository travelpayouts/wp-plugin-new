<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 */

namespace tp\admin\includes\forms\widgets\airlines;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_Airtickets_Offers extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;
    const FILTER_BY_DIRECTIONS = 0;
    const FILTER_BY_AIRLINES = 1;
    const WIDGET_DESIGN_TILE = 0;
    const WIDGET_DESIGN_SLIDER = 1;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', includes\form\type\TP_form_type_radio::class,
                array(
                    'label' => _x('Widget design', 'TP_Form_Airtickets_Offers label', TP_PLUGIN_TEXTDOMAIN),
                    'label_attr' => array(
                        'big_title'=> true
                    ),
                    'choices' =>
                        array(
                            _x('Tile', 'TP_Form_Airtickets_Offers value', TP_PLUGIN_TEXTDOMAIN) => self::WIDGET_DESIGN_TILE,
                            _x('Slider', 'TP_Form_Airtickets_Offers value', TP_PLUGIN_TEXTDOMAIN) => self::WIDGET_DESIGN_SLIDER
                        ),
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true
                )
            )
            ->add('dimensions::full_width', Type\CheckboxType::class, array(
                'label' => _x('Stretch width', 'TP_Form_Airtickets_Low_Price_Calendar', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('dimensions::width', Type\IntegerType::class, array(
                'label' => _x('Width', 'TP_Form_Airtickets_Low_Price_Calendar label', TP_PLUGIN_TEXTDOMAIN),
                'required' => false
            ))
            ->add('limit', includes\form\type\TP_form_type_slider::class, array(
                'label' => _x('Limit for special offers', 'TP_Form_Airtickets_Offers label', TP_PLUGIN_TEXTDOMAIN),
                'label_attr' => array(
                    'big_title'=> true
                ),
                'required' => false,
                'attr' => array(
                    'isRange' => 'false',
                    'from' => 0,
                    'to' => 30
                )
            ))
            ->add('filter', includes\form\type\TP_form_type_radio::class,
                array(
                    'label' => _x('Direction', 'TP_Form_Airtickets_Offers label', TP_PLUGIN_TEXTDOMAIN),
                    'choices' =>
                        array(
                            _x('by directions', 'TP_Form_Airtickets_Offers value', TP_PLUGIN_TEXTDOMAIN) => self::FILTER_BY_DIRECTIONS,
                            _x('by airlines', 'TP_Form_Airtickets_Offers value', TP_PLUGIN_TEXTDOMAIN) => self::FILTER_BY_AIRLINES
                        ),
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true
                )
            );
        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {
        $rules = array();
        return $rules;
    }
}