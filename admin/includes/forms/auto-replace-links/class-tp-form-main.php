<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\forms\auto_replace_links;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_main extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('replace_count', Type\IntegerType::class, array(
                'label' => _x('Anchor replacements limit', 'tp_admin_page_auto_links_input_limit_label', TP_PLUGIN_TEXTDOMAIN)
            ))
            ->add('insert_title', Type\CheckboxType::class, array(
                'required' => false,
                'label' => _x('Don\'t add links to all titles', 'tp_admin_page_auto_links_input_not_title_label', TP_PLUGIN_TEXTDOMAIN)
            ))
            ->add('insert_post', Type\CheckboxType::class, array(
                'required' => false,
                'label' => _x('Enable auto-links for new posts', 'tp_admin_page_auto_links_input_tp_auto_replac_link_label', TP_PLUGIN_TEXTDOMAIN)
            ))
            ->add('insert_exist', Type\CheckboxType::class, array(
                'required' => false,
                'label' => _x('Place auto-links in all existing posts', 'tp admin page auto links btn auto links', TP_PLUGIN_TEXTDOMAIN)
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {

    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {

    }
}