<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.10.2017
 */

namespace tp\admin\includes\forms\auto_replace_links;

use tp\includes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class TP_Form_items extends AbstractType
{
    use includes\TP_Form_base;
    public static $form = __CLASS__;

    public function form_builder(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('url', Type\TextType::class, array(
                'label' => _x('Link',
                    'tp admin page auto links edit input arl url label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('Enter a name for the form',
                        'tp admin page auto links edit input arl label help', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('anchor', Type\TextareaType::class, array(
                'label' => _x('Anchor phrase',
                    'tp admin page auto links edit textarea arl anchor label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('You may add several anchors, use comma as separator',
                        'tp admin page auto links edit textarea arl anchor label help', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('onclick', Type\TextareaType::class, array(
                'required' => false,
                'label' => _x('Events onclick',
                    'tp admin page auto links edit textarea arl event label', TP_PLUGIN_TEXTDOMAIN),
                'attr' => array(
                    'placeholder' => _x('You can add here custom events (e.g. Google Analytics events) that will be fired after the click on a link',
                        'tp admin page auto links edit textarea arl event label help', TP_PLUGIN_TEXTDOMAIN)
                )
            ))
            ->add('nofollow', Type\CheckboxType::class, array(
                'required' => false,
                'label' => _x('Add "nofollow" attribute',
                    'tp admin page auto links edit input arl nofollow label', TP_PLUGIN_TEXTDOMAIN)
            ))
            ->add('replace', Type\CheckboxType::class, array(
                'required' => false,
                'label' =>  _x('Replace existing links',
                    'tp admin page auto links edit input arl replace label', TP_PLUGIN_TEXTDOMAIN)
            ))
            ->add('target_blank', Type\CheckboxType::class, array(
                'required' => false,
                'label' =>  _x('Open in a new tab',
                    'tp admin page auto links edit input arl target blank label', TP_PLUGIN_TEXTDOMAIN)
            ));

        //Add transformers to form fields
        $this->add_transformers($builder);
    }

    protected function add_transformers(FormBuilderInterface $builder)
    {
        $builder->get('nofollow')->addModelTransformer($this->checkbox_transformer());
        $builder->get('replace')->addModelTransformer($this->checkbox_transformer());
        $builder->get('target_blank')->addModelTransformer($this->checkbox_transformer());
    }

    /**
     * Array of all validation rules
     * @return array
     */
    public function rules()
    {

    }
}