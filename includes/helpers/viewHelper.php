<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\helpers;

class viewHelper
{
    public static function js_object($data)
    {
        if ($data = json_encode($data)) {
            $data = addcslashes($data, '\'');
            return str_replace('"', '\'', $data);
        }
        return '{}';
    }

    public static function get_label_value_from_array($source)
    {
        return array_map(function ($table_key, $table_name) {
            return [
                'value' => $table_key,
                'label' => $table_name,
            ];
        }, array_keys($source), $source);
    }

    public static function array_to_element_attributes($params)
    {
        $mapped_params = array_map(function ($paramKey, $paramValue) {
            if (is_array($paramValue)) $paramValue = self::js_object($paramValue);
            return "{$paramKey}=\"{$paramValue}\"";
        }, array_keys($params), $params);
        return implode(' ', $mapped_params);
    }

    public static function wrap_in_brackets($input)
    {
        if (is_string($input)) {
            return '{{' . $input . '}}';
        }
        return '';
    }
}