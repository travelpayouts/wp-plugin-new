<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 18.10.2017
 */

namespace tp\includes;

use Symfony\Component\Form\Form;
use Adbar\Dot;
use tp\includes\TP_Request as Request;

abstract class TP_Model
{
    const DELIMITER = '/';

    public $section = '';
    protected $form = '';

    protected $section_options;
    protected $tp_options;


    public function __construct($section = false)
    {
        if ($section) {
            $this->section = $section;
        } elseif (empty($this->section)) {
            $class = get_called_class();
            throw new \Exception("[$class]: Model property \$section is empty");
        }
        $this->init();
    }

    public function init()
    {
        $this->tp_options = $this->get_plugin_options();
        $this->section_options = $this->get_section_options();
    }

    public function set_attributes($values)
    {
        if (is_array($values)) {
            $this->section_options->merge($values);
            return true;
        }
        return false;
    }

    public function get_attributes()
    {
        return $this->section_options->all();
    }

    public function get_data()
    {
        return $this->section_options->all();
    }

    /**
     * Get default options from model
     * @return array|mixed
     */
    protected function get_default_options()
    {
        $default_model_options = array();
        // getting default options if exists
        if (property_exists($this, 'defaultOptions'))
            $default_model_options = $this->defaultOptions;
        return $default_model_options;
    }

    /**
     * Getting values for $section
     * @param bool $section
     * @return Dot
     */
    protected function get_section_options($section = false)
    {
        if (!$section) $section = $this->section;
        $default_model_options = $this->get_default_options();
        $options = $this->tp_options->get($section, $default_model_options);
        return new Dot($options, self::DELIMITER);
    }

    protected function set_section_options($data = array(), $section = false)
    {
        if (!$section) $section = $this->section;
        $this->tp_options->set($section, $data);
        return true;
    }

    public function reset_section_options()
    {
        $default_model_options = $this->get_default_options();
        if ($this->set_section_options($default_model_options)) {
            $updated_plugin_options = $this->get_plugin_options()->all();
            update_option(TP_PLUGIN_OPTION_NAME, $updated_plugin_options);
        }
        return true;
    }

    /**
     * Getting all plugin options
     * @return Dot
     */
    protected function get_plugin_options()
    {
        if (!$this->tp_options) {
            $options = get_option(TP_PLUGIN_OPTION_NAME);
            if (!$options) $options = array();
            return new Dot($options, self::DELIMITER);
        }
        return $this->tp_options;
    }

    /**
     * Section option getter
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Section option getter
     * @param $name
     * @param $default_value
     * @return mixed
     */
    public function get($name, $default_value = null)
    {
        $method_function = strtolower($name);
        $method_name = "get_{$method_function}";
        // call method that equals $name if it exists
        if (method_exists($this, $method_name))
            return $this->{$method_name}();

        return $this->section_options->get($name, $default_value);
    }

    /**
     * Section option setter
     * @param $name
     * @param $value
     * @return bool
     */
    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }

    /**
     * Section option setter
     * @param $name
     * @param $value
     * @return bool
     */
    public function set($name, $value)
    {
        $method_function = strtolower($name);
        $method_name = "set_{$method_function}";
        // call method that equals $name if it exists
        if (method_exists($this, $method_name))
            return $this->{$method_name}($value);

        $this->section_options->set($name, $value);
        return true;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->section_options->has($name);
    }

    /**
     * Saving data to wordpress options
     */
    public function save()
    {
        $attributes = $this->attributes;
        $this->set_section_options($attributes);
        $updated_plugin_options = $this->get_plugin_options()->all();
        return update_option(TP_PLUGIN_OPTION_NAME, $updated_plugin_options);
    }

    public function reset_form()
    {
        ob_clean();
        if ($this->reset_section_options()) {
            echo json_encode(array(
                'status' => 'ok'
            ));
        }
        die();
    }

    /**
     * Handle submitting of a form
     * @param Form $form
     * @param bool $message
     */
    public function submit_form(Form $form, $message = false)
    {
        if (Request::instance()->is_get() && Request::instance()->get_query($form->getName())) {
            ob_clean();
            $data = $form->getData();
            $form_data = array();
            if (method_exists($data, 'get_attributes')) {
                $form_data = array(
                    'name' => $form->getName(),
                    'data' => $form->getData()->attributes,
                );
            }
            echo json_encode($form_data);
            die();
        }

        if (Request::instance()->get_query('form_action') === 'reset_____') {
//            $this->reset_form();
        } elseif (Request::instance()->is_post()) {
            TP_form_helper::submit_form($form, function () {
                $this->save();
                return true;
            });
        }
    }

    public static function get_class()
    {
        return get_called_class();
    }

    abstract public function get_form();
}