<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.09.17
 */

namespace tp\includes;

class TP_Admin_pages
{
    protected $plugin_name;

    public function __construct($plugin_name)
    {
        $this->plugin_name = $plugin_name;
    }

    /**
     * return class
     * @param $plugin_name
     * @return TP_Admin_pages
     */
    public static function get($plugin_name)
    {
        return new self($plugin_name);
    }


    /**
     * Add an options page under the Settings submenu
     */

    public function run()
    {
        $routes = \tp\includes\TP_Config::get()->get_admin_pages();
        $this->set_options_page($routes);
    }

    /**
     * Adding menu and submenu to admin page
     * @param $routes
     */
    private function set_options_page($routes)
    {
        foreach ($routes as $route => $route_subcategories) {
            $route_controller = $this->get_controller_path($route);

            if ($route_controller) {
                $route_menu = $this->get_menu_element($route);
                $parent_slug = $route_menu['menu_slug'];
                call_user_func_array('add_menu_page', $route_menu);
                if (is_array($route_subcategories)) {
                    foreach ($route_subcategories as $route_subcategory) {
                        $subcategory_controller = $this->get_controller_path($route_subcategory);
                        if ($subcategory_controller) {
                            $sub_route_menu = $this->get_sub_menu_element($route_subcategory, $parent_slug);
                            call_user_func_array('add_submenu_page', $sub_route_menu);
                        }

                    }
                }
            }
        }
    }

    /**
     * Get controller path by class name
     * @param $className
     * @return bool|string
     */
    private function get_controller_path($className)
    {
        $name = '\tp\admin\controllers\TP_' . $className . '_Controller';
        if (class_exists($name)) {
            return $name;
        }
        return false;
    }

    /**
     * Prepare data for add_menu_page function
     * @param $className
     * @return array
     */
    private function get_menu_element($className)
    {
        $controller = $this->get_controller_path($className);
        $result = array();
        if ($controller) {
            $result = array(
                'title' => $controller::get()->get_page_title(),
                'menuTitle' => $controller::get()->get_menu_title(),
                'capability' => 'manage_options',
                'menu_slug' => $this->plugin_name . '_' . strtolower($controller::get()->get_route_name()),
                'function' => array($controller::get(), 'render_controller'),
                'icon_url' => $controller::get()->get_icon());
        }
        return $result;
    }

    /**
     * Prepare data for add_submenu_page function
     * @param $className
     * @param $parent_slug
     * @return array
     */
    private function get_sub_menu_element($className, $parent_slug)
    {
        $menu_element = $this->get_menu_element($className);
        if (!empty($menu_element)) {
            array_unshift($menu_element, $parent_slug);
            unset($menu_element['icon_url']);
        }
        return $menu_element;
    }

}