<?php

/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes;

use Adbar\Dot;

class TP_Request
{
    private static $instance;
    private $query_params;
    private $post_data;

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    private function get_query_params()
    {
        if (!$this->query_params) {
            $this->query_params = new Dot($_GET, '/');
        }
        return $this->query_params;
    }

    public function get_query($name = null, $default_value = null)
    {
        $params = $this->get_query_params();
        if (!$name) return $params->all();
        return $params->get($name, $default_value);
    }

    public function is_get()
    {
        return $this->get_method() === 'GET';
    }

    private function get_post_params()
    {
        if (!$this->post_data) {
            $this->post_data = new Dot($_POST, '/');
        }
        return $this->post_data;
    }

    public function post($name = null, $default_value = null)
    {
        $params = $this->get_post_params();
        if (!$name) return $params->all();
        return $params->get($name, $default_value);
    }

    public function is_post()
    {
        return $this->get_method() === 'POST';
    }

    public function get_method()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            return strtoupper($_SERVER['REQUEST_METHOD']);
        }
        return 'GET';
    }

}