<?php

/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes;

class Tp_Webpack
{

    protected $path = false;
    protected $assets = false;

    public function __construct($path)
    {
        $path_info = pathinfo($path);
        if ($path_info['extension'] === 'json' && file_exists($path)) {
            $this->path = $path;
            $assets_file = json_decode(file_get_contents($path), true);
            if ($assets_file && isset($assets_file['assetsByChunkName'])) {
                $this->assets = $assets_file['assetsByChunkName'];
            }
        }
    }

    public function get_assets_by_name($name, $url, $type = false)
    {
        if ($this->assets && isset($this->assets[$name])) {
            $asset = $this->assets[$name];

            if ($type) {
                $asset = array_filter($asset, function ($path) use ($type) {
                    $path_info = pathinfo($path);
                    return $path_info['extension'] === $type;
                });
            }
            return array_map(function ($path) use ($url) {
                return $url . $path;
            }, $asset);
        }
        return array();
    }
}