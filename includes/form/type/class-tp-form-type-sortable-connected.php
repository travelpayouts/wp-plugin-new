<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\CallbackTransformer;

/**
 * Class TP_form_type_sortable_connected
 * @package tp\includes\form\type
 *
 * Usage
 *
 *  add('field', includes\form\type\TP_form_type_sortable_connected::class, array(
 *        'label' => '...',
 *        'description' => '...',
 *        'rows' => array(
 *            'notSelected' => array(
 *                'label' => 'Not selected label',
 *                'defaultValueCollection' => true
 *           ),
 *            'selected' => array(
 *                'label' => 'Selected label'
 *          )
 *        ),
 *        'values' => array(
 *            0 => 'test1',
 *            1 => 'test2',
 *        ),
 *    ));
 */
class TP_form_type_sortable_connected extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($values) {
                return $values;
            },
            function ($values) {
                // clean empty values before saving
                if (is_array($values)) {
                    foreach ($values as $key => $val) {
                        if (is_null($val)) {
                            unset($values[$key]);
                        }
                    }
                }
                return $values;
            }
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'values' => array(),
            'description' => '',
            'rows' => array(),
            'allow_add' => true,
            'required' => false
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_replace($view->vars, array(
            'rows' => $options['rows'],
            'values' => $options['values'],
            'description' => $options['description'],
        ));

        if ($view->vars['value']) {
            $categories = $this->flatten_values($view->vars['value']);
            $view->vars['all_values_used'] = count($categories) === count($options['values']);
            $unused_values = array_diff(array_keys($options['values']), $categories);

            if (!$view->vars['all_values_used'] && !empty($unused_values)) {

                $default_row = array_filter($view->vars['rows'], function ($item) {
                    return isset($item['defaultValueCollection']) && $item['defaultValueCollection'];
                });

                if (!empty($default_row)) {
                    $default_row = array_shift(array_keys($default_row));
                    $default_row_values = $view->vars['value'][$default_row];
                    if (is_null($default_row_values)) {
                        $view->vars['value'][$default_row] = $unused_values;
                    } elseif (is_array($default_row_values)) {
                        $view->vars['value'][$default_row] = array_merge($view->vars['value'][$default_row], $unused_values);

                    }
                    $view->vars['all_values_used'] = true;
                }


            }
        } else {
            $view->vars['all_values_used'] = false;
        }
    }

    private function flatten_values($values)
    {
        $categories = array();
        foreach ($values as $value_categories) {
            if (is_array($value_categories)) {
                $categories = array_merge($categories, $value_categories);
            }
        }
        return $categories;
    }


    public function getBlockPrefix()
    {

        return 'tp_ui_sortable_connected';
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}