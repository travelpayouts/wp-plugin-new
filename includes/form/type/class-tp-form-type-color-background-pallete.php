<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * Class TP_Form_Type_Color_Background_Pallete
 * @package tp\includes\form\type
 * Example
 * ```
 * ->add('color_scheme', includes\form\type\TP_Form_Type_Color_Background_Pallete::class,
 * array(
 * 'label' => _x('Color scheme', 'TP_Form_Airtickets_Price_Subscription label', TP_PLUGIN_TEXTDOMAIN),
 * 'choices' =>
 * array(
 * '#222222' => '#222222',
 * '#98056A' => '#98056A',
 * '#00AFE4' => '#00AFE4',
 * '#74BA00' => '#74BA00',
 * '#DB5521' => '#DB5521',
 * '#FFBC00' => '#FFBC00',
 * ),
 * 'label_attr' => array(
 * 'big_title' => true
 * )
 * )
 * )
 * ```
 */
class TP_Form_Type_Color_Background_Pallete extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(),
            'label_custom_color' => 'Background color'
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_merge($view->vars, array(
            'choices' => $options['choices'],
            'label_custom_color' => $options['label_custom_color']
        ));
    }


    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'tp_color_pallete_bg';
    }
}