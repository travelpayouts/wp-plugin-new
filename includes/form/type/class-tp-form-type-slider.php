<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class TP_form_type_slider
 * @package tp\includes\form\type
 * usage
 * add('field', includes\form\type\TP_form_type_slider::class, array(
 *         'label' => 'field label',
 *         'attr' => array(
 *             'isRange' => 'true',
 *             'from' => 0,
 *             'to' => 30
 *         )
 *   ))
 */
class TP_form_type_slider extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setNormalizer('attr', function ($options, $value) {
            // Merge the child constraints with the these, the parent constraints
            return array_merge(array(
                'from' => 0,
                'to' => 1,
                'step' => 1,
                'width' => 270,
                'showScale' => 'false',
                'format' => '%s',
                'showLabels' => 'true',
                'isRange' => 'false'
            ), $value);
        });

        $resolver->setDefaults(array(
            'compound' => false,
        ));
    }


    public function getBlockPrefix()
    {
        return 'tp_slider';
    }
}