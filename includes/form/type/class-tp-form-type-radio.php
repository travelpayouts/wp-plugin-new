<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TP_form_type_radio extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices_as_values' => true,
            'multiple' => false,
            'expanded' => true
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix()
    {
        return 'tp_radio';
    }
}