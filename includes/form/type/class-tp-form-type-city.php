<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TP_form_type_city extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=> array(
                'class'=> 'prompt'
            )
        ));
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix(){
        return 'tp_city';
    }
}