<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 15.12.2017
 */

namespace tp\includes\form\type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TP_Form_Type_Color_Pallete
 * @package tp\includes\form\type
 *
 * Example:
 *
 * ```
 * ->add('color_scheme_two', includes\form\type\TP_Form_Type_Color_Pallete::class,
 * array(
 * 'label' => _x('Color scheme', 'TP_Form_Airtickets_Price_Subscription label', TP_PLUGIN_TEXTDOMAIN),
 * 'choices' =>
 * array(
 * '#222222' => array(
 * 'pin_color' => '#222222',
 * 'text_color' => '#FFFFFF'),
 * '#98056A' => array(
 * 'pin_color' => '#98056A',
 * 'text_color' => '#FFFFFF'),
 * '#00AFE4' => array(
 * 'pin_color' => '#00AFE4',
 * 'text_color' => '#FFFFFF'),
 * '#74BA00' => array(
 * 'pin_color' => '#74BA00',
 * 'text_color' => '#FFFFFF'),
 * '#DADADA' => array(
 * 'pin_color' => '#DADADA',
 * 'text_color' => '#000')
 * ),
 * 'label_attr' => array(
 * 'big_title' => true
 * ),
 * 'required' => false
 *
 * )
 * );
 * ```
 */
class TP_Form_Type_Color_Pallete extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('pin_color', TP_form_type_Colorpicker::class)
            ->add('text_color', TP_form_type_Colorpicker::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'entry_type' => TextType::class,
            'by_reference' => false,
            'choices' => array(),
            'label_custom_color' => 'Background color'
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_merge($view->vars, array(
            'choices' => $options['choices'],
            'label_custom_color' => $options['label_custom_color']
        ));
    }


    public function getParent()
    {
        return FormType::class;
    }


}