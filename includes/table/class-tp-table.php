<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 3/1/2018
 * Time: 9:19 AM
 */

namespace tp\includes\table;


class TP_Table
{
    const COLUMN_BUTTON = 'button'; // Button
    const COLUMN_URL = 'url'; // URL
    const DATA_COLUMN_RAW = 'val';
    const DATA_COLUMN_OUTPUT = 'output';

    /**
     * Column Button
     * @return string
     */
    public static function get_column_button(){
        return self::COLUMN_BUTTON;
    }

    /**
     * Column option label Button
     * @return string|void
     */
    public static function get_column_option_label_button(){
        return _x('Button', 'TP_Table get_column_option_label_button', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * @param string $column
     * @param string $field
     * @return string
     */
    public static function get_column_field_class($column = '', $field = ''){
        $class = 'travel-table-body__col_';
        if (!empty($column)){
            $class .= $column;
        }
        if (!empty($field)){
            $class .= '_field_'.$field;
        }
        return $class;
    }
}