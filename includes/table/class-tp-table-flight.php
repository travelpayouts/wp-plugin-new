<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 3/1/2018
 * Time: 9:27 AM
 */

namespace tp\includes\table;


class TP_Table_Flight extends TP_Table
{
    const COLUMN_FLIGHT_NUMBER = 'flight_number'; // Flight number
    const COLUMN_FLIGHT = 'flight'; // Flight
    const COLUMN_DEPART_DATE = 'depart_date'; // Departure date
    const COLUMN_RETURN_DATE = 'return_date'; // Return date
    const COLUMN_NUMBER_OF_CHANGES = 'number_of_changes'; // Stops
    const COLUMN_PRICE = 'price'; // Price
    const COLUMN_AIRLINE = 'airline'; // Airlines
    const COLUMN_AIRLINE_LOGO = 'airline_logo'; // Logo airlines
    const COLUMN_ORIGIN = 'origin'; // Origin
    const COLUMN_DESTINATION = 'destination'; // Destination
    const COLUMN_PLACE = 'place'; // Rank
    const COLUMN_DIRECTION = 'direction'; // Direction
    const COLUMN_TRIP_CLASS = 'trip_class'; // Flight class
    const COLUMN_DISTANCE = 'distance'; // Distance
    const COLUMN_PRICE_DISTANCE = 'price_distance'; // Price/distance
    const COLUMN_FOUND_AT = 'found_at'; // When found
    const COLUMN_BACK_AND_FORTH = 'back_and_forth'; // One way / Round-Trip
    const COLUMN_ORIGIN_DESTINATION = 'origin_destination'; // Origin - Destination


    /**
     * Column Flight number
     * @return string
     */
    public static function get_column_flight_number(){
        return self::COLUMN_FLIGHT_NUMBER;
    }
    /**
     * Column option label Flight number
     * @return string|void
     */
    public static function get_column_option_label_flight_number(){
        return _x('Flight number', 'TP_Table_Flight get_column_option_label_flight_number', TP_PLUGIN_TEXTDOMAIN);
    }
    /**
     * Column Flight
     * @return string
     */
    public static function get_column_flight(){
        return self::COLUMN_FLIGHT;
    }

    /**
     * Column option label Flight
     * @return string|void
     */
    public static function get_column_option_label_flight(){
        return _x('Flight', 'TP_Table_Flight get_column_option_label_flight', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Departure date
     * @return string
     */
    public static function get_column_depart_date(){
        return self::COLUMN_DEPART_DATE;
    }

    /**
     * Column option label Departure date
     * @return string|void
     */
    public static function get_column_option_label_depart_date(){
        return _x('Departure date', 'TP_Table_Flight get_column_option_label_depart_date', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Return date
     * @return string
     */
    public static function get_column_return_date(){
        return self::COLUMN_RETURN_DATE;
    }

    /**
     * Column option label Return date
     * @return string|void
     */
    public static function get_column_option_label_return_date(){
        return _x('Return date', 'TP_Table_Flight get_column_option_label_return_date', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Stops
     * @return string
     */
    public static function get_column_number_of_changes(){
        return self::COLUMN_NUMBER_OF_CHANGES;
    }

    /**
     * Column option label Stops
     * @return string|void
     */
    public static function get_column_option_label_number_of_changes(){
        return _x('Stops', 'TP_Table_Flight get_column_option_label_number_of_changes', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Price
     * @return string
     */
    public static function get_column_price(){
        return self::COLUMN_PRICE;
    }

    /**
     * Column option label Price
     * @return string|void
     */
    public static function get_column_option_label_price(){
        return _x('Price', 'TP_Table_Flight get_column_option_label_price', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column airlines
     * @return string
     */
    public static function get_column_airline(){
        return self::COLUMN_AIRLINE;
    }

    /**
     * Column option label airlines
     * @return string|void
     */
    public static function get_column_option_label_airline(){
        return _x('Airlines', 'TP_Table_Flight get_column_option_label_airline', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column airline logo
     * @return string
     */
    public static function get_column_airline_logo(){
        return self::COLUMN_AIRLINE_LOGO;
    }

    /**
     * Column option label airline logo
     * @return string|void
     */
    public static function get_column_option_label_airline_logo(){
        return _x('Logo airlines', 'TP_Table_Flight get_column_option_label_airline_logo', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Origin
     * @return string
     */
    public static function get_column_origin(){
        return self::COLUMN_ORIGIN;
    }

    /**
     * Column option label Origin
     * @return string|void
     */
    public static function get_column_option_label_origin(){
        return _x('Origin', 'TP_Table_Flight get_column_option_label_origin', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column destination
     * @return string
     */
    public static function get_column_destination(){
        return self::COLUMN_DESTINATION;
    }

    /**
     * Column option label destination
     * @return string|void
     */
    public static function get_column_option_label_destination(){
        return _x('Destination', 'TP_Table_Flight get_column_option_label_destination', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Rank
     * @return string
     */
    public static function get_column_place(){
        return self::COLUMN_PLACE;
    }

    /**
     * Column option label Rank
     * @return string|void
     */
    public static function get_column_option_label_place(){
        return _x('Rank', 'TP_Table_Flight get_column_option_label_place', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column direction
     * @return string
     */
    public static function get_column_direction(){
        return self::COLUMN_DIRECTION;
    }

    /**
     * Column option label direction
     * @return string|void
     */
    public static function get_column_option_label_direction(){
        return _x('Direction', 'TP_Table_Flight get_column_option_label_direction', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column Flight class
     * @return string
     */
    public static function get_column_trip_class(){
        return self::COLUMN_TRIP_CLASS;
    }

    /**
     * Column option label Flight class
     * @return string|void
     */
    public static function get_column_option_label_trip_class(){
        return _x('Flight class', 'TP_Table_Flight get_column_option_label_trip_class', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column distance
     * @return string
     */
    public static function get_column_distance(){
        return self::COLUMN_DISTANCE;
    }

    /**
     * Column option label distance
     * @return string|void
     */
    public static function get_column_option_label_distance(){
        return _x('Distance', 'TP_Table_Flight get_column_option_label_distance', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column price distance
     * @return string
     */
    public static function get_column_price_distance(){
        return self::COLUMN_PRICE_DISTANCE;
    }

    /**
     * Column option label distance
     * @return string|void
     */
    public static function get_column_option_label_price_distance(){
        return _x('Price/distance', 'TP_Table_Flight get_column_option_label_price_distance', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column When found
     * @return string
     */
    public static function get_column_found_at(){
        return self::COLUMN_FOUND_AT;
    }

    /**
     * Column option label When found
     * @return string|void
     */
    public static function get_column_option_label_found_at(){
        return _x('When found', 'TP_Table_Flight get_column_option_label_found_at', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column One way / Round-Trip
     * @return string
     */
    public static function get_column_back_and_forth(){
        return self::COLUMN_BACK_AND_FORTH;
    }

    /**
     * Column option label One way / Round-Trip
     * @return string|void
     */
    public static function get_column_option_label_back_and_forth(){
        return _x('One way / Round-Trip', 'TP_Table_Flight get_column_option_label_back_and_forth', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column origin destination
     * @return string
     */
    public static function get_column_origin_destination(){
        return self::COLUMN_ORIGIN_DESTINATION;
    }

    /**
     * Column option label destination
     * @return string|void
     */
    public static function get_column_option_label_origin_destination(){
        return _x('Origin - Destination', 'TP_Table_Flight get_column_option_label_origin_destination', TP_PLUGIN_TEXTDOMAIN);
    }

    public static function get_columns($columns){

    }

}