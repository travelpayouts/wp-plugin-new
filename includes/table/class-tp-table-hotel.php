<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 6/15/18
 * Time: 10:16
 */

namespace tp\includes\table;


class TP_Table_Hotel extends TP_Table
{
    const COLUMN_HOTEL = 'hotel';                                        // Hotel
    const COLUMN_STARS = 'stars';                                       // Stars
    const COLUMN_PRICE_PN = 'price_pn';                                 // Price per night, from
    const COLUMN_DISTANCE = 'distance';                                 // To the center
    const COLUMN_RATING = 'rating';                                     // Rating
    const COLUMN_DISCOUNT = 'discount';                                 // Discount
    const COLUMN_OLD_PRICE_AND_NEW_PRICE = 'old_price_and_new_price';   // Old and new price
    const COLUMN_OLD_PRICE_AND_DISCOUNT = 'old_price_and_discount';     // Price before and discount
    const COLUMN_OLD_PRICE_PN = 'old_price_pn';                         // Price before discount


    /**
     * Column hotel
     * @return string
     */
    public static function get_column_hotel(){
        return self::COLUMN_HOTEL;
    }

    /**
     * Column option label hotel
     * @return string|void
     */
    public static function get_column_option_label_hotel(){
        return _x('Hotel', 'TP_Table_Hotel get_column_option_label_hotel', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column stars
     * @return string
     */
    public static function get_column_stars(){
        return self::COLUMN_STARS;
    }

    /**
     * Column option label stars
     * @return string|void
     */
    public static function get_column_option_label_stars(){
        return _x('Stars', 'TP_Table_Hotel get_column_option_label_stars', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column price_pn
     * @return string
     */
    public static function get_column_price_pn(){
        return self::COLUMN_PRICE_PN;
    }

    /**
     * Column option label price_pn
     * @return string|void
     */
    public static function get_column_option_label_price_pn(){
        return _x('Price per night, from', 'TP_Table_Hotel get_column_option_label_price_pn', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column distance
     * @return string
     */
    public static function get_column_distance(){
        return self::COLUMN_DISTANCE;
    }

    /**
     * Column option label distance
     * @return string|void
     */
    public static function get_column_option_label_distance(){
        return _x('To the center', 'TP_Table_Hotel get_column_option_label_distance', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column rating
     * @return string
     */
    public static function get_column_rating(){
        return self::COLUMN_RATING;
    }

    /**
     * Column option label rating
     * @return string|void
     */
    public static function get_column_option_label_rating(){
        return _x('Rating', 'TP_Table_Hotel get_column_option_label_rating', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column discount
     * @return string
     */
    public static function get_column_discount(){
        return self::COLUMN_DISCOUNT;
    }

    /**
     * Column option label discount
     * @return string|void
     */
    public static function get_column_option_label_discount(){
        return _x('Discount', 'TP_Table_Hotel get_column_option_label_discount', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column old_price_and_new_price
     * @return string
     */
    public static function get_column_old_price_and_new_price(){
        return self::COLUMN_OLD_PRICE_AND_NEW_PRICE;
    }

    /**
     * Column option label old_price_and_new_price
     * @return string|void
     */
    public static function get_column_option_label_old_price_and_new_price(){
        return _x('Old and new price', 'TP_Table_Hotel get_column_option_label_old_price_and_new_price', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column old_price_and_discount
     * @return string
     */
    public static function get_column_old_price_and_discount(){
        return self::COLUMN_OLD_PRICE_AND_DISCOUNT;
    }

    /**
     * Column option label old_price_and_discount
     * @return string|void
     */
    public static function get_column_option_label_old_price_and_discount(){
        return _x('Price before and discount', 'TP_Table_Hotel get_column_option_label_old_price_and_discount', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column old_price_pn
     * @return string
     */
    public static function get_column_old_price_pn(){
        return self::COLUMN_OLD_PRICE_PN;
    }

    /**
     * Column option label old_price_pn
     * @return string|void
     */
    public static function get_column_option_label_old_price_pn(){
        return _x('Price before discount', 'TP_Table_Hotel get_column_option_label_old_price_pn', TP_PLUGIN_TEXTDOMAIN);
    }
}