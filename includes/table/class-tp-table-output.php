<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 6/8/18
 * Time: 14:23
 */

namespace tp\includes\table;


class TP_Table_Output
{
    protected $rows;
    protected $title;

    public function set_rows($rows){
        $this->rows = $rows;
    }

    public function get_rows(){
        return $this->rows;
    }

    public function set_title($title){
        $this->title = $title;
    }

    public function get_title(){
        return $this->title;
    }
}