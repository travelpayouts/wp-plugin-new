<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 6/21/18
 * Time: 16:30
 */

namespace tp\includes\table;


class TP_Table_Railway extends TP_Table
{
    const COLUMN_TRAIN = 'train';                                                // Train
    const COLUMN_TRAIN_FIELD_TRAIN_NUMBER = 'train_number';                      // Train Field Train Number
    const COLUMN_TRAIN_FIELD_NAME = 'name';                                      // Train Field Name
    const COLUMN_TRAIN_FIELD_FIRM = 'firm';                                      // Train Field Firm
    const COLUMN_ROUTE = 'route';                                                // Route
    const COLUMN_ROUTE_FIELD_DEPARTURE_STATION = 'departure_station';            // Route Field Departure Station
    const COLUMN_ROUTE_FIELD_ARRIVAL_STATION = 'arrival_station';                // Route Field Arrival Station
    const COLUMN_ROUTE_FIELD_RUN_DEPARTURE_STATION = 'run_departure_station';    // Route Field Run Departure Station
    const COLUMN_ROUTE_FIELD_RUN_ARRIVAL_STATION = 'run_arrival_station';        // Route Field Run Arrival Station
    const COLUMN_DEPARTURE = 'departure';                                        // Departure
    const COLUMN_DEPARTURE_FIELD_DEPARTURE_TIME = 'departure_time';              // Departure Field Departure Time
    const COLUMN_DEPARTURE_FIELD_TIME = 'time';                                  // Departure Field Time
    const COLUMN_DEPARTURE_FIELD_DEPARTURE_STATION = 'departure_station';        // Departure Field Departure Station
    const COLUMN_ARRIVAL = 'arrival';                                            // Arrival
    const COLUMN_ARRIVAL_FIELD_ARRIVAL_TIME = 'arrival_time';                    // Arrival Field Arrival Time
    const COLUMN_ARRIVAL_FIELD_TIME = 'time';                                    // Arrival Field Time
    const COLUMN_ARRIVAL_FIELD_DURATION_DAY = 'duration_day';                    // Arrival Field Duration Day
    const COLUMN_ARRIVAL_FIELD_ARRIVAL_STATION = 'arrival_station';              // Arrival Field Arrival Station
    const COLUMN_DURATION = 'duration';                                  // Duration
    const COLUMN_PRICES = 'prices';                                      // Prices
    const COLUMN_DATES = 'dates';                                        // Dates
    const COLUMN_ORIGIN = 'origin';                                      // From
    const COLUMN_DESTINATION = 'destination';                            // To
    const COLUMN_DEPARTURE_TIME = 'departure_time';                      // Departure Time
    const COLUMN_ARRIVAL_TIME = 'arrival_time';                          // Arrival Time
    const COLUMN_ROUTE_FIRST_STATION = 'route_first_station';            // Route's First Station
    const COLUMN_ROUTE_LAST_STATION = 'route_last_station';              // Route's Last Station



    /**
     * Column train
     * @return string
     */
    public static function get_column_train(){
        return self::COLUMN_TRAIN;
    }

    /**
     * Column option label train
     * @return string|void
     */
    public static function get_column_option_label_train(){
        return _x('Train', 'TP_Table_Railway get_column_option_label_train', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column route
     * @return string
     */
    public static function get_column_route(){
        return self::COLUMN_ROUTE;
    }

    /**
     * Column option label route
     * @return string|void
     */
    public static function get_column_option_label_route(){
        return _x('Route', 'TP_Table_Railway get_column_option_label_route', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column departure
     * @return string
     */
    public static function get_column_departure(){
        return self::COLUMN_DEPARTURE;
    }

    /**
     * Column option label departure
     * @return string|void
     */
    public static function get_column_option_label_departure(){
        return _x('Departure', 'TP_Table_Railway get_column_option_label_departure', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column arrival
     * @return string
     */
    public static function get_column_arrival(){
        return self::COLUMN_ARRIVAL;
    }

    /**
     * Column option label arrival
     * @return string|void
     */
    public static function get_column_option_label_arrival(){
        return _x('Arrival', 'TP_Table_Railway get_column_option_label_arrival', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column duration
     * @return string
     */
    public static function get_column_duration(){
        return self::COLUMN_DURATION;
    }

    /**
     * Column option label duration
     * @return string|void
     */
    public static function get_column_option_label_duration(){
        return _x('Duration', 'TP_Table_Railway get_column_option_label_duration', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column prices
     * @return string
     */
    public static function get_column_prices(){
        return self::COLUMN_PRICES;
    }

    /**
     * Column option label prices
     * @return string|void
     */
    public static function get_column_option_label_prices(){
        return _x('Prices', 'TP_Table_Railway get_column_option_label_prices', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column dates
     * @return string
     */
    public static function get_column_dates(){
        return self::COLUMN_DATES;
    }

    /**
     * Column option label dates
     * @return string|void
     */
    public static function get_column_option_label_dates(){
        return _x('Dates', 'TP_Table_Railway get_column_option_label_dates', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column origin
     * @return string
     */
    public static function get_column_origin(){
        return self::COLUMN_ORIGIN;
    }

    /**
     * Column option label origin
     * @return string|void
     */
    public static function get_column_option_label_origin(){
        return _x('From', 'TP_Table_Railway get_column_option_label_origin', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column destination
     * @return string
     */
    public static function get_column_destination(){
        return self::COLUMN_DESTINATION;
    }

    /**
     * Column option label destination
     * @return string|void
     */
    public static function get_column_option_label_destination(){
        return _x('To', 'TP_Table_Railway get_column_option_label_destination', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column departure_time
     * @return string
     */
    public static function get_column_departure_time(){
        return self::COLUMN_DEPARTURE_TIME;
    }

    /**
     * Column option label departure_time
     * @return string|void
     */
    public static function get_column_option_label_departure_time(){
        return _x('Departure Time', 'TP_Table_Railway get_column_option_label_departure_time', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column arrival_time
     * @return string
     */
    public static function get_column_arrival_time(){
        return self::COLUMN_ARRIVAL_TIME;
    }

    /**
     * Column option label arrival_time
     * @return string|void
     */
    public static function get_column_option_label_arrival_time(){
        return _x('Arrival Time', 'TP_Table_Railway get_column_option_label_arrival_time', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column route_first_station
     * @return string
     */
    public static function get_column_route_first_station(){
        return self::COLUMN_ROUTE_FIRST_STATION;
    }

    /**
     * Column option label route_first_station
     * @return string|void
     */
    public static function get_column_option_label_route_first_station(){
        return _x('Route\'s First Station', 'TP_Table_Railway get_column_option_label_route_first_station', TP_PLUGIN_TEXTDOMAIN);
    }

    /**
     * Column route_last_station
     * @return string
     */
    public static function get_column_route_last_station(){
        return self::COLUMN_ROUTE_LAST_STATION;
    }

    /**
     * Column option label route_last_station
     * @return string|void
     */
    public static function get_column_option_label_route_last_station(){
        return _x('Route\'s Last Station', 'TP_Table_Railway get_column_option_label_route_last_station', TP_PLUGIN_TEXTDOMAIN);
    }

}