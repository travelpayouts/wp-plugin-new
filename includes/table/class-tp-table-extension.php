<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 4/25/18
 * Time: 13:02
 */

namespace tp\includes\table;


class TP_Table_Extension extends \Twig_Extension
{
    public function getName()
    {
        return 'tp_table_extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'hide_column_class',
                array($this, 'getHiddenResponsiveClassName')
            ),
            new \Twig_SimpleFunction(
                'is_shortcode_table',
                array($this, 'isShortcodeTable')
            ),
            new \Twig_SimpleFunction(
                'column_field_class',
                array($this, 'columnFieldClass')
            )
        );
    }

    public function hideColumnClass($hiddenColumns, $column)
    {
        $classHide = '';
        if (is_array($hiddenColumns) && in_array($column, $hiddenColumns)) {
            $classHide = 'travel-table__col_hidden';
        }
        return $classHide;
    }

    /**
     * Append classes to column based on hidden category name
     * @param $hiddenCategories
     * @param $currentColumn
     * @return string
     */
    public function getHiddenResponsiveClassName($hiddenCategories = [], $currentColumn = '')
    {
        $prefixClassName = 'r-travel-table-hidden_';
        $appendedClassNames = [];
        // failback to old method if array of categories is not multidimensional
        if (count($hiddenCategories) === count($hiddenCategories, COUNT_RECURSIVE))
            return $this->hideColumnClass($hiddenCategories, $currentColumn);

        foreach ($hiddenCategories as $hiddenCategoryName => $columns) {
            if (is_array($columns) && in_array($currentColumn, $columns)) {
                $appendedClassNames[] = $prefixClassName . $hiddenCategoryName;
            }
        }
        return implode(' ', $appendedClassNames);
    }

    public function isShortcodeTable($shortcodeType)
    {
        if (in_array($shortcodeType,
            array(
                TP_PLUGIN_SHORTCODE_TYPE_FLIGHT,
                TP_PLUGIN_SHORTCODE_TYPE_HOTEl,
                TP_PLUGIN_SHORTCODE_TYPE_RAILWAY
            ))) {
            return true;
        }
        return false;
    }

    public static function columnFieldClass($column = '', $field = '')
    {
        $class = 'travel-table-body__col_';
        if (!empty($column)) {
            $class .= $column;
        }
        if (!empty($field)) {
            $class .= '_field_' . $field;
        }
        return $class;
    }
}