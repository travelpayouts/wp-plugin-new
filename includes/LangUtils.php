<?php

namespace tp\includes;

/**
 * Language helper
 * Class LangUtils
 * @package tp\includes
 */
class LangUtils
{
    public $langList = [];

    private static $instance = null;

    /**
     * @return LangUtils
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }


    public function __construct()
    {
        $this->langList = [
            [
                'code' => 'sq',
                'label' => 'Albanian',
            ],
            [
                'code' => 'en_us',
                'label' => 'American English',
            ],
            [
                'code' => 'ar',
                'label' => 'Arabic',
            ],
            [
                'code' => 'hy',
                'label' => 'Armenian',
            ],
            [
                'code' => 'az',
                'label' => 'Azerbaijani',
            ],
            [
                'code' => 'be',
                'label' => 'Belarusian',
            ],
            [
                'code' => 'bn',
                'label' => 'Bengali',
            ],
            [
                'code' => 'bs',
                'label' => 'Bosnian',
            ],
            [
                'code' => 'pt_br',
                'label' => 'Brazilian Portuguese',
            ],
            [
                'code' => 'bg',
                'label' => 'Bulgarian',
            ],
            [
                'code' => 'ca',
                'label' => 'Catalan; Valencian',
            ],
            [
                'code' => 'ce',
                'label' => 'Chechen',
            ],
            [
                'code' => 'zh-hans',
                'label' => 'Chinese (Simplified)',
            ],
            [
                'code' => 'zh-hant',
                'label' => 'Chinese (Traditional)',
            ],
            [
                'code' => 'hr',
                'label' => 'Croatian',
            ],
            [
                'code' => 'cs',
                'label' => 'Czech',
            ],
            [
                'code' => 'da',
                'label' => 'Danish',
            ],
            [
                'code' => 'nl',
                'label' => 'Dutch; Flemish',
            ],
            [
                'code' => 'en',
                'label' => 'English (Great Britain)',
            ],
            [
                'code' => 'et',
                'label' => 'Estonian',
            ],
            [
                'code' => 'fi',
                'label' => 'Finnish',
            ],
            [
                'code' => 'fr',
                'label' => 'French',
            ],
            [
                'code' => 'ka',
                'label' => 'Georgian',
            ],
            [
                'code' => 'de',
                'label' => 'German',
            ],
            [
                'code' => 'el',
                'label' => 'Greek',
            ],
            [
                'code' => 'he',
                'label' => 'Hebrew',
            ],
            [
                'code' => 'hi',
                'label' => 'Hindi',
            ],
            [
                'code' => 'hu',
                'label' => 'Hungarian',
            ],
            [
                'code' => 'is',
                'label' => 'Icelandic',
            ],
            [
                'code' => 'id',
                'label' => 'Indonesian',
            ],
            [
                'code' => 'it',
                'label' => 'Italian',
            ],
            [
                'code' => 'ja',
                'label' => 'Japanese',
            ],
            [
                'code' => 'kk',
                'label' => 'Kazakh',
            ],
            [
                'code' => 'ko',
                'label' => 'Korean',
            ],
            [
                'code' => 'ku',
                'label' => 'Kurdish',
            ],
            [
                'code' => 'kmr',
                'label' => 'Kurmanji (Kurdish)',
            ],
            [
                'code' => 'lv',
                'label' => 'Latvian',
            ],
            [
                'code' => 'lt',
                'label' => 'Lithuanian',
            ],
            [
                'code' => 'ms',
                'label' => 'Malay',
            ],
            [
                'code' => 'mn',
                'label' => 'Mongolian',
            ],
            [
                'code' => 'me',
                'label' => 'Montenegrin (Latin)',
            ],
            [
                'code' => 'no',
                'label' => 'Norwegian',
            ],
            [
                'code' => 'fa',
                'label' => 'Persian',
            ],
            [
                'code' => 'pl',
                'label' => 'Polish',
            ],
            [
                'code' => 'pt',
                'label' => 'Portuguese',
            ],
            [
                'code' => 'ro',
                'label' => 'Romanian',
            ],
            [
                'code' => 'ru',
                'label' => 'Russian',
            ],
            [
                'code' => 'sr_cs',
                'label' => 'Serbian',
            ],
            [
                'code' => 'sk',
                'label' => 'Slovak',
            ],
            [
                'code' => 'sl',
                'label' => 'Slovenian',
            ],
            [
                'code' => 'es',
                'label' => 'Spanish',
            ],
            [
                'code' => 'sv',
                'label' => 'Swedish',
            ],
            [
                'code' => 'tl',
                'label' => 'Tagalog (Filipino)',
            ],
            [
                'code' => 'tg',
                'label' => 'Tajik',
            ],
            [
                'code' => 'th',
                'label' => 'Thai',
            ],
            [
                'code' => 'tr',
                'label' => 'Turkish',
            ],
            [
                'code' => 'uk',
                'label' => 'Ukrainian',
            ],
            [
                'code' => 'uz',
                'label' => 'Uzbek',
            ],
            [
                'code' => 'vi',
                'label' => 'Vietnamese',
            ],
        ];

    }


    public function getAvailableLabels()
    {
        $list = [];
        foreach ($this->langList as $langData) {
            if (isset($langData['code']) && isset($langData['label'])) {
                $list[$langData['code']] = $langData['label'];
            }
        }
        return $list;
    }

    public function getLang()
    {
        return Options::instance()->lang;
    }
}