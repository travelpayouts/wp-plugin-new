<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/22/2018
 * Time: 2:35 PM
 */

namespace tp\includes\request_api;


use tp\frontend\controllers\shortcodes\railways\TP_Tutu_Controller;
use tp\includes\table\TP_Table_Output_Railway;
use tp\includes\table\TP_Table_Railway;

class TP_Request_Api_Railway extends TP_Request_Api
{
    const TP_API_URL = 'https://www.tutu.ru/poezda/api';
    const TP_LINK_URL = 'https://c45.travelpayouts.com/click';
    const TP_LINK_TUTU_URL = 'https://www.tutu.ru/poezda/order/';
    /**
     * @return string
     */
    private function get_api_url(){
        return self::TP_API_URL;
    }

    public function redirect()
    {
        if (!$this->redirect_url || !isset($_GET['searches_railway']) || !isset($_GET['searches_railway_tutu'])){
            return false;
        }
        $searches_railway = $_GET['searches_railway'];
        $searches_railway_tutu = urlencode($_GET['searches_railway_tutu']);
        $custom_url = '&custom_url='.$searches_railway_tutu;
        $url = $this->get_column_link_url().urldecode($searches_railway).$custom_url;
        $this->redirect_url($url);
    }

    /**
     * @param string $shortcode
     * @param array $shortcode_atts
     * @return array|bool|mixed
     */
    public function get_tutu($shortcode = 'tp_tutu', array $shortcode_atts = array()){
        $data = array();
        if ($this->is_cache($shortcode_atts['return_type'])){
            //cache
            $key = $this->create_cache_key($shortcode, $shortcode_atts);
            if ($shortcode_atts['update_cache'] === true){
                $this->instance_cache->delete($key);
            }
            $data = $this->instance_cache->get($key);
            if (is_null($data)) {
                $data = $this->get_api_tutu($shortcode, $shortcode_atts);
                if ($data == false){
                    return false;
                }
                $this->instance_cache->set($key, $data, $this->get_cache_time());
            }
        } else {
            //api
            $data = $this->get_api_tutu($shortcode, $shortcode_atts);
        }
        return $data;
    }

    /**
     * @param string $shortcode
     * @param array  $shortcode_atts
     *
     * @return array|bool|mixed|TP_Table_Output_Railway
     */
    private function get_api_tutu($shortcode = 'tp_tutu', array $shortcode_atts = array()){
        $request = $this->_get_tutu($shortcode_atts['origin'], $shortcode_atts['destination'], $shortcode_atts['return_type']);
        if ($shortcode_atts['return_type'] == TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL){
            return $request;
        }
        if ($this->is_empty_array($request)){
            return false;
        }
        $request = array_shift($request);
        if ($this->is_empty_array($request)){
            return false;
        }
        $rows = $this->prepare_data($request, $shortcode, $shortcode_atts);
        if ($this->is_empty_array($rows)){
            return false;
        }
        $data = new TP_Table_Output_Railway();
        $title = $this->prepare_title($rows, $shortcode, $shortcode_atts);
        $data->set_title($title);
        $data->set_rows($rows);
        return $data;
    }



    /**
     * @param $station
     * @return mixed
     */
    private function make_station($station){
        return $station;
    }

    /**
     * @return string
     */
    public function get_column_link_url(){
        $url = self::TP_LINK_URL;
        return $url;
    }

    /**
     * @return string
     */
    public function get_column_link_tutu_url(){
        $url = self::TP_LINK_TUTU_URL;
        return $url;
    }

    /**
     * @param string $departureStation
     * @param string $arrivalStation
     * @param string $return_type
     *
     * @return array|bool
     */
    private function _get_tutu($departureStation = '', $arrivalStation = '', $return_type = 'view'){
        if (!$this->is_status()){
            return false;
        }
        if (!$this->is_valid_date($departureStation)){
            return false;
        }
        if (!$this->is_valid_date($arrivalStation)){
            return false;
        }
        $params = array(
            'departureStation' => $departureStation,
            'arrivalStation' => $arrivalStation,
        );
        $url = $this->get_api_url().'/travelpayouts/';
        $url = $this->create_url_from_params($params, $url);
        if ($return_type == TP_PLUGIN_SHORTCODE_RETURN_TYPE_URL){
            return array($url);
        }
        return $this->get_request($url);
    }

    /**
     * @param $url
     * @param array $args
     * @return array|bool
     */
    protected function get_request($url, $args = array()){
        if (empty($url)){
            return false;
        }
        $args = array_merge($args,  array('headers' => array(
            'Accept-Encoding' => 'gzip, deflate',
        )));

        $response = wp_remote_request( $url, $args );

        if ( is_wp_error( $response ) ) {
            return false;
        }
        $body = wp_remote_retrieve_body( $response );
        $json = json_decode($body);
        if (is_wp_error($json)) {
            return false;
        }
        return $this->object_to_array($json);
    }

    /**
     * Prepare title
     * @param array $rows
     * @param string $shortcode
     * @param array $shortcode_atts
     * @return mixed
     */
    protected function prepare_title(array $rows = array(), $shortcode = '', array $shortcode_atts = array())
    {
        // TODO: Implement prepare_title() method.
        $title = $shortcode_atts['title'];
        if(strpos($title, '{destination}') !== false){
            $destination = $shortcode_atts['destination'];
            if (!empty($destination)) {
                $destination = '<span data-tp-station-code="'.$destination.'">'.$this->make_station($destination).'</span>';
            }
            $title = str_replace('{destination}', $destination, $title);
        }
        if(strpos($title, '{origin}') !== false){
            $origin = $shortcode_atts['origin'];
            if (!empty($origin)) {
                $origin = '<span data-tp-station-code="'.$origin.'">'.$this->make_station($origin).'</span>';
            }
            $title = str_replace('{origin}', $origin, $title);
        }
        $title = '<'.$shortcode_atts['title_tag'].'>'.$title.'</'.$shortcode_atts['title_tag'].'>';
        return $title;
    }

    /**
     * Get response classes
     * @return array
     */
    protected function get_response_classes()
    {
        // TODO: Implement get_response_classes() method.
        return array(
            'railway\\TP_Response_Tutu' => $this->merge_arrays(
                TP_Tutu_Controller::get_shortcode_tag()
            ),
        );
    }

    /**
     * Prepare data
     *
     * @param array  $rows
     * @param string $shortcode
     * @param array  $shortcode_atts
     *
     * @return mixed
     */
    protected function prepare_data(array $rows = array(), $shortcode = '', array $shortcode_atts = array())
    {
        $shortcode_atts = array_merge(
            array(
                'redirect_url' => $this->redirect_url,
                'marker' => $this->marker,
                'token' => $this->token,
                'white_label_flight' => $this->white_label_flight,
                'white_label_hotel' => $this->white_label_hotel,
                'host' => $this->host,
                'base_url' => $this->get_column_link_url(),
                'tutu_url' => $this->get_column_link_tutu_url(),
            ),
            $shortcode_atts
        );
        return parent::prepare_data($rows, $shortcode, $shortcode_atts);
    }
}