<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 1/22/2018
 * Time: 12:06 PM
 */

namespace tp\includes\request_api;

use phpFastCache\CacheManager;
use phpFastCache\Core\phpFastCache;
use tp\includes\table\TP_Table_Output;
use tp\includes\request_api\response\TP_Response_Base;


abstract class TP_Request_Api
{
    private static $instances;
    protected $marker = '';
    protected $token = '';
    protected $white_label_flight = '';
    protected $white_label_hotel = '';
    protected $cache_in_hours = 12;
    protected $instance_cache;
    protected $redirect_url = false;
    protected $host;


    /**
     * @return mixed
     */
    final public static function get_instance()
    {
        $class_name = get_called_class();
        if (isset(self::$instances[$class_name]) == false) {
            self::$instances[$class_name] = new static();
        }
        return self::$instances[$class_name];
    }

    protected function __construct()
    {
        $this->instance_cache = CacheManager::Files(array(
            'storage' => 'files',
            'default_chmod' => 0777,
            'htaccess' => true,
            'securityKey' => TP_PLUGIN_TEXTDOMAIN,
            'path' => tp_create_dir_cache(),
        ));
        add_action('wp', array(&$this, 'redirect'));
    }

    final private function __clone()
    {
    }

    public function init($marker = '', $token = '', $white_label_flight = '', $white_label_hotel = '',
                         $cache_in_hours = 12, $redirect_url = false, $host = '')
    {
        $this->marker = $marker;
        $this->token = $token;
        $this->set_white_label_flight($white_label_flight);
        $this->set_white_label_hotel($white_label_hotel);
        $this->cache_in_hours = $cache_in_hours;
        $this->redirect_url = $redirect_url;
        $this->host = $host;
    }

    protected function make_marker_url($extra_table_marker, $subid)
    {
        $marker_url = '';
        $marker_url = $this->get_marker() . '.wpplugin';
        if (!empty($extra_table_marker)) {
            $marker_url .= '_' . $extra_table_marker;
        }
        if (!empty($subid)) {
            $marker_url .= '_' . $subid;
        }
        $marker_url .= '.$69';
        return $marker_url;
    }

    public function redirect()
    {
    }

    /**
     * @param $url
     */
    public function redirect_url($url)
    {
        header('Location: ' . $url, true, 302);
        die();
    }

    /**
     * @return mixed
     */
    protected function get_marker()
    {
        return $this->marker;
    }

    /**
     * @return mixed
     */
    protected function get_token()
    {
        return $this->token;
    }

    /**
     * @param $white_label
     * @return string
     */
    private function filter_white_label($white_label)
    {
        if (!empty($white_label)) {
            if (strpos($white_label, 'http') === false) {
                $white_label = 'http://' . $white_label;
            }
        }
        return $white_label;
    }

    /**
     * @param $white_label_flight
     */
    private function set_white_label_flight($white_label_flight)
    {
        $white_label_flight = $this->filter_white_label($white_label_flight);
        $this->white_label_flight = $white_label_flight;
    }

    protected function get_white_label_flight()
    {
        return $this->white_label_flight;
    }

    protected function is_white_label_flight()
    {
        return (!$this->white_label_flight || empty($this->white_label_flight)) ? false : true;
    }

    /**
     * @param $white_label_hotel
     */
    private function set_white_label_hotel($white_label_hotel)
    {
        $white_label_hotel = $this->filter_white_label($white_label_hotel);
        $this->white_label_hotel = $white_label_hotel;
    }

    protected function get_white_label_hotel()
    {
        return $this->white_label_hotel;
    }

    protected function is_white_label_hotel()
    {
        return (!$this->white_label_hotel || empty($this->white_label_hotel)) ? false : true;
    }

    /**
     * @param string $shortcode
     * @param array $shortcode_atts
     * @return string
     */
    protected function create_cache_key($shortcode = '', array $shortcode_atts = array())
    {
        $post_id = get_the_ID();
        $cache_key = TP_PLUGIN_TEXTDOMAIN . '_' . $shortcode . '_' . md5(json_encode($shortcode_atts));
        if ($post_id !== false) {
            $cache_key .= '_' . get_post_type() . '_' . $post_id;
        }
        return strtolower($cache_key);
    }

    /**
     * @return bool|int
     */
    protected function get_cache_time()
    {
        return DAY_IN_SECONDS;
        $cache_time = false;
        if ($this->cache_in_hours == 0) {
            $cache_time = false;
        } elseif (empty($this->cache_in_hours)) {
            $cache_time = DAY_IN_SECONDS;
        } else {
            $cache_time = $this->cache_in_hours * HOUR_IN_SECONDS;
        }

        return $cache_time;
    }

    /**
     * @param $return_type Return type view | url | array_cache | array_api
     * @return bool
     */
    protected function is_cache($return_type)
    {
        if ($this->get_cache_time() == false || in_array($return_type, explode('|', TP_PLUGIN_SHORTCODE_NOCACHE_MODE))) {
            return false;
        }
        return true;
    }

    /**
     * @param $url
     * @param array $args
     * @return array|bool
     */
    protected function get_request($url, $args = array())
    {
        if (empty($url)) {
            return false;
        }
        $args = array_merge($args, array('headers' => array(
            'Accept-Encoding' => 'gzip, deflate',
        )));

        $response = wp_remote_request($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $json = json_decode($body);
        if (is_wp_error($json) || $json->success != true) {
            error_log('get_request $url = ' . $url);
            error_log(print_r($response, true));
            return false;
        }
        return $this->object_to_array($json->data);
    }

    /**
     * @param $params
     * @param $url
     * @return mixed
     */
    protected function create_url_from_params($params, $url)
    {
        $params = urlencode_deep($params);
        $url = add_query_arg($params, $url);
        return $url;
    }

    /**
     * object to array
     * @param $d
     * @return array
     */
    protected function object_to_array($d)
    {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            return array_map(array(&$this, __FUNCTION__), $d);
        } else {
            return $d;
        }
    }

    /**
     * @return bool
     */
    protected function is_status()
    {
        if (!isset($this->marker) || empty($this->marker) || !is_string($this->marker)) {
            return false;
        } elseif (!isset($this->token) || empty($this->token) || !is_string($this->token)) {
            return false;
        }
        return true;
    }

    /**
     * @param $iata
     * @return bool
     */
    protected function is_valid_iata($iata)
    {
        if (!isset($iata) || empty($iata) || !is_string($iata) || strlen($iata) != 3) {
            return false;
        }
        return true;
    }

    /**
     * @param $date
     * @return bool
     */
    protected function is_valid_date($date)
    {
        if (!isset($date) || empty($date) || !is_string($date)) {
            return false;
        }
        return true;
    }

    /**
     * @param $airline
     * @return bool
     */
    protected function is_valid_airline($airline)
    {
        if (!isset($airline) || empty($airline) || !is_string($airline) || strlen($airline) != 2) {
            return false;
        }
        return true;
    }

    /**
     * @param $param
     * @return bool
     */
    protected function is_valid_param($param)
    {
        if (!isset($param) || empty($param)) {
            return false;
        }
        return true;
    }

    /**
     * @param $flight_number
     * @return bool
     */
    protected function is_valid_flight_number($flight_number)
    {
        if (!isset($flight_number) || empty($flight_number) || !is_string($flight_number)) {
            return false;
        }
        return true;
    }

    protected function filter_bool_param($param)
    {
        if ($param == true) {
            $param = 1;
        } else {
            $param = 0;
        }
        return $param;
    }

    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function cmp_sort_dates($item1, $item2)
    {
        if ($cmp = strcmp(substr($item1['depart_date'], 0, 10), substr($item2['depart_date'], 0, 10)))
            return $cmp;
        return $item1['value'] - $item2['value'];
    }

    /**
     * @param $return
     * @return mixed
     */
    protected function sort_dates($data)
    {
        if (!$data) {
            return false;
        }
        usort($data, array(&$this, 'cmp_sort_dates'));
        $date = '';
        foreach ($data as $key => $item) {
            $depart_date = substr($item['depart_date'], 0, 10);
            if ($depart_date == $date) {
                unset($data[$key]);
            } else {
                $date = $depart_date;
            }
        }
        return $data;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function is_empty_array($value)
    {
        if ($value instanceof TP_Table_Output) {
            if (is_array($value->get_rows()) || count($value) != $value->get_rows()) {
                return false;
            }
        }
        if (!is_array($value)) {
            return true;
        }
        if (count($value) == 0) {
            return true;
        }
        return false;
    }

    /**
     * Prepare title
     * @param array $rows
     * @param string $shortcode
     * @param array $shortcode_atts
     * @return mixed
     */
    abstract protected function prepare_title(array $rows = array(), $shortcode = '', array $shortcode_atts = array());


    /**
     * Prepare data
     *
     * @param array $rows
     * @param string $shortcode
     * @param array $shortcode_atts
     *
     * @return mixed
     */
    protected function prepare_data(array $rows = array(), $shortcode = '', array $shortcode_atts = array())
    {
        if ($this->is_empty_array($rows) || empty($shortcode)) {
            return false;
        }

        $shortcode_atts = apply_filters(TP_PLUGIN_TEXTDOMAIN . '/shortcode_type_' . $shortcode_atts['shortcode_type']
            . '/prepare_data/shortcode_atts', $shortcode_atts, $shortcode);
        $rows = apply_filters(TP_PLUGIN_TEXTDOMAIN . '/shortcode_type_' . $shortcode_atts['shortcode_type']
            . '/prepare_data/rows', $rows, $shortcode, $shortcode_atts);

        $response_class = $this->get_response_class($shortcode);
        $rows_keys = array_keys($rows);

        $mapped_rows = array_map(function ($key, $rowData) use ($response_class, $shortcode, $shortcode_atts, $rows_keys) {
            if ($response_class) {
                $response_model = new $response_class($rowData, $shortcode, $shortcode_atts);
                $response_model->set('_key', $key);
                $response_model->set('_count', array_search($key, $rows_keys, true));
                $data = $response_model->get_data();
                return $data;
            }
        }, $rows_keys, $rows);

        return $mapped_rows;
    }

    /**
     * @param mixed ...$arrays
     *
     * @return array
     */
    protected function merge_arrays(...$arrays)
    {
        return array_merge(...$arrays);
    }

    /**
     * Get classes response
     * @return array
     */
    abstract protected function get_response_classes();

    /**
     * @param $shortcode_name
     *
     * @return null|string
     */
    public function get_response_class($shortcode_name)
    {
        $response_classes = $this->get_response_classes();

        $filtered_classes = array_filter($response_classes, function ($shortcodes) use ($shortcode_name) {
            return in_array($shortcode_name, $shortcodes);
        });

        if ($filtered_classes) {
            $shortcode_classes_list = array_keys($filtered_classes);
            $shortcode_response_class_name = 'tp\\includes\\request_api\\response\\' . array_shift($shortcode_classes_list);
            if (class_exists($shortcode_response_class_name)) {
                return $shortcode_response_class_name;
            }

        }

        return null;
    }

    /**
     * @param $response_class
     * @param mixed ...$attributes
     * @return TP_Response_Base
     */
    protected function get_response_class_instance($response_class, ...$attributes)
    {
        return new $response_class(...$attributes);
    }
}