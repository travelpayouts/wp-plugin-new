<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response;

use Adbar\Dot;

/**
 * Class TP_Response_Base
 * @package tp\includes\request_api\response
 */
abstract class TP_Response_Base extends TP_Response_Deprecated
{
    protected $_row_data;
    protected $_shortcode_attributes;
    protected $_shortcode_name;

    /**
     * TP_Response_Base constructor.
     *
     * @param      $row_data
     * @param bool $name
     * @param bool $attributes
     */
    public function __construct($row_data, $name = false, $attributes = false)
    {
        if (is_array($row_data)) {
            $this->_row_data = new Dot($row_data);
        } else {
            $this->_row_data = new Dot(array('value' => $row_data));

        }

        if ($attributes)
            $this->set_shortcode_attributes($attributes);
        if ($name)
            $this->set_shortcode_name($name);
    }

    /**
     * Get all attributes as array
     * @return Dot|array
     */
    public function get_attributes()
    {
        if ($this->_shortcode_attributes instanceof Dot) {
            return $this->_row_data->all();
        }
        return $this->_row_data;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        return $this->_row_data->set($name);

    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        $function_name = 'get_' . $name;
        if (method_exists($this, $function_name)) {
            return $this->{$function_name}();
        }
        return $this->_row_data->get($name);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        $function_name = 'get_' . $name;
        if (method_exists($this, $function_name)) return true;
        return $this->_row_data->has($name);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __unset($name)
    {
        return false;
    }

    /**
     * Get row param
     * @param $name
     * @param $default_value
     * @return mixed
     */
    public function get($name, $default_value = null)
    {
        return $this->_row_data->get($name, $default_value);
    }

    /**
     * Get row param
     * @param $name
     * @param $default_value
     * @return mixed
     */
    public function set($name, $value)
    {
        return $this->_row_data->set($name, $value);

    }

    /**
     * Setter function for shortcode attributes
     * @param $attributes
     */
    public function set_shortcode_attributes($attributes)
    {
        if (is_array($attributes)) {
            $this->_shortcode_attributes = new Dot($attributes);
        }
    }

    /**
     * Getter function for shortcode attributes
     * @return array
     */
    public function get_shortcode_attributes()
    {
        if ($this->_shortcode_attributes instanceof Dot) {
            return $this->_shortcode_attributes->all();
        }
        return array();
    }


    /**
     * Setter function for shortcode name
     * @param $attributes
     */
    public function set_shortcode_name($name)
    {
        if (is_string($name)) {
            $this->_shortcode_name = $name;
        }
    }

    /**
     * Getter function for shortcode name
     * @return array
     */
    public function get_shortcode_name()
    {
        return $this->_shortcode_name;
    }

    public function get_shortcode_param($name, $default_value = null)
    {
        if ($this->_shortcode_attributes instanceof Dot) {
            return $this->_shortcode_attributes->get($name, $default_value);
        }
        return null;
    }


    /**
     * Evaluates a PHP expression or callback under the context of this component.
     *
     * Valid PHP callback can be class method name in the form of
     * array(ClassName/Object, MethodName), or anonymous function (only available in PHP 5.3.0 or above).
     *
     * If a PHP callback is used, the corresponding function/method signature should be
     * <pre>
     * function foo($param1, $param2, ..., $component) { ... }
     * </pre>
     * where the array elements in the second parameter to this method will be passed
     * to the callback as $param1, $param2, ...; and the last parameter will be the component itself.
     *
     * If a PHP expression is used, the second parameter will be "extracted" into PHP variables
     * that can be directly accessed in the expression. See {@link http://us.php.net/manual/en/function.extract.php PHP extract}
     * for more details. In the expression, the component object can be accessed using $this.
     *
     * A PHP expression can be any PHP code that has a value. To learn more about what an expression is,
     * please refer to the {@link http://www.php.net/manual/en/language.expressions.php php manual}.
     *
     * @param mixed $_expression_ a PHP expression or PHP callback to be evaluated.
     * @param array $_data_ additional parameters to be passed to the above expression/callback.
     * @return mixed the expression result
     * @author  Qiang Xue <qiang.xue@gmail.com>
     */
    private function evaluate_expression($_expression_, $_data_ = array())
    {
        if (is_string($_expression_)) {
            extract($_data_);
            try {
                return eval('return ' . $_expression_ . ';');
            } catch (\Exception $e) {
                return false;
            }
        } else {
            $_data_[] = $this;
            return call_user_func_array($_expression_, $_data_);
        }
    }

    /**
     * @deprecated
     * @param $date_time
     * @param string $format
     * @return false|string
     */
    protected function make_date($date_time, $format = 'd.m.Y')
    {
        if (empty($date_time)) $date_time = time();
        return date($format, $date_time);
    }

    /**
     * @deprecated
     * @param $date
     * @return false|int
     */
    protected function make_date_time($date)
    {
        $time = strtotime($date);
        if (empty($time) || $time === false) $time = time();
        return $time;
    }

    /**
     * Mapping columns
     * @return array
     */
    abstract public function mapping();

    public function get_mapped_values()
    {
        $mapped_values = $this->mapping();
        array_walk($mapped_values, function (&$mapped_value, $mapped_key) {
            $mapped_value = $this->evaluate_expression($mapped_value, array(
                'model' => $this,
                'attribute' => $mapped_key,
            ));
        });

        return $mapped_values;
    }

    /**
     * @return Dot|array
     */
    public function get_data()
    {
        $attributes = $this->get_attributes();
        $attributes['val'] = $this->get_mapped_values();

        return $attributes;
    }

    /**
     * @return string
     */
    protected function make_marker_url()
    {
        $extra_table_marker = $this->get_shortcode_param('extra_table_marker');
        $subid = $this->get_shortcode_param('subid');
        $marker_params = array_filter(array(
            $this->get_shortcode_param('marker') . '.wpplugin',
            $extra_table_marker ? '_' . $extra_table_marker : null,
            $subid ? '_' . $subid : null,
            '.$69'
        ));
        return implode('', $marker_params);
    }
}