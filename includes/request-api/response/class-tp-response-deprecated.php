<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response;

use Adbar\Dot;

class TP_Response_Deprecated
{
    /**
     * @param $params
     * @param $url
     * @return mixed
     */
    protected function create_url_from_params($params, $url)
    {
        $params = urlencode_deep($params);
        $url = add_query_arg($params, $url);
        return $url;
    }





}