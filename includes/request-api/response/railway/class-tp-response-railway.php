<?php


namespace tp\includes\request_api\response\railway;


use tp\includes\request_api\response\TP_Response_Base;
use tp\includes;
use tp\includes\table\TP_Table_Railway;

abstract class TP_Response_Railway extends TP_Response_Base
{
    /**
     * Train
     * @return array
     */
    public function get_train_attribute()
    {
        return array(
            TP_Table_Railway::COLUMN_TRAIN_FIELD_TRAIN_NUMBER => $this->get('trainNumber', ''),
            TP_Table_Railway::COLUMN_TRAIN_FIELD_NAME => $this->get('name', ''),
            TP_Table_Railway::COLUMN_TRAIN_FIELD_FIRM => $this->get('firm', false)
        );
    }

    /**
     * Route
     * @return array
     */
    public function get_route_attribute()
    {
        $departure_station = $this->get('departureStation', '');
        $arrival_station = $this->get('arrivalStation', '');
        $run_departure_station = $this->get('runDepartureStation', '');
        $run_arrival_station = $this->get('runArrivalStation', '');
        $departure_station = $this->make_station($departure_station);
        $arrival_station = $this->make_station($arrival_station);
        $run_departure_station = $this->make_station($run_departure_station);
        $run_arrival_station = $this->make_station($run_arrival_station);

        return array(
            TP_Table_Railway::COLUMN_ROUTE_FIELD_DEPARTURE_STATION => $departure_station,
            TP_Table_Railway::COLUMN_ROUTE_FIELD_ARRIVAL_STATION => $arrival_station,
            TP_Table_Railway::COLUMN_ROUTE_FIELD_RUN_DEPARTURE_STATION => $run_departure_station,
            TP_Table_Railway::COLUMN_ROUTE_FIELD_RUN_ARRIVAL_STATION => $run_arrival_station
        );
    }

    /**
     * Departure
     * @return array
     */
    public function get_departure_attribute()
    {
        $departure_station = $this->get('departureStation', '');
        $departure_time = strtotime($this->get('departureTime', 0));
        $departure_station = $this->make_station($departure_station);
        $time = date('H:i', $departure_time);
        return array(
            TP_Table_Railway::COLUMN_DEPARTURE_FIELD_DEPARTURE_TIME => $departure_time,
            TP_Table_Railway::COLUMN_DEPARTURE_FIELD_TIME => $time,
            TP_Table_Railway::COLUMN_DEPARTURE_FIELD_DEPARTURE_STATION => $departure_station,
        );
    }

    /**
     * Arrival
     * @return array
     */
    public function get_arrival_attribute()
    {
        $arrival_station = $this->get('arrivalStation', '');
        $arrival_time = strtotime($this->get('arrivalTime', 0));
        $time = date('H:i', $arrival_time);
        $arrival_station = $this->make_station($arrival_station);
        $duration_day = $this->get_duration_day();
        return array(
            TP_Table_Railway::COLUMN_ARRIVAL_FIELD_ARRIVAL_TIME => $arrival_time,
            TP_Table_Railway::COLUMN_ARRIVAL_FIELD_TIME => $time,
            TP_Table_Railway::COLUMN_ARRIVAL_FIELD_DURATION_DAY => $duration_day,
            TP_Table_Railway::COLUMN_ARRIVAL_FIELD_ARRIVAL_STATION => $arrival_station,
        );
    }

    /**
     * Duration day
     * @return string
     */
    public function get_duration_day(){
        $duration_day = '';
        $travel_time_in_seconds = $this->get('travelTimeInSeconds', 0);
        if ($travel_time_in_seconds > 0){
            $day = floor($travel_time_in_seconds/DAY_IN_SECONDS);
            if ($day > 0){
                $duration_day = $this->make_date_label($duration_day, 'day');
            }
        }
        return $duration_day;
    }

    /**
     * @param $value
     * @param string $type
     * @return string
     */
    private function make_date_label($value, $type = 'day'){
        $date_label = '';
        if ($type == 'day'){
            $date_label = $value.' day';
        } elseif ($type == 'hour') {
            $date_label = $value.' hour';
        } elseif ($type == 'minute') {
            $date_label = $value.' minute';
        }
        return $date_label;
    }

    /*
    * public function getPluralType($n){
       if (TPLang::getLang() == TPLang::getLangRU()){
           return ($n%10==1 && $n%100!=11 ? 0 : ($n%10>=2 && $n%10<=4 && ($n%100<10 || $n%100>=20) ? 1 : 2));
       } else {
           return ($n != 1) ? 1 : 0;
       }

   }

    */


    /**
     * Duration
     * @return array
     */
    public function get_duration_attribute(){
        $travel_time_in_seconds = $this->get('travelTimeInSeconds', 0);
        $duration = $this->get_duration_by_seconds($travel_time_in_seconds);

        return $duration;
    }

    /**
     * @param $seconds
     * @return array
     */
    private function get_duration_by_seconds($seconds){
        $string = '';
        $day = floor($seconds/DAY_IN_SECONDS);
        $seconds -= $day * DAY_IN_SECONDS;
        $hour = floor($seconds/HOUR_IN_SECONDS);
        $seconds -= $hour * HOUR_IN_SECONDS;
        $minute = floor($seconds/MINUTE_IN_SECONDS);
        if ($day > 0){
            $string .= $this->make_date_label($day,'day').' ';
            if ($minute > 30){
                $hour += 1;
            }
            if ($hour > 0){
                $string .= $this->make_date_label($hour,'hour').' ';
            }
        } else {
            if ($hour > 0){
                $string .= $this->make_date_label($hour,'hour').' ';
            }
            if ($minute > 0){
                $string .= $this->make_date_label($minute,'minute').' ';
            }
        }
        $duration = preg_replace('/\s+/',' ',$string);

        return array(
            'seconds' => $seconds,
            'day' => $day,
            'hour' => $hour,
            'minute' => $minute,
            'duration' => $duration
        );
    }


    /**
     * @param $station
     *
     * @return array
     */
    private function make_station($station){
        return array(
            'label' => $station,
            'code' => $station,
        );
    }


    /**
     * Prices
     * @return array
     */
    public function get_prices_attribute(){
        $categories = $this->get('categories', 0);
        $prices = $this->get_prices_by_categories($categories);
        return $prices;
    }

    /**
     * @param $type
     * @return mixed
     */
    private function make_train_wagon_type_label($type){
        return $type;
    }

    /**
     * @param $row
     * @return array
     *
     */
    private function get_prices_by_categories($categories){
        $prices = array();
        if (count($categories) < 1 || $categories == false) {
            return $prices;
        }
        foreach ($categories as $category){
            $type = '';
            $price = '';
            if (array_key_exists('type', $category)) {
                $type = $category['type'];
            }
            if (array_key_exists('price', $category)) {
                $price = $category['price'];
            }
            $prices[] = array(
                'price' => $price,
                'type' => $type,
                'type_label' => $this->make_train_wagon_type_label($type),
            );
        }
        return $prices;
    }


    /**
     * Dates
     * @return array
     */
    public function get_dates_attribute(){
        $dates = $this->make_button_title($this->get_shortcode_param('button_title', ''));
        return array(
            'button_title' => $dates
        );
    }

    private function make_button_title($button_title){
        return $button_title;
    }

    /**
     * From
     * @return array
     */
    public function get_origin_attribute(){
        $departure_station = $this->get('departureStation', '');
        $origin = $this->make_station($departure_station);
        return $origin;
    }


    /**
     * To
     * @return array
     */
    public function get_destination_attribute(){
        $arrival_station = $this->get('arrivalStation', '');
        $destination = $this->make_station($arrival_station);
        return $destination;
    }

    /**
     * Departure Time
     * @return array
     */
    public function get_departure_time_attribute(){
        $departure_time = strtotime($this->get('departureTime', 0));
        return array(
            'time' => date('H:i', $departure_time),
            'time_second' => $departure_time
        );
    }


    /**
     * Arrival Time
     * @return array
     */
    public function get_arrival_time_attribute(){
        $arrival_time = strtotime($this->get('arrivalTime', 0));
        return array(
            'time' => date('H:i', $arrival_time),
            'time_second' => $arrival_time
        );
    }

    /**
     * Route's First Station
     * @return array|mixed
     */
    public function get_route_first_station_attribute(){
        $run_departure_station = $this->get('runDepartureStation', '');
        $run_departure_station = $this->make_station($run_departure_station);
        return $run_departure_station;
    }


    /**
     * Route's Last Station
     * @return array|mixed
     */
    public function get_route_last_station_attribute(){
        $run_arrival_station = $this->get('runArrivalStation', '');
        $run_arrival_station = $this->make_station($run_arrival_station);
        return $run_arrival_station;
    }

    /**
     * URL
     * @return mixed|string
     */
    public function get_url_attribute(){
        $tn_number = $this->get('numberForUrl', '');
        $url_tutu = $this->get_shortcode_param('tutu_url', '');
        $base_url = $this->get_shortcode_param('base_url', '');
        $params_tutu = array(
            'dep_st' => $this->get_shortcode_param('origin', ''),
            'arr_st' => $this->get_shortcode_param('destination', ''),
            'tn' => $tn_number,
            'from' => 'calendar',
            //'departure_st' => $row['runDepartureStationCode'],
            //'arrival_st' => $row['runArrivalStationCode'],
            'date' => '',
        );
        $url_tutu = urlencode($this->create_url_from_params($params_tutu, $url_tutu));
        $url = '';
        $params = array(
            'shmarker' => $this->make_marker_url(),
            // @TODO is it correct?
            'promo_id' => '1294',
            'source_type' => 'customlink',
            'type' => 'click',
        );
        $url = $this->create_url_from_params($params, $url);
        $custom_url = '&custom_url='.$url_tutu;
        if ($this->redirect_url){
            $url = get_option('home').'/?searches_railway='.rawurlencode($url).'&searches_railway_tutu='.$url_tutu;
        } else {
            $url = $base_url.$url.$custom_url;
        }
        return $url;
    }
}