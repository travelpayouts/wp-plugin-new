<?php


namespace tp\includes\request_api\response\railway;


use tp\includes\table\TP_Table_Railway;

class TP_Response_Tutu extends TP_Response_Railway
{

    /**
     * Mapping columns
     * @return array
     */
    public function mapping()
    {
        // TODO: Implement mapping() method.
        // Train
        // Route
        // Departure
        // Arrival
        // Duration
        // Prices
        // Dates
        // From
        // To
        // Departure Time
        // Arrival Time
        // Route's First Station
        // Route's Last Station
        // URL
        return array(
            TP_Table_Railway::COLUMN_TRAIN => '$model->get_train_attribute()',
            TP_Table_Railway::COLUMN_ROUTE => '$model->get_route_attribute()',
            TP_Table_Railway::COLUMN_DEPARTURE => '$model->get_departure_attribute()',
            TP_Table_Railway::COLUMN_ARRIVAL => '$model->get_arrival_attribute()',
            TP_Table_Railway::COLUMN_DURATION => '$model->get_duration_attribute()',
            TP_Table_Railway::COLUMN_PRICES => '$model->get_prices_attribute()',
            TP_Table_Railway::COLUMN_DATES => '$model->get_dates_attribute()',
            TP_Table_Railway::COLUMN_ORIGIN => '$model->get_origin_attribute()',
            TP_Table_Railway::COLUMN_DESTINATION => '$model->get_destination_attribute()',
            TP_Table_Railway::COLUMN_DEPARTURE_TIME => '$model->get_departure_time_attribute()',
            TP_Table_Railway::COLUMN_ARRIVAL_TIME => '$model->get_arrival_time_attribute()',
            TP_Table_Railway::COLUMN_ROUTE_FIRST_STATION => '$model->get_route_first_station_attribute()',
            TP_Table_Railway::COLUMN_ROUTE_LAST_STATION => '$model->get_route_last_station_attribute()',
            TP_Table_Railway::COLUMN_URL => '$model->get_url_attribute()',

        );
    }
}