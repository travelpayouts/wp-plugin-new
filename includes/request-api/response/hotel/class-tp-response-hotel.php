<?php


namespace tp\includes\request_api\response\hotel;


use tp\frontend\controllers\shortcodes\hotels\TP_Hotels_Selections_Discount_Controller;
use tp\includes\request_api\response\TP_Response_Base;
use tp\includes;

/**
 * Class TP_Response_Hotel
 * @package tp\includes\request_api\response\hotel
 */
abstract class TP_Response_Hotel extends TP_Response_Base
{
    /**
     * @return float|int
     */
    public function get_rating_attribute()
    {
        if (isset($this->rating)) {
            return $this->rating / 10;
        }
        return 0;
    }

    /**
     * @return array
     */
    public function get_old_price_and_new_price_attribute(){
        return array(
            'old_price_pn' => $this->get('last_price_info.old_price_pn', 0),
            'price_pn' => $this->get('last_price_info.price_pn', 0),
        );
    }

    /**
     * @return array
     */
    public function get_old_price_and_discount_attribute(){
        return array(
            'old_price_pn' => $this->get('last_price_info.old_price_pn', 0),
            'discount' => $this->get('last_price_info.discount', 0),
        );
    }

    /**
     * @return mixed|null
     */
    public function get_button_attribute(){
        return $this->get_shortcode_param('button_title', '');
    }

    /**
     * @return string
     */
    public function get_url_attribute()
    {
        $params = array(
            'locationId' => $this->get_shortcode_param('city', ''),
            'hotelId' => $this->get('hotel_id', 0),
            'currency' => $this->get_shortcode_param('currency', includes\TP_Currency_Utils::get_default_currency()),
            'marker' => $this->make_marker_url()
        );
        $params = $this->add_params_dates_url($params);
        if( $this->is_white_label_hotel() == false){
            $params = array_merge($params, array(
                'language' => $this->get_shortcode_param('host_lang'),
            ));
        }

        $url = '';
        $url = $this->create_url_from_params($params, $url);
        if ($this->redirect_url){
            $url = get_option('home').'/?search_hotel='.rawurlencode($url);
        } else {
            $url = $this->get_shortcode_param('base_url', '').$url;
        }

        return $url;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    private function add_params_dates_url(array $params){
        $check_in = $this->get_shortcode_param('check_in', '');
        $check_out = $this->get_shortcode_param('check_out', '');
        if ($this->get_shortcode_param('link_without_dates', false) == false){
            if (in_array($this->get_shortcode_name(), TP_Hotels_Selections_Discount_Controller::get_shortcode_tag())){
                if (empty($check_in) || empty($check_out)) {
                    $params = array_merge($params, array(
                        'autoDates' => '1',
                    ));
                } else {
                    $params = array_merge($params, array(
                        'checkIn' => $check_in,
                        'checkOut' => $check_out,
                    ));
                }
            } else {
                if (!empty($check_in)){
                    $params = array_merge($params, array(
                        'checkIn' => $check_in,
                    ));
                }

                if (!empty($check_out)){
                    $params = array_merge($params, array(
                        'checkOut' => $check_out,
                    ));
                }
            }
        }
        return $params;
    }


    /**
     * @return string
     */
    public function get_url(){
        if( $this->is_white_label_hotel() == false){
            $url = $this->get_shortcode_param('host');
        } else {
            $url = $this->get_shortcode_param('white_label_hotel', '').'/hotels/';
        }
        return $url;
    }

    /**
     * @return bool
     */
    public function is_white_label_hotel(){
        $white_label = $this->get_shortcode_param('white_label_hotel');
        return (! $white_label || empty( $white_label )) ? false : true;
    }

}