<?php


namespace tp\includes\request_api\response\hotel;

use tp\includes\table\TP_Table_Hotel;

/**
 * Class TP_Response_Selections
 * @package tp\includes\request_api\response\hotel
 */
class TP_Response_Selections extends TP_Response_Hotel
{

    /**
     * Mapping columns
     * @return array
     */
    public function mapping()
    {
        // TODO: Implement mapping() method.
        // Hotel
        // Stars
        // Price per night, from
        // To the center
        // Rating
        // Discount
        // Old and new price
        // Price before and discount
        // Price before discount
        // Button
        // URL
        return array(
            TP_Table_Hotel::COLUMN_HOTEL => '$model->get("name", "")',
            TP_Table_Hotel::COLUMN_STARS => '$model->get("stars", "")',
            TP_Table_Hotel::COLUMN_PRICE_PN => '$model->get("last_price_info.price_pn", 0)',
            TP_Table_Hotel::COLUMN_DISTANCE => '$model->get("distance", 0)',
            TP_Table_Hotel::COLUMN_RATING => '$model->get_rating_attribute()',
            TP_Table_Hotel::COLUMN_DISCOUNT => '$model->get("last_price_info.discount", 0)',
            TP_Table_Hotel::COLUMN_OLD_PRICE_AND_NEW_PRICE => '$model->get_old_price_and_new_price_attribute()',
            TP_Table_Hotel::COLUMN_OLD_PRICE_AND_DISCOUNT => '$model->get_old_price_and_discount_attribute()',
            TP_Table_Hotel::COLUMN_OLD_PRICE_PN => '$model->get("last_price_info.price_pn", 0)',
            TP_Table_Hotel::COLUMN_BUTTON => '$model->get_button_attribute()',
            TP_Table_Hotel::COLUMN_URL => '$model->get_url_attribute()',
        );
    }
}