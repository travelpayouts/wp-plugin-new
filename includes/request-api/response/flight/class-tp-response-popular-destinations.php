<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\table\TP_Table_Flight;

class TP_Response_Popular_Destinations extends TP_Response_Flight
{
    public function mapping()
    {
        // origin
        // destination
        // origin_destination
        // place
        // direction
        // button
        // url
        return array(
            TP_Table_Flight::COLUMN_ORIGIN => '$model->get_origin_attribute()',
            TP_Table_Flight::COLUMN_DESTINATION => '$model->get_destination_attribute()',
            TP_Table_Flight::COLUMN_ORIGIN_DESTINATION => '$model->get_origin_destination_attribute()',
            TP_Table_Flight::COLUMN_PLACE => '$model->get("_count",0)',
            TP_Table_Flight::COLUMN_DIRECTION => '$model->get_origin_destination_attribute()',
            TP_Table_Flight::COLUMN_BUTTON => '$model->get_shortcode_param("button_title")',
            TP_Table_Flight::COLUMN_URL => '$model->get_url_attribute("departure_date")',
        );
    }


    public function get_origin()
    {
        $cities = $this->get_cities();
        return $cities['origin'];
    }

    public function get_destination()
    {
        $cities = $this->get_cities();
        return $cities['destination'];
    }

    public function get_cities()
    {
        $key = $this->get('_key');
        if ($key && preg_match('/^(?<origin>[A-Z]{3})-(?<destination>[A-Z]{3})$/iu', $key, $cities_match)) {
            return array(
                'origin' => $cities_match['origin'],
                'destination' => $cities_match['destination'],
            );

        }
        return array(
            'origin' => '',
            'destination' => '',
        );
    }

    public function get_departure_date()
    {
        return time() + DAY_IN_SECONDS;
    }
}