<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\table\TP_Table_Flight;

class TP_Response_Our_City extends TP_Response_Flight
{
    public function mapping()
    {
        // depart_date
        // return_date
        // number_of_changes
        // price
        // origin
        // destination
        // trip_class
        // distance
        // price_distance
        // found_at
        // button
        // origin_destination
        // url
        return array(
            TP_Table_Flight::COLUMN_DEPART_DATE => '$model->get_date_attribute("depart_date")',
            TP_Table_Flight::COLUMN_RETURN_DATE => '$model->get_date_attribute("return_date")',
            TP_Table_Flight::COLUMN_NUMBER_OF_CHANGES => '$model->get("number_of_changes",0)',
            TP_Table_Flight::COLUMN_PRICE => '$model->get("value",0)',
            TP_Table_Flight::COLUMN_ORIGIN => '$model->get_origin_attribute()',
            TP_Table_Flight::COLUMN_DESTINATION => '$model->get_destination_attribute()',
            TP_Table_Flight::COLUMN_TRIP_CLASS => '$model->get("trip_class",0)',
            TP_Table_Flight::COLUMN_DISTANCE => '$model->get("distance",0)',
            TP_Table_Flight::COLUMN_PRICE_DISTANCE => '$model->get_price_distance_attribute()',
            TP_Table_Flight::COLUMN_FOUND_AT => '$model->get_date_attribute("found_at")',
            TP_Table_Flight::COLUMN_ORIGIN_DESTINATION => '$model->get_origin_destination_attribute()',
            TP_Table_Flight::COLUMN_BUTTON => '$model->get_shortcode_param("button_title")',
            TP_Table_Flight::COLUMN_URL => '$model->get_url_attribute("depart_date","return_date")',
        );
    }

    public function get_price()
    {
        return (float)$this->get('value', 0);
    }

    public function get_distance()
    {
        return (float)$this->get('distance', 0);
    }
}