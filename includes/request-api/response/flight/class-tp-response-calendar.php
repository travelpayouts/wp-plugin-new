<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\table\TP_Table_Flight;

class TP_Response_Calendar extends TP_Response_Flight
{
    /**
     * @return array
     */
    public function mapping()
    {

        // depart_date
        // return_date
        // number_of_changes
        // price
        // trip_class
        // button
        // url


        return array(
            TP_Table_Flight::COLUMN_DEPART_DATE => '$model->get_date_attribute($attribute)',
            TP_Table_Flight::COLUMN_RETURN_DATE => '$model->get_date_attribute($attribute)',
            TP_Table_Flight::COLUMN_NUMBER_OF_CHANGES => '$model->get("number_of_changes",0)',
            TP_Table_Flight::COLUMN_PRICE => '$model->get("value",0)',
            TP_Table_Flight::COLUMN_TRIP_CLASS => '$model->get("trip_class",0)',
            TP_Table_Flight::COLUMN_BUTTON => '$model->get_shortcode_param("button_title")',
            TP_Table_Flight::COLUMN_URL => '$model->get_url_attribute("depart_date","return_date")',
        );
    }
}