<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\table\TP_Table_Flight;

class TP_Response_From_City extends TP_Response_Flight
{
    public function mapping()
    {
        // flight_number
        // flight
        // depart_date
        // return_date
        // price
        // airline
        // airline_logo
        // button
        // destination
        // origin_destination
        // url
        return array(
            TP_Table_Flight::COLUMN_FLIGHT_NUMBER => '$model->get_flight_number_attribute()',
            TP_Table_Flight::COLUMN_FLIGHT => '$model->get_flight_attribute()',
            TP_Table_Flight::COLUMN_DEPART_DATE => '$model->get_date_attribute("departure_at")',
            TP_Table_Flight::COLUMN_RETURN_DATE => '$model->get_date_attribute("return_at")',
            TP_Table_Flight::COLUMN_PRICE => '$model->get("price",0)',
            TP_Table_Flight::COLUMN_AIRLINE => '$model->get_airline_attribute()',
            TP_Table_Flight::COLUMN_AIRLINE_LOGO => '$model->get_airline_logo_attribute()',
            TP_Table_Flight::COLUMN_BUTTON => '$model->get_shortcode_param("button_title")',
            TP_Table_Flight::COLUMN_DESTINATION => '$model->get_destination_attribute()',
            TP_Table_Flight::COLUMN_ORIGIN_DESTINATION => '$model->get_origin_destination_attribute()',
            TP_Table_Flight::COLUMN_URL => '$model->get_url_attribute("departure_at","return_at")',

        );
    }
}