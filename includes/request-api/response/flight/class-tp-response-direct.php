<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\table\TP_Table_Flight;

class TP_Response_Direct extends TP_Response_Flight
{
    public function mapping()
    {
        // flight_number
        // flight
        // depart_date
        // return_date
        // price
        // airline
        // airline_logo
        // destination
        // origin_destination
        // button
        // url

        return array(
            TP_Table_Flight::COLUMN_FLIGHT_NUMBER => '$model->get_flight_number_attribute()',
            TP_Table_Flight::COLUMN_FLIGHT => '$model->get_flight_attribute()',
            TP_Table_Flight::COLUMN_DEPART_DATE => '$model->get_date_attribute("departure_at")',
            TP_Table_Flight::COLUMN_RETURN_DATE => '$model->get_date_attribute("return_at")',
            TP_Table_Flight::COLUMN_PRICE => '$model->get("price",0)',
            TP_Table_Flight::COLUMN_AIRLINE => '$model->get_airline_attribute()',
            TP_Table_Flight::COLUMN_AIRLINE_LOGO => '$model->get_airline_logo_attribute()',
            TP_Table_Flight::COLUMN_DESTINATION => '$model->get_destination_attribute()',
            TP_Table_Flight::COLUMN_ORIGIN_DESTINATION => '$model->get_origin_destination_attribute()',
            TP_Table_Flight::COLUMN_BUTTON => '$model->get_shortcode_param("button_title")',
            TP_Table_Flight::COLUMN_URL => '$model->get_url_attribute("departure_at","return_at")',
        );
    }

    public function get_origin()
    {
        return $this->get_shortcode_param('origin');
    }

    public function get_destination()
    {
        return $this->get('_key');
    }


//get_origin_destination_attribute
//get_destination_attribute

//$this->set_row_column_destination($row, $shortcode, $shortcode_atts, $key);
//$this->set_row_column_origin_destination($row, $shortcode, $shortcode_atts, $shortcode_atts['origin'], $key);

//$this->set_row_column_button($row, $shortcode, $shortcode_atts);
//$this->set_row_column_url($row, $shortcode_atts['origin'], $key,
//$row[TP_Table_Flight::DATA_COLUMN_RAW][TP_Table_Flight::COLUMN_DEPART_DATE]['date_time'],
//$row[TP_Table_Flight::DATA_COLUMN_RAW][TP_Table_Flight::COLUMN_RETURN_DATE]['date_time'],
//false, $shortcode_atts['subid'], $shortcode_atts['extra_table_marker'],
//$shortcode_atts['currency']);

}