<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use Adbar\Dot;

class TP_Response_Title extends TP_Response_Flight
{
    public function __construct($row_data, $name = false, $attributes = false)
    {
        $attributes_dot = new Dot($attributes);

        $updated_row_data = array(
            'destination' => $attributes_dot->get('destination'),
            'origin' => $attributes_dot->get('origin'),
            'airline' => $attributes_dot->get('airline'),
            'title' => $attributes_dot->get('title'),
            'title_tag' => $attributes_dot->get('title_tag', 'h3'),
        );

        parent::__construct($updated_row_data, $name, $attributes);

    }

    public function mapping()
    {
        return array(
            'destination_data' => '$model->get_destination_attribute()',
            'origin_data' => '$model->get_origin_attribute()',
            'airline_data' => '$model->get_airline_attribute()',
            'title' => '$model->get_title_attribute()',
            'title_tag' => '$model->title_tag',
        );
    }


    public function get_title_attribute()
    {
        return preg_replace('/{([\w_]+)}/iu', '{{$1}}', $this->title);
    }

    public function get_origin_attribute()
    {
        return array(
            'label' => $this->get_city_label($this->get_shortcode_param("origin"),
                $this->get_shortcode_param("case_origin")),
            'code' => $this->get_shortcode_param("origin"),
        );
    }

    public function get_destination_attribute()
    {
        return array(
            'label' => $this->get_city_label($this->get_shortcode_param("destination"), $this->get_shortcode_param("case_destination")),
            'code' => $this->get_shortcode_param("destination"),
        );
    }

    public function get_airline_attribute()
    {
        return array(
            'label' => $this->get_airline_label($this->get_shortcode_param("airline_code")),
            'code' => $this->get_shortcode_param("airline_code"),
        );
    }

}