<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\request_api\response\flight;

use tp\includes\request_api\response\TP_Response_Base;
use tp\includes\table\TP_Table_Flight;
use tp\includes\dictionary;

abstract class TP_Response_Flight extends TP_Response_Base
{
    /**
     * @var dictionary\TP_Dictionary_Airports
     */
    protected $dictionary_airports;
    /**
     * @var dictionary\TP_Dictionary_Airlines
     */
    protected $dictionary_airlines;
    /**
     * @var dictionary\TP_Dictionary_Cities
     */
    protected $dictionary_cities;
    /**
     * @var dictionary\TP_Dictionary_Countries
     */
    protected $dictionary_countries;


    public function __construct($row_data, $name = false, $attributes = false)
    {
        parent::__construct($row_data, $name, $attributes);
        try {
            $this->dictionary_airports = dictionary\TP_Dictionary_Airports::instance(tp_get_lang());
        } catch (\Exception $e) {
        }

        try {
            $this->dictionary_airlines = dictionary\TP_Dictionary_Airlines::instance(tp_get_lang());
        } catch (\Exception $e) {
        }

        try {
            $this->dictionary_cities = dictionary\TP_Dictionary_Cities::instance(tp_get_lang());
        } catch (\Exception $e) {
        }

        try {
            $this->dictionary_countries = dictionary\TP_Dictionary_Countries::instance(tp_get_lang());
        } catch (\Exception $e) {
        }
    }


    /**
     * Get date attribute
     * @return array
     */
    public function get_date_attribute($attribute)
    {
        if (isset($this->{$attribute})) {
            $format_date = $this->get_shortcode_param('format_date');
            $date_time = $this->make_date_time($this->{$attribute});

            return array(
                'date_time' => $date_time,
                'date' => $this->make_date($date_time, $format_date),
            );
        }
        return null;
    }

    public function get_date_time_attribute($attribute)
    {
        if (isset($this->{$attribute})) {
            return $this->make_date_time($this->{$attribute});
        }
        return null;
    }

    public function get_url_attribute($departure_date_attribute = '', $return_date_attribute = '')
    {
        $url = 'new';
        $departure_date = $this->get_date_time_attribute($departure_date_attribute);
        $return_date = $this->get_date_time_attribute($return_date_attribute);

        $params = array_filter(array(
            'origin' => $this->origin,
            'destination' => $this->destination,
            'return_date' => $return_date ? date('Y-m-d', $return_date) : null,
            'depart_date' => $departure_date ? date('Y-m-d', $departure_date) : null,
            'currency' => $this->get_shortcode_param('currency'),
            'marker' => $this->make_marker_url(),
            'one_way' => $this->get_shortcode_param('one_way', false),
            'with_request' => $this->get_shortcode_param('after_url') === true ? 'true' : null,
        ));

        $url = $this->create_url_from_params($params, $url);
        if ($this->get_shortcode_param('redirect_url')) {
            $url = get_option('home') . '/?searches=' . rawurlencode($url);
        } else {
            $url = $this->get_url() . $url;
        }

        return $url;
    }


    public function get_airline_attribute()
    {
        return array(
            'code' => $this->airline,
            'label' => $this->get_airline_label($this->airline),
        );
    }

    public function get_airline_logo_attribute()
    {
        $airline_data = $this->get_airline_attribute();

        if ($airline_data['code']) {
            $logo_params = array(
                $this->get_shortcode_param('airline_logo_host'),
                $this->get_shortcode_param('airline_logo_width'),
                $this->get_shortcode_param('airline_logo_heigth'),
                $airline_data['code'] . '@2x.png'
            );
            $airline_data['logo'] = implode('/', $logo_params);
        }
        return $airline_data;
    }

    public function get_flight_attribute()
    {
        return array(
            'airline' => $this->get_airline_label($this->airline),
            'airline_iata' => $this->airline,
            'flight_number' => $this->flight_number,
        );
    }

    public function get_flight_number_attribute()
    {
        return array(
            'airline_iata' => $this->airline,
            'flight_number' => $this->flight_number,
            'value' => $this->airline . ' ' . $this->flight_number,
        );
    }

    public function get_origin_destination_attribute()
    {
        return array(
            'origin' => $this->get_origin_attribute(),
            'destination' => $this->get_destination_attribute(),
        );
    }

    public function get_destination_attribute()
    {
        return array(
            'label' => $this->get_city_label($this->destination),
            'code' => $this->destination,
        );
    }

    public function get_origin_attribute()
    {
        return array(
            'label' => $this->get_city_label($this->origin),
            'code' => $this->origin,
        );
    }


    public function get_price_distance_attribute()
    {
        return array(
            'price_distance' => round($this->price / $this->distance, 2),
            'price' => $this->price,
            'distance' => $this->distance,
        );
    }



    protected function get_airline_label($iata)
    {
        $airline = '';
        if ($iata) {
            try {
                $airline = $this->dictionary_airlines->get_item($iata)->get_name();
            } catch (\Exception $e) {
            }
        }
        return $airline;
    }

    /**
     * @param      $iata
     * @param bool $case
     *
     * @return string
     */
    protected function get_city_label($iata, $case = false){
        $city = '';
        if ($iata) {
            try {
                 $city = $this->dictionary_cities->get_item($iata)->get_name($case);
            } catch (\Exception $e) {
            }
        }
        return $city;
    }

    /**
     * @return string
     */
    public function get_url()
    {
        $white_label = $this->get_shortcode_param('white_label_flight');
        if (empty($white_label)) {
            $url = $this->get_shortcode_param('host') . '/searches/';
        } else {
            $url = $white_label . '/flights/';
        }
        return $url;
    }
}