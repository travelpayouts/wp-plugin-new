<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes;

class TP_Api_Hosts
{

    private static
        $instance = null;

    /**
     * @return TP_Api_Hosts
     */
    public static function get_instance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __construct()
    {
    }


    protected $hotels = array(
        'hotellook_ru' => array(
            'label' => 'hotellook.ru',
        ),
        'hotellook_com_en_GB' => array(
            'label' => 'hotellook.com en-GB',
        ),
        'hotellook_com_en_US' => array(
            'label' => 'hotellook.com en-US',
        ),
        'hotellook_com_pt_BR' => array(
            'label' => 'hotellook.com pt-BR',
        ),
        'hotellook_com_pt_PT' => array(
            'label' => 'hotellook.com pt-PT',
        ),
        'hotellook_com_id_ID' => array(
            'label' => 'hotellook.com id-ID',
        ),
        'hotellook_com_fr_FR' => array(
            'label' => 'hotellook.com fr-FR',
        ),
        'hotellook_com_it_IT' => array(
            'label' => 'hotellook.com it-IT',
        ),
        'hotellook_com_de_DE' => array(
            'label' => 'hotellook.com de-DE',
        ),
        'hotellook_com_pl_PL' => array(
            'label' => 'hotellook.com pl-PL',
        ),
        'hotellook_com_es_ES' => array(
            'label' => 'hotellook.com es-ES',
        ),
        'hotellook_com_th_TH' => array(
            'label' => 'hotellook.com th-TH',
        ),
        'hotellook_com_en_AU' => array(
            'label' => 'hotellook.com en-AU',
        ),
        'hotellook_com_en_CA' => array(
            'label' => 'hotellook.com en-CA',
        ),
        'hotellook_com_en_IE' => array(
            'label' => 'hotellook.com en-IE',
        )
    );
    protected $flights = array(
        'aviasales_ru' => array(
            'label' => 'aviasales.ru',
        ),
        'jetradar_com' => array(
            'label' => 'jetradar.com',
        ),
        'jetradar_com_br' => array(
            'label' => 'jetradar.com.br',
        ),
        'ca_jetradar_com' => array(
            'label' => 'ca.jetradar.com',
        ),
        'jetradar_ch' => array(
            'label' => 'jetradar.ch',
        ),
        'jetradar_at' => array(
            'label' => 'jetradar.at',
        ),
        'jetradar_be' => array(
            'label' => 'jetradar.be',
        ),
        'jetradar_co_nl' => array(
            'label' => 'jetradar.co.nl',
        ),
        'jetradar_gr' => array(
            'label' => 'jetradar.gr',
        ),
        'jetradar_com_au' => array(
            'label' => 'jetradar.com.au',
        ),
        'jetradar_de' => array(
            'label' => 'jetradar.de',
        ),
        'jetradar_es' => array(
            'label' => 'jetradar.es',
        ),
        'jetradar_fr' => array(
            'label' => 'jetradar.fr',
        ),
        'jetradar_it' => array(
            'label' => 'jetradar.it',
        ),
        'jetradar_pt' => array(
            'label' => 'jetradar.pt',
        ),
        'ie_jetradar_com' => array(
            'label' => 'ie.jetradar.com',
        ),
        'jetradar_co_uk' => array(
            'label' => 'jetradar.co.uk',
        ),
        'jetradar_hk' => array(
            'label' => 'jetradar.hk',
        ),
        'jetradar_in' => array(
            'label' => 'jetradar.in',
        ),
        'jetradar_co_nz' => array(
            'label' => 'jetradar.co.nz',
        ),
        'jetradar_ph' => array(
            'label' => 'jetradar.ph',
        ),
        'jetradar_pl' => array(
            'label' => 'jetradar.pl',
        ),
        'jetradar_sg' => array(
            'label' => 'jetradar.sg',
        ),
        'jetradar_co_th' => array(
            'label' => 'jetradar.co.th')
    );


    public function get_flights_label()
    {
        return array_map(function ($flight) {
            return $flight['label'];
        }, $this->flights);
    }

    public function get_hotels_label()
    {
        return array_map(function ($hotel) {
            return $hotel['label'];
        }, $this->hotels);
    }
}