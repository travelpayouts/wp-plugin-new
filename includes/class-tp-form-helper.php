<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 13.12.2017
 */

namespace tp\includes;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Session;

class TP_form_helper
{
    protected static $instance;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }


    /**
     * Handle submitting of a form
     * @param Form $form
     * @param callable|bool $callback
     * @param string|bool $message
     * @param bool|array $options
     * @return bool
     */
    public static function submit_form(Form $form, $callback = false, $message = false, $options = false)
    {
        if (empty($_POST)) return true;
        if (self::is_ajax_request() && $_POST) ob_clean();

        $result = array(
            'status' => 'ok'
        );

        $callback_result = true;

        if ($form->isSubmitted() && $form->isValid()) {
            $callback_args = array(
                'form' => $form,
                'options' => $options
            );

            // call callback function if exist
            if (is_array($callback) || is_callable($callback)) {
                $callback_result = call_user_func_array($callback, $callback_args);
            }
            if (is_null($callback_result) || $callback_result === true) {
                if (self::is_ajax_request()) {
                    if ($message) {
                        $result['message'] = $message;
                    } else {
                        $result['message'] = _x('Data has been successfully saved', 'Tp_plugin success saved message', TP_PLUGIN_TEXTDOMAIN);
                    }

                    if (isset($options['redirect'])) {
                        $result['redirect'] = $options['redirect'];
                    }
                    echo(json_encode($result));
                }
            } else {
                self::return_error();
            }
        } else {
            if (self::is_ajax_request()) {
                $result = array(
                    'field_messages' => self::get_form_errors($form)
                );
                self::return_error($result);
            }
        }

        if (self::is_ajax_request()) die();
    }

    /**
     * Show json error message
     * @param bool $data
     */
    public static function return_error($data = false)
    {

        $result = array(
            'status' => 'error',
            'message' => _x('Please correct errors', 'Tp_plugin save errors message', TP_PLUGIN_TEXTDOMAIN),
        );
        if (is_array($data)) {
            $result = array_merge($result, $data);
        }

        echo(json_encode($result));
    }

    /**
     * List all errors of a given bound form.
     *
     * @param Form $form
     *
     * @return array
     */
    public static function get_form_errors(Form $form)
    {
        $errors = array();

        // Global
        foreach ($form->getErrors() as $error) {
            $errors[$form->getName()][] = $error->getMessage();
        }

        // Fields
        foreach ($form as $child/** @var Form $child */) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$form->getName() . '_' . $child->getName()][] = $error->getMessage();
                }
            }
        }

        return $errors;
    }

    public static function is_ajax_request()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

}