<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 07.09.17
 * Time: 19:36
 */

namespace tp\includes;


class TP_Config
{
    public function get_admin_pages()
    {
        $routes = array(
            'index' => array(
                'auto-links',
                'tickets',
                'hotels',
                'railway',
                'widgets',
                'search-forms',
                'settings'
            ),
        );

        return $routes;
    }


    public static function get()
    {
        return new self;
    }

    public static function get_yii_config()
    {
        global $wpdb;

        return [
            'id' => 'travelPayouts_wp_plugin',
            'basePath' => TP_PLUGIN_PATH,
            'components' => [
                'request' => [
                    'parsers' => [
                        'application/json' => 'yii\web\JsonParser',
                    ]
                ],
                'cache' => [
                    'class' => 'yii\caching\FileCache',
                ],
                'db' => [
                    'class' => 'yii\db\Connection',
                    'dsn' => 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,
                    'username' => DB_USER,
                    'password' => DB_PASSWORD,
                    'charset' => 'utf8',
                    'tablePrefix' => $wpdb->prefix,
                ],
            ]
        ];
    }

}