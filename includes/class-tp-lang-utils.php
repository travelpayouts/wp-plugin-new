<?php


namespace tp\includes;

/**
 * Language helper
 * Class TP_Lang_Utils
 * @package tp\includes
 */
class TP_Lang_Utils
{
    /**
     * English language
     */
    const TP_LANG_EN = "en";
    /**
     * Russian language
     */
    const TP_LANG_RU = "ru";
    /**
     * Thai language
     */
    const TP_LANG_TH = "th";

    /**
     * Get language english
     * @return string
     */
    public static function getLangEN()
    {
        return self::TP_LANG_EN;
    }

    /**
     * Get language russian
     * @return string
     */
    public static function getLangRU()
    {
        return self::TP_LANG_RU;
    }

    /**
     * Get language thai
     * @return string
     */
    public static function getLangTH()
    {
        return self::TP_LANG_TH;
    }

    /**
     * Get default language english
     * @return string
     */
    public static function getDefaultLang()
    {
        return self::getLangEN();
    }

    /**
     * Get lang by option plugin
     * @return string
     */
    public static function getLang()
    {
        //Options::get();
        return self::getLangRU();
    }
}