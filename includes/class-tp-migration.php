<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 26.09.2017
 */

namespace tp\includes;

class TP_Migration
{
    protected $runner;

    public function __construct()
    {
        $phinx = new \Phinx\Console\PhinxApplication();
        $this->runner = new \Phinx\Wrapper\TextWrapper($phinx);
    }

    public function run()
    {
        global $wpdb;
        $migrations_list = array_reverse(glob(TP_PLUGIN_PATH . "includes/db/migrations/[0-9]*.php"));
        $last_migration = null;
        if (count($migrations_list) > 0) {
           $last_migration = basename(array_shift($migrations_list),'.php');
        }

        if ($last_migration && get_option("tp_plugin_db_version") !== $last_migration) {
            $this->runner->setOption('configuration', TP_PLUGIN_PATH . '/includes/db/config.php');
            $this->runner->setOption('parser', 'php');
            $this->runner->setOption('environment', 'production');
            $this->runner->setOption('environments', '');

            $this->runner->getMigrate();
            update_option("tp_plugin_db_version", $last_migration);
        }

    }

}