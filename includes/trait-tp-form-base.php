<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 18.10.2017
 */

namespace tp\includes;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\CallbackTransformer;


trait TP_Form_base
{

    protected $_rules = array();

    protected function checkbox_transformer()
    {
        $call_back_transformer = new CallbackTransformer(
            function ($getValue) {
                // transform the array to a string
                return $getValue == 1;
            },
            function ($setValue) {
                // transform the string back to an array
                return $setValue ? 1 : 0;
            }
        );
        return $call_back_transformer;
    }

    public function get_rule($field)
    {
        if (method_exists($this, 'rules')) {

            $results = $this->get_rules();
            if (array_key_exists($field, $results)) {
                return $results[$field];
            }
        }
        return array();
    }

    protected function get_rules()
    {
        if (method_exists($this, 'rules') && empty($this->_rules)) {
            $this->_rules = $this->rules();
        }
        return $this->_rules;
    }

    /**
     * Symfony buildForm function, don't change it
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->form_builder($builder, $options);
    }

}
