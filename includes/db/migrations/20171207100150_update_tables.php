<?php


use Phinx\Migration\AbstractMigration;

class UpdateTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        if ($this->hasTable('tp_search_shortcodes')) {
            $tp_search_shortcodes = $this->table('tp_search_shortcodes');

            $tp_search_shortcodes
                ->changeColumn('title', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('type_shortcode', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('code_form', 'text', array('null' => true))
                ->changeColumn('from_city', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('to_city', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('hotel_city', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('type_form', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->changeColumn('slug', 'string', array(
                    'null' => true,
                    'limit' => 255,
                ))
                ->update();
        }

        if ($this->hasTable('tp_auto_replace_links')) {
            $tp_auto_replace_links = $this->table('tp_auto_replace_links');

            $tp_auto_replace_links
                ->changeColumn('url', 'string', array(
                    'limit' => 255,
                    'null' => true
                ))
                ->changeColumn('anchor', 'text', array('null' => true))
                ->changeColumn('onclick', 'text', array('null' => true))
                ->changeColumn('nofollow', 'integer', array(
                        'null' => true,
                        'limit' => 1)
                )
                ->changeColumn('replace', 'integer', array(
                        'null' => true,
                        'limit' => 1)
                )
                ->changeColumn('target_blank', 'integer', array(
                        'null' => true,
                        'limit' => 1)
                )
                ->update();
        }
    }
}
