<?php


use Phinx\Migration\AbstractMigration;

class CreateTpTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */


    public function up()
    {

        if (!$this->hasTable('tp_hotel_list_shortcode')) {
            $tp_hotel_list_shortcode = $this->table('tp_hotel_list_shortcode');
            $tp_hotel_list_shortcode
                ->addColumn('location_id', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('date_add', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('hotel_list', 'text')
                ->save();
        }

        if ($this->hasTable('tp_auto_replac_links')) {
            $table = $this->table('tp_auto_replac_links');
            $table->rename('tp_auto_replace_links');
        }

        if (!$this->hasTable('tp_auto_replace_links')) {

            $tp_auto_replace_links = $this->table('tp_auto_replace_links');
            $tp_auto_replace_links
                ->addColumn('arl_url', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('arl_anchor', 'text')
                ->addColumn('arl_event', 'text')
                ->addColumn('arl_nofollow', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('arl_replace', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('arl_target_blank', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('date_add', 'integer', array('limit' => 11, 'null' => false))
                ->save();
        }


        if (!$this->hasTable('tp_search_shortcodes')) {
            $tp_search_shortcodes = $this->table('tp_search_shortcodes');

            $tp_search_shortcodes
                ->addColumn('title', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('date_add', 'integer', array('limit' => 11, 'null' => false))
                ->addColumn('type_shortcode', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('code_form', 'text')
                ->addColumn('from_city', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('to_city', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('hotel_city', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('type_form', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('slug', 'string', array('limit' => 255, 'null' => false))
                ->save();
        }
    }
}
