<?php


use Phinx\Migration\AbstractMigration;

class RenameHotelListShortcode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        if ($this->hasTable('tp_hotel_list_shortcode')) {
            $table = $this->table('tp_hotel_list_shortcode');
            $table->rename('tp_hotel_list_shortcodes');
        }
    }
    public function down(){
        if ($this->hasTable('tp_hotel_list_shortcodes')) {
            $table = $this->table('tp_hotel_list_shortcodes');
            $table->rename('tp_hotel_list_shortcode');
        }
    }
}
