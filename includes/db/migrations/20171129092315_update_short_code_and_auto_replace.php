<?php


use Phinx\Migration\AbstractMigration;

class UpdateShortCodeAndAutoReplace extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up()
    {
        if ($this->hasTable('tp_search_shortcodes')) {
            $tp_search_shortcodes = $this->table('tp_search_shortcodes');

            $tp_search_shortcodes
                ->removeColumn('date_add')
                ->addColumn('created_at', 'datetime')
                ->addColumn('updated_at', 'datetime')
                ->update();
        }


        if ($this->hasTable('tp_auto_replace_links')) {
            $tp_auto_replace_links = $this->table('tp_auto_replace_links');

            $tp_auto_replace_links
                ->removeColumn('date_add')
                ->addColumn('created_at', 'datetime')
                ->addColumn('updated_at', 'datetime')
                ->renameColumn('arl_url', 'url')
                ->renameColumn('arl_anchor', 'anchor')
                ->renameColumn('arl_event', 'onclick')
                ->renameColumn('arl_nofollow', 'nofollow')
                ->renameColumn('arl_replace', 'replace')
                ->renameColumn('arl_target_blank', 'target_blank')
                ->update();
        }
    }
}
