<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 26.09.2017
 */

global $wpdb;

return [
    "paths" => [
        "migrations" => __DIR__ . "/migrations",
        "seeds" => __DIR__ . "/seeds"
    ],
    "environments" => [
        "default_migration_table" => $wpdb->prefix . "tp_plugin_migrations",
        "default_database" => "production",
        "production" => [
            "adapter" => "mysql",
            "host" => DB_HOST,
            "name" => DB_NAME,
            "user" => DB_USER,
            "pass" => DB_PASSWORD,
            "port" => 3306,
            "table_prefix" => $wpdb->prefix
        ]
    ]
];