<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.travelpayouts.com/?locale=en
 * @since      1.0.0
 *
 * @package    Travelpayouts
 * @subpackage Travelpayouts/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Travelpayouts
 * @subpackage Travelpayouts/includes
 * @author     travelpayouts <solomashenko.roman.1991@gmail.com>
 */
namespace tp\includes;

class TP_I18n {

    private $localization = array(
        'en_US',
        'ru_RU',
        'it_IT'
    );
    const TP_PLUGIN_LOCALIZATION_DEFAULT = 'en_US';

    public function __construct()
    {
        add_action('plugins_loaded', array($this, 'load_plugin_textdomain'));
    }

    /**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {


        if ($this->isLocale() == true || TP_PLUGIN_LOCALIZATION_CUSTOM == true){
            //error_log('standart or custom local');
            load_plugin_textdomain(
                TP_PLUGIN_TEXTDOMAIN,
                false,
                TP_PLUGIN_LOCALIZATION_PATH);
        } else {
            //error_log('default local');
            load_textdomain(
                TP_PLUGIN_TEXTDOMAIN,
                TP_PLUGIN_LOCALIZATION_ABSPATH.'/'.TP_PLUGIN_TEXTDOMAIN.'-'.self::TP_PLUGIN_LOCALIZATION_DEFAULT.'.mo' );
        }


	}
	private function isLocale(){
        return in_array(get_locale(), $this->localization);
    }



}
