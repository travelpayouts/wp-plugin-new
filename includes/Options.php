<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes;

use Adbar\Dot;

/**
 * Class Options
 * @property $lang
 */
class Options
{
    const DELIMITER = '/';

    protected $tp_options;
    private static $instance = null;

    /**
     * @return self
     */
    public static function instance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    public function __construct()
    {
        $this->tp_options = $this->get_plugin_options();
    }


    public function get_options()
    {
        return $this->tp_options->all();
    }

    public function get_sections()
    {
        return array_keys($this->get_options());
    }

    /**
     * Getting all plugin options
     * @return Dot
     */
    protected function get_plugin_options()
    {
        if (!$this->tp_options) {
            $options = get_option(TP_PLUGIN_OPTION_NAME);
            if (!$options) $options = array();
            return new Dot($options, self::DELIMITER);
        }
        return $this->tp_options;
    }

    /**
     * Section option getter
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Section option getter
     * @param $name
     * @param $default_value
     * @return mixed
     */
    public function get($name, $default_value = null)
    {
        $methodNames = [
            'get' . ucfirst($name), // camelCase
            'get_' . strtolower($name), // shake_case
        ];

        foreach ($methodNames as $methodName) {
            // call method that equals $name if it exists
            if (method_exists($this, $methodName))
                return $this->{$methodName}();
        }
        return $this->tp_options->get($name, $default_value);
    }

    /**
     * Section option setter
     * @param $name
     * @param $value
     * @return self
     */
    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }

    /**
     * Section option setter
     * @param $name
     * @param $value
     * @return self
     */
    public function set($name, $value)
    {
        $method_function = strtolower($name);
        $method_name = "set_{$method_function}";
        // call method that equals $name if it exists
        if (method_exists($this, $method_name))
            return $this->{$method_name}($value);

        $this->tp_options->set($name, $value);
        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->tp_options->has($name);
    }

    /**
     * Delete the given key or keys
     *
     * @param array|int|string $keys
     * @return self;
     */
    public function delete($keys)
    {
        $this->tp_options->delete($keys);
        return $this;
    }

    /**
     * Updating wordpress options
     * @return bool
     */
    public function save()
    {
        return update_option(TP_PLUGIN_OPTION_NAME, $this->get_options());
    }

    public function getLang()
    {
        return $this->get('settings/localization_lang', 'en_US');
    }
}