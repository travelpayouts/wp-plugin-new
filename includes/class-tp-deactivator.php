<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.travelpayouts.com/?locale=en
 * @since      1.0.0
 *
 * @package    Travelpayouts
 * @subpackage Travelpayouts/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Travelpayouts
 * @subpackage Travelpayouts/includes
 * @author     travelpayouts <solomashenko.roman.1991@gmail.com>
 */
namespace tp\includes;

class TP_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
