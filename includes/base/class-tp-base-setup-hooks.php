<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/17/2017
 * Time: 8:42 PM
 */

namespace tp\includes\base;


abstract class TP_Base_Setup_Hooks
{
    /**
     *
     * @return mixed
     */
    public function init()
    {
        add_action('init', array($this, 'app_output_buffer'));
        $this->define_hooks();
        return $this;
    }

    /**
     * Resolving wp_redirect and the “Headers Already Sent” message
     */
    public function app_output_buffer()
    {
        ob_start();
    }


    abstract protected function define_hooks();
}