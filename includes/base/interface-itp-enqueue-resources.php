<?php
/**
 * Created by PhpStorm.
 * User: SolomashenkoR
 * Date: 10/18/2017
 * Time: 11:04 AM
 */

namespace tp\includes\base;


interface ITP_Enqueue_Resources
{
    public function enqueue_script($hook);
    public function enqueue_style($hook);
    public function set_custom_webpack_path();
}