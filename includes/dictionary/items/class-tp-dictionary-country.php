<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary\items;

class TP_Dictionary_Country extends TP_Dictionary_Item
{

    public function get_name($case = false)
    {
        if ($case) {
            $case_path = "cases.{$case}";
            if ($this->data->has($case_path))
                return $this->data->get($case_path);
        }
        return $this->data->get('name');
    }
}