<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary\items;

class TP_Dictionary_City extends TP_Dictionary_Item
{
    public function get_name($case = false)
    {
        if ($case) {
            $case_path = "cases.{$case}";
            $name = $this->data->get($case_path);
            if ($name !== null)
                return $name;
        }
        return $this->data->get('name');
    }
}