<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary\items;

use Adbar\Dot;

class TP_Dictionary_Item
{
    protected $data;

    public function __construct($data)
    {
        $this->data = new Dot($data);
    }

    public function __get($name)
    {
        return $this->data->get($name);
    }

    public function __set($name, $value)
    {
        return false;
    }

    public function __isset($name)
    {
        return $this->data->has($name);
    }

    public function get_data()
    {
        return $this->data->all();
    }

    public function get_translations($code, $lang = false)
    {
        if ($lang) {
            return $this->data->get("name_translations.{$lang}");
        }
        return $this->data->get('name_translations');
    }
}