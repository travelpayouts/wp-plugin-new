<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary\items;

class TP_Dictionary_Railway extends TP_Dictionary_Item
{

    public function get_name()
    {
        return $this->data->get('name');
    }
}