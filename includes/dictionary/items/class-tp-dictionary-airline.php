<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary\items;

class TP_Dictionary_Airline extends TP_Dictionary_Item
{
    protected $fallback_locale = 'en';

    public function get_name()
    {
        $fallback_locale = $this->fallback_locale;
        $name = $this->data->get('name');
        if ($name === null) {
            $fallback_path = "name_translations.{$fallback_locale}";
            if ($this->data->has($fallback_path)) {
                return $this->data->get($fallback_path);
            }
        }
        return $name;
    }
}