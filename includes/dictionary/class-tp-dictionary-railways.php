<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary;

class TP_Dictionary_Railways extends TP_Dictionary_Base
{
    public $type = 'railways';
    public $item_class = 'tp\includes\dictionary\items\TP_Dictionary_Railway';
    public $_id = 'number';

    protected function get_remote_locale($type, $lang)
    {
        $cache_key = implode('|', array($type, 'ru'));
        $data = $this->cache->get($cache_key);
        if ($data === null) {
            $data_path = TP_PLUGIN_PATH . 'data/railways.json';
            if (file_exists($data_path)) {
                $data = json_decode(file_get_contents($data_path), true);
                $this->cache->set($cache_key, $data, self::CACHE_TIME);
            } else {
                throw new \Exception('Cannot get api data');
            }
        }
        return $data;
    }
}