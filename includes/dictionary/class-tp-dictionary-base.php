<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary;

use Adbar\Dot;
use phpFastCache\CacheManager;

class TP_Dictionary_Base
{
    const CACHE_TIME = 604800; // one week

    const CASE_NOMINATIVE = false; // именительный падеж
    const CASE_GENITIVE = 'ro'; // родительный
    const CASE_ACCUSATIVE = 'vi'; // винительный
    const CASE_DATIVE = 'da'; // дательный
    const CASE_INSTRUMENTAL = 'tv'; // творительный
    const CASE_PREPOSITIONAL = 'pr'; // предложный

    public $_id = 'code';
    protected $cache;
    protected $type;
    protected $lang;
    protected $data = false;
    protected $locales_fallback = array(
        'be' => 'ru',
        'bs' => 'hr',
        'ca' => 'es',
        'ce' => 'ru',
        'hy' => 'ru',
        'kk' => 'ru',
        'me' => 'sr',
        'tg' => 'ru',
        'uz' => 'ru',
    );

    protected $locales = array(
        'ar',
        'bg',
        'cs',
        'da',
        'de',
        'el',
        'en',
        'en-AU',
        'en-CA',
        'en-GB',
        'en-IE',
        'en-IN',
        'en-NZ',
        'en-SG',
        'es',
        'fa',
        'fi',
        'fr',
        'he',
        'hi',
        'hr',
        'hu',
        'id',
        'it',
        'jp',
        'ka',
        'ko',
        'lt',
        'lv',
        'ms',
        'nl',
        'no',
        'pl',
        'pt',
        'pt-BR',
        'ro',
        'ru',
        'sk',
        'sl',
        'sr',
        'sv',
        'th',
        'tl',
        'tr',
        'uk',
        'vi',
        'zh-CN',
        'zh-Hans',
        'zh-Hant',
        'zh-TW',
    );

    protected $item_class;
    private static $instances = array();

    public function __construct($lang)
    {
        $this->cache = CacheManager::Files(array(
            'storage' => 'files',
            'default_chmod' => 0777,
            'htaccess' => true,
            'securityKey' => TP_PLUGIN_TEXTDOMAIN,
            'path' => tp_create_dir_cache(),
        ));
        $this->set_lang($lang);
    }

    public static function instance($lang)
    {
        if (!is_string($lang)) throw new \Exception('Language param must be a string');

        $called_class = get_called_class();
        $instance_name = implode('_', array(
            $called_class,
            $lang
        ));

        if (!isset(self::$instances[$instance_name])) {
            self::$instances[$instance_name] = new $called_class($lang);
        }

        return self::$instances[$instance_name];
    }

    public function set_lang($lang)
    {
        if (!is_string($lang)) throw new \Exception('Language param must be a string');

        if (in_array($lang, $this->locales, true)) {
            $this->lang = $lang;
        } elseif (array_key_exists($lang, $this->locales_fallback)) {
            $this->lang = $this->locales_fallback[$lang];
        } else {
            $this->lang = 'en';
        }
    }

    /**
     * Get api data
     * @param $type
     * @param $lang
     * @throws \Exception
     * @return array|bool
     */
    protected function get_remote_locale($type, $lang)
    {
        $cache_key = implode('|', array($type, $lang));
        $response_json = $this->cache->get($cache_key);
        if ($response_json === null) {
            $request = wp_remote_get("https://api.travelpayouts.com/data/{$lang}/{$type}.json");
            if (!is_wp_error($request) && wp_remote_retrieve_response_code($request) === 200) {
                $response_body = wp_remote_retrieve_body($request);
                $response_json = json_decode($response_body, true);
                $this->cache->set($cache_key, $response_json, self::CACHE_TIME);
            } else {
                throw new \Exception('Cannot get api data');
            }
        }
        return $response_json;
    }

    /**
     * Get response
     * @throws \Exception
     * @return array
     */
    public function get_data()
    {
        if (!$this->data) {
            $response = $this->get_remote_locale($this->type, $this->lang);
            $mapped_response = $this->map_response($response);
            $this->data = new Dot($mapped_response);
        }
        return $this->data->all();
    }

    /**
     * @param $response
     * @return array
     */
    public function map_response($response)
    {
        if (is_array($response)) {
            $mapped_response = array();
            foreach ($response as $value) {
                if (isset($value[$this->_id])) {
                    $item_id = $value[$this->_id];
                    $mapped_response[$item_id] = $value;
                }
            }
            return $mapped_response;
        }
        return array();
    }

    /**
     * Return TP_Dictionary_Item instance
     * @param $code
     * @throws \Exception
     * @return null
     */
    public function get_item($code)
    {
        $cache_key = $this->get_item_cache_key($code);

        $data = $this->cache->get($cache_key);
        if ($data === null) {
            // load api response data
            if (!$this->data) $this->get_data();
            $data = $this->data->get($code);
            $this->cache->set($cache_key, $data, self::CACHE_TIME);
        }
        if (class_exists($this->item_class)) {
            $item_class = $this->item_class;
            return new $item_class($data);
        }
        return null;
    }

    protected function get_item_cache_key($code)
    {
        $cache_key_params = array(
            class_basename($this),
            $this->lang,
            $code,
        );
        return implode('|', $cache_key_params);
    }

}