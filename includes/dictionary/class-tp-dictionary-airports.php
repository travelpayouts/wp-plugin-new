<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\dictionary;

class TP_Dictionary_Airports extends TP_Dictionary_Base
{
    public $type = 'airports';
    public $item_class = 'tp\includes\dictionary\items\TP_Dictionary_Airport';
}