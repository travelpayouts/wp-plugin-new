<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes\behaviors;
use yii;
use yii\base\Behavior;

/**
 * Class ModelMapBehavior
 * @package tp\includes\behaviors
 * @property $owner Yii\base\Model
 */
class ModelMapBehavior extends Behavior
{

    public function getMappedAttributes($safeOnly = true)
    {

        $accessors = $this->getAccessors($safeOnly);
        $hiddenAttributes = $this->getHidden();
        $formAttributes = array();
        foreach ($accessors as $name => $value) {
            if (!empty($value)) {
                $attributeValue = false;
                if (is_array($value) && isset($value['expression'])) {
                    $attributeValue = $value['expression'];
                } elseif (is_string($value)) {
                    $attributeValue = $value;
                }

                if ($attributeValue) {
                    $formAttributes[$name] = $this->evaluateExpression($attributeValue, array(
                        'model' => $this->owner,
                        'attribute' => $name,
                        'options' => isset($value['options']) ? $value['options'] : false
                    ));
                }
            }
        }

        $attributes = $this->getModelAttributes($safeOnly);
        $mappedAttributes = array_merge($attributes, $formAttributes);

        foreach ($hiddenAttributes as $hiddenAttributeKey => $hiddenAttributeValue) {
            if (array_key_exists($hiddenAttributeKey, $mappedAttributes))
                unset($mappedAttributes[$hiddenAttributeKey]);
        }


        $visibleAttributes = $this->getShow();

        foreach ($visibleAttributes as $visibleAttributeKey => $visibleAttribute) {
            if (isset($this->owner->{$visibleAttributeKey})) {
                $mappedAttributes[$visibleAttributeKey] = $this->owner->{$visibleAttributeKey};
            }
        }


        return $mappedAttributes;
    }

    /**
     * Получаем аттрибуты модели
     * @param $safeOnly
     * @return array|mixed|null
     */
    private function getModelAttributes($safeOnly)
    {

        if ($safeOnly) {
            $attributes = $this->owner->attributes;
            $safeAttributeNames = $this->owner->safeAttributes();
            return array_filter($attributes, function ($name) use ($safeAttributeNames) {
                return in_array($name, $safeAttributeNames, true);
            }, ARRAY_FILTER_USE_KEY);
        }
        return $this->owner->attributes;
    }


    public function setMappedAttributes($values, $safeOnly = true)
    {
        if (!is_array($values))
            return false;

        $mutators = $this->getMutators($safeOnly);
        $attributes = array_flip($safeOnly ? $this->owner->safeAttributes() : $this->owner->attributeNames());
        foreach ($values as $name => $value) {
            if (isset($mutators[$name])) {
                $this->evaluateExpression($mutators[$name],
                    array(
                        'model' => $this->owner,
                        'value' => $value,
                        'attribute' => $name,
                    )
                );
            } elseif (isset($attributes[$name])) {
                $this->owner->$name = $value;

            } elseif ($safeOnly) {
                $this->owner->onUnsafeAttribute($name, $value);

            }
        }
        return true;
    }

    private function filterMapping($type, $safeOnly = true)
    {
        if (method_exists($this->owner, 'mapping')) {
            $mappingSource = $this->owner->mapping();

            if ($safeOnly) {
                $safeAttributeNames = $this->owner->safeAttributes();
                $mappingSource = array_filter($mappingSource, function ($name) use ($safeAttributeNames) {
                    return in_array($name, $safeAttributeNames, true);
                }, ARRAY_FILTER_USE_KEY);
            }

            $mapped = array_map(function ($map) use ($type) {
                return is_array($map) ?
                    (isset($map[$type]) ?
                        $map[$type] :
                        null) :
                    $map;
            }, $mappingSource);

            return array_filter($mapped);
        }
        return array();
    }


    /**
     * Передаем значение поля в виде массива {'id','value'}
     * @param $property
     * @param $options
     * @return array|null
     */
    public function getIdLabel($property, $options = false)
    {
        if (null !== $this->owner->{$property} && $options && isset($options['label'])) {
            $label = $this->evaluateExpression($options['label'], array(
                'model' => $this->owner,
            ));
            return array(
                'id' => $this->owner->{$property},
                'label' => $label
            );
        }
        return null;
    }

    /** Получаем данные для поля из массива вида {'id','value'}
     * @param $property
     * @param $value
     * @return bool
     */
    public function setIdLabel($property, $value)
    {
        if (is_array($value) && isset($value['id'])) {
            $this->owner->{$property} = $value['id'];
            return true;
        }
        return false;
    }

    private function getAccessors($safeOnly = true)
    {
        return $this->filterMapping('accessor', $safeOnly);
    }

    private function getMutators($safeOnly = true)
    {
        return $this->filterMapping('mutator', $safeOnly);
    }

    private function getHidden($safeOnly = false)
    {
        return $this->filterMapping('hidden', $safeOnly);
    }

    private function getShow()
    {
        return $this->filterMapping('show', false);
    }

    public function evaluateExpression($_expression_,$_data_=array())
    {
        if(is_string($_expression_))
        {
            extract($_data_);
            try
            {
                return eval('return ' . $_expression_ . ';');
            }
            catch (ParseError $e)
            {
                return false;
            }
        }
        else
        {
            $_data_[]=$this;
            return call_user_func_array($_expression_, $_data_);
        }
    }

}