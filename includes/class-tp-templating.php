<?php
/**
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * Date: 02.11.2017
 */

namespace tp\includes;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Form;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Forms;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;

class TP_Templating
{
    protected $translator;
    protected $builder;
    protected $engine;
    protected $form_themes = array();

    protected $view_paths = array(
        TP_PLUGIN_PATH . 'vendor/symfony/twig-bridge/Resources/views/Form'
    );

    public function __construct($lang)
    {
        $this->lang = $lang;
    }


    /**
     * Add form theme path
     * @param $file
     */
    public function add_form_theme($file)
    {
        $this->form_themes[] = $file;
    }

    /**
     * get form themes array
     * @return array
     */
    public function get_form_themes()
    {
        $default_themes = array('bootstrap_3_layout.html.twig');

        if (empty($this->form_themes)) {
            return $default_themes;
        } else {
            return $this->form_themes;
        }
    }


    /**
     * Add template path
     * @param $path
     * @throws \Exception
     */
    public function add_template($path)
    {
        if (file_exists($path)) {
            $this->view_paths[] = $path;
        } else {
            throw new \Exception('Template path not found');
        }
    }


    /**
     * Set Translator
     * @param string $lang
     */
    protected function set_translator($lang = 'ru')
    {
        // @TODO: Учитывать, что язык ВП может быть не русским
        $translator = new Translator($lang);
        $translator->addLoader('xlf', new XliffFileLoader());
        $translator->addResource('xlf', TP_PLUGIN_PATH . 'vendor/symfony/validator/Resources/translations/validators.' . $lang . '.xlf', $lang, 'messages');
        $translator->addResource('xlf', TP_PLUGIN_PATH . 'vendor/symfony/form/Resources/translations/validators.' . $lang . '.xlf', $lang, 'messages');
        $this->translator = $translator;
    }


    /**
     * Get Translator
     * @return mixed
     */
    protected function get_translator()
    {
        if (empty($this->translator)) {
            $this->set_translator($this->lang);
        }
        return $this->translator;
    }


    /**
     * Set FormFactory
     */
    protected function set_form()
    {
        $builder = Forms::createFormFactoryBuilder();
        // configure validator
        $validator = Validation::createValidatorBuilder()
            ->setTranslator($this->get_translator())
            ->addMethodMapping('rules')
            ->getValidator();
        $builder
            ->addExtension(new ValidatorExtension($validator))
           ;

        $this->builder = $builder->getFormFactory();
    }


    /**
     * Get FormFactory
     * @return Form\FormFactory mixed
     */
    public function form()
    {
        if (empty($this->builder)) {
            $this->set_form();
        }
        return $this->builder;

    }

    /**
     * Get Template engine
     * @return \Twig_Environment mixed
     */
    public function template()
    {
        if (empty($this->engine)) {
            $this->set_template();
        }
        return $this->engine;
    }

    /**
     * Set Template engine
     */
    protected function set_template()
    {
        $twig = new \Twig_Environment(new \Twig_Loader_Filesystem($this->view_paths), array(
            'debug' => true,
        ));
        $twig->addExtension(new \Twig_Extension_Debug());
        $twig->addExtension(new TranslationExtension($this->get_translator()));

        $twig->addFunction(new \Twig_SimpleFunction('bloginfo', function ($show = '', $filter = 'raw') {
            return get_bloginfo($show, $filter);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('__', function ($text, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return __($text, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('translate', function ($text, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return translate($text, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_e', function ($text, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _e($text, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_n', function ($single, $plural, $number, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _n($single, $plural, $number, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_x', function ($text, $context, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _x($text, $context, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_ex', function ($text, $context, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _ex($text, $context, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_nx', function ($single, $plural, $number, $context, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _nx($single, $plural, $number, $context, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_n_noop', function ($singular, $plural, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _n_noop($singular, $plural, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('_nx_noop', function ($singular, $plural, $context, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return _nx_noop($singular, $plural, $context, $domain);
        }));
        $twig->addFunction(new \Twig_SimpleFunction('translate_nooped_plural', function ($nooped_plural, $count, $domain = TP_PLUGIN_TEXTDOMAIN) {
            return translate_nooped_plural($nooped_plural, $count, $domain);
        }));

        $twig->addFunction(new \Twig_SimpleFunction('kebab_case', function ($input) {
            return str_replace('_', '-', $input);
        }));

        $twig->addFunction(new \Twig_SimpleFunction('json', function ($input) {
            return json_encode($input);
        }));

        $twig->addFunction(new \Twig_SimpleFunction('js_object', function ($input) {
            return str_replace('"', '\'', json_encode($input));
        }));

        $form_engine = new TwigRendererEngine($this->get_form_themes());
        $form_engine->setEnvironment($twig);
        $twig->addExtension(new TranslationExtension($this->get_translator()));
        $twig->addExtension(
            new FormExtension(new TwigRenderer($form_engine, null))
        );
        $twig->addExtension(new \Twig_Extension_StringLoader());
        //
        $this->engine = $twig;

    }

    public function render($name, array $context = array(), $return = false)
    {
        $render_result = $this->template()->render($name, $context);

        if ($return) {
            return $render_result;
        } else {
            echo $render_result;
        }
        return false;
    }
}