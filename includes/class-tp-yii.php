<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

namespace tp\includes;
define('YII_DEBUG', true);
define('YII_ENABLE_ERROR_HANDLER', false);

require TP_PLUGIN_PATH . '/vendor/yiisoft/yii2/Yii.php';

class TP_Yii
{
    public $config = [
        'id' => 'yii2Standalone',
        'basePath' => __DIR__,
    ];
    private static $instance = null;

    /**
     * Try to initialize a application at first time
     * at next time do nothing
     * @param $config array|bool
     * @return bool
     */
    public static function create_app($config = false)
    {
        if (null === self::$instance) {
            self::$instance = new self($config);
        }
        return false;
    }

    private function __clone()
    {
    }

    private function __construct($config)
    {
        if ($config && is_array('config'))
            $this->config = $config;
        new \yii\web\Application($config);
    }
}