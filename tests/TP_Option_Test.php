<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */


use PHPUnit\Framework\TestCase;
use tp\includes;

class Options_Test extends TestCase
{
    const SECTION_NAME = 'test';
    protected $optionsClass;

    public function setUp()
    {
        PHPUnit_Framework_Error_Notice::$enabled = false;
        PHPUnit_Framework_Error_Warning::$enabled = false;
        if (!$this->optionsClass) {
            $this->optionsClass = includes\Options::instance();
        }

        parent::setUp(); // TODO: Change the autogenerated stub
    }

    /**
     * @return includes\Options
     */
    public function getOptions()
    {
        return $this->optionsClass;
    }

    /**
     * @covers \tp\includes\Options::delete
     * @covers \tp\includes\Options::get
     * @covers \tp\includes\Options::set
     */
    public function testCreatingSection()
    {
        $this->getOptions()->delete(self::SECTION_NAME)->save();

        // checking thats sections don't exists before create
        $this->assertNull($this->getOptions()->get(self::SECTION_NAME));
        $this->getOptions()
            ->set(self::SECTION_NAME, array())->save();
        $this->assertInternalType('array', $this->getOptions()->get(self::SECTION_NAME));
        $this->assertContains(self::SECTION_NAME, $this->getOptions()->sections);
    }

    /**
     * @covers \tp\includes\Options::set
     */
    public function testUpdatingSection()
    {
        $new_data = array(
            'someData' => 'hello world',
            'someNumber' => rand(),
        );

        $this->getOptions()->set(self::SECTION_NAME, $new_data)->save();
        $this->assertEquals($new_data, $this->getOptions()->get(self::SECTION_NAME));
    }

    /**
     * @covers \tp\includes\Options::set
     */
    public function testUpdatingValue()
    {
        $section_name = self::SECTION_NAME;
        $this->getOptions()->set("{$section_name}/someData", 'goodbye world')->save();
        $this->assertEquals('goodbye world', $this->getOptions()->get("{$section_name}/someData"));
    }

    /**
     * @covers \tp\includes\Options::set
     */
    public function testUpdatingDataByNestedPath()
    {
        $section_name = self::SECTION_NAME;
        $path = "{$section_name}/some/nested/path/to/save";
        $data = array(
            'id' => 123
        );
        $this->getOptions()->set($path, $data);

        $this->assertNotNull($this->getOptions()->get($path));
        $this->assertEquals($data, $this->getOptions()->get($path));
    }

    /**
     * @covers \tp\includes\Options::get
     */
    public function testGettingValueOrSection()
    {
        $section_name = self::SECTION_NAME;
        $this->assertEquals('goodbye world', $this->getOptions()->get("{$section_name}/someData"));
        $this->assertNotEquals('hello world', $this->getOptions()->get("{$section_name}/someNumber"));
        $this->assertInternalType('int', $this->getOptions()->get("{$section_name}/someNumber"));
        $this->assertInternalType('string', $this->getOptions()->get("{$section_name}/someData"));
        $this->assertNotNull($this->getOptions()->get($section_name));
    }

    /**
     * @covers \tp\includes\Options::delete
     */
    public function testDeletingValue()
    {
        $section_name = self::SECTION_NAME;
        $this->assertNotEmpty($this->getOptions()->get("{$section_name}/some/nested/path/to/save"));
        $this->getOptions()->delete("{$section_name}/some/nested/path/to/save/id")->save();
        $this->assertEmpty($this->getOptions()->get("{$section_name}/some/nested/path/to/save"));
        $this->getOptions()->delete($section_name)->save();
        $this->assertNull($this->getOptions()->get($section_name));
        $options = $this->getOptions()->options;
        $this->assertFalse(isset($options['test']));
    }

}