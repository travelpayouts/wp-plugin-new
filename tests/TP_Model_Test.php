<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use PHPUnit\Framework\TestCase;


class TP_Model_Test extends TestCase
{
    public function testSome()
    {
        $default_options = array(
            'someDefaultValue' => true,
            'someDefaultValueThatNeedsToBeUpdated' => false,
        );
        $model = new \tp\admin\includes\models\TP_model_account();
        $model->section = 'test.test';

        $model->defaultOptions = $default_options;
        $model->init();
        $model->reset_section_options();
        $model->init();


        $this->assertEquals($default_options['someDefaultValue'], $model->someDefaultValue);
        // checking that magic property was exists
        $this->assertTrue(isset($model->someDefaultValue));
        $this->assertFalse(isset($model->someValueThatNotExists));

        $this->assertTrue(is_array($model->attributes));
        // checking that default options was set correctly
        $this->assertEquals($default_options, $model->attributes);
        // trying to update attributes
        $new_attributes = array(
            'someDefaultValueThatNeedsToBeUpdated' => true,
            'someData' => array(
                'test' => true,
            ));

        $model->attributes = $new_attributes;
        $new_attributes_merged = array_merge($default_options, $new_attributes);

        $this->assertEquals($model->attributes, $new_attributes_merged);
        $new_attributes_merged['someValueThatNotAssinged'] = true;
        $this->assertNotEquals($model->attributes, $new_attributes_merged);

        // trying to update attribute with get and set functions
        $new_attribute_value = rand();

        $model->set('helloWorld', $new_attribute_value);
        $this->assertEquals($new_attribute_value, $model->get('helloWorld'));

        // trying to get and update attribute with getters and setters
        $new_attribute_value = '_' . rand();
        $this->assertFalse(isset($model->goodByeWorld));
        $model->goodByeWorld = $new_attribute_value;
        $this->assertTrue(isset($model->goodByeWorld));
        $this->assertEquals($new_attribute_value, $model->goodByeWorld);
        $this->assertEquals($model->goodByeWorld, $model->get('goodByeWorld'));


        $nested_data = array(
            'values' => array(
                '123',
                '456',
            )
        );
        $model->set('some/nested/data', $nested_data);
        // For nested properties we can user only get(...) method
        $this->assertEquals($nested_data, $model->get('some/nested/data'));

        $this->assertTrue($model->save());
    }
}