<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
error_reporting(0);
define('TP_PHPUNIT', true);
$wp_test_lib_path = __DIR__.'/../../../../wp-tests';

if (!file_exists($wp_test_lib_path . '/includes/functions.php')) {
    echo "Could not find {$wp_test_lib_path}/includes/functions.php" . PHP_EOL;
    exit(1);
}

// Give access to tests_add_filter() function.
require_once $wp_test_lib_path . '/includes/functions.php';
// Start up the WP testing environment.
require $wp_test_lib_path . '/includes/bootstrap.php';
