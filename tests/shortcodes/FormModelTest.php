<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use tp\admin\includes\models\shortcodes\FormModel;
use PHPUnit\Framework\TestCase;
use Adbar\Dot;

class FormModelTest extends TestCase
{
    public $attributes = array(
        'destination' =>
            array(
                'location' =>
                    array(
                        'lat' => 54.565403,
                        'lon' => 55.884544,
                    ),
                '_label' => 'Ufa, Russia Ufa International Airport [UFA]',
                'searches_count' => 2547751,
                'index_strings' =>
                    array(
                        0 => 'aeroportul internațional ufa',
                        1 => 'mednarodno letališče ufa',
                        2 => 'medzinárodné letisko ufa',
                        3 => 'mezinárodní letiště ufa',
                        4 => 'međunarodna zračna luka ufa',
                        5 => 'oefa',
                        6 => 'ufa',
                        7 => 'ufa flyplass',
                        8 => 'ufa international airport',
                        9 => 'ufa internationale luchthaven',
                        10 => 'ufa internationale lufthavn',
                        11 => 'ufa međunarodni aerodrom',
                        12 => 'ufa nemzetközi repülőtér',
                        13 => 'ufa russland',
                        14 => 'ufan kansainvälinen lentokenttä',
                        15 => 'ufas internationella flygplats',
                        16 => 'ufas starptautiskā lidosta',
                        17 => 'ουφά',
                        18 => 'ουφά αεροδρόμιο',
                        19 => 'международно летище уфа',
                        20 => 'уфа',
                        21 => 'ուֆա',
                        22 => 'אופה',
                        23 => 'נמל התעופה הבינלאומי אופה',
                        24 => 'أوفا',
                        25 => 'اوفا',
                        26 => 'فرودگاه بین المللی اوفا',
                        27 => 'مطار أوفا الدولي',
                        28 => 'ऊफ़ा',
                        29 => 'यूफा अंतरराष्ट्रीय हवाई अड्डा',
                        30 => 'สนามบินอูฟา',
                        31 => 'อูฟา',
                        32 => 'უფა',
                        33 => 'უფას აეროპორტი',
                        34 => 'ウファ',
                        35 => '乌法',
                        36 => '乌法机场',
                        37 => '烏法',
                        38 => '우파',
                    ),
                'country_iata' => 'RU',
                'city_iata' => 'UFA',
                'airport_name' => 'Ufa International Airport',
                'coordinates' =>
                    array(
                        0 => 54.565403,
                        1 => 55.884544,
                    ),
                'iata' => 'UFA',
                'name' => 'Ufa, Russia',
            ),
        'subId' => null,
        'subid' => '123erw',
        'search_form' => '2',
        'origin' =>
            array(
                'location' =>
                    array(
                        'lat' => 55.755786,
                        'lon' => 37.617633,
                    ),
                '_label' => 'Moscow, Russia  [MOW]',
                'searches_count' => 1006321,
                'index_strings' =>
                    array(
                        0 => 'defaultcity',
                        1 => 'defaultcity',
                        2 => 'maskava',
                        3 => 'maskva',
                        4 => 'mosca',
                        5 => 'moscou',
                        6 => 'moscova',
                        7 => 'moscovo',
                        8 => 'moscow',
                        9 => 'moscú',
                        10 => 'moskau',
                        11 => 'moskou',
                        12 => 'moskova',
                        13 => 'moskow',
                        14 => 'moskva',
                        15 => 'moskwa',
                        16 => 'moszkva',
                        17 => 'μόσχα',
                        18 => 'москва',
                        19 => 'нерезиновая',
                        20 => 'нерезиновая',
                        21 => 'нерезиновск',
                        22 => 'нерезиновск',
                        23 => 'понаехавск',
                        24 => 'понаехавск',
                        25 => 'մոսկվա',
                        26 => 'מוסקבה',
                        27 => 'مسکو',
                        28 => 'موسكو',
                        29 => 'मास्को',
                        30 => 'มอสโก',
                        31 => 'მოსკოვი',
                        32 => 'モスクワ',
                        33 => '莫斯科',
                        34 => '모스크바',
                    ),
                'country_iata' => 'RU',
                'airport_name' => null,
                'iata' => 'MOW',
                'coordinates' =>
                    array(
                        0 => 55.755786,
                        1 => 37.617633,
                    ),
                'name' => 'Moscow, Russia',
            ),
    );

    public function testGettingShortcodeAttributes()
    {
        $expectedAttributes = [
            FormModel::shortcode_form => [
                'slug',
                'origin',
                'destination',
                'subid',
            ]];
        $shortcodeList = [FormModel::shortcode_form];

        foreach ($shortcodeList as $shortcode) {
            $model = new FormModel();
            $model->current_table = $shortcode;
            $model->attributes = $this->attributes;
            $shortcodeData = $model->shortcodeData;
            $shortcodeAttributes = $shortcodeData->params;
            $this->assertEquals($shortcode, $shortcodeData->shortcode);
            if (isset($expectedAttributes[$shortcode])) {
                foreach ($expectedAttributes[$shortcode] as $expectedAttribute) {
                    // Check that every attribute is not null
                    $this->assertNotNull($shortcodeAttributes->get($expectedAttribute), "$shortcode - expected $expectedAttribute but it's empty");
                }
            }
        }
    }
//[tp_search_shortcodes  slug="32223c57" origin="MOW" destination="UFA" type="" subid="123"]
}
