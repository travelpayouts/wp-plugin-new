<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use tp\admin\includes\models\shortcodes\HotelsModel;
use PHPUnit\Framework\TestCase;
use Adbar\Dot;

class HotelsModelTest extends TestCase
{

    public $attributes = array(
        'title' => 'NO TITLE',
        'city' =>
            array(
                'countryCode' => 'RU',
                'country' => 'Russia',
                'latinFullName' => 'Ufa, Russia',
                'fullname' => 'Ufa, Russia',
                'clar' => 'Russia',
                'latinClar' => 'Russia',
                'location' =>
                    array(
                        'lat' => 54.749474,
                        'lon' => 55.971381,
                    ),
                'hotelsCount' => 105,
                'iata' =>
                    array(
                        0 => 'UFA',
                        1 => 'UFA',
                    ),
                'city' => 'Ufa',
                'latinCity' => 'Ufa',
                'timezone' => 'Asia/Yekaterinburg',
                'timezonesec' => 21600,
                'latinCountry' => 'Russia',
                'id' => 12209,
                'countryId' => 186,
                '_score' => 31500,
                'state' => null,
                '_label' => 'Ufa, Russia',
            ),
        'city_label' => null,
        'subid' => '324dfrd',
        'check_in' => '28-11-2018',
        'check_out' => '01-12-2018',
        'number_results' => 20,
        'paginate' => true,
        'off_title' => true,
        'link_without_dates' => true,
        'type_selections' => 'popularity',
        'type_selections_label' => null,
    );

    //[tp_hotels_selections_discount_shortcodes city="18335" title="321432" paginate=true off_title=true type_selections="popularity"  number_results="20" subid="213123" city_label="Moscow" type_selections_label="Popularity" link_without_dates=true]
    //[tp_hotels_selections_date_shortcodes city="12153" title="no title" paginate=true off_title=true type_selections="3-stars"  city_label="Moscow"  number_results="20" subid="123" check_in="28-11-2018" check_out="29-11-2018" type_selections_label="3 stars" link_without_dates=true]
    public function testGettingShortcodeAttributes()
    {
        $expectedAttributes = [
            HotelsModel::shortcode_date => [
                'city',
                'title',
                'paginate',
                'off_title',
                'type_selections',
                'city_label',
                'number_results',
                'subid',
                'check_in',
                'check_out',
                'type_selections_label',
                'link_without_dates',
            ],
            HotelsModel::shortcode_discount => [
                'city',
                'title',
                'paginate',
                'off_title',
                'number_results',
                'subid',
                'city_label',
                'type_selections_label',
                'link_without_dates',
            ],
        ];
        $shortcodeList = array_keys(HotelsModel::instance()->table_fields);

        foreach ($shortcodeList as $shortcode) {
            $model = new HotelsModel();
            $model->current_table = $shortcode;
            $model->attributes = $this->attributes;
            $shortcodeData = $model->shortcodeData;
            $shortcodeAttributes = $shortcodeData->params;
            $this->assertEquals($shortcode, $shortcodeData->shortcode);
            if (isset($expectedAttributes[$shortcode])) {
                foreach ($expectedAttributes[$shortcode] as $expectedAttribute) {
                    // Check that every attribute is not null
                    $this->assertNotNull($shortcodeAttributes->get($expectedAttribute), "$shortcode - expected $expectedAttribute but it's empty");
                }
            }
        }

    }


}
