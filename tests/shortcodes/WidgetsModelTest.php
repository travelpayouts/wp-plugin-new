<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use tp\admin\includes\models\shortcodes\WidgetsModel;
use PHPUnit\Framework\TestCase;
use Adbar\Dot;

class WidgetsModelTest extends TestCase
{


//[tp_map_widget origin=MOW width=800 height=500 direct=true subid="123"]
//[tp_hotelmap_widget coordinates="55.752041, 37.617508" width=500 height=500 zoom=12 subid="123"]
//[tp_calendar_widget origin="MOW" destination="UFA" direct=true one_way=true responsive=true subid="123" period_day_from="7"  period_day_to="14" period="current_month"]
//[tp_subscriptions_widget origin=MOW destination=UFA responsive=true subid="123"]
//[tp_hotel_widget hotel_id=333578 responsive=true subid="123"]
//
//<div class="TP-PopularRoutesWidgets">
//<div class='TP-PopularRoutesWidget'>[tp_popular_routes_widget destination=MOW responsive=true subid="123"]</div><div class='TP-PopularRoutesWidget'>[tp_popular_routes_widget destination=UFA responsive=true subid="123"]</div><div class='TP-PopularRoutesWidget'>[tp_popular_routes_widget destination=ROV responsive=true subid="123"]</div></div>
//
//[tp_hotel_selections_widget id=12153  cat1="3stars"  cat2="distance"  cat3="tophotels"  type=full limit=10 subid="123"]
//[tp_ducklett_widget responsive=true limit=9 type=brickwork filter=0 airline="KE,NZ," subid="123"]
//[tp_ducklett_widget responsive=true limit=9 type=brickwork filter=1 origin=MOW destination=UFA  subid="123"]
    public $attributes = array(
        'subid' => '3424erd32q',
        'responsive_width' => 800,
        'airlines' =>
            array(
                0 =>
                    array(
                        'iata' => '9D',
                        'name' => 'Toumai Air Tchad',
                        '_label' => 'Toumai Air Tchad',
                    ),
                1 =>
                    array(
                        'iata' => 'NZ',
                        'name' => 'Air New Zealand',
                        '_label' => 'Air New Zealand',
                    ),
            ),
        'limit' => '10',
        'period_from' => '7',
        'direct' => true,
        'hotel_name' =>
            array(
                'location' =>
                    array(
                        'lat' => 55.74374,
                        'lon' => 37.625156,
                    ),
                'stars' => 0,
                'timezonesec' => 14400,
                'latinCountry' => 'Russia',
                'latinCity' => 'Moscow',
                'country' => 'Russia',
                '_score' => 2000,
                'photoCount' => 24,
                '_label' => 'Hostel Sunflower, Moscow, Russia',
                'latinLocationFullName' => 'Moscow, Russia',
                'clar' => 'Russia',
                'locationHotelsCount' => 1907,
                'city' => 'Moscow',
                'locationFullName' => 'Moscow, Russia',
                'locationId' => 12153,
                'latinClar' => 'Russia',
                'name' => 'Hostel Sunflower',
                'state' => null,
                'countryId' => 186,
                'id' => 29892965,
                'timezone' => 'Europe/Moscow',
                'distance' => 1,
                'latinName' => 'Hostel Sunflower',
                'photos' =>
                    array(
                        0 => 5684504282,
                    ),
                'hotelFullName' => 'Hostel Sunflower, Moscow, Russia',
                'rating' => 87,
                'address' => 'Ulitsa Bolshaya Ordynka 13 9 Bldg 1 Entrance 4, Moscow',
            ),
        'one_way' => false,
        'origin' =>
            array(
                'location' =>
                    array(
                        'lon' => 37.617633,
                        'lat' => 55.755786,
                    ),
                '_label' => 'Moscow, Russia  [MOW]',
                'searches_count' => 1006321,
                'index_strings' =>
                    array(
                        0 => 'defaultcity',
                        1 => 'defaultcity',
                        2 => 'maskava',
                        3 => 'maskva',
                        4 => 'mosca',
                        5 => 'moscou',
                        6 => 'moscova',
                        7 => 'moscovo',
                        8 => 'moscow',
                        9 => 'moscú',
                        10 => 'moskau',
                        11 => 'moskou',
                        12 => 'moskova',
                        13 => 'moskow',
                        14 => 'moskva',
                        15 => 'moskwa',
                        16 => 'moszkva',
                        17 => 'μόσχα',
                        18 => 'москва',
                        19 => 'нерезиновая',
                        20 => 'нерезиновая',
                        21 => 'нерезиновск',
                        22 => 'нерезиновск',
                        23 => 'понаехавск',
                        24 => 'понаехавск',
                        25 => 'մոսկվա',
                        26 => 'מוסקבה',
                        27 => 'مسکو',
                        28 => 'موسكو',
                        29 => 'मास्को',
                        30 => 'มอสโก',
                        31 => 'მოსკოვი',
                        32 => 'モスクワ',
                        33 => '莫斯科',
                        34 => '모스크바',
                    ),
                'country_iata' => 'RU',
                'airport_name' => null,
                'coordinates' =>
                    array(
                        0 => 55.755786,
                        1 => 37.617633,
                    ),
                'iata' => 'MOW',
                'name' => 'Moscow, Russia',
            ),
        'responsive' => false,
        'destination_multiple' =>
            array(
                0 =>
                    array(
                        'location' =>
                            array(
                                'lat' => 55.755786,
                                'lon' => 37.617633,
                            ),
                        '_label' => 'Moscow, Russia  [MOW]',
                        'searches_count' => 1006321,
                        'index_strings' =>
                            array(
                                0 => 'defaultcity',
                                1 => 'defaultcity',
                                2 => 'maskava',
                                3 => 'maskva',
                                4 => 'mosca',
                                5 => 'moscou',
                                6 => 'moscova',
                                7 => 'moscovo',
                                8 => 'moscow',
                                9 => 'moscú',
                                10 => 'moskau',
                                11 => 'moskou',
                                12 => 'moskova',
                                13 => 'moskow',
                                14 => 'moskva',
                                15 => 'moskwa',
                                16 => 'moszkva',
                                17 => 'μόσχα',
                                18 => 'москва',
                                19 => 'нерезиновая',
                                20 => 'нерезиновая',
                                21 => 'нерезиновск',
                                22 => 'нерезиновск',
                                23 => 'понаехавск',
                                24 => 'понаехавск',
                                25 => 'մոսկվա',
                                26 => 'מוסקבה',
                                27 => 'مسکو',
                                28 => 'موسكو',
                                29 => 'मास्को',
                                30 => 'มอสโก',
                                31 => 'მოსკოვი',
                                32 => 'モスクワ',
                                33 => '莫斯科',
                                34 => '모스크바',
                            ),
                        'country_iata' => 'RU',
                        'airport_name' => null,
                        'iata' => 'MOW',
                        'coordinates' =>
                            array(
                                0 => 55.755786,
                                1 => 37.617633,
                            ),
                        'name' => 'Moscow, Russia',
                    ),
                1 =>
                    array(
                        'location' =>
                            array(
                                'lat' => 54.565403,
                                'lon' => 55.884544,
                            ),
                        '_label' => 'Ufa, Russia Ufa International Airport [UFA]',
                        'searches_count' => 2547751,
                        'index_strings' =>
                            array(
                                0 => 'aeroportul internațional ufa',
                                1 => 'mednarodno letališče ufa',
                                2 => 'medzinárodné letisko ufa',
                                3 => 'mezinárodní letiště ufa',
                                4 => 'međunarodna zračna luka ufa',
                                5 => 'oefa',
                                6 => 'ufa',
                                7 => 'ufa flyplass',
                                8 => 'ufa international airport',
                                9 => 'ufa internationale luchthaven',
                                10 => 'ufa internationale lufthavn',
                                11 => 'ufa međunarodni aerodrom',
                                12 => 'ufa nemzetközi repülőtér',
                                13 => 'ufa russland',
                                14 => 'ufan kansainvälinen lentokenttä',
                                15 => 'ufas internationella flygplats',
                                16 => 'ufas starptautiskā lidosta',
                                17 => 'ουφά',
                                18 => 'ουφά αεροδρόμιο',
                                19 => 'международно летище уфа',
                                20 => 'уфа',
                                21 => 'ուֆա',
                                22 => 'אופה',
                                23 => 'נמל התעופה הבינלאומי אופה',
                                24 => 'أوفا',
                                25 => 'اوفا',
                                26 => 'فرودگاه بین المللی اوفا',
                                27 => 'مطار أوفا الدولي',
                                28 => 'ऊफ़ा',
                                29 => 'यूफा अंतरराष्ट्रीय हवाई अड्डा',
                                30 => 'สนามบินอูฟา',
                                31 => 'อูฟา',
                                32 => 'უფა',
                                33 => 'უფას აეროპორტი',
                                34 => 'ウファ',
                                35 => '乌法',
                                36 => '乌法机场',
                                37 => '烏法',
                                38 => '우파',
                            ),
                        'country_iata' => 'RU',
                        'city_iata' => 'UFA',
                        'airport_name' => 'Ufa International Airport',
                        'coordinates' =>
                            array(
                                0 => 54.565403,
                                1 => 55.884544,
                            ),
                        'iata' => 'UFA',
                        'name' => 'Ufa, Russia',
                    ),
                2 =>
                    array(
                        'location' =>
                            array(
                                'lat' => 47.49417,
                                'lon' => 39.92472,
                            ),
                        '_label' => 'Rostov, Russia Platov International Airport [ROV]',
                        'searches_count' => 51,
                        'index_strings' =>
                            array(
                                0 => 'aeroportul rostov-pe-don',
                                1 => 'letališče rostov',
                                2 => 'letisko rostov',
                                3 => 'letiště rostov',
                                4 => 'platov international airport',
                                5 => 'platov internationale lufthavn',
                                6 => 'rostov',
                                7 => 'rostov',
                                8 => 'rostov aan de don',
                                9 => 'rostov aerodrom',
                                10 => 'rostov luchthaven',
                                11 => 'rostov na donu',
                                12 => 'rostov on don',
                                13 => 'rostov repülőtér',
                                14 => 'rostov trên sông đông',
                                15 => 'rostov ved don',
                                16 => 'rostov-na-donu',
                                17 => 'rostov-na-donu flyplass',
                                18 => 'rostov-on-don',
                                19 => 'rostov-pe-don',
                                20 => 'rostova pie donas',
                                21 => 'rostovas pie donas lidosta',
                                22 => 'rostovin lentoasema',
                                23 => 'rostovs flygplats',
                                24 => 'rostów',
                                25 => 'rosztov',
                                26 => 'zračna luka rostov',
                                27 => 'ροστόφ',
                                28 => 'ροστόφ αεροδρόμιο',
                                29 => 'летище ростов на дон',
                                30 => 'платов международный аэропорт',
                                31 => 'ростов на дон',
                                32 => 'ростов-на-дону',
                                33 => 'ростов-на-дону (платов)',
                                34 => 'դոնի ռոստով',
                                35 => 'ռոստով դոնի',
                                36 => 'נמל התעופה רוסטוב',
                                37 => 'רוסטוב על הדון',
                                38 => 'روستوف-نا-دونو',
                                39 => 'روستوڤ',
                                40 => 'فرودگاه روستوف-نا-دونو',
                                41 => 'مطار روستوڤ',
                                42 => 'रोतोव-ऑन-डॉन',
                                43 => 'रोस्तोव हवाई अड्डा',
                                44 => 'รอสตอฟ',
                                45 => 'როსტოვ ნა დონ',
                                46 => 'როსტოვის აეროპორტი',
                                47 => 'ロストフ・ナ・ドヌー',
                                48 => '罗斯托夫',
                                49 => '頓河畔羅斯托夫',
                                50 => '顿河畔罗斯托夫',
                                51 => '로스토프 온 돈',
                            ),
                        'country_iata' => 'RU',
                        'city_iata' => 'ROV',
                        'airport_name' => 'Platov International Airport',
                        'iata' => 'ROV',
                        'coordinates' =>
                            array(
                                0 => 47.49417,
                                1 => 39.92472,
                            ),
                        'name' => 'Rostov, Russia',
                    ),
            ),
        'widget_type' => 'brickwork',
        'limit_ducklett' => 9,
        'type' => 'full',
        'destination' =>
            array(
                'location' =>
                    array(
                        'lon' => 55.884544,
                        'lat' => 54.565403,
                    ),
                '_label' => 'Ufa, Russia Ufa International Airport [UFA]',
                'searches_count' => 2547751,
                'index_strings' =>
                    array(
                        0 => 'aeroportul internațional ufa',
                        1 => 'mednarodno letališče ufa',
                        2 => 'medzinárodné letisko ufa',
                        3 => 'mezinárodní letiště ufa',
                        4 => 'međunarodna zračna luka ufa',
                        5 => 'oefa',
                        6 => 'ufa',
                        7 => 'ufa flyplass',
                        8 => 'ufa international airport',
                        9 => 'ufa internationale luchthaven',
                        10 => 'ufa internationale lufthavn',
                        11 => 'ufa međunarodni aerodrom',
                        12 => 'ufa nemzetközi repülőtér',
                        13 => 'ufa russland',
                        14 => 'ufan kansainvälinen lentokenttä',
                        15 => 'ufas internationella flygplats',
                        16 => 'ufas starptautiskā lidosta',
                        17 => 'ουφά',
                        18 => 'ουφά αεροδρόμιο',
                        19 => 'международно летище уфа',
                        20 => 'уфа',
                        21 => 'ուֆա',
                        22 => 'אופה',
                        23 => 'נמל התעופה הבינלאומי אופה',
                        24 => 'أوفا',
                        25 => 'اوفا',
                        26 => 'فرودگاه بین المللی اوفا',
                        27 => 'مطار أوفا الدولي',
                        28 => 'ऊफ़ा',
                        29 => 'यूफा अंतरराष्ट्रीय हवाई अड्डा',
                        30 => 'สนามบินอูฟา',
                        31 => 'อูฟา',
                        32 => 'უფა',
                        33 => 'უფას აეროპორტი',
                        34 => 'ウファ',
                        35 => '乌法',
                        36 => '乌法机场',
                        37 => '烏法',
                        38 => '우파',
                    ),
                'country_iata' => 'RU',
                'city_iata' => 'UFA',
                'airport_name' => 'Ufa International Airport',
                'iata' => 'UFA',
                'coordinates' =>
                    array(
                        0 => 54.565403,
                        1 => 55.884544,
                    ),
                'name' => 'Ufa, Russia',
            ),
        'city' =>
            array(
                'id' => 12153,
                'state' => null,
                'latinFullName' => 'Moscow, Russia',
                'iata' =>
                    array(
                        0 => 'MOW',
                        1 => 'ZIA',
                        2 => 'VKO',
                        3 => 'SVO',
                        4 => 'DME',
                    ),
                'latinCity' => 'Moscow',
                'latinCountry' => 'Russia',
                '_label' => 'Moscow, Russia',
                'countryCode' => 'RU',
                '_score' => 1907,
                'hotelsCount' => 1907,
                'countryId' => 186,
                'latinClar' => 'Russia',
                'location' =>
                    array(
                        'lat' => 55.752041,
                        'lon' => 37.617508,
                    ),
                'city' => 'Moscow',
                'timezonesec' => 14400,
                'fullname' => 'Moscow, Russia',
                'clar' => 'Russia',
                'country' => 'Russia',
                'timezone' => 'Europe/Moscow',
            ),
        'city_categories' =>
            array(
                0 =>
                    array(
                        'value' => 'distance',
                    ),
                1 =>
                    array(
                        'value' => '3stars',
                    ),
                2 =>
                    array(
                        'value' => 'tophotels',
                    ),
            ),
        'size_height' => 500,
        'size_width' => 800,
        'calendar_period' => 'current_month',
        'hotel_coordinates' =>
            array(
                'id' => 12153,
                'state' => null,
                'latinFullName' => 'Moscow, Russia',
                'iata' =>
                    array(
                        0 => 'MOW',
                        1 => 'ZIA',
                        2 => 'VKO',
                        3 => 'SVO',
                        4 => 'DME',
                    ),
                'latinCity' => 'Moscow',
                'latinCountry' => 'Russia',
                '_label' => 'Moscow, Russia [1907 hotels]',
                'countryCode' => 'RU',
                '_score' => 1907,
                'hotelsCount' => 1907,
                'countryId' => 186,
                'latinClar' => 'Russia',
                'location' =>
                    array(
                        'lat' => 55.752041,
                        'lon' => 37.617508,
                    ),
                'city' => 'Moscow',
                'timezonesec' => 14400,
                'fullname' => 'Moscow, Russia',
                'clar' => 'Russia',
                'country' => 'Russia',
                'timezone' => 'Europe/Moscow',
            ),
        'zoom' => '12',
        'filter_by' => WidgetsModel::FILTER_BY_AIRLINES,
        'period_to' => '14',
    );

    public function testGettingShortcodeAttributes()
    {
        $expectedAttributes = [
            WidgetsModel::shortcode_map => [
                'origin',
                'width',
                'height',
                'direct',
                'subid',
            ],
            WidgetsModel::shortcode_hotelmap => [
                'coordinates',
                'width',
                'height',
                'zoom',
                'subid',
            ],
            WidgetsModel::shortcode_calendar => [
                'origin',
                'destination',
                'direct',
                'one_way',
                'responsive',
                'subid',
                'period_day_from',
                'period_day_to',
                'period',
            ],
            WidgetsModel::shortcode_subscriptions => [
                'origin',
                'destination',
                'responsive',
                'subid',
            ],
            WidgetsModel::shortcode_hotel => [
                'hotel_id',
                'responsive',
                'subid',
            ],
            WidgetsModel::shortcode_popular_routes => [

            ],
            WidgetsModel::shortcode_hotel_selections => [
                'id',
                'cat1',
                'cat2',
                'cat3',
                'type',
                'limit',
                'subid',
            ],
            WidgetsModel::shortcode_ducklett => [
                'responsive',
                'limit',
                'type',
                'filter',
                'airline',
                'subid',
                'origin',
                'destination',
            ],
        ];
        $shortcodeList = array_keys(WidgetsModel::instance()->table_fields);

        foreach ($shortcodeList as $shortcode) {
            $model = new WidgetsModel();
            $model->current_table = $shortcode;
            $model->attributes = $this->attributes;
            $shortcodeData = $model->shortcodeData;
            $shortcodeAttributes = $shortcodeData->params;
            $this->assertEquals($shortcode, $shortcodeData->shortcode);
            var_dump($shortcodeAttributes);
            if (isset($expectedAttributes[$shortcode])) {
                foreach ($expectedAttributes[$shortcode] as $expectedAttribute) {
                    // Check that every attribute is not null
                    $this->assertNotNull($shortcodeAttributes->get($expectedAttribute), "$shortcode - expected $expectedAttribute but it's empty");
                }
            }
        }

    }
}
