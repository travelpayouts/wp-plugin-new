<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use PHPUnit\Framework\TestCase;
use tp\admin\includes\models\shortcodes\FlightsModel;
use Adbar\Dot;

class FlightsModelTest extends TestCase
{
    const CLEAR = "\e[0m";
    const OK = "\e[42;30m";

    public $attributes = array(
        'title' => 'NO TITLE',
        'origin' =>
            array(
                'location' =>
                    array(
                        'lon' => 37.617633,
                        'lat' => 55.755786,
                    ),
                'index_strings' =>
                    array(
                        0 => 'defaultcity',
                        1 => 'defaultcity',
                        2 => 'maskava',
                        3 => 'maskva',
                        4 => 'mosca',
                        5 => 'moscou',
                        6 => 'moscova',
                        7 => 'moscovo',
                        8 => 'moscow',
                        9 => 'moscú',
                        10 => 'moskau',
                        11 => 'moskou',
                        12 => 'moskova',
                        13 => 'moskow',
                        14 => 'moskva',
                        15 => 'moskwa',
                        16 => 'moszkva',
                        17 => 'μόσχα',
                        18 => 'москва',
                        19 => 'нерезиновая',
                        20 => 'нерезиновая',
                        21 => 'нерезиновск',
                        22 => 'нерезиновск',
                        23 => 'понаехавск',
                        24 => 'понаехавск',
                        25 => 'մոսկվա',
                        26 => 'מוסקבה',
                        27 => 'مسکو',
                        28 => 'موسكو',
                        29 => 'मास्को',
                        30 => 'มอสโก',
                        31 => 'მოსკოვი',
                        32 => 'モスクワ',
                        33 => '莫斯科',
                        34 => '모스크바',
                    ),
                'coordinates' =>
                    array(
                        0 => 55.755786,
                        1 => 37.617633,
                    ),
                'searches_count' => 1006321,
                'airport_name' => null,
                'country_iata' => 'RU',
                'name' => 'Moscow, Russia',
                'iata' => 'MOW',
                '_label' => 'Moscow, Russia  [MOW]',
            ),
        'destination' =>
            array(
                'iata' => 'UFA',
                'name' => 'Ufa, Russia',
                'country_iata' => 'RU',
                'index_strings' =>
                    array(
                        0 => 'aeroportul internațional ufa',
                        1 => 'mednarodno letališče ufa',
                        2 => 'medzinárodné letisko ufa',
                        3 => 'mezinárodní letiště ufa',
                        4 => 'međunarodna zračna luka ufa',
                        5 => 'oefa',
                        6 => 'ufa',
                        7 => 'ufa flyplass',
                        8 => 'ufa international airport',
                        9 => 'ufa internationale luchthaven',
                        10 => 'ufa internationale lufthavn',
                        11 => 'ufa međunarodni aerodrom',
                        12 => 'ufa nemzetközi repülőtér',
                        13 => 'ufa russland',
                        14 => 'ufan kansainvälinen lentokenttä',
                        15 => 'ufas internationella flygplats',
                        16 => 'ufas starptautiskā lidosta',
                        17 => 'ουφά',
                        18 => 'ουφά αεροδρόμιο',
                        19 => 'международно летище уфа',
                        20 => 'уфа',
                        21 => 'ուֆա',
                        22 => 'אופה',
                        23 => 'נמל התעופה הבינלאומי אופה',
                        24 => 'أوفا',
                        25 => 'اوفا',
                        26 => 'فرودگاه بین المللی اوفا',
                        27 => 'مطار أوفا الدولي',
                        28 => 'ऊफ़ा',
                        29 => 'यूफा अंतरराष्ट्रीय हवाई अड्डा',
                        30 => 'สนามบินอูฟา',
                        31 => 'อูฟา',
                        32 => 'უფა',
                        33 => 'უფას აეროპორტი',
                        34 => 'ウファ',
                        35 => '乌法',
                        36 => '乌法机场',
                        37 => '烏法',
                        38 => '우파',
                    ),
                'airport_name' => 'Ufa International Airport',
                'location' =>
                    array(
                        'lat' => 54.565403,
                        'lon' => 55.884544,
                    ),
                'city_iata' => 'UFA',
                'searches_count' => 2547751,
                'coordinates' =>
                    array(
                        0 => 54.565403,
                        1 => 55.884544,
                    ),
                '_label' => 'Ufa, Russia Ufa International Airport [UFA]',
            ),
        'country' => null,
        'airline' =>
            array(
                'iata' => 'KE',
                'name' => 'Korean Air',
                '_label' => 'Korean Air',
            ),
        'subid' => 'subisd',
        'flight_number' => null,
        'currency' => 4,
        'limit' => 100,
        'paginate' => true,
        'one_way' => false,
        'off_title' => true,
        'stops' => 0,
    );

    //[shortcode_price_calendar_month origin=MOW destination=UFA title="widget 1 " paginate=true stops=1  subid="123" currency="RUB"]
    //[shortcode_price_calendar_week origin=MOW destination=UFA title="widget 2" paginate=true  subid="123"  currency="RUB"]
    //[tp_cheapest_flights_shortcodes origin=MOW destination=UFA title="widget 3" paginate=true  subid="123"  currency="RUB" filter_airline="KE" filter_flight_number="123"]
    //[tp_cheapest_ticket_each_day_month_shortcodes origin=MOW destination=UFA title="widget 4" paginate=true stops=1  subid="123"  currency="RUB" filter_airline="KE" filter_flight_number="123"]
    //[tp_cheapest_tickets_each_month_shortcodes origin=MOW destination=UFA title="widget 5" paginate=true  subid="123"  currency="RUB" filter_airline="KE" filter_flight_number="123"]
    //[tp_direct_flights_route_shortcodes origin=MOW destination=UFA title="widget 6" paginate=true  subid="123"  currency="RUB" filter_airline="KE" filter_flight_number="123"]
    //[tp_direct_flights_shortcodes origin=MOW  title="widget 7" limit=10 paginate=true  subid="123"  currency="RUB" filter_airline="KE" filter_flight_number="123"]
    //[tp_popular_routes_from_city_shortcodes origin=MOW  title="widget 8" paginate=true  subid="123"]
    //[tp_popular_destinations_airlines_shortcodes airline=KE  title="widget 9" limit="10" paginate=true  subid="123"]
    //[tp_our_site_search_shortcodes  title="widget 10" limit=100 paginate=true stops=0 one_way=true  subid="123"  currency="RUB"]
    //[tp_from_our_city_fly_shortcodes origin=MOW  title="widget 11" limit=100 paginate=true stops=0 one_way=true  subid="123"  currency="RUB"]

    public function testGettingShortcodeAttributes()
    {

        $expectedAttributes = [
            FlightsModel::shortcode_price_calendar_month => [
                'origin',
                'destination',
                'title',
                'paginate',
                'subid',
                'currency',
                'theme',
                'off_title',
                'stops',
            ],
            FlightsModel::shortcode_price_calendar_week => [
                'origin',
                'destination',
                'title',
                'paginate',
                'subid',
                'currency',
                'off_title',
            ],
            FlightsModel::shortcode_cheapest_flights => [
                'origin',
                'destination',
                'title',
                'paginate',
                'subid',
                'currency',
                'filter_airline',
                'filter_flight_number',
                'off_title',
            ],
            FlightsModel::shortcode_cheapest_ticket_each_day_month => [
                'origin',
                'destination',
                'title',
                'paginate',
                'stops',
                'subid',
                'currency',
                'filter_airline',
                'filter_flight_number',
                'off_title',
            ],
            FlightsModel::shortcode_cheapest_tickets_each_month => [
                'origin',
                'destination',
                'title',
                'paginate',
                'subid',
                'currency',
                'filter_airline',
                'filter_flight_number',
                'off_title',
            ],
            FlightsModel::shortcode_direct_flights_route => [
                'origin',
                'destination',
                'title',
                'paginate',
                'subid',
                'currency',
                'filter_airline',
                'filter_flight_number',
                'off_title',
            ],
            FlightsModel::shortcode_direct_flights => [
                'origin',
                'title',
                'limit',
                'paginate',
                'subid',
                'currency',
                'filter_airline',
                'filter_flight_number',
                'off_title',
            ],
            FlightsModel::shortcode_popular_routes_from_city => [
                'origin',
                'title',
                'paginate',
                'subid',
                'off_title',
            ],
            FlightsModel::shortcode_popular_destinations_airlines => [
                'airline',
                'title',
                'limit',
                'paginate',
                'subid',
                'off_title',
            ],
            FlightsModel::shortcode_our_site_search => [
                'title',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
            ],
            FlightsModel::shortcode_from_our_city_fly => [
                'origin',
                'title',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
            ],
            FlightsModel::shortcode_in_our_city_fly => [
                'destination',
                'title',
                'limit',
                'paginate',
                'stops',
                'one_way',
                'subid',
                'currency',
                'off_title',
            ],
        ];
        $shortcodeList = array_keys(FlightsModel::instance()->table_fields);

        foreach ($shortcodeList as $shortcode) {
            $model = new FlightsModel();
            $model->current_table = $shortcode;
            $model->attributes = $this->attributes;
            $shortcodeData = $model->shortcodeData;
            $shortcodeAttributes = $shortcodeData->params;
            $this->assertEquals($shortcode, $shortcodeData->shortcode);
            if (isset($expectedAttributes[$shortcode])) {
                foreach ($expectedAttributes[$shortcode] as $expectedAttribute) {
                    // Check that every attribute is not null
                    $this->assertNotNull($shortcodeAttributes->get($expectedAttribute), "$shortcode - expected $expectedAttribute but it's empty");
                }
            }
        }
    }
}
