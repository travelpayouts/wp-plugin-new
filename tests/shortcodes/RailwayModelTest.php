<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use tp\admin\includes\models\shortcodes\RailwayModel;
use PHPUnit\Framework\TestCase;
use Adbar\Dot;

class RailwayModelTest extends TestCase
{
    public $attributes = array(
        'origin' =>
            array(
                'id' => '2000000',
                'value' => 'Москва',
                '_label' => 'Москва',
            ),
        'destination' =>
            array(
                'id' => '2024600',
                'value' => 'Уфа',
                '_label' => 'Уфа',
            ),
        'current_table' => RailwayModel::shortcode_tutu,
        'paginate' => true,
        'title' => 'fewfew',
        'subid' => '21321312',
        'off_title' => true,
    );

    public function testGettingShortcodeAttributes()
    {
        $expectedAttributes = [
            RailwayModel::shortcode_tutu => [
                'origin',
                'destination',
                'title',
                'paginate',
                'off_title',
                'subid',
            ]];
        $shortcodeList = [RailwayModel::shortcode_tutu];

        foreach ($shortcodeList as $shortcode) {
            $model = new RailwayModel();
            $model->current_table = $shortcode;
            $model->attributes = $this->attributes;
            $shortcodeData = $model->shortcodeData;
            $shortcodeAttributes = $shortcodeData->params;
            $this->assertEquals($shortcode, $shortcodeData->shortcode);

            if (isset($expectedAttributes[$shortcode])) {
                foreach ($expectedAttributes[$shortcode] as $expectedAttribute) {
                    // Check that every attribute is not null
                    $this->assertNotNull($shortcodeAttributes->get($expectedAttribute), "$shortcode - expected $expectedAttribute but it's empty");
                }
            }
        }
    }
//[tp_tutu origin=2000000 destination=2024600 title="no title" paginate=true off_title=true subid="12345"]
}
