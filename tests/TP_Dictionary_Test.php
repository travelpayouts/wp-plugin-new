<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use PHPUnit\Framework\TestCase;
use tp\includes\dictionary;

class TP_Dictionary_Test extends TestCase
{
    const CLEAR = "\e[0m";
    const OK = "\e[42;30m";

    public function testGetCities()
    {
        $fixtures = array(
            array(
                'lang' => 'ru',
                'code' => 'MOW',
                'case' => dictionary\TP_Dictionary_Cities::CASE_NOMINATIVE,
                'result' => 'Москва'
            ),
            array(
                'lang' => 'ru',
                'code' => 'MOW',
                'case' => dictionary\TP_Dictionary_Cities::CASE_PREPOSITIONAL,
                'result' => 'Москве'
            ),
            array(
                'lang' => 'ru',
                'code' => 'AGD',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => 'Ангги'
            ),
            array(
                'lang' => 'ru',
                'code' => 'AGD',
                'case' => false,
                'result' => 'Ангги'
            ),
            array(
                'lang' => 'ru',
                'code' => '000',
                'case' => false,
                'result' => null
            ),
            array(
                'lang' => 'ru',
                'code' => '000',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => null
            ),
            array(
                'lang' => 'ru',
                'code' => 'ADF',
                'case' => dictionary\TP_Dictionary_Cities::CASE_DATIVE,
                'result' => 'Адыяману'
            ),
            array(
                'lang' => 'en',
                'code' => 'ADF',
                'case' => dictionary\TP_Dictionary_Cities::CASE_DATIVE,
                'result' => 'Adiyaman'
            ),
            array(
                'lang' => '000',
                'code' => 'ADF',
                'case' => false,
                'result' => 'Adiyaman'
            ),
            array(
                'lang' => 'en',
                'code' => '000',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => null
            ),
            array(
                'lang' => '12233',
                'code' => '000',
                'case' => '12233',
                'result' => null
            ),
            array(
                'lang' => 'id',
                'code' => 'AAZ',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => 'Quetzaltenango'
            ),
            array(
                'lang' => 'es',
                'code' => 'AEH',
                'case' => false,
                'result' => 'Abéché'
            ),
        );
        $this->log('Test - testGetCities');
        foreach ($fixtures as $fixture) {
            $cities_dictionary = new dictionary\TP_Dictionary_Cities($fixture['lang']);
            $result = $cities_dictionary->get_item($fixture['code'])->get_name($fixture['case']);
            $this->assertEquals($fixture['result'], $result);
            $this->log("expected = '{$fixture['result']}', actual = '{$result}'");
        }
    }

    public function testGetCountries()
    {

        $fixtures = array(
            array(
                'lang' => 'ru',
                'code' => 'CW',
                'case' => dictionary\TP_Dictionary_Cities::CASE_NOMINATIVE,
                'result' => 'Кюрасао',
                'currency' => 'ANG'
            ),
            array(
                'lang' => 'ru',
                'code' => 'GL',
                'case' => dictionary\TP_Dictionary_Cities::CASE_PREPOSITIONAL,
                'result' => 'Гренландии'
            ),
            array(
                'lang' => 'ru',
                'code' => 'AS',
                'case' => dictionary\TP_Dictionary_Cities::CASE_DATIVE,
                'result' => 'Американскому Самоа'
            ),
            array(
                'lang' => 'ru',
                'code' => 'CZ',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => 'Чехии'
            ),
            array(
                'lang' => 'es',
                'code' => 'KY',
                'case' => dictionary\TP_Dictionary_Cities::CASE_GENITIVE,
                'result' => 'Islas Caimán',
                'currency' => 'KYD'
            ),

            array(
                'lang' => 'es',
                'code' => 'LK',
                'case' => dictionary\TP_Dictionary_Cities::CASE_DATIVE,
                'result' => 'Sri Lanka'
            ),
            array(
                'lang' => 'en',
                'code' => 'KY',
                'case' => dictionary\TP_Dictionary_Cities::CASE_NOMINATIVE,
                'result' => 'Cayman Islands'
            ),
        );
        $this->log('Test - testGetCountries');

        foreach ($fixtures as $fixture) {
            $countries_dictionary = new dictionary\TP_Dictionary_Countries($fixture['lang']);
            $result = $countries_dictionary->get_item($fixture['code'])->get_name($fixture['case']);
            $this->assertEquals($fixture['result'], $result);
            $this->log('Country Testing');
            $this->log("expected = '{$fixture['result']}', actual = '{$result}'");

            if (isset($fixture['currency'])) {
                $currency = $countries_dictionary->get_item($fixture['code'])->currency;
                $this->assertEquals($fixture['currency'], $currency);
                $this->log('Currency Testing');
                $this->log("expected = '{$fixture['currency']}', actual = '{$currency}'");
            }
        }

    }

    public function testGetAirline()
    {
        $fixtures = array(
            array(
                'lang' => 'ru',
                'code' => 'EE',
                'result' => 'Regional Jet OÜ'
            ),
            array(
                'lang' => 'ru',
                'code' => 'HT',
                'result' => 'Ningxia Cargo Airlines CO.,LTD'
            ),
            array(
                'lang' => 'ru',
                'code' => 'AU',
                'result' => 'Austral Lineas Aereas'
            ),
            array(
                'lang' => 'tr',
                'code' => '7Q',
                'result' => 'Elite Airways'
            ),
            array(
                'lang' => 'sr',
                'code' => 'TQ',
                'result' => 'Tandem Aero'
            ),
            array(
                'lang' => 'en',
                'code' => 'TA',
                'result' => 'TACA Airlines'
            ),
        );
        $this->log('Test - testGetAirline');

        foreach ($fixtures as $fixture) {
            $airline_dictionary = new dictionary\TP_Dictionary_Airlines($fixture['lang']);

            $result = $airline_dictionary->get_item($fixture['code'])->get_name();
            $this->assertEquals($fixture['result'], $result);
            $this->log("expected = '{$fixture['result']}', actual = '{$result}'");
        }
    }

    public function testGetAirport()
    {

        $fixtures = array(
            array(
                'lang' => 'tg',
                'code' => 'AAC',
                'result' => 'El Arish International Airport',
                'attributes' => array(
                    'country_code' => 'EG',
                    'city_code' => 'AAC',
                    'flightable' => false,
                    'time_zone' => 'Africa/Cairo',
                )
            ),
            array(
                'lang' => 'en',
                'code' => 'ZFX',
                'result' => 'Szczecin Bus Station',
                'attributes' => array(
                    'flightable' => true,
                )
            ),
        );
        $this->log('Test - testGetAirport');

        foreach ($fixtures as $fixture) {
            $this->log('Airport testing');

            $airline_dictionary = new dictionary\TP_Dictionary_Airports($fixture['lang']);
            $item = $airline_dictionary->get_item($fixture['code']);
            $result = $item->get_name();
            $this->assertEquals($fixture['result'], $result);
            $this->log("expected = '{$fixture['result']}', actual = '{$result}'");

            if (isset($fixture['attributes'])) {
                $this->log('**** Attributes testing');

                $attributes = $fixture['attributes'];
                foreach ($attributes as $attribute_key => $attribute_value) {
                    $item_value = $item->{$attribute_key};
                    $this->assertEquals($attribute_value, $item_value);
                    $this->log("expected = '{$attribute_value}', actual = '{$item_value}'");
                }
            }
        }
    }

    public function testGetInstanceSingleton()
    {
        $fixtures = array(
            array(
                'expected' => 'El Arish International Airport',
                'actual' => dictionary\TP_Dictionary_Airports::instance('ru')->get_item('AAC')->get_name(),
            ),
            array(
                'expected' => 'Elite Airways',
                'actual' => dictionary\TP_Dictionary_Airlines::instance('tr')->get_item('7Q')->get_name(),
            ),
            array(
                'expected' => 'Москве',
                'actual' => dictionary\TP_Dictionary_Cities::instance('ru')->get_item('MOW')->get_name(dictionary\TP_Dictionary_Cities::CASE_PREPOSITIONAL),
            ),
            array(
                'expected' => 'Moscow',
                'actual' => dictionary\TP_Dictionary_Cities::instance('en')->get_item('MOW')->get_name(dictionary\TP_Dictionary_Cities::CASE_PREPOSITIONAL),
            ),
            array(
                'expected' => 'Moscow',
                'actual' => dictionary\TP_Dictionary_Cities::instance('someNonExistingLanguage')->get_item('MOW')->get_name(dictionary\TP_Dictionary_Cities::CASE_PREPOSITIONAL),
            ),
            array(
                'expected' => 'Cayman Islands',
                'actual' => dictionary\TP_Dictionary_Countries::instance('en')->get_item('KY')->get_name(dictionary\TP_Dictionary_Cities::CASE_NOMINATIVE),
            ),
            array(
                'expected' => 'Дувр-Остенде',
                'actual' => dictionary\TP_Dictionary_Railways::instance('ru')->get_item('70881')->get_name(),
            ),

        );

        $this->log('Test - testGetInstanceSingleton');

        foreach ($fixtures as $fixture) {
            $this->assertEquals($fixture['expected'], $fixture['actual']);
            $this->log("expected = '{$fixture['expected']}', actual = '{$fixture['actual']}'");
        }
    }

    public function testGetRailway(){

        $fixtures = array(
            array(
                'number' => '2700130',
                'result' => 'Рзд 12',
            ),
            array(
                'number' => '2064308',
                'result' => 'Имеретинский Курорт (Олимпийский парк)',
            ),
            array(
                'number' => '2001081',
                'result' => 'Ряжск-2',
            ),
            array(
                'number' => '10208',
                'result' => 'Лужайка-Вайниккала',
            ),
        );

        $this->log('Test - testGetRailway');

        foreach ($fixtures as $fixture) {
            $railway_dictionary = new dictionary\TP_Dictionary_Railways('ru');
            $result = $railway_dictionary->get_item($fixture['number'])->get_name();
            $this->assertEquals($fixture['result'], $result);
            $this->log("expected = '{$fixture['result']}', actual = '{$result}'");
        }
    }

    public function tearDown()
    {
        echo("\n\n\n");
        parent::tearDown();
    }

    private function log($message)
    {
        echo(self::OK . $message . "\n----------\n" . self::CLEAR);
    }
}
