<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use PHPUnit\Framework\TestCase;
use tp\includes\request_api;
use tp\tests;

class TP_Request_Api_Flight_Test extends TestCase
{
    public $request_api;
    public $default_attrs = array();

    public function setUp()
    {
        $this->request_api = request_api\TP_Request_Api_Flight::get_instance();
        $this->api()->init(
            '',
            '321d6a221f8926b5ec41ae89a3b2ae7b', //test token from api.travelpayouts.com
            '',
            '',
            12,
            true,
            'https://hydra.aviasales.ru',
            true);
    }

    /**
     * @return request_api\TP_Request_Api_Flight
     */
    private function api()
    {
        return $this->request_api;
    }

    public function get_protected_api_method($method, ...$arguments)
    {
        $api = $this->request_api;
        $method = tests\TP_Php_Unit_Utils::get_private_method($api, $method);
        return $method->invoke($api, ...$arguments);
    }


    public function test_get_response_by_shortcode()
    {
        $fixtures = glob(__DIR__ . '/fixtures/TP_Request_Api_Flight_Test/*.json');

        foreach ($fixtures as $fixture) {
            $shortcode_name = basename($fixture, '.json');
            $attributes = json_decode(file_get_contents($fixture), true);

            $api_response = $this->api()->get_response_by_shortcode($shortcode_name, $attributes);
            $this->assertNotFalse($api_response, "Empty response by {$shortcode_name} shortcode");
            if ($api_response) {
                $this->assertNotEmpty($api_response->get_rows(), "Empty rows by {$shortcode_name} shortcode");
            }
        }
    }
}