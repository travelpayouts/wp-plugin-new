<?php
/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

use PHPUnit\Framework\TestCase;
use tp\includes;

class TP_Request_Test extends TestCase
{
    public function testAccessingRequest()
    {
        $_GET['someSimpleParam'] = false;
        $_GET['someNestedParam'] = array(
            'someArray' => array(
                'value' => 123
            )
        );

        $_POST['someSimpleParam'] = false;
        $_POST['someNestedParam'] = array(
            'someArray' => array(
                'value' => 123
            )
        );
        $request = new includes\TP_Request();

        $this->assertEquals('GET', $request->get_method());
        $this->assertFalse(($request->get_query('someSimpleParam')));
        $this->assertEquals(true, $request->get_query('someNestedParam/someArray/value'));
        $this->assertEquals(123, $request->get_query('someNestedParam/someArray/value'));
        $this->assertEquals(null, $request->get_query('someNestedParam/someArray/valueThatNotExists'));


        $this->assertFalse(($request->post('someSimpleParam')));
        $this->assertEquals(true, $request->post('someNestedParam/someArray/value'));
        $this->assertEquals(123, $request->post('someNestedParam/someArray/value'));
        $this->assertEquals(null, $request->post('someNestedParam/someArray/valueThatNotExists'));

        // Testing getInstance method
        $this->assertInstanceOf('tp\includes\TP_Request', includes\TP_Request::instance());
        $this->assertFalse((includes\TP_Request::instance()->post('someSimpleParam')));


    }
}