/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import mainConfig from './webpack.userConfig';

export default [
    {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
            presets: [
                '@babel/preset-env'
            ]
        }
    },
    {
        test: /\.s(c|a)ss$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
                loader: 'css-loader',
                options: {
                    importLoaders: 3,
                    sourceMap: mainConfig.enabled.sourceMaps,
                    // url: false
                }
            },
                {
                    loader: 'postcss-loader',
                    options: {
                        config: {
                            path: __dirname,
                            ctx: mainConfig
                        },
                        sourceMap: mainConfig.enabled.sourceMaps,
                    },
                },
                {
                    loader: 'resolve-url-loader',
                },
                {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: mainConfig.enabled.sourceMaps,
                    }
                },
                {
                    loader: 'sass-resources-loader',
                    options: {
                        resources: [
                            path.resolve(__dirname, '../admin/assets/styles/_variables.scss'),
                        ]
                    }
                }
            ]
        })
    },
    {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
                {loader: 'css-loader', options: {sourceMap: mainConfig.enabled.sourceMaps}},
                {
                    loader: 'postcss-loader', options: {
                        config: {path: __dirname, ctx: mainConfig},
                        sourceMap: mainConfig.enabled.sourceMaps,
                    },
                },
                {
                    loader: 'resolve-url-loader',
                },
            ]
        })
    },
    {
        test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
        include: mainConfig.paths.assets,
        loader: 'url-loader',
        options: {
            limit: 4096,
            name: `[path][name].[ext]`,
        },
    },
    {
        test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
        include: /node_modules|bower_components/,
        loader: 'url-loader',
        options: {
            limit: 4096,
            outputPath: 'vendor/',
            name: `${mainConfig.cacheBusting}.[ext]`,
        },
    },
    {
        test: /\.(html)$/,
        use: {
            loader: 'html-loader',
            options: {
                template: false,
                import: false,
                url: false
            }
        }
    }
];