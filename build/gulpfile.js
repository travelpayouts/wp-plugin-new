const gulp = require('gulp');
const config = require('./deploy-config');
const gutil = require('gulp-util');
const ftp = require('vinyl-ftp');
const argv = require('yargs').argv; // pass arguments
const git = require('gulp-git'); // git
const exec = require('gulp-exec');
var currentBranch;
/**
 * Deploy plugin using FTP
 * Created by PhpStorm.
 * User: Andrey Polyakov
 * @param --vendor - Upload only vendor folder
 * @param --frontend - Upload only admin/dist, frontend/dist
 */

gulp.task('webpackBuild', () => {
    process.chdir('../');

    return gulp.src('.', {buffer: false})
        .on('end', () => {
            // Get current branch name
            git.revParse({args: '--abbrev-ref HEAD'}, (err, branch) => {
                currentBranch = branch;

                // if arg branch is set  (--branch=master)
                if (argv.branch !== undefined) {
                    branch = argv.branch;
                }

                // if current branch != arg.argv.branch = pull and checkout branch
                if (branch !== currentBranch) {
                    gutil.log('Git: checkout ' + branch);

                    git.checkout(branch, (err) => {
                        if (err) throw err;
                        gutil.log('Git: pull ' + branch);
                        git.pull('origin', branch, (err) => {
                            if (err) throw err;
                        });
                    });
                }
            });

        })
        .pipe(exec('npm run build-admin:production'))
        .on('end', () => {
            gutil.log('Webpack: build-admin success');
        })
        .pipe(exec('npm run build-frontend:production'))
        .on('end', () => {
            gutil.log('Webpack: build-frontend success');
        });
});


gulp.task('deploy', ['webpackBuild'], () => {
    process.chdir('./build');

    let conn = ftp.create(config.ftp);
    let globs = [
        './**/*',
        '*',
        '!.git',
        '!*.md',
        '!assets',
        '!build/**',
        '!dandelion.yml',
        '!bower_components',
        '!gulpfile.js',
        '!node_modules',
        '!node_modules/**',
        '!package.json',
        '!package-lock.json',
        '!composer.json',
        '!composer.lock',
        '!deploy-config.js',
        '!build.sh',
        '!yarn.lock',
        '!frontend/build/**',
        '!admin/build/**',
        '!vendor/**'
    ];

    // Update vendor with --vendor flag
    if (argv.vendor !== undefined) {
        globs = [
            './vendor/**'
        ];
    } else if (argv.frontend !== undefined) {
        globs = [
            'frontend/dist/**',
            'admin/dist/**',
            'admin/partials/**',
            'frontend/partials/**',
        ];
    }

    // turn off buffering in gulp.src for best performance
    return gulp.src(globs, {cwd: '../', base: '../', buffer: false})
        .pipe(conn.dest(config.uploadPath))
        .on('end', () => {
            gutil.log('Upload: Done');

            //Return to current branch
            if (argv.branch !== undefined) {
                git.revParse({args: '--abbrev-ref HEAD'}, (err, branch) => {
                    if (branch !== currentBranch) {
                        gutil.log('Git: return to ' + currentBranch);
                        git.checkout(currentBranch, (err) => {
                            if (err) throw err;
                        });
                    }
                });
            }
        });
});