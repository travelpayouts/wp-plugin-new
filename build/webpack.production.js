/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

'use strict'; // eslint-disable-line
import ImageminPlugin from 'imagemin-webpack-plugin';
import imageminMozjpeg from 'imagemin-mozjpeg';
import mainConfig from './webpack.userConfig';

export default {
    plugins: [
        new ImageminPlugin({
            optipng: {optimizationLevel: 3},
            gifsicle: {optimizationLevel: 3},
            pngquant: {quality: '85-90', speed: 4},
            svgo: {removeUnknownsAndDefaults: false, cleanupIDs: false},
            plugins: [imageminMozjpeg({quality: 90})],
            disable: mainConfig.isDev,
        }),
    ],
};
