/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */

'use strict'; // eslint-disable-line

import webpack from 'webpack';
import CleanPlugin from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import FriendlyErrorsWebpackPlugin from 'friendly-errors-webpack-plugin';
import webpackMerge from 'webpack-merge';
import webpackRules from './webpack.rules';
import webpackProduction from './webpack.production';
import {StatsWriterPlugin} from 'webpack-stats-plugin';
import {get, has} from 'lodash';
import mainConfig from './webpack.userConfig';
import {argv} from 'yargs';


let webpackConfig = {
    target: 'web',
    mode: mainConfig.isProd ? 'production' : 'development',
    context: mainConfig.paths.assets,
    entry: mainConfig.entry,
    devtool: (mainConfig.enabled.sourceMaps ? 'eval-source-map' : undefined),
    output: {
        path: mainConfig.paths.dist,
        filename: `scripts/[name].js`,
        chunkFilename: mainConfig.isDev ? 'scripts/[id].js' : 'scripts/[id].[chunkhash].js'
    },
    node: {
        setImmediate: false,
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
    },
    stats: {
        hash: false,
        version: false,
        timings: false,
        children: false,
        errors: false,
        errorDetails: false,
        warnings: false,
        chunks: false,
        modules: false,
        reasons: false,
        source: false,
        publicPath: false,
    },
    module: {
        rules: webpackRules
    },
    resolve: {
        modules: [
            mainConfig.paths.assets,
            'node_modules',
            'bower_components',
        ],
        alias: {
            '@': mainConfig.paths.assets,
        },
        mainFields: ['main', 'module'],
        enforceExtension: false,
    },
    externals: {
        jquery: 'jQuery',
        jqueryui: 'jQuery'
    },
    plugins: [
        new CleanPlugin([mainConfig.paths.dist], {
            root: mainConfig.paths.root,
            verbose: false,
        }),
        new CopyWebpackPlugin(mainConfig.copy),
        new ExtractTextPlugin({
            filename: mainConfig.isDev ? 'styles/[name].css' : 'styles/[name].[hash].css',
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: 'popper.js/dist/umd/popper.js',
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: mainConfig.enabled.optimize,
            debug: mainConfig.enabled.watcher,
            stats: {colors: true},
        }),
        new StatsWriterPlugin({
            filename: 'stats.json' // Default
        }),
        new FriendlyErrorsWebpackPlugin(),
    ],
    optimization: {
        runtimeChunk: {
            name: 'manifest'
        },
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
};

if (mainConfig.isServer) {

    webpackConfig.devServer = {
        host: '0.0.0.0',
        port: 8080,
        historyApiFallback: true,
        headers: {"Access-Control-Allow-Origin": "*"},
        proxy: {
        },
    }
}
if (mainConfig.isProd) {
    webpackConfig = webpackMerge(webpackConfig, webpackProduction);
}

export default webpackConfig;
