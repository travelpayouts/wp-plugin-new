/**
 * Created by: Andrey Polyakov (andrey@polyakov.im)
 */
import webpackMerge from 'webpack-merge';
import desire from './util/desire';
import {argv} from 'yargs';
import {get, has} from 'lodash';
import path from 'path';

class webpackConfiguration {
    constructor() {
        if (!webpackConfiguration.instance) {
            webpackConfiguration.instance = this.getConfig();
        }
        return webpackConfiguration.instance;
    }

    getConfig() {
        const userConfig = webpackConfiguration.getUserConfig();
        if (!userConfig.section) throw new Error('Cannot find "section" attribute');
        const isProduction = get(argv, 'env.production');
        const rootPath = (userConfig.paths && userConfig.paths.root) ? userConfig.paths.root : process.cwd();
        const assetsPath = path.join(rootPath, `${userConfig.section}/assets`);
        const distPath = path.join(rootPath, `${userConfig.section}/dist`);

        // Parse paths to CopyWebpackPlugin
        if (userConfig.copy) {
            const interpolateScope = {
                'assetsPath': assetsPath,
                'distPath': distPath
            };

            userConfig.copy = userConfig.copy.map((value) => {
                if (value.from && value.to) {
                    return {
                        from: webpackConfiguration.interpolate(value.from, interpolateScope),
                        to: webpackConfiguration.interpolate(value.to, interpolateScope)
                    }
                }
                return null;
            });
        }

        return webpackMerge({
            paths: {
                root: rootPath,
                assets: assetsPath,
                dist: distPath,
            },
            enabled: {
                sourceMaps: false,
                optimize: isProduction,
                cacheBusting: isProduction,
                watcher: has(argv, 'watch'),
            },
            isServer: get(argv, 'env.isServer'),
            isProd: isProduction,
            isDev: !isProduction
        }, userConfig);
    }

    static getUserConfig() {
        let configPath = `${__dirname}/../config`;
        if (has(argv, 'env.configJson')) {
            configPath = `${__dirname}/../${get(argv, 'env.configJson')}`;
        }
        return webpackMerge(desire(configPath), desire(`${configPath}_local`));
    }

    static interpolate(template, params = {}) {
        const names = Object.keys(params);
        const vals = Object.values(params);
        return new Function(...names, `return \`${template}\`;`)(...vals);
    }
}

const instance = new webpackConfiguration();
Object.freeze(instance);
export default instance;