<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.travelpayouts.com/?locale=en
 * @since             1.0.0
 * @package           Travelpayouts
 *
 * @wordpress-plugin
 * Plugin Name:       Travelpayouts
 * Plugin URI:        https://wordpress.org/plugins/travelpayouts/
 * Description:       Earn money and make your visitors happy! Offer them useful tools to find cheap flights and hotels. Earn on commission for each booking.
 * Version:           0.7.4
 * Author:            travelpayouts
 * Author URI:        http://www.travelpayouts.com/?locale=en
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       travelpayouts
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

require_once dirname(__FILE__) . '/tp-functions.php';
require_once dirname(__FILE__) . '/tp-config.php';
// Import vendor
$auto_load = __DIR__ . '/vendor/autoload.php';

if(!file_exists($auto_load)){
    deactivate_plugins( plugin_basename( __FILE__ ) );
    wp_die( 'Main autoloader file is not exist' );
}

require_once $auto_load;
tp_register_wp_autoload('tp\\', __DIR__);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-travelpayouts-activator.php
 */
function activate_travelpayouts()
{
    \tp\includes\TP_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-travelpayouts-deactivator.php
 */
function deactivate_travelpayouts()
{
    \tp\includes\TP_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_travelpayouts');
register_deactivation_hook(__FILE__, 'deactivate_travelpayouts');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_travelpayouts()
{
    $plugin = new tp\includes\TP_Plugin();
    $plugin->run();
}

run_travelpayouts();